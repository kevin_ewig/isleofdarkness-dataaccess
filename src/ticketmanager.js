"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ibasestorage_1 = require("./storage/ibasestorage");
var ticket_1 = require("isleofdarkness-common/src/model/ticket");
var missingconstructorargumenterror_1 = require("isleofdarkness-common/src/error/missingconstructorargumenterror");
/**
 * Contains logic that validates tickets and creates (and saves) new tickets.
 */
var TicketManager = /** @class */ (function () {
    /**
     * Create ticket manager.
     * @param storageFactory
     */
    function TicketManager(storageFactory, numberOfSecondsUntilExpiration) {
        this._numOfSecondsUntilExpiration = 360;
        this._numOfSecondsUntilExpiration = numberOfSecondsUntilExpiration;
        if (storageFactory) {
            this._ticketStorage = storageFactory.getStorage(ibasestorage_1.StorageType.Ticket);
            if (!this._ticketStorage) {
                throw new Error("No ticket storage in storage factory given");
            }
        }
        else {
            throw new missingconstructorargumenterror_1.MissingConstructorArgumentError();
        }
    }
    /**
     * Get a new ticket for the given user. The ticket will be saved in the ticket storage
     * so it is valid from here on out.
     */
    TicketManager.prototype.getTicket = function (userId) {
        return __awaiter(this, void 0, void 0, function () {
            var ticket, ticketResult;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ticket = ticket_1.default.NewTicket(userId, this._numOfSecondsUntilExpiration);
                        return [4 /*yield*/, this._ticketStorage.addTicket(ticket)];
                    case 1:
                        ticketResult = _a.sent();
                        return [2 /*return*/, ticket];
                }
            });
        });
    };
    /**
     * Validate that the given ticket is valid.
     * @param ticket
     */
    TicketManager.prototype.validateTicket = function (ticket) {
        return __awaiter(this, void 0, void 0, function () {
            var now, result, ticketFromStorage;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!ticket) {
                            return [2 /*return*/, false];
                        }
                        if (!ticket.expirationDate) {
                            return [2 /*return*/, false];
                        }
                        now = new Date();
                        result = false;
                        if (ticket.expirationDate.getTime() < now.getTime()) {
                            return [2 /*return*/, false];
                        }
                        return [4 /*yield*/, this._ticketStorage.getTicket(ticket.ticketId)];
                    case 1:
                        ticketFromStorage = _a.sent();
                        if (ticketFromStorage) {
                            if (ticketFromStorage.expirationDate) {
                                if (ticketFromStorage.expirationDate.getTime() > now.getTime() &&
                                    ticketFromStorage.expirationDate.getTime() === ticket.expirationDate.getTime() &&
                                    ticketFromStorage.ticketId === ticket.ticketId &&
                                    ticketFromStorage.userId === ticket.userId) {
                                    result = true;
                                }
                            }
                        }
                        return [2 /*return*/, result];
                }
            });
        });
    };
    return TicketManager;
}());
exports.TicketManager = TicketManager;
exports.default = TicketManager;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRpY2tldG1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLHVEQUFtRDtBQUNuRCxpRUFBNEQ7QUFDNUQsbUhBQWtIO0FBRWxIOztHQUVHO0FBQ0g7SUFLSTs7O09BR0c7SUFDSCx1QkFBWSxjQUErQixFQUFFLDhCQUFzQztRQU4zRSxpQ0FBNEIsR0FBRyxHQUFHLENBQUM7UUFPdkMsSUFBSSxDQUFDLDRCQUE0QixHQUFHLDhCQUE4QixDQUFDO1FBQ25FLEVBQUUsQ0FBQyxDQUFFLGNBQWUsQ0FBQyxDQUFDLENBQUM7WUFDbkIsSUFBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUMsVUFBVSxDQUFrQiwwQkFBVyxDQUFDLE1BQU0sQ0FBRSxDQUFDO1lBQ3RGLEVBQUUsQ0FBQyxDQUFFLENBQUMsSUFBSSxDQUFDLGNBQWUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pCLE1BQU0sSUFBSSxLQUFLLENBQUMsNENBQTRDLENBQUMsQ0FBQztZQUNsRSxDQUFDO1FBQ0wsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLGlFQUErQixFQUFFLENBQUM7UUFDaEQsQ0FBQztJQUNMLENBQUM7SUFFRDs7O09BR0c7SUFDVSxpQ0FBUyxHQUF0QixVQUF1QixNQUFjOzs7Ozs7d0JBQzdCLE1BQU0sR0FBRyxnQkFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLDRCQUE0QixDQUFDLENBQUM7d0JBQ3RELHFCQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBMUQsWUFBWSxHQUFHLFNBQTJDO3dCQUM5RCxzQkFBTyxNQUFNLEVBQUM7Ozs7S0FDakI7SUFFRDs7O09BR0c7SUFDVSxzQ0FBYyxHQUEzQixVQUE0QixNQUFtQjs7Ozs7O3dCQUUzQyxFQUFFLENBQUMsQ0FBRSxDQUFDLE1BQU8sQ0FBQyxDQUFDLENBQUM7NEJBQ1osTUFBTSxnQkFBQyxLQUFLLEVBQUM7d0JBQ2pCLENBQUM7d0JBQ0QsRUFBRSxDQUFDLENBQUUsQ0FBQyxNQUFNLENBQUMsY0FBZSxDQUFDLENBQUMsQ0FBQzs0QkFDM0IsTUFBTSxnQkFBQyxLQUFLLEVBQUM7d0JBQ2pCLENBQUM7d0JBRUcsR0FBRyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7d0JBQ2pCLE1BQU0sR0FBRyxLQUFLLENBQUM7d0JBRW5CLEVBQUUsQ0FBQyxDQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLEdBQUcsR0FBRyxDQUFDLE9BQU8sRUFBRyxDQUFDLENBQUMsQ0FBQzs0QkFDcEQsTUFBTSxnQkFBQyxLQUFLLEVBQUM7d0JBQ2pCLENBQUM7d0JBRXVCLHFCQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBQTs7d0JBQXhFLGlCQUFpQixHQUFHLFNBQW9EO3dCQUM1RSxFQUFFLENBQUMsQ0FBRSxpQkFBa0IsQ0FBQyxDQUFDLENBQUM7NEJBQ3RCLEVBQUUsQ0FBQyxDQUFFLGlCQUFpQixDQUFDLGNBQWUsQ0FBQyxDQUFDLENBQUM7Z0NBQ3JDLEVBQUUsQ0FBQyxDQUFFLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxHQUFHLENBQUMsT0FBTyxFQUFFO29DQUMxRCxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLEtBQUssTUFBTSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUU7b0NBQzlFLGlCQUFpQixDQUFDLFFBQVEsS0FBSyxNQUFNLENBQUMsUUFBUTtvQ0FDOUMsaUJBQWlCLENBQUMsTUFBTSxLQUFLLE1BQU0sQ0FBQyxNQUFPLENBQUMsQ0FBQyxDQUFDO29DQUMvQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2dDQUNsQixDQUFDOzRCQUNMLENBQUM7d0JBQ0wsQ0FBQzt3QkFDRCxzQkFBTyxNQUFNLEVBQUM7Ozs7S0FFakI7SUFFTCxvQkFBQztBQUFELENBbEVBLEFBa0VDLElBQUE7QUFsRVksc0NBQWE7QUFvRTFCLGtCQUFlLGFBQWEsQ0FBQyIsImZpbGUiOiJ0aWNrZXRtYW5hZ2VyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IElUaWNrZXRTdG9yYWdlIGZyb20gXCIuL3N0b3JhZ2UvdGlja2V0c3RvcmFnZVwiO1xyXG5pbXBvcnQgSVN0b3JhZ2VGYWN0b3J5IGZyb20gXCIuL3N0b3JhZ2UvaXN0b3JhZ2VmYWN0b3J5XCI7XHJcbmltcG9ydCB7U3RvcmFnZVR5cGV9IGZyb20gXCIuL3N0b3JhZ2UvaWJhc2VzdG9yYWdlXCI7XHJcbmltcG9ydCBUaWNrZXQgZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbW9kZWwvdGlja2V0XCI7XHJcbmltcG9ydCB7IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IgfSBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcblxyXG4vKipcclxuICogQ29udGFpbnMgbG9naWMgdGhhdCB2YWxpZGF0ZXMgdGlja2V0cyBhbmQgY3JlYXRlcyAoYW5kIHNhdmVzKSBuZXcgdGlja2V0cy5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBUaWNrZXRNYW5hZ2VyIHtcclxuXHJcbiAgICBwcml2YXRlIF90aWNrZXRTdG9yYWdlOiBJVGlja2V0U3RvcmFnZTtcclxuICAgIHByaXZhdGUgX251bU9mU2Vjb25kc1VudGlsRXhwaXJhdGlvbiA9IDM2MDtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZSB0aWNrZXQgbWFuYWdlci5cclxuICAgICAqIEBwYXJhbSBzdG9yYWdlRmFjdG9yeVxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcihzdG9yYWdlRmFjdG9yeTogSVN0b3JhZ2VGYWN0b3J5LCBudW1iZXJPZlNlY29uZHNVbnRpbEV4cGlyYXRpb246IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX251bU9mU2Vjb25kc1VudGlsRXhwaXJhdGlvbiA9IG51bWJlck9mU2Vjb25kc1VudGlsRXhwaXJhdGlvbjtcclxuICAgICAgICBpZiAoIHN0b3JhZ2VGYWN0b3J5ICkge1xyXG4gICAgICAgICAgICB0aGlzLl90aWNrZXRTdG9yYWdlID0gc3RvcmFnZUZhY3RvcnkuZ2V0U3RvcmFnZTxJVGlja2V0U3RvcmFnZT4oIFN0b3JhZ2VUeXBlLlRpY2tldCApO1xyXG4gICAgICAgICAgICBpZiAoICF0aGlzLl90aWNrZXRTdG9yYWdlICkge1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiTm8gdGlja2V0IHN0b3JhZ2UgaW4gc3RvcmFnZSBmYWN0b3J5IGdpdmVuXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgYSBuZXcgdGlja2V0IGZvciB0aGUgZ2l2ZW4gdXNlci4gVGhlIHRpY2tldCB3aWxsIGJlIHNhdmVkIGluIHRoZSB0aWNrZXQgc3RvcmFnZVxyXG4gICAgICogc28gaXQgaXMgdmFsaWQgZnJvbSBoZXJlIG9uIG91dC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGFzeW5jIGdldFRpY2tldCh1c2VySWQ6IHN0cmluZyk6IFByb21pc2U8VGlja2V0PiB7XHJcbiAgICAgICAgbGV0IHRpY2tldCA9IFRpY2tldC5OZXdUaWNrZXQodXNlcklkLCB0aGlzLl9udW1PZlNlY29uZHNVbnRpbEV4cGlyYXRpb24pO1xyXG4gICAgICAgIGxldCB0aWNrZXRSZXN1bHQgPSBhd2FpdCB0aGlzLl90aWNrZXRTdG9yYWdlLmFkZFRpY2tldCh0aWNrZXQpO1xyXG4gICAgICAgIHJldHVybiB0aWNrZXQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBWYWxpZGF0ZSB0aGF0IHRoZSBnaXZlbiB0aWNrZXQgaXMgdmFsaWQuXHJcbiAgICAgKiBAcGFyYW0gdGlja2V0XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBhc3luYyB2YWxpZGF0ZVRpY2tldCh0aWNrZXQ6IFRpY2tldHxudWxsKTogUHJvbWlzZTxib29sZWFuPiB7XHJcblxyXG4gICAgICAgIGlmICggIXRpY2tldCApIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoICF0aWNrZXQuZXhwaXJhdGlvbkRhdGUgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBub3cgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgIGxldCByZXN1bHQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgaWYgKCB0aWNrZXQuZXhwaXJhdGlvbkRhdGUuZ2V0VGltZSgpIDwgbm93LmdldFRpbWUoKSApIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHRpY2tldEZyb21TdG9yYWdlID0gYXdhaXQgdGhpcy5fdGlja2V0U3RvcmFnZS5nZXRUaWNrZXQodGlja2V0LnRpY2tldElkKTtcclxuICAgICAgICBpZiAoIHRpY2tldEZyb21TdG9yYWdlICkge1xyXG4gICAgICAgICAgICBpZiAoIHRpY2tldEZyb21TdG9yYWdlLmV4cGlyYXRpb25EYXRlICkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCB0aWNrZXRGcm9tU3RvcmFnZS5leHBpcmF0aW9uRGF0ZS5nZXRUaW1lKCkgPiBub3cuZ2V0VGltZSgpICYmXHJcbiAgICAgICAgICAgICAgICAgICAgIHRpY2tldEZyb21TdG9yYWdlLmV4cGlyYXRpb25EYXRlLmdldFRpbWUoKSA9PT0gdGlja2V0LmV4cGlyYXRpb25EYXRlLmdldFRpbWUoKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICB0aWNrZXRGcm9tU3RvcmFnZS50aWNrZXRJZCA9PT0gdGlja2V0LnRpY2tldElkICYmXHJcbiAgICAgICAgICAgICAgICAgICAgIHRpY2tldEZyb21TdG9yYWdlLnVzZXJJZCA9PT0gdGlja2V0LnVzZXJJZCApIHtcclxuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcblxyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgVGlja2V0TWFuYWdlcjtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
