"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Publisher = /** @class */ (function () {
    function Publisher(redisClient) {
        if (redisClient) {
            this._client = redisClient;
        }
        else {
            throw new Error("No Redis Client defined");
        }
    }
    /**
     * Send a response message to all subscribers.
     * @param channel The channel to subscribe to.
     * @param response The response object to send.
     */
    Publisher.prototype.publish = function (channel, response) {
        var json = response.toJSON();
        var jsonString = JSON.stringify(json);
        this._client.publish(channel, jsonString);
    };
    /**
     * Close the connection.
     */
    Publisher.prototype.quit = function () {
        this._client.quit();
    };
    return Publisher;
}());
exports.Publisher = Publisher;
exports.default = Publisher;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInB1YnN1Yi9wdWJsaXNoZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFHQTtJQUlJLG1CQUFhLFdBQWdCO1FBQ3pCLEVBQUUsQ0FBQyxDQUFFLFdBQVksQ0FBQyxDQUFDLENBQUM7WUFDaEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUM7UUFDL0IsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQy9DLENBQUM7SUFDTCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLDJCQUFPLEdBQWQsVUFBZSxPQUFlLEVBQUUsUUFBa0I7UUFDOUMsSUFBSSxJQUFJLEdBQVEsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2xDLElBQUksVUFBVSxHQUFXLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRDs7T0FFRztJQUNJLHdCQUFJLEdBQVg7UUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFTCxnQkFBQztBQUFELENBOUJBLEFBOEJDLElBQUE7QUE5QlksOEJBQVM7QUFnQ3RCLGtCQUFlLFNBQVMsQ0FBQyIsImZpbGUiOiJwdWJzdWIvcHVibGlzaGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IElQdWJsaXNoZXIgZnJvbSBcIi4vaXB1Ymxpc2hlclwiO1xyXG5pbXBvcnQgUmVzcG9uc2UgZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvcHJvdG9jb2wvcmVzcG9uc2UvcmVzcG9uc2VcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBQdWJsaXNoZXIgaW1wbGVtZW50cyBJUHVibGlzaGVyIHtcclxuXHJcbiAgICBwcml2YXRlIF9jbGllbnQ6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvciggcmVkaXNDbGllbnQ6IGFueSApIHtcclxuICAgICAgICBpZiAoIHJlZGlzQ2xpZW50ICkge1xyXG4gICAgICAgICAgICB0aGlzLl9jbGllbnQgPSByZWRpc0NsaWVudDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJObyBSZWRpcyBDbGllbnQgZGVmaW5lZFwiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZW5kIGEgcmVzcG9uc2UgbWVzc2FnZSB0byBhbGwgc3Vic2NyaWJlcnMuXHJcbiAgICAgKiBAcGFyYW0gY2hhbm5lbCBUaGUgY2hhbm5lbCB0byBzdWJzY3JpYmUgdG8uXHJcbiAgICAgKiBAcGFyYW0gcmVzcG9uc2UgVGhlIHJlc3BvbnNlIG9iamVjdCB0byBzZW5kLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgcHVibGlzaChjaGFubmVsOiBzdHJpbmcsIHJlc3BvbnNlOiBSZXNwb25zZSk6IHZvaWQge1xyXG4gICAgICAgIGxldCBqc29uOiBhbnkgPSByZXNwb25zZS50b0pTT04oKTtcclxuICAgICAgICBsZXQganNvblN0cmluZzogc3RyaW5nID0gSlNPTi5zdHJpbmdpZnkoanNvbik7XHJcbiAgICAgICAgdGhpcy5fY2xpZW50LnB1Ymxpc2goY2hhbm5lbCwganNvblN0cmluZyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDbG9zZSB0aGUgY29ubmVjdGlvbi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHF1aXQoKSB7XHJcbiAgICAgICAgdGhpcy5fY2xpZW50LnF1aXQoKTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFB1Ymxpc2hlcjtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
