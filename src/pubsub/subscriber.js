"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Subscriber = /** @class */ (function () {
    function Subscriber(redisClient) {
        if (redisClient) {
            this._client = redisClient;
        }
        else {
            throw new Error("No Redis Client defined");
        }
    }
    /**
     * Subscribe to a channel.
     * @param channel The name of the channel.
     */
    Subscriber.prototype.subscribe = function (channel) {
        this._client.subscribe(channel);
    };
    /**
     * Recieve a message.
     * @param onMessageFunction
     */
    Subscriber.prototype.onMessage = function (onMessageFunction) {
        this._client.on("message", onMessageFunction);
    };
    /**
     * Unsubscribe from all channels.
     */
    Subscriber.prototype.unsubscribe = function () {
        this._client.unsubscribe();
    };
    /**
     * Close the connection.
     */
    Subscriber.prototype.quit = function () {
        this._client.quit();
    };
    return Subscriber;
}());
exports.Subscriber = Subscriber;
exports.default = Subscriber;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInB1YnN1Yi9zdWJzY3JpYmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7SUFJSSxvQkFBYSxXQUFnQjtRQUN6QixFQUFFLENBQUMsQ0FBRSxXQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLElBQUksQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDO1FBQy9CLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUMvQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOzs7T0FHRztJQUNJLDhCQUFTLEdBQWhCLFVBQWlCLE9BQWU7UUFDNUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVEOzs7T0FHRztJQUNJLDhCQUFTLEdBQWhCLFVBQWtCLGlCQUEyQjtRQUN6QyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxnQ0FBVyxHQUFsQjtRQUNJLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVEOztPQUVHO0lBQ0kseUJBQUksR0FBWDtRQUNJLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQUVMLGlCQUFDO0FBQUQsQ0ExQ0EsQUEwQ0MsSUFBQTtBQTFDWSxnQ0FBVTtBQTRDdkIsa0JBQWUsVUFBVSxDQUFDIiwiZmlsZSI6InB1YnN1Yi9zdWJzY3JpYmVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IElTdWJzY3JpYmVyIGZyb20gXCIuL2lzdWJzY3JpYmVyXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgU3Vic2NyaWJlciBpbXBsZW1lbnRzIElTdWJzY3JpYmVyIHtcclxuXHJcbiAgICBwcml2YXRlIF9jbGllbnQ6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvciggcmVkaXNDbGllbnQ6IGFueSApIHtcclxuICAgICAgICBpZiAoIHJlZGlzQ2xpZW50ICkge1xyXG4gICAgICAgICAgICB0aGlzLl9jbGllbnQgPSByZWRpc0NsaWVudDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJObyBSZWRpcyBDbGllbnQgZGVmaW5lZFwiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTdWJzY3JpYmUgdG8gYSBjaGFubmVsLlxyXG4gICAgICogQHBhcmFtIGNoYW5uZWwgVGhlIG5hbWUgb2YgdGhlIGNoYW5uZWwuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdWJzY3JpYmUoY2hhbm5lbDogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fY2xpZW50LnN1YnNjcmliZShjaGFubmVsKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlY2lldmUgYSBtZXNzYWdlLlxyXG4gICAgICogQHBhcmFtIG9uTWVzc2FnZUZ1bmN0aW9uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBvbk1lc3NhZ2UoIG9uTWVzc2FnZUZ1bmN0aW9uOiBGdW5jdGlvbiApOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9jbGllbnQub24oXCJtZXNzYWdlXCIsIG9uTWVzc2FnZUZ1bmN0aW9uKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFVuc3Vic2NyaWJlIGZyb20gYWxsIGNoYW5uZWxzLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgdW5zdWJzY3JpYmUoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fY2xpZW50LnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDbG9zZSB0aGUgY29ubmVjdGlvbi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHF1aXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fY2xpZW50LnF1aXQoKTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFN1YnNjcmliZXI7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
