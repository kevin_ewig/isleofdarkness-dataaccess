import ISubscriber from "./isubscriber";

export class Subscriber implements ISubscriber {

    private _client: any;

    constructor( redisClient: any ) {
        if ( redisClient ) {
            this._client = redisClient;
        } else {
            throw new Error("No Redis Client defined");
        }
    }

    /**
     * Subscribe to a channel.
     * @param channel The name of the channel.
     */
    public subscribe(channel: string): void {
        this._client.subscribe(channel);
    }

    /**
     * Recieve a message.
     * @param onMessageFunction
     */
    public onMessage( onMessageFunction: Function ): void {
        this._client.on("message", onMessageFunction);
    }

    /**
     * Unsubscribe from all channels.
     */
    public unsubscribe(): void {
        this._client.unsubscribe();
    }

    /**
     * Close the connection.
     */
    public quit(): void {
        this._client.quit();
    }

}

export default Subscriber;
