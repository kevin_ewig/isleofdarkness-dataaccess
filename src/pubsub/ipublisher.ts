import Response from "isleofdarkness-common/src/protocol/response/response";

interface IPublisher {

    /**
     * Send a response message to all subscribers.
     * @param channel The channel to subscribe to.
     * @param response The response object to send.
     */
    publish(channel: string, response: Response): void;

    /**
     * Close the connection.
     */
    quit(): void;

}

export default IPublisher;
