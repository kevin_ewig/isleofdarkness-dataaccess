import IPublisher from "./ipublisher";
import Response from "isleofdarkness-common/src/protocol/response/response";

export class Publisher implements IPublisher {

    private _client: any;

    constructor( redisClient: any ) {
        if ( redisClient ) {
            this._client = redisClient;
        } else {
            throw new Error("No Redis Client defined");
        }
    }

    /**
     * Send a response message to all subscribers.
     * @param channel The channel to subscribe to.
     * @param response The response object to send.
     */
    public publish(channel: string, response: Response): void {
        let json: any = response.toJSON();
        let jsonString: string = JSON.stringify(json);
        this._client.publish(channel, jsonString);
    }

    /**
     * Close the connection.
     */
    public quit() {
        this._client.quit();
    }

}

export default Publisher;
