interface ISubscriber {

    /**
     * Subscribe to a channel.
     * @param channel The name of the channel.
     */
    subscribe(channel: any): void;

    /**
     * Recieve a message.
     * @param onMessageFunction
     */
    onMessage( onMessageFunction: Function ): void;

    /**
     * Unsubscribe to all channels.
     */
    unsubscribe(): void;

    /**
     * Close the connection.
     */
    quit(): void;

}

export default ISubscriber;
