import UserMetaData from "isleofdarkness-common/src/model/usermetadata";
import IUserMetaDataStorage from "./iusermetadatastorage";
import BaseStorage from "./basestorage";
import {StorageType} from "./ibasestorage";

let redis = require("redis");

export class UserMetaDataStorage extends BaseStorage implements IUserMetaDataStorage {

    private readonly WORLD_USERMETADATA: string = "usermetadata.";
    private readonly WORLD_USERMETADATA_ID: string = this.WORLD_USERMETADATA + "id.";
    private readonly WORLD_USERMETADATA_LIST: string = this.WORLD_USERMETADATA + "list";

    private _redisClient: any;

    constructor( redisClient: any ) {
        super(redisClient);
        if ( redisClient ) {
            this._redisClient = redisClient;
        } else {
            throw new Error("No Redis Client defined");
        }
    }

    /**
     * Return the type of this storage.
     */
    public getStorageType(): StorageType {
        return StorageType.UserMetaData;
    }

    /**
     * Lock UserMetaData.
     * @param userId
     */
    public lockUserMetaData( userMetaData: UserMetaData ): Promise<void> {

        let promise = new Promise<void>((resolve, reject) => {

            function afterWatch(err: any) {
                if (err) {
                    reject();
                } else {
                    resolve();
                }
            }

            this._redisClient.watch( this.WORLD_USERMETADATA_ID + userMetaData.userId, afterWatch );

        });

        return promise;

    }

    /**
     * Store a user meta data in a storage.
     * @param userMetaData
     */
    public addUpdateUserMetaData( userMetaData: UserMetaData ): Promise<boolean> {

        let self: UserMetaDataStorage = this;
        let key: string = userMetaData.userId;
        let value: string = JSON.stringify( userMetaData.toJSON() );

        let promise = new Promise<boolean>((resolve, reject) => {

            function afterExec( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    resolve(true);
                }
            }

            function afterWatchingList( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    self._redisClient.multi().hset(self.WORLD_USERMETADATA_LIST, key, value).exec(afterExec);
                }
            }

            self._redisClient.watch( self.WORLD_USERMETADATA_LIST, afterWatchingList );

        });

        return promise;

    }

    /**
     * Retrieve a user meta data from storage. Return undefined if none is found.
     * @param userId
     */
    public getUserMetaData( userId: string ): Promise<UserMetaData> {

        let promise = new Promise<UserMetaData>((resolve, reject) => {

            function afterHGet(err: any, value: string) {
                if ( err ) {
                    reject();
                } else {
                    if ( value === null ) {
                        resolve(undefined);
                    } else {
                        let json = JSON.parse(value);
                        let result: UserMetaData = UserMetaData.fromJSON(json);
                        resolve(result);
                    }
                }
            }

            this._redisClient.hget(this.WORLD_USERMETADATA_LIST, userId, afterHGet);

        });

        return promise;

    }

    /**
     * Remove a user meta data from storage.
     * @param userId
     */
    public removeUserMetaData( userId: string ): Promise<boolean> {

        let self: UserMetaDataStorage = this;
        let key: string = userId;

        let promise = new Promise<boolean>((resolve, reject) => {

            function afterExec( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    resolve(true);
                }
            }

            function afterWatchingList( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    self._redisClient.multi().hdel(self.WORLD_USERMETADATA_LIST, key).exec(afterExec);
                }
            }

            self._redisClient.watch( self.WORLD_USERMETADATA_LIST, afterWatchingList );

        });

        return promise;

    }

}

export default UserMetaDataStorage;
