"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var usermetadata_1 = require("isleofdarkness-common/src/model/usermetadata");
var basestorage_1 = require("./basestorage");
var ibasestorage_1 = require("./ibasestorage");
var redis = require("redis");
var UserMetaDataStorage = /** @class */ (function (_super) {
    __extends(UserMetaDataStorage, _super);
    function UserMetaDataStorage(redisClient) {
        var _this = _super.call(this, redisClient) || this;
        _this.WORLD_USERMETADATA = "usermetadata.";
        _this.WORLD_USERMETADATA_ID = _this.WORLD_USERMETADATA + "id.";
        _this.WORLD_USERMETADATA_LIST = _this.WORLD_USERMETADATA + "list";
        if (redisClient) {
            _this._redisClient = redisClient;
        }
        else {
            throw new Error("No Redis Client defined");
        }
        return _this;
    }
    /**
     * Return the type of this storage.
     */
    UserMetaDataStorage.prototype.getStorageType = function () {
        return ibasestorage_1.StorageType.UserMetaData;
    };
    /**
     * Lock UserMetaData.
     * @param userId
     */
    UserMetaDataStorage.prototype.lockUserMetaData = function (userMetaData) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            function afterWatch(err) {
                if (err) {
                    reject();
                }
                else {
                    resolve();
                }
            }
            _this._redisClient.watch(_this.WORLD_USERMETADATA_ID + userMetaData.userId, afterWatch);
        });
        return promise;
    };
    /**
     * Store a user meta data in a storage.
     * @param userMetaData
     */
    UserMetaDataStorage.prototype.addUpdateUserMetaData = function (userMetaData) {
        var self = this;
        var key = userMetaData.userId;
        var value = JSON.stringify(userMetaData.toJSON());
        var promise = new Promise(function (resolve, reject) {
            function afterExec(err) {
                if (err) {
                    reject();
                }
                else {
                    resolve(true);
                }
            }
            function afterWatchingList(err) {
                if (err) {
                    reject();
                }
                else {
                    self._redisClient.multi().hset(self.WORLD_USERMETADATA_LIST, key, value).exec(afterExec);
                }
            }
            self._redisClient.watch(self.WORLD_USERMETADATA_LIST, afterWatchingList);
        });
        return promise;
    };
    /**
     * Retrieve a user meta data from storage. Return undefined if none is found.
     * @param userId
     */
    UserMetaDataStorage.prototype.getUserMetaData = function (userId) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            function afterHGet(err, value) {
                if (err) {
                    reject();
                }
                else {
                    if (value === null) {
                        resolve(undefined);
                    }
                    else {
                        var json = JSON.parse(value);
                        var result = usermetadata_1.default.fromJSON(json);
                        resolve(result);
                    }
                }
            }
            _this._redisClient.hget(_this.WORLD_USERMETADATA_LIST, userId, afterHGet);
        });
        return promise;
    };
    /**
     * Remove a user meta data from storage.
     * @param userId
     */
    UserMetaDataStorage.prototype.removeUserMetaData = function (userId) {
        var self = this;
        var key = userId;
        var promise = new Promise(function (resolve, reject) {
            function afterExec(err) {
                if (err) {
                    reject();
                }
                else {
                    resolve(true);
                }
            }
            function afterWatchingList(err) {
                if (err) {
                    reject();
                }
                else {
                    self._redisClient.multi().hdel(self.WORLD_USERMETADATA_LIST, key).exec(afterExec);
                }
            }
            self._redisClient.watch(self.WORLD_USERMETADATA_LIST, afterWatchingList);
        });
        return promise;
    };
    return UserMetaDataStorage;
}(basestorage_1.default));
exports.UserMetaDataStorage = UserMetaDataStorage;
exports.default = UserMetaDataStorage;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvdXNlcm1ldGFkYXRhc3RvcmFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSw2RUFBd0U7QUFFeEUsNkNBQXdDO0FBQ3hDLCtDQUEyQztBQUUzQyxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7QUFFN0I7SUFBeUMsdUNBQVc7SUFRaEQsNkJBQWEsV0FBZ0I7UUFBN0IsWUFDSSxrQkFBTSxXQUFXLENBQUMsU0FNckI7UUFiZ0Isd0JBQWtCLEdBQVcsZUFBZSxDQUFDO1FBQzdDLDJCQUFxQixHQUFXLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDaEUsNkJBQXVCLEdBQVcsS0FBSSxDQUFDLGtCQUFrQixHQUFHLE1BQU0sQ0FBQztRQU1oRixFQUFFLENBQUMsQ0FBRSxXQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLEtBQUksQ0FBQyxZQUFZLEdBQUcsV0FBVyxDQUFDO1FBQ3BDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUMvQyxDQUFDOztJQUNMLENBQUM7SUFFRDs7T0FFRztJQUNJLDRDQUFjLEdBQXJCO1FBQ0ksTUFBTSxDQUFDLDBCQUFXLENBQUMsWUFBWSxDQUFDO0lBQ3BDLENBQUM7SUFFRDs7O09BR0c7SUFDSSw4Q0FBZ0IsR0FBdkIsVUFBeUIsWUFBMEI7UUFBbkQsaUJBa0JDO1FBaEJHLElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxDQUFPLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFNUMsb0JBQW9CLEdBQVE7Z0JBQ3hCLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ04sTUFBTSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLEVBQUUsQ0FBQztnQkFDZCxDQUFDO1lBQ0wsQ0FBQztZQUVELEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFFLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxZQUFZLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBRSxDQUFDO1FBRTVGLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUVuQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksbURBQXFCLEdBQTVCLFVBQThCLFlBQTBCO1FBRXBELElBQUksSUFBSSxHQUF3QixJQUFJLENBQUM7UUFDckMsSUFBSSxHQUFHLEdBQVcsWUFBWSxDQUFDLE1BQU0sQ0FBQztRQUN0QyxJQUFJLEtBQUssR0FBVyxJQUFJLENBQUMsU0FBUyxDQUFFLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBRSxDQUFDO1FBRTVELElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxDQUFVLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFL0MsbUJBQW9CLEdBQVE7Z0JBQ3hCLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLENBQUM7WUFDTCxDQUFDO1lBRUQsMkJBQTRCLEdBQVE7Z0JBQ2hDLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDN0YsQ0FBQztZQUNMLENBQUM7WUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBRSxJQUFJLENBQUMsdUJBQXVCLEVBQUUsaUJBQWlCLENBQUUsQ0FBQztRQUUvRSxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFFbkIsQ0FBQztJQUVEOzs7T0FHRztJQUNJLDZDQUFlLEdBQXRCLFVBQXdCLE1BQWM7UUFBdEMsaUJBd0JDO1FBdEJHLElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxDQUFlLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFcEQsbUJBQW1CLEdBQVEsRUFBRSxLQUFhO2dCQUN0QyxFQUFFLENBQUMsQ0FBRSxHQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNSLE1BQU0sRUFBRSxDQUFDO2dCQUNiLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osRUFBRSxDQUFDLENBQUUsS0FBSyxLQUFLLElBQUssQ0FBQyxDQUFDLENBQUM7d0JBQ25CLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDdkIsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDSixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUM3QixJQUFJLE1BQU0sR0FBaUIsc0JBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3ZELE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDcEIsQ0FBQztnQkFDTCxDQUFDO1lBQ0wsQ0FBQztZQUVELEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyx1QkFBdUIsRUFBRSxNQUFNLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFFNUUsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBRW5CLENBQUM7SUFFRDs7O09BR0c7SUFDSSxnREFBa0IsR0FBekIsVUFBMkIsTUFBYztRQUVyQyxJQUFJLElBQUksR0FBd0IsSUFBSSxDQUFDO1FBQ3JDLElBQUksR0FBRyxHQUFXLE1BQU0sQ0FBQztRQUV6QixJQUFJLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBVSxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRS9DLG1CQUFvQixHQUFRO2dCQUN4QixFQUFFLENBQUMsQ0FBRSxHQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNSLE1BQU0sRUFBRSxDQUFDO2dCQUNiLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsQixDQUFDO1lBQ0wsQ0FBQztZQUVELDJCQUE0QixHQUFRO2dCQUNoQyxFQUFFLENBQUMsQ0FBRSxHQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNSLE1BQU0sRUFBRSxDQUFDO2dCQUNiLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixFQUFFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDdEYsQ0FBQztZQUNMLENBQUM7WUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBRSxJQUFJLENBQUMsdUJBQXVCLEVBQUUsaUJBQWlCLENBQUUsQ0FBQztRQUUvRSxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFFbkIsQ0FBQztJQUVMLDBCQUFDO0FBQUQsQ0FySkEsQUFxSkMsQ0FySndDLHFCQUFXLEdBcUpuRDtBQXJKWSxrREFBbUI7QUF1SmhDLGtCQUFlLG1CQUFtQixDQUFDIiwiZmlsZSI6InN0b3JhZ2UvdXNlcm1ldGFkYXRhc3RvcmFnZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBVc2VyTWV0YURhdGEgZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbW9kZWwvdXNlcm1ldGFkYXRhXCI7XHJcbmltcG9ydCBJVXNlck1ldGFEYXRhU3RvcmFnZSBmcm9tIFwiLi9pdXNlcm1ldGFkYXRhc3RvcmFnZVwiO1xyXG5pbXBvcnQgQmFzZVN0b3JhZ2UgZnJvbSBcIi4vYmFzZXN0b3JhZ2VcIjtcclxuaW1wb3J0IHtTdG9yYWdlVHlwZX0gZnJvbSBcIi4vaWJhc2VzdG9yYWdlXCI7XHJcblxyXG5sZXQgcmVkaXMgPSByZXF1aXJlKFwicmVkaXNcIik7XHJcblxyXG5leHBvcnQgY2xhc3MgVXNlck1ldGFEYXRhU3RvcmFnZSBleHRlbmRzIEJhc2VTdG9yYWdlIGltcGxlbWVudHMgSVVzZXJNZXRhRGF0YVN0b3JhZ2Uge1xyXG5cclxuICAgIHByaXZhdGUgcmVhZG9ubHkgV09STERfVVNFUk1FVEFEQVRBOiBzdHJpbmcgPSBcInVzZXJtZXRhZGF0YS5cIjtcclxuICAgIHByaXZhdGUgcmVhZG9ubHkgV09STERfVVNFUk1FVEFEQVRBX0lEOiBzdHJpbmcgPSB0aGlzLldPUkxEX1VTRVJNRVRBREFUQSArIFwiaWQuXCI7XHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IFdPUkxEX1VTRVJNRVRBREFUQV9MSVNUOiBzdHJpbmcgPSB0aGlzLldPUkxEX1VTRVJNRVRBREFUQSArIFwibGlzdFwiO1xyXG5cclxuICAgIHByaXZhdGUgX3JlZGlzQ2xpZW50OiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoIHJlZGlzQ2xpZW50OiBhbnkgKSB7XHJcbiAgICAgICAgc3VwZXIocmVkaXNDbGllbnQpO1xyXG4gICAgICAgIGlmICggcmVkaXNDbGllbnQgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlZGlzQ2xpZW50ID0gcmVkaXNDbGllbnQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiTm8gUmVkaXMgQ2xpZW50IGRlZmluZWRcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJuIHRoZSB0eXBlIG9mIHRoaXMgc3RvcmFnZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFN0b3JhZ2VUeXBlKCk6IFN0b3JhZ2VUeXBlIHtcclxuICAgICAgICByZXR1cm4gU3RvcmFnZVR5cGUuVXNlck1ldGFEYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTG9jayBVc2VyTWV0YURhdGEuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBsb2NrVXNlck1ldGFEYXRhKCB1c2VyTWV0YURhdGE6IFVzZXJNZXRhRGF0YSApOiBQcm9taXNlPHZvaWQ+IHtcclxuXHJcbiAgICAgICAgbGV0IHByb21pc2UgPSBuZXcgUHJvbWlzZTx2b2lkPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBhZnRlcldhdGNoKGVycjogYW55KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5fcmVkaXNDbGllbnQud2F0Y2goIHRoaXMuV09STERfVVNFUk1FVEFEQVRBX0lEICsgdXNlck1ldGFEYXRhLnVzZXJJZCwgYWZ0ZXJXYXRjaCApO1xyXG5cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHByb21pc2U7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU3RvcmUgYSB1c2VyIG1ldGEgZGF0YSBpbiBhIHN0b3JhZ2UuXHJcbiAgICAgKiBAcGFyYW0gdXNlck1ldGFEYXRhXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBhZGRVcGRhdGVVc2VyTWV0YURhdGEoIHVzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhICk6IFByb21pc2U8Ym9vbGVhbj4ge1xyXG5cclxuICAgICAgICBsZXQgc2VsZjogVXNlck1ldGFEYXRhU3RvcmFnZSA9IHRoaXM7XHJcbiAgICAgICAgbGV0IGtleTogc3RyaW5nID0gdXNlck1ldGFEYXRhLnVzZXJJZDtcclxuICAgICAgICBsZXQgdmFsdWU6IHN0cmluZyA9IEpTT04uc3RyaW5naWZ5KCB1c2VyTWV0YURhdGEudG9KU09OKCkgKTtcclxuXHJcbiAgICAgICAgbGV0IHByb21pc2UgPSBuZXcgUHJvbWlzZTxib29sZWFuPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBhZnRlckV4ZWMoIGVycjogYW55ICkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBlcnIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIGFmdGVyV2F0Y2hpbmdMaXN0KCBlcnI6IGFueSApIHtcclxuICAgICAgICAgICAgICAgIGlmICggZXJyICkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdCgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBzZWxmLl9yZWRpc0NsaWVudC5tdWx0aSgpLmhzZXQoc2VsZi5XT1JMRF9VU0VSTUVUQURBVEFfTElTVCwga2V5LCB2YWx1ZSkuZXhlYyhhZnRlckV4ZWMpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBzZWxmLl9yZWRpc0NsaWVudC53YXRjaCggc2VsZi5XT1JMRF9VU0VSTUVUQURBVEFfTElTVCwgYWZ0ZXJXYXRjaGluZ0xpc3QgKTtcclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHJpZXZlIGEgdXNlciBtZXRhIGRhdGEgZnJvbSBzdG9yYWdlLiBSZXR1cm4gdW5kZWZpbmVkIGlmIG5vbmUgaXMgZm91bmQuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRVc2VyTWV0YURhdGEoIHVzZXJJZDogc3RyaW5nICk6IFByb21pc2U8VXNlck1ldGFEYXRhPiB7XHJcblxyXG4gICAgICAgIGxldCBwcm9taXNlID0gbmV3IFByb21pc2U8VXNlck1ldGFEYXRhPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBhZnRlckhHZXQoZXJyOiBhbnksIHZhbHVlOiBzdHJpbmcpIHtcclxuICAgICAgICAgICAgICAgIGlmICggZXJyICkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdCgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIHZhbHVlID09PSBudWxsICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHVuZGVmaW5lZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGpzb24gPSBKU09OLnBhcnNlKHZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHJlc3VsdDogVXNlck1ldGFEYXRhID0gVXNlck1ldGFEYXRhLmZyb21KU09OKGpzb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHJlc3VsdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9yZWRpc0NsaWVudC5oZ2V0KHRoaXMuV09STERfVVNFUk1FVEFEQVRBX0xJU1QsIHVzZXJJZCwgYWZ0ZXJIR2V0KTtcclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbW92ZSBhIHVzZXIgbWV0YSBkYXRhIGZyb20gc3RvcmFnZS5cclxuICAgICAqIEBwYXJhbSB1c2VySWRcclxuICAgICAqL1xyXG4gICAgcHVibGljIHJlbW92ZVVzZXJNZXRhRGF0YSggdXNlcklkOiBzdHJpbmcgKTogUHJvbWlzZTxib29sZWFuPiB7XHJcblxyXG4gICAgICAgIGxldCBzZWxmOiBVc2VyTWV0YURhdGFTdG9yYWdlID0gdGhpcztcclxuICAgICAgICBsZXQga2V5OiBzdHJpbmcgPSB1c2VySWQ7XHJcblxyXG4gICAgICAgIGxldCBwcm9taXNlID0gbmV3IFByb21pc2U8Ym9vbGVhbj4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gYWZ0ZXJFeGVjKCBlcnI6IGFueSApIHtcclxuICAgICAgICAgICAgICAgIGlmICggZXJyICkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdCgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBhZnRlcldhdGNoaW5nTGlzdCggZXJyOiBhbnkgKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIGVyciApIHtcclxuICAgICAgICAgICAgICAgICAgICByZWplY3QoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5fcmVkaXNDbGllbnQubXVsdGkoKS5oZGVsKHNlbGYuV09STERfVVNFUk1FVEFEQVRBX0xJU1QsIGtleSkuZXhlYyhhZnRlckV4ZWMpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBzZWxmLl9yZWRpc0NsaWVudC53YXRjaCggc2VsZi5XT1JMRF9VU0VSTUVUQURBVEFfTElTVCwgYWZ0ZXJXYXRjaGluZ0xpc3QgKTtcclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG5cclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFVzZXJNZXRhRGF0YVN0b3JhZ2U7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
