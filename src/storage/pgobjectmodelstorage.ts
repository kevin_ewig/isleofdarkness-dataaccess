import ObjectModel from "isleofdarkness-common/src/model/objectmodel";
import IPgObjectModelStorage from "./ipgobjectmodelstorage";
import { Point } from "isleofdarkness-common/src/math/point";
import { Shape } from "isleofdarkness-common/src/math/shape";
import { Circle } from "isleofdarkness-common/src/math/circle";
import { Rectangle } from "isleofdarkness-common/src/math/rectangle";
import { StorageType } from "./ibasestorage";
import { Constants } from "isleofdarkness-common/src/math/constants";
import Uuid from "isleofdarkness-common/src/uuid";
import Logger from "isleofdarkness-common/src/logger";
import BasePgStorage from "./basepgstorage";

export class PgObjectModelStorage extends BasePgStorage implements IPgObjectModelStorage {

    constructor( pool: any ) {
        super( pool );
    }

    /**
     * Return the type of this storage.
     */
    public getStorageType(): StorageType {
        return StorageType.ObjectModel;
    }

    /**
     * Store a object model in a storage.
     * @param objectModel
     */
    public async addUpdateObjectModel( objectModel: ObjectModel ): Promise<boolean> {

        let result = false;
        let pool = this.getPool();
        let client = await pool.connect();

        try {

            await client.query("begin");

            // Check if object model already exists. If it does not then insert. Otherwise, update.
            let objectModelIdResult: any = await client.query(
                "SELECT objectmodel_id FROM iodobjectmodel WHERE objectmodel_id = $1",
                [objectModel.id]
            );
            if ( objectModelIdResult.rowCount === 0 ) {
                let objectModelId: string = await this._insertObjectModel(client, objectModel);
                result = true;
            } else {
                let objectModelId: string = await this._updateObjectModel(client, objectModel);
                result = true;
            }

            await client.query("commit");

        } catch ( error ) {
            result = false;
            Logger.error("PgObjectModelStorage", "Error: " + error);
            await client.query("rollback");
        } finally {
            client.release();
        }

        return result;

    }

    /**
     * Delete an object model.
     * @param objectModelId
     */
    public async deleteObjectModel( objectModelId: string ): Promise<boolean> {
        try {
            let pool = this.getPool();
            await pool.query(
                " DELETE FROM iodobjectmodel AS om " +
                "  WHERE om.objectmodel_id = $1 ",
                [objectModelId]
            );
        } catch ( exception ) {
            Logger.error("PgObjectModelStorage", "Error: " + exception);
            return false;
        }
        return true;
    }

    /**
     * Retrieve a object model from storage. Return undefined if none is found.
     * @param objectModelId
     */
    public async getObjectModel( objectModelId: string ): Promise<ObjectModel|null> {

        let pool = this.getPool();
        let resultSet: any = await pool.query(
            "SELECT objectmodel_id, shape_id, objectmodel_type, objectmodel_worldname, objectmodel_modelname, " +
            "       objectmodel_displayname, objectmodel_modeltexture, owner_user_id " +
            "  FROM iodobjectmodel WHERE objectmodel_id = $1", [objectModelId]);

        return await this._getObjectModelFromResult(resultSet);

    }

    private async _getObjectModelFromResult( resultSet: any ): Promise<ObjectModel|null> {

        let resultObj: ObjectModel|null = null;

        let resultRow: any = resultSet.rows[0];
        if ( resultSet.rowCount === 0 ) {
            return null;
        }

        let shapeId: string = resultRow.shape_id;
        let shape: Shape|null = await this._getShape(shapeId);
        if ( shape != null ) {
            resultObj =
                new ObjectModel(resultRow.objectmodel_id, shape as Shape, resultRow.objectmodel_modelname);
            resultObj.objectType = resultRow.objectmodel_type;
            resultObj.modelTexture = resultRow.objectmodel_modeltexture;
            resultObj.worldName = resultRow.objectmodel_worldname;
            resultObj.modelName = resultRow.objectmodel_modelname;
            resultObj.displayName = resultRow.objectmodel_displayname;
            resultObj.ownerUserId = resultRow.owner_user_id;
        } else {
            return null;
        }

        return resultObj;

    }

    private async _insertObjectModel( client: any, objectModel: ObjectModel ): Promise<string> {
        let objectModelId: string = objectModel.id;
        let shapeId = "";
        if ( objectModel.shape.type === Constants.CIRCLE ) {
            shapeId = await this._insertCircleShape( client, objectModel.shape as Circle );
        } else if ( objectModel.shape.type === Constants.RECTANGLE ) {
            shapeId = await this._insertRectangleShape( client, objectModel.shape as Rectangle );
        }
        await client.query(
            "INSERT INTO iodobjectmodel (objectmodel_id, shape_id, objectmodel_type, " +
            " objectmodel_worldname, objectmodel_modelname, objectmodel_displayname, " +
            " objectmodel_modeltexture, owner_user_id) " +
            "VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ",
            [objectModel.id,
             shapeId,
             objectModel.objectType,
             objectModel.worldName,
             objectModel.modelName,
             objectModel.displayName,
             objectModel.modelTexture,
             objectModel.ownerUserId]
        );
        return objectModelId;
    }

    private async _updateObjectModel( client: any, objectModel: ObjectModel ): Promise<string> {
        let resultSet: any =
            await client.query("SELECT shape_id FROM iodobjectmodel WHERE objectmodel_id = $1", [objectModel.id]);
        let shapeId = resultSet.rows[0].shape_id;
        if ( objectModel.shape.type === Constants.CIRCLE ) {
            shapeId = await this._updateCircleShape( client, shapeId, objectModel.shape as Circle );
        } else if ( objectModel.shape.type === Constants.RECTANGLE ) {
            shapeId = await this._updateRectangleShape( client, shapeId, objectModel.shape as Rectangle );
        }
        await client.query(
            "UPDATE iodobjectmodel " +
            "SET objectmodel_type = $1, objectmodel_worldname = $2, objectmodel_modelname = $3, " +
            "objectmodel_displayname = $4, objectmodel_modeltexture = $5, owner_user_id = $6 " +
            "WHERE objectmodel_id = $7 ",
            [objectModel.objectType,
             objectModel.worldName,
             objectModel.modelName,
             objectModel.displayName,
             objectModel.modelTexture,
             objectModel.ownerUserId,
             objectModel.id]
        );
        return objectModel.id;
    }

}

export default PgObjectModelStorage;
