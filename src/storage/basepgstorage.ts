import { Point } from "isleofdarkness-common/src/math/point";
import { Shape } from "isleofdarkness-common/src/math/shape";
import { Circle } from "isleofdarkness-common/src/math/circle";
import { Rectangle } from "isleofdarkness-common/src/math/rectangle";
import { Constants } from "isleofdarkness-common/src/math/constants";
import Uuid from "isleofdarkness-common/src/uuid";

export class BasePgStorage {

    private readonly _pool: any;

    constructor( pool: any ) {
        this._pool = pool;
    }

    /**
     * Get the application pool.
     */
    public getPool() {
        return this._pool;
    }

    /**
     * Retrieve a shape from storage. Return undefined if none is found.
     * @param shapeId
     */
    public async _getShape( shapeId: string ): Promise<Shape|null> {

        let resultSet: any = await this._pool.query(
            "SELECT shape_id, shape_type, circle_point_id, circle_radius, rectangle_point_id, " +
            "  rectangle_width, rectangle_height, poly_p0_point_id, poly_p1_point_id, poly_p2_point_id, " +
            "  poly_p3_point_id, poly_p4_point_id, poly_p5_point_id, poly_p6_point_id, poly_p7_point_id, " +
            "  poly_p8_point_id, poly_p9_point_id " +
            "FROM IODShape WHERE shape_id = $1", [shapeId]
        );

        let resultRow: any = resultSet.rows[0];

        let shapeType: string = resultRow.shape_type;
        let shape: Shape|null = null;
        if ( shapeType === Constants.CIRCLE ) {
            let point: Point|null = await this._getPoint( resultRow.circle_point_id);
            if ( point != null ) {
                let radius = resultRow.circle_radius;
                shape = new Circle( point, radius );
            }
        } else if ( shapeType === Constants.RECTANGLE ) {
            let point: Point|null = await this._getPoint( resultRow.rectangle_point_id);
            if ( point != null ) {
                let rectWidth = resultRow.rectangle_width;
                let rectHeight = resultRow.rectangle_height;
                shape = new Rectangle( point, rectWidth, rectHeight );
            }
        }

        return shape;

    }

    public async _getPoint( pointId: string ): Promise<Point|null> {
        let resultSet: any = await this._pool.query(
            "SELECT point_id, point_x, point_y, point_z " +
            "FROM IODPoint WHERE point_id = $1", [pointId]
        );
        if ( resultSet.rowCount === 0 ) {
            return null;
        }
        return new Point( resultSet.rows[0].point_x, resultSet.rows[0].point_y, resultSet.rows[0].point_z );
    }

    public async _insertCircleShape( client: any, circle: Circle ): Promise<string> {
        let shapeId: string = Uuid.getUuid();
        let pointId: string = await this._insertPoint( client, circle.getCenter() );
        await client.query(
            "INSERT INTO IODShape (shape_id, shape_type, circle_point_id, circle_radius) " +
            "VALUES ($1, $2, $3, $4) ",
            [shapeId, circle.type, pointId, circle.getRadius()]);
        return shapeId;
    }

    public async _updateCircleShape( client: any, shapeId: string, circle: Circle ): Promise<string> {
        let resultSet: any = await client.query("SELECT circle_point_id from IODShape WHERE shape_id = $1", [shapeId]);
        let pointId = resultSet.rows[0].circle_point_id;
        await this._updatePoint( client, pointId, circle.getCenter() );
        await client.query(
            "UPDATE IODShape SET circle_radius = $2 WHERE shape_id = $1",
            [shapeId, circle.getRadius()]);
        return shapeId;
    }

    public async _insertRectangleShape( client: any, rect: Rectangle ): Promise<string> {
        let shapeId: string = Uuid.getUuid();
        let pointId: string = await this._insertPoint( client, rect.position() );
        await client.query(
            "INSERT INTO IODShape (shape_id, shape_type, rectangle_point_id, rectangle_width, rectangle_height) " +
            "VALUES ($1, $2, $3, $4, $5) ",
            [shapeId, rect.type, pointId, rect.width(), rect.height()]);
        return shapeId;
    }

    public async _updateRectangleShape( client: any, shapeId: string, rect: Rectangle ): Promise<string> {
        let resultSet: any = await client.query("SELECT rectangle_point_id from IODShape WHERE shape_id = $1", [shapeId]);
        let pointId = resultSet.rows[0].rectangle_point_id;
        await this._updatePoint( client, pointId, rect.position() );
        await client.query(
            "UPDATE IODShape SET rectangle_width = $2, rectangle_height = $3 WHERE shape_id = $1",
            [shapeId, rect.width(), rect.height()]);
        return shapeId;
    }

    public async _insertPoint( client: any, point: Point ): Promise<string> {
        let pointId: string = Uuid.getUuid();
        let result = await client.query(
            "INSERT INTO IODPoint (point_id, point_x, point_y, point_z) " +
            "VALUES ($1, $2, $3, $4) ",
            [pointId, point.x, point.y, point.z]);
        return pointId;
    }

    public async _updatePoint( client: any, pointId: string, point: Point ): Promise<string> {
        await client.query(
            "UPDATE IODPoint SET point_x = $1, point_y = $2, point_z = $3 WHERE point_id = $4 ",
            [point.x, point.y, point.z, pointId]);
        return pointId;
    }


}

export default BasePgStorage;
