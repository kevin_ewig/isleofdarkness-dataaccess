import ObjectModel from "isleofdarkness-common/src/model/objectmodel";
import Point from "isleofdarkness-common/src/math/point";
import IBaseStorage from "./ibasestorage";

interface IObjectModelStorage extends IBaseStorage {

    /**
     * Lock ObjectModel.
     * @param objectModel
     */
    lockObjectModel( objectModel: ObjectModel ): Promise<void>;

    /**
     * Add or update a object model in storage.
     * @param objectModel
     */
    addUpdateObjectModel( objectModel: ObjectModel ): Promise<boolean>;

    /**
     * Retrieve a object model from storage. Return undefined if none is found.
     * @param objectModelId
     */
    getObjectModel( objectModelId: string ): Promise<ObjectModel>;

    /**
     * Retrieve all object model ids from a section coodinate.
     * @param coord The section coordinate
     */
    getObjectModelsIdsInSection( coord: Point ): Promise<Array<string>>;

    /**
     * Remove a object model from storage.
     * @param objectModelId
     */
    removeObjectModel( objectModelId: string ): Promise<boolean>;

}

export default IObjectModelStorage;
