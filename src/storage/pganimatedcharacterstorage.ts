import BaseAnimatedCharacter from "isleofdarkness-common/src/model/baseanimatedcharacter";
import ObjectModel from "isleofdarkness-common/src/model/objectmodel";
import Shape from "isleofdarkness-common/src/math/shape";
import { MissingConstructorArgumentError } from "isleofdarkness-common/src/error/missingconstructorargumenterror";
import { StorageType } from "./ibasestorage";
import BasePgStorage from "./basepgstorage";
import IPgAnimationProfileStorage from "./ipganimationprofilestorage";
import IPgAnimatedCharacterStorage from "./ipganimatedcharacterstorage";
import IPgBaseAnimatedCharacterStorage from "./ipgbaseanimatedcharacterstorage";
import { IPgObjectModelStorage } from "./ipgobjectmodelstorage";
import { AnimatedCharacter } from "isleofdarkness-common/src/model/animatedcharacter";

export class PgAnimatedCharacterStorage extends BasePgStorage implements IPgAnimatedCharacterStorage {

    private _pgAnimationProfileStorage: IPgAnimationProfileStorage;
    private _pgObjectModelStorage: IPgObjectModelStorage;

    constructor(pool: any,
                pgObjectModelStorage: IPgObjectModelStorage,
                pgAnimationProfileStorage: IPgAnimationProfileStorage) {
        super(pool);
        if ( pgAnimationProfileStorage && pgObjectModelStorage ) {
            this._pgObjectModelStorage = pgObjectModelStorage;
            this._pgAnimationProfileStorage = pgAnimationProfileStorage;
        } else {
            throw new MissingConstructorArgumentError();
        }
    }

    /**
     * Return the type of this storage.
     */
    public getStorageType(): StorageType {
        return StorageType.BaseAnimatedCharacter;
    }

    /**
     * Add or update the animated character into the database.
     * @param animatedCharacter
     */
    public async addUpdateAnimatedCharacter( animatedCharacter: AnimatedCharacter ): Promise<boolean> {

        let result = await this._pgObjectModelStorage.addUpdateObjectModel(animatedCharacter);
        let pool = this.getPool();
        let animationProfileId: any = null;
        if ( animatedCharacter.animationProfile ) {
            animationProfileId = animatedCharacter.animationProfile.id;
        }
        await pool.query(
            "INSERT INTO iodanimatedcharacter (objectmodel_id, animationprofile_id, speed) " +
            "VALUES ($1, $2, $3) " +
            "ON CONFLICT (objectmodel_id) DO UPDATE SET animationprofile_id = $2, speed = $3",
            [animatedCharacter.id,
             animationProfileId,
             animatedCharacter.speed]
        );
        return result;
    }

    /**
     * Get a base animated character object.
     */
    public async getAnimatedCharacter( animatedCharacterId: string ): Promise<AnimatedCharacter|null> {

        let pool = this.getPool();
        let result = await pool.query(
            "SELECT a.objectmodel_id, a.animationprofile_id, a.speed " +
            "  FROM iodanimatedcharacter AS a " +
            " WHERE a.objectmodel_id = $1", [animatedCharacterId]);

        let row: any = result.rows[0];

        let objectModelId: string = row.objectmodel_id;
        let animationProfileId: string = row.animationprofile_id;
        let speed: number = row.speed;

        let animatedCharacter: AnimatedCharacter|null = null;
        let objectModel: ObjectModel|null = await this._pgObjectModelStorage.getObjectModel(objectModelId);
        if ( objectModel ) {

            let animationProfile = await this._pgAnimationProfileStorage.getAnimationProfile(animationProfileId);
            animatedCharacter =
                new AnimatedCharacter(animatedCharacterId, objectModel.shape, objectModel.modelName);
            animatedCharacter.worldName = objectModel.worldName;
            animatedCharacter.displayName = objectModel.displayName;
            animatedCharacter.ownerUserId = objectModel.ownerUserId;
            animatedCharacter.objectType = objectModel.objectType;
            animatedCharacter.modelTexture = objectModel.modelTexture;

            animatedCharacter.animationProfile = animationProfile;
            animatedCharacter.speed = speed;

        }
        return animatedCharacter;

    }

    /**
     * Delete an animated character.
     */
    public async deleteAnimatedCharacter( objectModelId: string ): Promise<boolean> {
        let pool = this.getPool();
        let result1 = await pool.query("DELETE FROM iodanimatedcharacter WHERE objectmodel_id = $1", [objectModelId]);
        let result2 = await pool.query("DELETE FROM iodobjectmodel WHERE objectmodel_id = $1", [objectModelId]);
        return result1 && result2;
    }

}

export default PgAnimatedCharacterStorage;
