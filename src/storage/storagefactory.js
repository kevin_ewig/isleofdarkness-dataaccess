"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var uuid_1 = require("isleofdarkness-common/src/uuid");
var ibasestorage_1 = require("./ibasestorage");
var usermetadatastorage_1 = require("./usermetadatastorage");
var objectmodelstorage_1 = require("./objectmodelstorage");
var ticketstorage_1 = require("./ticketstorage");
var userstorage_1 = require("./userstorage");
var StorageFactory = /** @class */ (function () {
    function StorageFactory(redisClient) {
        if (redisClient) {
            this._redisClient = redisClient;
            this._userMetaDataStorage = new usermetadatastorage_1.default(redisClient);
            this._objectModelStorage = new objectmodelstorage_1.default(redisClient);
            this._ticketStorage = new ticketstorage_1.default(redisClient);
            this._userStorage = new userstorage_1.default(redisClient);
            this._userMetaDataStorage = new usermetadatastorage_1.default(redisClient);
        }
        else {
            throw new Error("No Redis Client defined");
        }
    }
    /**
     * Check if connection to database is established.
     */
    StorageFactory.prototype.isConnected = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var promise;
            return __generator(this, function (_a) {
                promise = new Promise(function (resolve, reject) {
                    var uuid = uuid_1.default.getUuid();
                    function afterGet(err, reply) {
                        if (err) {
                            reject();
                        }
                        else {
                            resolve(true);
                        }
                    }
                    // This will expire in 5 seconds. Check if client can set.
                    _this._redisClient.set(uuid, "OK", "EX", 5);
                    // This will check if client can get.
                    _this._redisClient.get(uuid, afterGet);
                });
                return [2 /*return*/, promise];
            });
        });
    };
    /**
     * Get storage type
     */
    StorageFactory.prototype.getStorage = function (storageType) {
        if (storageType === ibasestorage_1.StorageType.ObjectModel) {
            return this._objectModelStorage;
        }
        else if (storageType === ibasestorage_1.StorageType.UserMetaData) {
            return this._userMetaDataStorage;
        }
        else if (storageType === ibasestorage_1.StorageType.Ticket) {
            return this._ticketStorage;
        }
        else if (storageType === ibasestorage_1.StorageType.User) {
            return this._userStorage;
        }
        throw new Error("No enum type of StorageType." + ibasestorage_1.StorageType[storageType] + " found");
    };
    return StorageFactory;
}());
exports.StorageFactory = StorageFactory;
exports.default = StorageFactory;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2Uvc3RvcmFnZWZhY3RvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHVEQUFrRDtBQUlsRCwrQ0FBeUQ7QUFHekQsNkRBQXdEO0FBQ3hELDJEQUFzRDtBQUN0RCxpREFBNEM7QUFDNUMsNkNBQXdDO0FBSXhDO0lBU0ksd0JBQWEsV0FBZ0I7UUFDekIsRUFBRSxDQUFDLENBQUUsV0FBWSxDQUFDLENBQUMsQ0FBQztZQUNoQixJQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQztZQUNoQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSw2QkFBbUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNqRSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSw0QkFBa0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksdUJBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNyRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUkscUJBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSw2QkFBbUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNyRSxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDL0MsQ0FBQztJQUNMLENBQUM7SUFFRDs7T0FFRztJQUNVLG9DQUFXLEdBQXhCOzs7OztnQkFFUSxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQVUsVUFBQyxPQUFPLEVBQUUsTUFBTTtvQkFFL0MsSUFBSSxJQUFJLEdBQVcsY0FBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUVsQyxrQkFBbUIsR0FBUSxFQUFFLEtBQVU7d0JBQ25DLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7NEJBQ1IsTUFBTSxFQUFFLENBQUM7d0JBQ2IsQ0FBQzt3QkFBQyxJQUFJLENBQUMsQ0FBQzs0QkFDSixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ2xCLENBQUM7b0JBQ0wsQ0FBQztvQkFFRCwwREFBMEQ7b0JBQzFELEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUUzQyxxQ0FBcUM7b0JBQ3JDLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFFMUMsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsc0JBQU8sT0FBTyxFQUFDOzs7S0FFbEI7SUFFRDs7T0FFRztJQUNJLG1DQUFVLEdBQWpCLFVBQTJDLFdBQXdCO1FBQy9ELEVBQUUsQ0FBQyxDQUFFLFdBQVcsS0FBSywwQkFBVyxDQUFDLFdBQVksQ0FBQyxDQUFDLENBQUM7WUFDNUMsTUFBTSxDQUFTLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztRQUM1QyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLFdBQVcsS0FBSywwQkFBVyxDQUFDLFlBQWEsQ0FBQyxDQUFDLENBQUM7WUFDcEQsTUFBTSxDQUFTLElBQUksQ0FBQyxvQkFBb0IsQ0FBQztRQUM3QyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLFdBQVcsS0FBSywwQkFBVyxDQUFDLE1BQU8sQ0FBQyxDQUFDLENBQUM7WUFDOUMsTUFBTSxDQUFTLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDdkMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxXQUFXLEtBQUssMEJBQVcsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO1lBQzVDLE1BQU0sQ0FBUyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQ3JDLENBQUM7UUFDRCxNQUFNLElBQUksS0FBSyxDQUFDLDhCQUE4QixHQUFHLDBCQUFXLENBQUMsV0FBVyxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUM7SUFDMUYsQ0FBQztJQUVMLHFCQUFDO0FBQUQsQ0FuRUEsQUFtRUMsSUFBQTtBQW5FWSx3Q0FBYztBQXFFM0Isa0JBQWUsY0FBYyxDQUFDIiwiZmlsZSI6InN0b3JhZ2Uvc3RvcmFnZWZhY3RvcnkuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgVXVpZCBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy91dWlkXCI7XHJcblxyXG5pbXBvcnQgSVN0b3JhZ2VGYWN0b3J5IGZyb20gXCIuL2lzdG9yYWdlZmFjdG9yeVwiO1xyXG5pbXBvcnQgQmFzZVN0b3JhZ2UgZnJvbSBcIi4vYmFzZXN0b3JhZ2VcIjtcclxuaW1wb3J0IElCYXNlU3RvcmFnZSwge1N0b3JhZ2VUeXBlfSBmcm9tIFwiLi9pYmFzZXN0b3JhZ2VcIjtcclxuaW1wb3J0IElVc2VyTWV0YURhdGFTdG9yYWdlIGZyb20gXCIuL2l1c2VybWV0YWRhdGFzdG9yYWdlXCI7XHJcbmltcG9ydCBJT2JqZWN0TW9kZWxTdG9yYWdlIGZyb20gXCIuL2lvYmplY3Rtb2RlbHN0b3JhZ2VcIjtcclxuaW1wb3J0IFVzZXJNZXRhRGF0YVN0b3JhZ2UgZnJvbSBcIi4vdXNlcm1ldGFkYXRhc3RvcmFnZVwiO1xyXG5pbXBvcnQgT2JqZWN0TW9kZWxTdG9yYWdlIGZyb20gXCIuL29iamVjdG1vZGVsc3RvcmFnZVwiO1xyXG5pbXBvcnQgVGlja2V0U3RvcmFnZSBmcm9tIFwiLi90aWNrZXRzdG9yYWdlXCI7XHJcbmltcG9ydCBVc2VyU3RvcmFnZSBmcm9tIFwiLi91c2Vyc3RvcmFnZVwiO1xyXG5pbXBvcnQgSVRpY2tldFN0b3JhZ2UgZnJvbSBcIi4vaXRpY2tldHN0b3JhZ2VcIjtcclxuaW1wb3J0IElVc2VyU3RvcmFnZSBmcm9tIFwiLi9pdXNlcnN0b3JhZ2VcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBTdG9yYWdlRmFjdG9yeSBpbXBsZW1lbnRzIElTdG9yYWdlRmFjdG9yeSB7XHJcblxyXG4gICAgcHJpdmF0ZSByZWFkb25seSBfdXNlck1ldGFEYXRhU3RvcmFnZTogSVVzZXJNZXRhRGF0YVN0b3JhZ2U7XHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IF9vYmplY3RNb2RlbFN0b3JhZ2U6IElPYmplY3RNb2RlbFN0b3JhZ2U7XHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IF90aWNrZXRTdG9yYWdlOiBJVGlja2V0U3RvcmFnZTtcclxuICAgIHByaXZhdGUgcmVhZG9ubHkgX3VzZXJTdG9yYWdlOiBJVXNlclN0b3JhZ2U7XHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IF91c2VyTWV0YURhdGFTdG9yZ2U6IElVc2VyTWV0YURhdGFTdG9yYWdlO1xyXG4gICAgcHJpdmF0ZSByZWFkb25seSBfcmVkaXNDbGllbnQ6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvciggcmVkaXNDbGllbnQ6IGFueSApIHtcclxuICAgICAgICBpZiAoIHJlZGlzQ2xpZW50ICkge1xyXG4gICAgICAgICAgICB0aGlzLl9yZWRpc0NsaWVudCA9IHJlZGlzQ2xpZW50O1xyXG4gICAgICAgICAgICB0aGlzLl91c2VyTWV0YURhdGFTdG9yYWdlID0gbmV3IFVzZXJNZXRhRGF0YVN0b3JhZ2UocmVkaXNDbGllbnQpO1xyXG4gICAgICAgICAgICB0aGlzLl9vYmplY3RNb2RlbFN0b3JhZ2UgPSBuZXcgT2JqZWN0TW9kZWxTdG9yYWdlKHJlZGlzQ2xpZW50KTtcclxuICAgICAgICAgICAgdGhpcy5fdGlja2V0U3RvcmFnZSA9IG5ldyBUaWNrZXRTdG9yYWdlKHJlZGlzQ2xpZW50KTtcclxuICAgICAgICAgICAgdGhpcy5fdXNlclN0b3JhZ2UgPSBuZXcgVXNlclN0b3JhZ2UocmVkaXNDbGllbnQpO1xyXG4gICAgICAgICAgICB0aGlzLl91c2VyTWV0YURhdGFTdG9yYWdlID0gbmV3IFVzZXJNZXRhRGF0YVN0b3JhZ2UocmVkaXNDbGllbnQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIk5vIFJlZGlzIENsaWVudCBkZWZpbmVkXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIGlmIGNvbm5lY3Rpb24gdG8gZGF0YWJhc2UgaXMgZXN0YWJsaXNoZWQuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBhc3luYyBpc0Nvbm5lY3RlZCgpOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuXHJcbiAgICAgICAgbGV0IHByb21pc2UgPSBuZXcgUHJvbWlzZTxib29sZWFuPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICBsZXQgdXVpZDogc3RyaW5nID0gVXVpZC5nZXRVdWlkKCk7XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBhZnRlckdldCggZXJyOiBhbnksIHJlcGx5OiBhbnkgKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIGVyciApIHtcclxuICAgICAgICAgICAgICAgICAgICByZWplY3QoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gVGhpcyB3aWxsIGV4cGlyZSBpbiA1IHNlY29uZHMuIENoZWNrIGlmIGNsaWVudCBjYW4gc2V0LlxyXG4gICAgICAgICAgICB0aGlzLl9yZWRpc0NsaWVudC5zZXQodXVpZCwgXCJPS1wiLCBcIkVYXCIsIDUpO1xyXG5cclxuICAgICAgICAgICAgLy8gVGhpcyB3aWxsIGNoZWNrIGlmIGNsaWVudCBjYW4gZ2V0LlxyXG4gICAgICAgICAgICB0aGlzLl9yZWRpc0NsaWVudC5nZXQodXVpZCwgYWZ0ZXJHZXQpO1xyXG5cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHByb21pc2U7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHN0b3JhZ2UgdHlwZVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0U3RvcmFnZTxUIGV4dGVuZHMgSUJhc2VTdG9yYWdlPiggc3RvcmFnZVR5cGU6IFN0b3JhZ2VUeXBlICk6IFQge1xyXG4gICAgICAgIGlmICggc3RvcmFnZVR5cGUgPT09IFN0b3JhZ2VUeXBlLk9iamVjdE1vZGVsICkge1xyXG4gICAgICAgICAgICByZXR1cm4gPFQ+PGFueT50aGlzLl9vYmplY3RNb2RlbFN0b3JhZ2U7XHJcbiAgICAgICAgfSBlbHNlIGlmICggc3RvcmFnZVR5cGUgPT09IFN0b3JhZ2VUeXBlLlVzZXJNZXRhRGF0YSApIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxUPjxhbnk+dGhpcy5fdXNlck1ldGFEYXRhU3RvcmFnZTtcclxuICAgICAgICB9IGVsc2UgaWYgKCBzdG9yYWdlVHlwZSA9PT0gU3RvcmFnZVR5cGUuVGlja2V0ICkge1xyXG4gICAgICAgICAgICByZXR1cm4gPFQ+PGFueT50aGlzLl90aWNrZXRTdG9yYWdlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIHN0b3JhZ2VUeXBlID09PSBTdG9yYWdlVHlwZS5Vc2VyICkge1xyXG4gICAgICAgICAgICByZXR1cm4gPFQ+PGFueT50aGlzLl91c2VyU3RvcmFnZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiTm8gZW51bSB0eXBlIG9mIFN0b3JhZ2VUeXBlLlwiICsgU3RvcmFnZVR5cGVbc3RvcmFnZVR5cGVdICsgXCIgZm91bmRcIik7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBTdG9yYWdlRmFjdG9yeTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
