import BaseAnimatedCharacter from "isleofdarkness-common/src/model/baseanimatedcharacter";
import Shape from "isleofdarkness-common/src/math/shape";
import { MissingConstructorArgumentError } from "isleofdarkness-common/src/error/missingconstructorargumenterror";
import { StorageType } from "./ibasestorage";
import BasePgStorage from "./basepgstorage";
import IPgAnimationProfileStorage from "./ipganimationprofilestorage";
import IPgBaseAnimatedCharacterStorage from "./ipgbaseanimatedcharacterstorage";

export class PgBaseAnimatedCharacterStorage extends BasePgStorage implements IPgBaseAnimatedCharacterStorage {

    private _pgAnimationStorage: IPgAnimationProfileStorage;

    constructor(pool: any, pgAnimationProfileStorage: IPgAnimationProfileStorage) {
        super(pool);
        if ( pgAnimationProfileStorage ) {
            this._pgAnimationStorage = pgAnimationProfileStorage;
        } else {
            throw new MissingConstructorArgumentError();
        }
    }

    /**
     * Return the type of this storage.
     */
    public getStorageType(): StorageType {
        return StorageType.BaseAnimatedCharacter;
    }

    /**
     * Get a base animated character object.
     */
    public async getBaseAnimatedCharacter( baseAnimatedCharacterId: string ): Promise<BaseAnimatedCharacter> {

        let pool = this.getPool();
        let result = await pool.query(
            "SELECT baseanimatedcharacter_id, shape_id, baseanimatedcharacter_modelname, " +
            "       baseanimatedcharacter_modeltexture, animationprofile_id, speed " +
            "  FROM iodbaseanimatedcharacter AS a " +
            " WHERE baseanimatedcharacter_id = $1", [baseAnimatedCharacterId]);

        let row: any = result.rows[0];
        let id: string = row.baseanimatedcharacter_id;
        let shapeId: string = row.shape_id;
        let modelName: string = row.baseanimatedcharacter_modelname;
        let modelTexture: string = row.baseanimatedcharacter_modeltexture;
        let animationProfileId: string = row.animationprofile_id;
        let speed: number = row.baseanimatedcharacter_speed;

        let animationProfile = await this._pgAnimationStorage.getAnimationProfile(animationProfileId);
        let shape: Shape = <Shape><any>await this._getShape(shapeId);
        let animChar: BaseAnimatedCharacter = new BaseAnimatedCharacter(id, shape, modelName, animationProfile);
        animChar.modelTexture = modelTexture;
        animChar.speed = speed;
        return animChar;

    }

}

export default PgBaseAnimatedCharacterStorage;
