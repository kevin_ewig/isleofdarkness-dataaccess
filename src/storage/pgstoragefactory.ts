import IPgStorageFactory from "./ipgstoragefactory";
import IBaseStorage, {StorageType} from "./ibasestorage";
import PgUserStorage from "./pguserstorage";
import IPgObjectModelStorage from "./ipgobjectmodelstorage";
import IPgUserStorage from "./ipguserstorage";
import PgObjectModelStorage from "./pgobjectmodelstorage";
import IPgAnimationProfileStorage from "./ipganimationprofilestorage";
import PgBaseAnimatedCharacterStorage from "./pgbaseanimatedcharacterstorage";
import IPgBaseAnimatedCharacterStorage from "./ipgbaseanimatedcharacterstorage";
import PgAnimationProfileStorage from "./pganimationprofilestorage";
import IPgAnimatedCharacterStorage from "./ipganimatedcharacterstorage";
import PgAnimatedCharacterStorage from "./pganimatedcharacterstorage";

export class PgStorageFactory implements IPgStorageFactory {

    private readonly _userStorage: IPgUserStorage;
    private readonly _objectModelStorage: IPgObjectModelStorage;
    private readonly _animationProfileStorage: IPgAnimationProfileStorage;
    private readonly _baseAnimatedCharacterStorage: IPgBaseAnimatedCharacterStorage;
    private readonly _animatedCharacterStorage: IPgAnimatedCharacterStorage;
    private readonly _pool: any;

    constructor( pool: any ) {
        if ( pool ) {
            this._pool = pool;
            this._userStorage = new PgUserStorage(pool);
            this._objectModelStorage = new PgObjectModelStorage(pool);
            this._animationProfileStorage = new PgAnimationProfileStorage(pool);
            this._baseAnimatedCharacterStorage = new PgBaseAnimatedCharacterStorage(pool, this._animationProfileStorage);
            this._animatedCharacterStorage = new PgAnimatedCharacterStorage(pool, this._objectModelStorage,
                this._animationProfileStorage);
        } else {
            throw new Error("No Postgres Connection pool defined");
        }
    }

    /**
     * Check if connection to database is established.
     */
    public async isConnected(): Promise<boolean> {
        let result = false;
        let client: any = await this._pool.connect();
        let resultSet: any = await client.query("select now()");
        result = resultSet.rowCount === 1;
        client.release();
        return result;
    }

    /**
     * Get storage type
     */
    public getStorage<T extends IBaseStorage>( storageType: StorageType ): T {
        if ( storageType === StorageType.User ) {
            return <T><any> this._userStorage;
        } else if ( storageType === StorageType.ObjectModel ) {
            return <T><any> this._objectModelStorage;
        } else if ( storageType === StorageType.BaseAnimatedCharacter ) {
            return <T><any> this._baseAnimatedCharacterStorage;
        } else if ( storageType === StorageType.AnimationProfile ) {
            return <T><any> this._animationProfileStorage;
        } else if ( storageType === StorageType.AnimationCharacter ) {
            return <T><any> this._animatedCharacterStorage;
        }
        throw new Error("No enum type of StorageType." + StorageType[storageType] + " found");
    }

}

export default PgStorageFactory;
