import Ticket from "isleofdarkness-common/src/model/ticket";
import ITicketStorage from "./iticketstorage";
import BaseStorage from "./basestorage";
import {StorageType} from "./ibasestorage";

let redis = require("redis");

export class TicketStorage extends BaseStorage implements ITicketStorage {

    private readonly TICKET_PREFIX: string = "ticket.";

    private _redisClient: any;

    constructor( redisClient: any ) {
        super(redisClient);
        if ( redisClient ) {
            this._redisClient = redisClient;
        } else {
            throw new Error("No Redis Client defined");
        }
    }

    /**
     * Return the type of this storage.
     */
    public getStorageType(): StorageType {
        return StorageType.UserMetaData;
    }

    /**
     * Add a ticket in storage.
     * @param ticket The ticket object
     */
    public addTicket( ticket: Ticket ): Promise<boolean> {

        let self: TicketStorage = this;
        let key: string = this.TICKET_PREFIX + ticket.ticketId;
        let value: string = JSON.stringify( ticket.toJSON() );

        let promise = new Promise<boolean>((resolve, reject) => {

            function afterSetEx( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    resolve(true);
                }
            }

            let now = new Date();
            let dateToExpire = ticket.expirationDate;
            let secondsToExpire = Math.round( (dateToExpire.getTime() - now.getTime()) / 1000 );

            if ( secondsToExpire > 0 ) {
                self._redisClient.setex( key, secondsToExpire, value, afterSetEx );
            } else {
                resolve(true);
            }

        });

        return promise;

    }

    /**
     * Retrieve a ticket from storage. Return undefined if none is found.
     * @param ticketId
     */
    public getTicket( ticketId: string ): Promise<Ticket> {

        let self: TicketStorage = this;
        let key: string = this.TICKET_PREFIX + ticketId;

        let promise = new Promise<Ticket>((resolve, reject) => {

            function afterHGet(err: any, value: string) {
                if ( err ) {
                    reject();
                } else {
                    if ( value === null ) {
                        resolve(undefined);
                    } else {
                        let json = JSON.parse(value);
                        let result: Ticket = Ticket.fromJSON(json);
                        resolve(result);
                    }
                }
            }

            this._redisClient.get(key, afterHGet);

        });

        return promise;

    }

}

export default TicketStorage;
