import User from "isleofdarkness-common/src/model/user";
import IBaseStorage from "./ibasestorage";

interface IUserStorage extends IBaseStorage {

    /**
     * Add or update user into the database.
     * @param user
     */
    addUpdateUser( user: User ): Promise<boolean>;

    /**
     * Retrieve a user from storage. Return undefined if none is found.
     * @param userId
     */
    getUser( userId: string ): Promise<User>;

    /**
     * Delete a user.
     * @param user
     */
    deleteUser( userId: string ): Promise<boolean>;

}

export default IUserStorage;
