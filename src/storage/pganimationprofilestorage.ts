import Logger from "isleofdarkness-common/src/logger";
import IPgAnimationProfileStorage from "./ipganimationprofilestorage";
import { MissingConstructorArgumentError } from "isleofdarkness-common/src/error/missingconstructorargumenterror";
import { AnimationProfile } from "isleofdarkness-common/src/model/animationprofile";
import { StorageType } from "./ibasestorage";

export class PgAnimationProfileStorage implements IPgAnimationProfileStorage {

    private readonly _pool: any;

    constructor(pool: any) {
        if ( pool ) {
            this._pool = pool;
        } else {
            throw new MissingConstructorArgumentError();
        }
    }

    /**
     * Return the type of this storage.
     */
    public getStorageType(): StorageType {
        return StorageType.AnimationProfile;
    }

    /**
     * Retrieve a animated character from storage. Return undefined if none is found.
     * @param objectModelId
     */
    public getAnimationProfile( animationProfileId: string ): Promise<AnimationProfile> {

        let promise = new Promise<AnimationProfile>((resolve, reject) => {

            try {
                this._pool.query(
                    "SELECT animationprofile_id, animationprofile_name, animationprofile_idle, animationprofile_run, " +
                    "       animationprofile_walk, animationprofile_die " +
                    "  FROM iodanimationprofile " +
                    " WHERE animationprofile_id = $1", [animationProfileId],
                    (err: any, result: any) => {
                        if (err) {
                            reject(err);
                        } else if ( result.rows.length === 0 ) {
                            resolve(undefined);
                        } else {

                            let row: any = result.rows[0];
                            let id = row.animationprofile_id;
                            let name = row.animationprofile_name;
                            let die = row.animationprofile_die;
                            let idle = row.animationprofile_idle;
                            let run = row.animationprofile_run;
                            let walk = row.animationprofile_walk;
                            let profile: AnimationProfile = new AnimationProfile(id, name, idle, run, walk, die);
                            resolve(profile);

                        }
                    }
                );
            } catch ( exception ) {
                Logger.error("PgAnimationProfileStorage", "getAnimatedCharacter: " + exception);
            }
        });

        return promise;
    }

}

export default PgAnimationProfileStorage;
