"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var objectmodel_1 = require("isleofdarkness-common/src/model/objectmodel");
var basestorage_1 = require("./basestorage");
var ibasestorage_1 = require("./ibasestorage");
var redis = require("redis");
var ObjectModelStorage = /** @class */ (function (_super) {
    __extends(ObjectModelStorage, _super);
    function ObjectModelStorage(redisClient) {
        var _this = _super.call(this, redisClient) || this;
        _this.WORLD_OBJECTMODEL = "objectmodel.";
        _this.WORLD_OBJECTMODEL_ID = _this.WORLD_OBJECTMODEL + "id.";
        _this.WORLD_OBJECTMODEL_LIST = _this.WORLD_OBJECTMODEL + "list";
        if (redisClient) {
            _this._redisClient = redisClient;
        }
        else {
            throw new Error("No Redis Client defined");
        }
        return _this;
    }
    /**
     * Return the type of this storage.
     */
    ObjectModelStorage.prototype.getStorageType = function () {
        return ibasestorage_1.StorageType.ObjectModel;
    };
    /**
     * Lock ObjectModel.
     * @param objectModel
     */
    ObjectModelStorage.prototype.lockObjectModel = function (objectModel) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            function afterWatch(err) {
                if (err) {
                    reject();
                }
                else {
                    resolve();
                }
            }
            _this._redisClient.watch(_this.WORLD_OBJECTMODEL_ID + objectModel.id, afterWatch);
        });
        return promise;
    };
    /**
     * Store a object model in a storage.
     * @param objectModel
     */
    ObjectModelStorage.prototype.addUpdateObjectModel = function (objectModel) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            var self = _this;
            var key = objectModel.id;
            var value = JSON.stringify(objectModel.toJSON());
            var sectionKey = _this._getKeyFromCoordinate(objectModel);
            function afterExec(err) {
                if (err) {
                    reject();
                }
                else {
                    resolve(true);
                }
            }
            function afterWatchingList(err) {
                if (err) {
                    reject();
                }
                else {
                    self._redisClient.
                        multi().
                        sadd(sectionKey, key).
                        hset(self.WORLD_OBJECTMODEL_LIST, key, value).exec(afterExec);
                }
            }
            self._redisClient.watch(self.WORLD_OBJECTMODEL_LIST, afterWatchingList);
        });
        return promise;
    };
    /**
     * Retrieve all object model ids from a section coodinate.
     * @param coord The section coordinate
     */
    ObjectModelStorage.prototype.getObjectModelsIdsInSection = function (coord) {
        var self = this;
        var sectionKey = this._getKeyFromCoordinateFromPoint(coord);
        var promise = new Promise(function (resolve, reject) {
            function afterSMembers(err, listOfObjectModelIds) {
                if (err) {
                    reject();
                }
                else {
                    if (listOfObjectModelIds !== null && listOfObjectModelIds.length !== undefined) {
                        resolve(listOfObjectModelIds);
                    }
                    else {
                        resolve([]);
                    }
                }
            }
            self._redisClient.smembers(sectionKey, afterSMembers);
        });
        return promise;
    };
    /**
     * Retrieve an object model from quick storage.
     * @param objectModelId
     */
    ObjectModelStorage.prototype.getObjectModel = function (objectModelId) {
        var self = this;
        var key = objectModelId;
        var promise = new Promise(function (resolve, reject) {
            function afterHGet(err, value) {
                if (err) {
                    reject();
                }
                else {
                    if (value === null) {
                        resolve(undefined);
                    }
                    else {
                        var json = JSON.parse(value);
                        var result = objectmodel_1.default.fromJSON(json);
                        resolve(result);
                    }
                }
            }
            self._redisClient.hget(self.WORLD_OBJECTMODEL_LIST, key, afterHGet);
        });
        return promise;
    };
    /**
     * Remove an object model from storage.
     * @param objectModelId
     */
    ObjectModelStorage.prototype.removeObjectModel = function (objectModelId) {
        var self = this;
        var key = objectModelId;
        var sectionKey = "";
        var promise = new Promise(function (resolve, reject) {
            function afterExec(err) {
                if (err) {
                    reject();
                }
                else {
                    resolve(true);
                }
            }
            function afterWatchingList(err) {
                if (err) {
                    reject();
                }
                else {
                    self._redisClient.
                        multi().
                        srem(sectionKey, key).
                        hdel(self.WORLD_OBJECTMODEL_LIST, key).
                        exec(afterExec);
                }
            }
            function afterHGet(err, value) {
                if (err) {
                    reject();
                }
                else {
                    if (value === null) {
                        resolve(undefined);
                    }
                    else {
                        var json = JSON.parse(value);
                        var objectModel = objectmodel_1.default.fromJSON(json);
                        sectionKey = self._getKeyFromCoordinate(objectModel);
                        self._redisClient.watch(self.WORLD_OBJECTMODEL_LIST, afterWatchingList);
                    }
                }
            }
            self._redisClient.hget(self.WORLD_OBJECTMODEL_LIST, key, afterHGet);
        });
        return promise;
    };
    /**
     * Get the key representing the section coordinate
     * @param objModel
     */
    ObjectModelStorage.prototype._getKeyFromCoordinate = function (objectModel) {
        var coord = objectModel.getSectionCoordinate();
        return coord.x + "_" + coord.y;
    };
    /**
     * Get the key representing the section coordinate
     * @param objModel
     */
    ObjectModelStorage.prototype._getKeyFromCoordinateFromPoint = function (coord) {
        return coord.x + "_" + coord.y;
    };
    return ObjectModelStorage;
}(basestorage_1.default));
exports.ObjectModelStorage = ObjectModelStorage;
exports.default = ObjectModelStorage;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2Uvb2JqZWN0bW9kZWxzdG9yYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLDJFQUFzRTtBQUd0RSw2Q0FBd0M7QUFDeEMsK0NBQTJDO0FBRTNDLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUU3QjtJQUF3QyxzQ0FBVztJQVEvQyw0QkFBYSxXQUFnQjtRQUE3QixZQUNJLGtCQUFNLFdBQVcsQ0FBQyxTQU1yQjtRQWJlLHVCQUFpQixHQUFXLGNBQWMsQ0FBQztRQUMzQywwQkFBb0IsR0FBVyxLQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQzlELDRCQUFzQixHQUFXLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUM7UUFNN0UsRUFBRSxDQUFDLENBQUUsV0FBWSxDQUFDLENBQUMsQ0FBQztZQUNoQixLQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQztRQUNwQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDL0MsQ0FBQzs7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSSwyQ0FBYyxHQUFyQjtRQUNJLE1BQU0sQ0FBQywwQkFBVyxDQUFDLFdBQVcsQ0FBQztJQUNuQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksNENBQWUsR0FBdEIsVUFBd0IsV0FBd0I7UUFBaEQsaUJBa0JDO1FBaEJHLElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxDQUFPLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFNUMsb0JBQW9CLEdBQVE7Z0JBQ3hCLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ04sTUFBTSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLEVBQUUsQ0FBQztnQkFDZCxDQUFDO1lBQ0wsQ0FBQztZQUVELEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFFLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxXQUFXLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBRSxDQUFDO1FBRXRGLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUVuQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksaURBQW9CLEdBQTNCLFVBQTZCLFdBQXdCO1FBQXJELGlCQWtDQztRQWhDRyxJQUFJLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBVSxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRS9DLElBQUksSUFBSSxHQUF1QixLQUFJLENBQUM7WUFDcEMsSUFBSSxHQUFHLEdBQVcsV0FBVyxDQUFDLEVBQUUsQ0FBQztZQUNqQyxJQUFJLEtBQUssR0FBVyxJQUFJLENBQUMsU0FBUyxDQUFFLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBRSxDQUFDO1lBQzNELElBQUksVUFBVSxHQUFXLEtBQUksQ0FBQyxxQkFBcUIsQ0FBRSxXQUFXLENBQUUsQ0FBQztZQUVuRSxtQkFBb0IsR0FBUTtnQkFDeEIsRUFBRSxDQUFDLENBQUUsR0FBSSxDQUFDLENBQUMsQ0FBQztvQkFDUixNQUFNLEVBQUUsQ0FBQztnQkFDYixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEIsQ0FBQztZQUNMLENBQUM7WUFFRCwyQkFBNEIsR0FBUTtnQkFDaEMsRUFBRSxDQUFDLENBQUUsR0FBSSxDQUFDLENBQUMsQ0FBQztvQkFDUixNQUFNLEVBQUUsQ0FBQztnQkFDYixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLElBQUksQ0FBQyxZQUFZO3dCQUNqQixLQUFLLEVBQUU7d0JBQ1AsSUFBSSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUM7d0JBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDbEUsQ0FBQztZQUNMLENBQUM7WUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBRSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsaUJBQWlCLENBQUUsQ0FBQztRQUU5RSxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFFbkIsQ0FBQztJQUVEOzs7T0FHRztJQUNJLHdEQUEyQixHQUFsQyxVQUFvQyxLQUFZO1FBRTVDLElBQUksSUFBSSxHQUF1QixJQUFJLENBQUM7UUFDcEMsSUFBSSxVQUFVLEdBQVcsSUFBSSxDQUFDLDhCQUE4QixDQUFFLEtBQUssQ0FBRSxDQUFDO1FBRXRFLElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxDQUFnQixVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRXJELHVCQUF1QixHQUFRLEVBQUUsb0JBQW1DO2dCQUNoRSxFQUFFLENBQUMsQ0FBRSxHQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNSLE1BQU0sRUFBRSxDQUFDO2dCQUNiLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osRUFBRSxDQUFDLENBQUUsb0JBQW9CLEtBQUssSUFBSSxJQUFJLG9CQUFvQixDQUFDLE1BQU0sS0FBSyxTQUFVLENBQUMsQ0FBQyxDQUFDO3dCQUMvRSxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQztvQkFDbEMsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDSixPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ2hCLENBQUM7Z0JBQ0wsQ0FBQztZQUNMLENBQUM7WUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFFMUQsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBRW5CLENBQUM7SUFFRDs7O09BR0c7SUFDSSwyQ0FBYyxHQUFyQixVQUF1QixhQUFxQjtRQUV4QyxJQUFJLElBQUksR0FBdUIsSUFBSSxDQUFDO1FBQ3BDLElBQUksR0FBRyxHQUFXLGFBQWEsQ0FBQztRQUVoQyxJQUFJLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBYyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRW5ELG1CQUFtQixHQUFRLEVBQUUsS0FBYTtnQkFDdEMsRUFBRSxDQUFDLENBQUUsR0FBSSxDQUFDLENBQUMsQ0FBQztvQkFDUixNQUFNLEVBQUUsQ0FBQztnQkFDYixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLEVBQUUsQ0FBQyxDQUFFLEtBQUssS0FBSyxJQUFLLENBQUMsQ0FBQyxDQUFDO3dCQUNuQixPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3ZCLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDN0IsSUFBSSxNQUFNLEdBQWdCLHFCQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNyRCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3BCLENBQUM7Z0JBQ0wsQ0FBQztZQUNMLENBQUM7WUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsR0FBRyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBRXhFLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUVuQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksOENBQWlCLEdBQXhCLFVBQTBCLGFBQXFCO1FBRTNDLElBQUksSUFBSSxHQUF1QixJQUFJLENBQUM7UUFDcEMsSUFBSSxHQUFHLEdBQVcsYUFBYSxDQUFDO1FBQ2hDLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUVwQixJQUFJLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBVSxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRS9DLG1CQUFvQixHQUFRO2dCQUN4QixFQUFFLENBQUMsQ0FBRSxHQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNSLE1BQU0sRUFBRSxDQUFDO2dCQUNiLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsQixDQUFDO1lBQ0wsQ0FBQztZQUVELDJCQUE0QixHQUFRO2dCQUNoQyxFQUFFLENBQUMsQ0FBRSxHQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNSLE1BQU0sRUFBRSxDQUFDO2dCQUNiLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osSUFBSSxDQUFDLFlBQVk7d0JBQ2pCLEtBQUssRUFBRTt3QkFDUCxJQUFJLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQzt3QkFDckIsSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxHQUFHLENBQUM7d0JBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDcEIsQ0FBQztZQUNMLENBQUM7WUFFRCxtQkFBbUIsR0FBUSxFQUFFLEtBQWE7Z0JBQ3RDLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixFQUFFLENBQUMsQ0FBRSxLQUFLLEtBQUssSUFBSyxDQUFDLENBQUMsQ0FBQzt3QkFDbkIsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN2QixDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNKLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQzdCLElBQUksV0FBVyxHQUFnQixxQkFBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDMUQsVUFBVSxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBRSxXQUFXLENBQUUsQ0FBQzt3QkFDdkQsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUUsSUFBSSxDQUFDLHNCQUFzQixFQUFFLGlCQUFpQixDQUFFLENBQUM7b0JBQzlFLENBQUM7Z0JBQ0wsQ0FBQztZQUNMLENBQUM7WUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsR0FBRyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBRXhFLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUVuQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ssa0RBQXFCLEdBQTdCLFVBQStCLFdBQXdCO1FBQ25ELElBQUksS0FBSyxHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQy9DLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRDs7O09BR0c7SUFDSywyREFBOEIsR0FBdEMsVUFBd0MsS0FBWTtRQUNoRCxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUwseUJBQUM7QUFBRCxDQWhPQSxBQWdPQyxDQWhPdUMscUJBQVcsR0FnT2xEO0FBaE9ZLGdEQUFrQjtBQWtPL0Isa0JBQWUsa0JBQWtCLENBQUMiLCJmaWxlIjoic3RvcmFnZS9vYmplY3Rtb2RlbHN0b3JhZ2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgT2JqZWN0TW9kZWwgZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbW9kZWwvb2JqZWN0bW9kZWxcIjtcclxuaW1wb3J0IFBvaW50IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21hdGgvUG9pbnRcIjtcclxuaW1wb3J0IElPYmplY3RNb2RlbFN0b3JhZ2UgZnJvbSBcIi4vaW9iamVjdG1vZGVsc3RvcmFnZVwiO1xyXG5pbXBvcnQgQmFzZVN0b3JhZ2UgZnJvbSBcIi4vYmFzZXN0b3JhZ2VcIjtcclxuaW1wb3J0IHtTdG9yYWdlVHlwZX0gZnJvbSBcIi4vaWJhc2VzdG9yYWdlXCI7XHJcblxyXG5sZXQgcmVkaXMgPSByZXF1aXJlKFwicmVkaXNcIik7XHJcblxyXG5leHBvcnQgY2xhc3MgT2JqZWN0TW9kZWxTdG9yYWdlIGV4dGVuZHMgQmFzZVN0b3JhZ2UgaW1wbGVtZW50cyBJT2JqZWN0TW9kZWxTdG9yYWdlIHtcclxuXHJcbiAgICBwdWJsaWMgcmVhZG9ubHkgV09STERfT0JKRUNUTU9ERUw6IHN0cmluZyA9IFwib2JqZWN0bW9kZWwuXCI7XHJcbiAgICBwdWJsaWMgcmVhZG9ubHkgV09STERfT0JKRUNUTU9ERUxfSUQ6IHN0cmluZyA9IHRoaXMuV09STERfT0JKRUNUTU9ERUwgKyBcImlkLlwiO1xyXG4gICAgcHVibGljIHJlYWRvbmx5IFdPUkxEX09CSkVDVE1PREVMX0xJU1Q6IHN0cmluZyA9IHRoaXMuV09STERfT0JKRUNUTU9ERUwgKyBcImxpc3RcIjtcclxuXHJcbiAgICBwcml2YXRlIF9yZWRpc0NsaWVudDogYW55O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCByZWRpc0NsaWVudDogYW55ICkge1xyXG4gICAgICAgIHN1cGVyKHJlZGlzQ2xpZW50KTtcclxuICAgICAgICBpZiAoIHJlZGlzQ2xpZW50ICkge1xyXG4gICAgICAgICAgICB0aGlzLl9yZWRpc0NsaWVudCA9IHJlZGlzQ2xpZW50O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIk5vIFJlZGlzIENsaWVudCBkZWZpbmVkXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybiB0aGUgdHlwZSBvZiB0aGlzIHN0b3JhZ2UuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRTdG9yYWdlVHlwZSgpOiBTdG9yYWdlVHlwZSB7XHJcbiAgICAgICAgcmV0dXJuIFN0b3JhZ2VUeXBlLk9iamVjdE1vZGVsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTG9jayBPYmplY3RNb2RlbC5cclxuICAgICAqIEBwYXJhbSBvYmplY3RNb2RlbFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgbG9ja09iamVjdE1vZGVsKCBvYmplY3RNb2RlbDogT2JqZWN0TW9kZWwgKTogUHJvbWlzZTx2b2lkPiB7XHJcblxyXG4gICAgICAgIGxldCBwcm9taXNlID0gbmV3IFByb21pc2U8dm9pZD4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gYWZ0ZXJXYXRjaChlcnI6IGFueSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdCgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuX3JlZGlzQ2xpZW50LndhdGNoKCB0aGlzLldPUkxEX09CSkVDVE1PREVMX0lEICsgb2JqZWN0TW9kZWwuaWQsIGFmdGVyV2F0Y2ggKTtcclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFN0b3JlIGEgb2JqZWN0IG1vZGVsIGluIGEgc3RvcmFnZS5cclxuICAgICAqIEBwYXJhbSBvYmplY3RNb2RlbFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgYWRkVXBkYXRlT2JqZWN0TW9kZWwoIG9iamVjdE1vZGVsOiBPYmplY3RNb2RlbCApOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuXHJcbiAgICAgICAgbGV0IHByb21pc2UgPSBuZXcgUHJvbWlzZTxib29sZWFuPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICBsZXQgc2VsZjogT2JqZWN0TW9kZWxTdG9yYWdlID0gdGhpcztcclxuICAgICAgICAgICAgbGV0IGtleTogc3RyaW5nID0gb2JqZWN0TW9kZWwuaWQ7XHJcbiAgICAgICAgICAgIGxldCB2YWx1ZTogc3RyaW5nID0gSlNPTi5zdHJpbmdpZnkoIG9iamVjdE1vZGVsLnRvSlNPTigpICk7XHJcbiAgICAgICAgICAgIGxldCBzZWN0aW9uS2V5OiBzdHJpbmcgPSB0aGlzLl9nZXRLZXlGcm9tQ29vcmRpbmF0ZSggb2JqZWN0TW9kZWwgKTtcclxuXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIGFmdGVyRXhlYyggZXJyOiBhbnkgKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIGVyciApIHtcclxuICAgICAgICAgICAgICAgICAgICByZWplY3QoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gYWZ0ZXJXYXRjaGluZ0xpc3QoIGVycjogYW55ICkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBlcnIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYuX3JlZGlzQ2xpZW50LlxyXG4gICAgICAgICAgICAgICAgICAgIG11bHRpKCkuXHJcbiAgICAgICAgICAgICAgICAgICAgc2FkZChzZWN0aW9uS2V5LCBrZXkpLlxyXG4gICAgICAgICAgICAgICAgICAgIGhzZXQoc2VsZi5XT1JMRF9PQkpFQ1RNT0RFTF9MSVNULCBrZXksIHZhbHVlKS5leGVjKGFmdGVyRXhlYyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHNlbGYuX3JlZGlzQ2xpZW50LndhdGNoKCBzZWxmLldPUkxEX09CSkVDVE1PREVMX0xJU1QsIGFmdGVyV2F0Y2hpbmdMaXN0ICk7XHJcblxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gcHJvbWlzZTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXRyaWV2ZSBhbGwgb2JqZWN0IG1vZGVsIGlkcyBmcm9tIGEgc2VjdGlvbiBjb29kaW5hdGUuXHJcbiAgICAgKiBAcGFyYW0gY29vcmQgVGhlIHNlY3Rpb24gY29vcmRpbmF0ZVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0T2JqZWN0TW9kZWxzSWRzSW5TZWN0aW9uKCBjb29yZDogUG9pbnQgKTogUHJvbWlzZTxBcnJheTxzdHJpbmc+PiB7XHJcblxyXG4gICAgICAgIGxldCBzZWxmOiBPYmplY3RNb2RlbFN0b3JhZ2UgPSB0aGlzO1xyXG4gICAgICAgIGxldCBzZWN0aW9uS2V5OiBzdHJpbmcgPSB0aGlzLl9nZXRLZXlGcm9tQ29vcmRpbmF0ZUZyb21Qb2ludCggY29vcmQgKTtcclxuXHJcbiAgICAgICAgbGV0IHByb21pc2UgPSBuZXcgUHJvbWlzZTxBcnJheTxzdHJpbmc+PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBhZnRlclNNZW1iZXJzKGVycjogYW55LCBsaXN0T2ZPYmplY3RNb2RlbElkczogQXJyYXk8c3RyaW5nPikge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBlcnIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICggbGlzdE9mT2JqZWN0TW9kZWxJZHMgIT09IG51bGwgJiYgbGlzdE9mT2JqZWN0TW9kZWxJZHMubGVuZ3RoICE9PSB1bmRlZmluZWQgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUobGlzdE9mT2JqZWN0TW9kZWxJZHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoW10pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgc2VsZi5fcmVkaXNDbGllbnQuc21lbWJlcnMoc2VjdGlvbktleSwgYWZ0ZXJTTWVtYmVycyk7XHJcblxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gcHJvbWlzZTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXRyaWV2ZSBhbiBvYmplY3QgbW9kZWwgZnJvbSBxdWljayBzdG9yYWdlLlxyXG4gICAgICogQHBhcmFtIG9iamVjdE1vZGVsSWRcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldE9iamVjdE1vZGVsKCBvYmplY3RNb2RlbElkOiBzdHJpbmcgKTogUHJvbWlzZTxPYmplY3RNb2RlbD4ge1xyXG5cclxuICAgICAgICBsZXQgc2VsZjogT2JqZWN0TW9kZWxTdG9yYWdlID0gdGhpcztcclxuICAgICAgICBsZXQga2V5OiBzdHJpbmcgPSBvYmplY3RNb2RlbElkO1xyXG5cclxuICAgICAgICBsZXQgcHJvbWlzZSA9IG5ldyBQcm9taXNlPE9iamVjdE1vZGVsPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBhZnRlckhHZXQoZXJyOiBhbnksIHZhbHVlOiBzdHJpbmcpIHtcclxuICAgICAgICAgICAgICAgIGlmICggZXJyICkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdCgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIHZhbHVlID09PSBudWxsICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHVuZGVmaW5lZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGpzb24gPSBKU09OLnBhcnNlKHZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHJlc3VsdDogT2JqZWN0TW9kZWwgPSBPYmplY3RNb2RlbC5mcm9tSlNPTihqc29uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShyZXN1bHQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgc2VsZi5fcmVkaXNDbGllbnQuaGdldChzZWxmLldPUkxEX09CSkVDVE1PREVMX0xJU1QsIGtleSwgYWZ0ZXJIR2V0KTtcclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbW92ZSBhbiBvYmplY3QgbW9kZWwgZnJvbSBzdG9yYWdlLlxyXG4gICAgICogQHBhcmFtIG9iamVjdE1vZGVsSWRcclxuICAgICAqL1xyXG4gICAgcHVibGljIHJlbW92ZU9iamVjdE1vZGVsKCBvYmplY3RNb2RlbElkOiBzdHJpbmcgKTogUHJvbWlzZTxib29sZWFuPiB7XHJcblxyXG4gICAgICAgIGxldCBzZWxmOiBPYmplY3RNb2RlbFN0b3JhZ2UgPSB0aGlzO1xyXG4gICAgICAgIGxldCBrZXk6IHN0cmluZyA9IG9iamVjdE1vZGVsSWQ7XHJcbiAgICAgICAgbGV0IHNlY3Rpb25LZXkgPSBcIlwiO1xyXG5cclxuICAgICAgICBsZXQgcHJvbWlzZSA9IG5ldyBQcm9taXNlPGJvb2xlYW4+KChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIGFmdGVyRXhlYyggZXJyOiBhbnkgKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIGVyciApIHtcclxuICAgICAgICAgICAgICAgICAgICByZWplY3QoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gYWZ0ZXJXYXRjaGluZ0xpc3QoIGVycjogYW55ICkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBlcnIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYuX3JlZGlzQ2xpZW50LlxyXG4gICAgICAgICAgICAgICAgICAgIG11bHRpKCkuXHJcbiAgICAgICAgICAgICAgICAgICAgc3JlbShzZWN0aW9uS2V5LCBrZXkpLlxyXG4gICAgICAgICAgICAgICAgICAgIGhkZWwoc2VsZi5XT1JMRF9PQkpFQ1RNT0RFTF9MSVNULCBrZXkpLlxyXG4gICAgICAgICAgICAgICAgICAgIGV4ZWMoYWZ0ZXJFeGVjKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gYWZ0ZXJIR2V0KGVycjogYW55LCB2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIGVyciApIHtcclxuICAgICAgICAgICAgICAgICAgICByZWplY3QoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCB2YWx1ZSA9PT0gbnVsbCApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh1bmRlZmluZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBqc29uID0gSlNPTi5wYXJzZSh2YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBvYmplY3RNb2RlbDogT2JqZWN0TW9kZWwgPSBPYmplY3RNb2RlbC5mcm9tSlNPTihqc29uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2VjdGlvbktleSA9IHNlbGYuX2dldEtleUZyb21Db29yZGluYXRlKCBvYmplY3RNb2RlbCApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxmLl9yZWRpc0NsaWVudC53YXRjaCggc2VsZi5XT1JMRF9PQkpFQ1RNT0RFTF9MSVNULCBhZnRlcldhdGNoaW5nTGlzdCApO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgc2VsZi5fcmVkaXNDbGllbnQuaGdldChzZWxmLldPUkxEX09CSkVDVE1PREVMX0xJU1QsIGtleSwgYWZ0ZXJIR2V0KTtcclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUga2V5IHJlcHJlc2VudGluZyB0aGUgc2VjdGlvbiBjb29yZGluYXRlXHJcbiAgICAgKiBAcGFyYW0gb2JqTW9kZWxcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBfZ2V0S2V5RnJvbUNvb3JkaW5hdGUoIG9iamVjdE1vZGVsOiBPYmplY3RNb2RlbCApIHtcclxuICAgICAgICBsZXQgY29vcmQgPSBvYmplY3RNb2RlbC5nZXRTZWN0aW9uQ29vcmRpbmF0ZSgpO1xyXG4gICAgICAgIHJldHVybiBjb29yZC54ICsgXCJfXCIgKyBjb29yZC55O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBrZXkgcmVwcmVzZW50aW5nIHRoZSBzZWN0aW9uIGNvb3JkaW5hdGVcclxuICAgICAqIEBwYXJhbSBvYmpNb2RlbFxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIF9nZXRLZXlGcm9tQ29vcmRpbmF0ZUZyb21Qb2ludCggY29vcmQ6IFBvaW50ICkge1xyXG4gICAgICAgIHJldHVybiBjb29yZC54ICsgXCJfXCIgKyBjb29yZC55O1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgT2JqZWN0TW9kZWxTdG9yYWdlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
