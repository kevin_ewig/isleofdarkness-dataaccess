import ObjectModel from "isleofdarkness-common/src/model/objectmodel";
import IBaseStorage from "./ibasestorage";
import { StorageType } from "./ibasestorage";

export interface IPgObjectModelStorage extends IBaseStorage {

    /**
     * Return the type of this storage.
     */
    getStorageType(): StorageType;

    /**
     * Store a object model in a storage.
     * @param objectModel
     */
    addUpdateObjectModel( objectModel: ObjectModel ): Promise<boolean>;

    /**
     * Retrieve a object model from storage. Return undefined if none is found.
     * @param objectModelId
     */
    getObjectModel( objectModelId: string ): Promise<ObjectModel|null>;

    /**
     * Delete an object model.
     * @param objectModelId
     */
    deleteObjectModel( objectModelId: string ): Promise<boolean>;

}

export default IPgObjectModelStorage;
