import AnimationProfile from "isleofdarkness-common/src/model/animationprofile";
import IBaseStorage from "./ibasestorage";

interface IPgAnimationProfileStorage extends IBaseStorage {

    /**
     * Get an animation profile.
     */
    getAnimationProfile( animationProfileId: string ): Promise<AnimationProfile>;

}

export default IPgAnimationProfileStorage;
