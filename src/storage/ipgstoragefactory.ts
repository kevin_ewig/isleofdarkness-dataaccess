import BaseStorage from "./basestorage";
import IBaseStorage, {StorageType} from "./ibasestorage";

interface IPgStorageFactory {

    /**
     * Check if storage factory is connected.
     */
    isConnected(): Promise<boolean>;

    /**
     * Get storage type
     */
    getStorage<T extends IBaseStorage>( storageType: StorageType ): T;

}

export default IPgStorageFactory;
