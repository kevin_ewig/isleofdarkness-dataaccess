"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var objectmodel_1 = require("isleofdarkness-common/src/model/objectmodel");
var ibasestorage_1 = require("./ibasestorage");
var constants_1 = require("isleofdarkness-common/src/math/constants");
var logger_1 = require("isleofdarkness-common/src/logger");
var basepgstorage_1 = require("./basepgstorage");
var PgObjectModelStorage = /** @class */ (function (_super) {
    __extends(PgObjectModelStorage, _super);
    function PgObjectModelStorage(pool) {
        return _super.call(this, pool) || this;
    }
    /**
     * Return the type of this storage.
     */
    PgObjectModelStorage.prototype.getStorageType = function () {
        return ibasestorage_1.StorageType.ObjectModel;
    };
    /**
     * Store a object model in a storage.
     * @param objectModel
     */
    PgObjectModelStorage.prototype.addUpdateObjectModel = function (objectModel) {
        return __awaiter(this, void 0, void 0, function () {
            var result, pool, client, objectModelIdResult, objectModelId, objectModelId, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = false;
                        pool = this.getPool();
                        return [4 /*yield*/, pool.connect()];
                    case 1:
                        client = _a.sent();
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 10, 12, 13]);
                        return [4 /*yield*/, client.query("begin")];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, client.query("SELECT objectmodel_id FROM iodobjectmodel WHERE objectmodel_id = $1", [objectModel.id])];
                    case 4:
                        objectModelIdResult = _a.sent();
                        if (!(objectModelIdResult.rowCount === 0)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this._insertObjectModel(client, objectModel)];
                    case 5:
                        objectModelId = _a.sent();
                        result = true;
                        return [3 /*break*/, 8];
                    case 6: return [4 /*yield*/, this._updateObjectModel(client, objectModel)];
                    case 7:
                        objectModelId = _a.sent();
                        result = true;
                        _a.label = 8;
                    case 8: return [4 /*yield*/, client.query("commit")];
                    case 9:
                        _a.sent();
                        return [3 /*break*/, 13];
                    case 10:
                        error_1 = _a.sent();
                        result = false;
                        logger_1.default.error("PgObjectModelStorage", "Error: " + error_1);
                        return [4 /*yield*/, client.query("rollback")];
                    case 11:
                        _a.sent();
                        return [3 /*break*/, 13];
                    case 12:
                        client.release();
                        return [7 /*endfinally*/];
                    case 13: return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * Delete an object model.
     * @param objectModelId
     */
    PgObjectModelStorage.prototype.deleteObjectModel = function (objectModelId) {
        return __awaiter(this, void 0, void 0, function () {
            var pool, exception_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        pool = this.getPool();
                        return [4 /*yield*/, pool.query(" DELETE FROM iodobjectmodel AS om " +
                                "  WHERE om.objectmodel_id = $1 ", [objectModelId])];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        exception_1 = _a.sent();
                        logger_1.default.error("PgObjectModelStorage", "Error: " + exception_1);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/, true];
                }
            });
        });
    };
    /**
     * Retrieve a object model from storage. Return undefined if none is found.
     * @param objectModelId
     */
    PgObjectModelStorage.prototype.getObjectModel = function (objectModelId) {
        return __awaiter(this, void 0, void 0, function () {
            var pool, resultSet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        pool = this.getPool();
                        return [4 /*yield*/, pool.query("SELECT objectmodel_id, shape_id, objectmodel_type, objectmodel_worldname, objectmodel_modelname, " +
                                "       objectmodel_displayname, objectmodel_modeltexture, owner_user_id " +
                                "  FROM iodobjectmodel WHERE objectmodel_id = $1", [objectModelId])];
                    case 1:
                        resultSet = _a.sent();
                        return [4 /*yield*/, this._getObjectModelFromResult(resultSet)];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    PgObjectModelStorage.prototype._getObjectModelFromResult = function (resultSet) {
        return __awaiter(this, void 0, void 0, function () {
            var resultObj, resultRow, shapeId, shape;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        resultObj = null;
                        resultRow = resultSet.rows[0];
                        if (resultSet.rowCount === 0) {
                            return [2 /*return*/, null];
                        }
                        shapeId = resultRow.shape_id;
                        return [4 /*yield*/, this._getShape(shapeId)];
                    case 1:
                        shape = _a.sent();
                        if (shape != null) {
                            resultObj =
                                new objectmodel_1.default(resultRow.objectmodel_id, shape, resultRow.objectmodel_modelname);
                            resultObj.objectType = resultRow.objectmodel_type;
                            resultObj.modelTexture = resultRow.objectmodel_modeltexture;
                            resultObj.worldName = resultRow.objectmodel_worldname;
                            resultObj.modelName = resultRow.objectmodel_modelname;
                            resultObj.displayName = resultRow.objectmodel_displayname;
                            resultObj.ownerUserId = resultRow.owner_user_id;
                        }
                        else {
                            return [2 /*return*/, null];
                        }
                        return [2 /*return*/, resultObj];
                }
            });
        });
    };
    PgObjectModelStorage.prototype._insertObjectModel = function (client, objectModel) {
        return __awaiter(this, void 0, void 0, function () {
            var objectModelId, shapeId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        objectModelId = objectModel.id;
                        shapeId = "";
                        if (!(objectModel.shape.type === constants_1.Constants.CIRCLE)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this._insertCircleShape(client, objectModel.shape)];
                    case 1:
                        shapeId = _a.sent();
                        return [3 /*break*/, 4];
                    case 2:
                        if (!(objectModel.shape.type === constants_1.Constants.RECTANGLE)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this._insertRectangleShape(client, objectModel.shape)];
                    case 3:
                        shapeId = _a.sent();
                        _a.label = 4;
                    case 4: return [4 /*yield*/, client.query("INSERT INTO iodobjectmodel (objectmodel_id, shape_id, objectmodel_type, " +
                            " objectmodel_worldname, objectmodel_modelname, objectmodel_displayname, " +
                            " objectmodel_modeltexture, owner_user_id) " +
                            "VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ", [objectModel.id,
                            shapeId,
                            objectModel.objectType,
                            objectModel.worldName,
                            objectModel.modelName,
                            objectModel.displayName,
                            objectModel.modelTexture,
                            objectModel.ownerUserId])];
                    case 5:
                        _a.sent();
                        return [2 /*return*/, objectModelId];
                }
            });
        });
    };
    PgObjectModelStorage.prototype._updateObjectModel = function (client, objectModel) {
        return __awaiter(this, void 0, void 0, function () {
            var resultSet, shapeId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, client.query("SELECT shape_id FROM iodobjectmodel WHERE objectmodel_id = $1", [objectModel.id])];
                    case 1:
                        resultSet = _a.sent();
                        shapeId = resultSet.rows[0].shape_id;
                        if (!(objectModel.shape.type === constants_1.Constants.CIRCLE)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this._updateCircleShape(client, shapeId, objectModel.shape)];
                    case 2:
                        shapeId = _a.sent();
                        return [3 /*break*/, 5];
                    case 3:
                        if (!(objectModel.shape.type === constants_1.Constants.RECTANGLE)) return [3 /*break*/, 5];
                        return [4 /*yield*/, this._updateRectangleShape(client, shapeId, objectModel.shape)];
                    case 4:
                        shapeId = _a.sent();
                        _a.label = 5;
                    case 5: return [4 /*yield*/, client.query("UPDATE iodobjectmodel " +
                            "SET objectmodel_type = $1, objectmodel_worldname = $2, objectmodel_modelname = $3, " +
                            "objectmodel_displayname = $4, objectmodel_modeltexture = $5, owner_user_id = $6 " +
                            "WHERE objectmodel_id = $7 ", [objectModel.objectType,
                            objectModel.worldName,
                            objectModel.modelName,
                            objectModel.displayName,
                            objectModel.modelTexture,
                            objectModel.ownerUserId,
                            objectModel.id])];
                    case 6:
                        _a.sent();
                        return [2 /*return*/, objectModel.id];
                }
            });
        });
    };
    return PgObjectModelStorage;
}(basepgstorage_1.default));
exports.PgObjectModelStorage = PgObjectModelStorage;
exports.default = PgObjectModelStorage;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvcGdvYmplY3Rtb2RlbHN0b3JhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwyRUFBc0U7QUFNdEUsK0NBQTZDO0FBQzdDLHNFQUFxRTtBQUVyRSwyREFBc0Q7QUFDdEQsaURBQTRDO0FBRTVDO0lBQTBDLHdDQUFhO0lBRW5ELDhCQUFhLElBQVM7ZUFDbEIsa0JBQU8sSUFBSSxDQUFFO0lBQ2pCLENBQUM7SUFFRDs7T0FFRztJQUNJLDZDQUFjLEdBQXJCO1FBQ0ksTUFBTSxDQUFDLDBCQUFXLENBQUMsV0FBVyxDQUFDO0lBQ25DLENBQUM7SUFFRDs7O09BR0c7SUFDVSxtREFBb0IsR0FBakMsVUFBbUMsV0FBd0I7Ozs7Ozt3QkFFbkQsTUFBTSxHQUFHLEtBQUssQ0FBQzt3QkFDZixJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUNiLHFCQUFNLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBQTs7d0JBQTdCLE1BQU0sR0FBRyxTQUFvQjs7Ozt3QkFJN0IscUJBQU0sTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsRUFBQTs7d0JBQTNCLFNBQTJCLENBQUM7d0JBR0cscUJBQU0sTUFBTSxDQUFDLEtBQUssQ0FDN0MscUVBQXFFLEVBQ3JFLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUNuQixFQUFBOzt3QkFIRyxtQkFBbUIsR0FBUSxTQUc5Qjs2QkFDSSxDQUFBLG1CQUFtQixDQUFDLFFBQVEsS0FBSyxDQUFDLENBQUEsRUFBbEMsd0JBQWtDO3dCQUNQLHFCQUFNLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsV0FBVyxDQUFDLEVBQUE7O3dCQUExRSxhQUFhLEdBQVcsU0FBa0Q7d0JBQzlFLE1BQU0sR0FBRyxJQUFJLENBQUM7OzRCQUVjLHFCQUFNLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsV0FBVyxDQUFDLEVBQUE7O3dCQUExRSxhQUFhLEdBQVcsU0FBa0Q7d0JBQzlFLE1BQU0sR0FBRyxJQUFJLENBQUM7OzRCQUdsQixxQkFBTSxNQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxFQUFBOzt3QkFBNUIsU0FBNEIsQ0FBQzs7Ozt3QkFHN0IsTUFBTSxHQUFHLEtBQUssQ0FBQzt3QkFDZixnQkFBTSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsRUFBRSxTQUFTLEdBQUcsT0FBSyxDQUFDLENBQUM7d0JBQ3hELHFCQUFNLE1BQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEVBQUE7O3dCQUE5QixTQUE4QixDQUFDOzs7d0JBRS9CLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQzs7NkJBR3JCLHNCQUFPLE1BQU0sRUFBQzs7OztLQUVqQjtJQUVEOzs7T0FHRztJQUNVLGdEQUFpQixHQUE5QixVQUFnQyxhQUFxQjs7Ozs7Ozt3QkFFekMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDMUIscUJBQU0sSUFBSSxDQUFDLEtBQUssQ0FDWixvQ0FBb0M7Z0NBQ3BDLGlDQUFpQyxFQUNqQyxDQUFDLGFBQWEsQ0FBQyxDQUNsQixFQUFBOzt3QkFKRCxTQUlDLENBQUM7Ozs7d0JBRUYsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsc0JBQXNCLEVBQUUsU0FBUyxHQUFHLFdBQVMsQ0FBQyxDQUFDO3dCQUM1RCxzQkFBTyxLQUFLLEVBQUM7NEJBRWpCLHNCQUFPLElBQUksRUFBQzs7OztLQUNmO0lBRUQ7OztPQUdHO0lBQ1UsNkNBQWMsR0FBM0IsVUFBNkIsYUFBcUI7Ozs7Ozt3QkFFMUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDTCxxQkFBTSxJQUFJLENBQUMsS0FBSyxDQUNqQyxtR0FBbUc7Z0NBQ25HLDBFQUEwRTtnQ0FDMUUsaURBQWlELEVBQUUsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFBOzt3QkFIbkUsU0FBUyxHQUFRLFNBR2tEO3dCQUVoRSxxQkFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsU0FBUyxDQUFDLEVBQUE7NEJBQXRELHNCQUFPLFNBQStDLEVBQUM7Ozs7S0FFMUQ7SUFFYSx3REFBeUIsR0FBdkMsVUFBeUMsU0FBYzs7Ozs7O3dCQUUvQyxTQUFTLEdBQXFCLElBQUksQ0FBQzt3QkFFbkMsU0FBUyxHQUFRLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3ZDLEVBQUUsQ0FBQyxDQUFFLFNBQVMsQ0FBQyxRQUFRLEtBQUssQ0FBRSxDQUFDLENBQUMsQ0FBQzs0QkFDN0IsTUFBTSxnQkFBQyxJQUFJLEVBQUM7d0JBQ2hCLENBQUM7d0JBRUcsT0FBTyxHQUFXLFNBQVMsQ0FBQyxRQUFRLENBQUM7d0JBQ2pCLHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUE7O3dCQUFqRCxLQUFLLEdBQWUsU0FBNkI7d0JBQ3JELEVBQUUsQ0FBQyxDQUFFLEtBQUssSUFBSSxJQUFLLENBQUMsQ0FBQyxDQUFDOzRCQUNsQixTQUFTO2dDQUNMLElBQUkscUJBQVcsQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLEtBQWMsRUFBRSxTQUFTLENBQUMscUJBQXFCLENBQUMsQ0FBQzs0QkFDL0YsU0FBUyxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUMsZ0JBQWdCLENBQUM7NEJBQ2xELFNBQVMsQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFDLHdCQUF3QixDQUFDOzRCQUM1RCxTQUFTLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQzs0QkFDdEQsU0FBUyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMscUJBQXFCLENBQUM7NEJBQ3RELFNBQVMsQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDLHVCQUF1QixDQUFDOzRCQUMxRCxTQUFTLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQyxhQUFhLENBQUM7d0JBQ3BELENBQUM7d0JBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ0osTUFBTSxnQkFBQyxJQUFJLEVBQUM7d0JBQ2hCLENBQUM7d0JBRUQsc0JBQU8sU0FBUyxFQUFDOzs7O0tBRXBCO0lBRWEsaURBQWtCLEdBQWhDLFVBQWtDLE1BQVcsRUFBRSxXQUF3Qjs7Ozs7O3dCQUMvRCxhQUFhLEdBQVcsV0FBVyxDQUFDLEVBQUUsQ0FBQzt3QkFDdkMsT0FBTyxHQUFHLEVBQUUsQ0FBQzs2QkFDWixDQUFBLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLHFCQUFTLENBQUMsTUFBTSxDQUFBLEVBQTNDLHdCQUEyQzt3QkFDbEMscUJBQU0sSUFBSSxDQUFDLGtCQUFrQixDQUFFLE1BQU0sRUFBRSxXQUFXLENBQUMsS0FBZSxDQUFFLEVBQUE7O3dCQUE5RSxPQUFPLEdBQUcsU0FBb0UsQ0FBQzs7OzZCQUN2RSxDQUFBLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLHFCQUFTLENBQUMsU0FBUyxDQUFBLEVBQTlDLHdCQUE4Qzt3QkFDNUMscUJBQU0sSUFBSSxDQUFDLHFCQUFxQixDQUFFLE1BQU0sRUFBRSxXQUFXLENBQUMsS0FBa0IsQ0FBRSxFQUFBOzt3QkFBcEYsT0FBTyxHQUFHLFNBQTBFLENBQUM7OzRCQUV6RixxQkFBTSxNQUFNLENBQUMsS0FBSyxDQUNkLDBFQUEwRTs0QkFDMUUsMEVBQTBFOzRCQUMxRSw0Q0FBNEM7NEJBQzVDLDBDQUEwQyxFQUMxQyxDQUFDLFdBQVcsQ0FBQyxFQUFFOzRCQUNkLE9BQU87NEJBQ1AsV0FBVyxDQUFDLFVBQVU7NEJBQ3RCLFdBQVcsQ0FBQyxTQUFTOzRCQUNyQixXQUFXLENBQUMsU0FBUzs0QkFDckIsV0FBVyxDQUFDLFdBQVc7NEJBQ3ZCLFdBQVcsQ0FBQyxZQUFZOzRCQUN4QixXQUFXLENBQUMsV0FBVyxDQUFDLENBQzVCLEVBQUE7O3dCQWJELFNBYUMsQ0FBQzt3QkFDRixzQkFBTyxhQUFhLEVBQUM7Ozs7S0FDeEI7SUFFYSxpREFBa0IsR0FBaEMsVUFBa0MsTUFBVyxFQUFFLFdBQXdCOzs7Ozs0QkFFL0QscUJBQU0sTUFBTSxDQUFDLEtBQUssQ0FBQywrREFBK0QsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFBOzt3QkFEckcsU0FBUyxHQUNULFNBQXFHO3dCQUNyRyxPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7NkJBQ3BDLENBQUEsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUsscUJBQVMsQ0FBQyxNQUFNLENBQUEsRUFBM0Msd0JBQTJDO3dCQUNsQyxxQkFBTSxJQUFJLENBQUMsa0JBQWtCLENBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxXQUFXLENBQUMsS0FBZSxDQUFFLEVBQUE7O3dCQUF2RixPQUFPLEdBQUcsU0FBNkUsQ0FBQzs7OzZCQUNoRixDQUFBLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLHFCQUFTLENBQUMsU0FBUyxDQUFBLEVBQTlDLHdCQUE4Qzt3QkFDNUMscUJBQU0sSUFBSSxDQUFDLHFCQUFxQixDQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsV0FBVyxDQUFDLEtBQWtCLENBQUUsRUFBQTs7d0JBQTdGLE9BQU8sR0FBRyxTQUFtRixDQUFDOzs0QkFFbEcscUJBQU0sTUFBTSxDQUFDLEtBQUssQ0FDZCx3QkFBd0I7NEJBQ3hCLHFGQUFxRjs0QkFDckYsa0ZBQWtGOzRCQUNsRiw0QkFBNEIsRUFDNUIsQ0FBQyxXQUFXLENBQUMsVUFBVTs0QkFDdEIsV0FBVyxDQUFDLFNBQVM7NEJBQ3JCLFdBQVcsQ0FBQyxTQUFTOzRCQUNyQixXQUFXLENBQUMsV0FBVzs0QkFDdkIsV0FBVyxDQUFDLFlBQVk7NEJBQ3hCLFdBQVcsQ0FBQyxXQUFXOzRCQUN2QixXQUFXLENBQUMsRUFBRSxDQUFDLENBQ25CLEVBQUE7O3dCQVpELFNBWUMsQ0FBQzt3QkFDRixzQkFBTyxXQUFXLENBQUMsRUFBRSxFQUFDOzs7O0tBQ3pCO0lBRUwsMkJBQUM7QUFBRCxDQXZLQSxBQXVLQyxDQXZLeUMsdUJBQWEsR0F1S3REO0FBdktZLG9EQUFvQjtBQXlLakMsa0JBQWUsb0JBQW9CLENBQUMiLCJmaWxlIjoic3RvcmFnZS9wZ29iamVjdG1vZGVsc3RvcmFnZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBPYmplY3RNb2RlbCBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tb2RlbC9vYmplY3Rtb2RlbFwiO1xyXG5pbXBvcnQgSVBnT2JqZWN0TW9kZWxTdG9yYWdlIGZyb20gXCIuL2lwZ29iamVjdG1vZGVsc3RvcmFnZVwiO1xyXG5pbXBvcnQgeyBQb2ludCB9IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21hdGgvcG9pbnRcIjtcclxuaW1wb3J0IHsgU2hhcGUgfSBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tYXRoL3NoYXBlXCI7XHJcbmltcG9ydCB7IENpcmNsZSB9IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21hdGgvY2lyY2xlXCI7XHJcbmltcG9ydCB7IFJlY3RhbmdsZSB9IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21hdGgvcmVjdGFuZ2xlXCI7XHJcbmltcG9ydCB7IFN0b3JhZ2VUeXBlIH0gZnJvbSBcIi4vaWJhc2VzdG9yYWdlXCI7XHJcbmltcG9ydCB7IENvbnN0YW50cyB9IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21hdGgvY29uc3RhbnRzXCI7XHJcbmltcG9ydCBVdWlkIGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL3V1aWRcIjtcclxuaW1wb3J0IExvZ2dlciBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9sb2dnZXJcIjtcclxuaW1wb3J0IEJhc2VQZ1N0b3JhZ2UgZnJvbSBcIi4vYmFzZXBnc3RvcmFnZVwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIFBnT2JqZWN0TW9kZWxTdG9yYWdlIGV4dGVuZHMgQmFzZVBnU3RvcmFnZSBpbXBsZW1lbnRzIElQZ09iamVjdE1vZGVsU3RvcmFnZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoIHBvb2w6IGFueSApIHtcclxuICAgICAgICBzdXBlciggcG9vbCApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJuIHRoZSB0eXBlIG9mIHRoaXMgc3RvcmFnZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFN0b3JhZ2VUeXBlKCk6IFN0b3JhZ2VUeXBlIHtcclxuICAgICAgICByZXR1cm4gU3RvcmFnZVR5cGUuT2JqZWN0TW9kZWw7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTdG9yZSBhIG9iamVjdCBtb2RlbCBpbiBhIHN0b3JhZ2UuXHJcbiAgICAgKiBAcGFyYW0gb2JqZWN0TW9kZWxcclxuICAgICAqL1xyXG4gICAgcHVibGljIGFzeW5jIGFkZFVwZGF0ZU9iamVjdE1vZGVsKCBvYmplY3RNb2RlbDogT2JqZWN0TW9kZWwgKTogUHJvbWlzZTxib29sZWFuPiB7XHJcblxyXG4gICAgICAgIGxldCByZXN1bHQgPSBmYWxzZTtcclxuICAgICAgICBsZXQgcG9vbCA9IHRoaXMuZ2V0UG9vbCgpO1xyXG4gICAgICAgIGxldCBjbGllbnQgPSBhd2FpdCBwb29sLmNvbm5lY3QoKTtcclxuXHJcbiAgICAgICAgdHJ5IHtcclxuXHJcbiAgICAgICAgICAgIGF3YWl0IGNsaWVudC5xdWVyeShcImJlZ2luXCIpO1xyXG5cclxuICAgICAgICAgICAgLy8gQ2hlY2sgaWYgb2JqZWN0IG1vZGVsIGFscmVhZHkgZXhpc3RzLiBJZiBpdCBkb2VzIG5vdCB0aGVuIGluc2VydC4gT3RoZXJ3aXNlLCB1cGRhdGUuXHJcbiAgICAgICAgICAgIGxldCBvYmplY3RNb2RlbElkUmVzdWx0OiBhbnkgPSBhd2FpdCBjbGllbnQucXVlcnkoXHJcbiAgICAgICAgICAgICAgICBcIlNFTEVDVCBvYmplY3Rtb2RlbF9pZCBGUk9NIGlvZG9iamVjdG1vZGVsIFdIRVJFIG9iamVjdG1vZGVsX2lkID0gJDFcIixcclxuICAgICAgICAgICAgICAgIFtvYmplY3RNb2RlbC5pZF1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgaWYgKCBvYmplY3RNb2RlbElkUmVzdWx0LnJvd0NvdW50ID09PSAwICkge1xyXG4gICAgICAgICAgICAgICAgbGV0IG9iamVjdE1vZGVsSWQ6IHN0cmluZyA9IGF3YWl0IHRoaXMuX2luc2VydE9iamVjdE1vZGVsKGNsaWVudCwgb2JqZWN0TW9kZWwpO1xyXG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gdHJ1ZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGxldCBvYmplY3RNb2RlbElkOiBzdHJpbmcgPSBhd2FpdCB0aGlzLl91cGRhdGVPYmplY3RNb2RlbChjbGllbnQsIG9iamVjdE1vZGVsKTtcclxuICAgICAgICAgICAgICAgIHJlc3VsdCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGF3YWl0IGNsaWVudC5xdWVyeShcImNvbW1pdFwiKTtcclxuXHJcbiAgICAgICAgfSBjYXRjaCAoIGVycm9yICkge1xyXG4gICAgICAgICAgICByZXN1bHQgPSBmYWxzZTtcclxuICAgICAgICAgICAgTG9nZ2VyLmVycm9yKFwiUGdPYmplY3RNb2RlbFN0b3JhZ2VcIiwgXCJFcnJvcjogXCIgKyBlcnJvcik7XHJcbiAgICAgICAgICAgIGF3YWl0IGNsaWVudC5xdWVyeShcInJvbGxiYWNrXCIpO1xyXG4gICAgICAgIH0gZmluYWxseSB7XHJcbiAgICAgICAgICAgIGNsaWVudC5yZWxlYXNlKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERlbGV0ZSBhbiBvYmplY3QgbW9kZWwuXHJcbiAgICAgKiBAcGFyYW0gb2JqZWN0TW9kZWxJZFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgYXN5bmMgZGVsZXRlT2JqZWN0TW9kZWwoIG9iamVjdE1vZGVsSWQ6IHN0cmluZyApOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBsZXQgcG9vbCA9IHRoaXMuZ2V0UG9vbCgpO1xyXG4gICAgICAgICAgICBhd2FpdCBwb29sLnF1ZXJ5KFxyXG4gICAgICAgICAgICAgICAgXCIgREVMRVRFIEZST00gaW9kb2JqZWN0bW9kZWwgQVMgb20gXCIgK1xyXG4gICAgICAgICAgICAgICAgXCIgIFdIRVJFIG9tLm9iamVjdG1vZGVsX2lkID0gJDEgXCIsXHJcbiAgICAgICAgICAgICAgICBbb2JqZWN0TW9kZWxJZF1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGNhdGNoICggZXhjZXB0aW9uICkge1xyXG4gICAgICAgICAgICBMb2dnZXIuZXJyb3IoXCJQZ09iamVjdE1vZGVsU3RvcmFnZVwiLCBcIkVycm9yOiBcIiArIGV4Y2VwdGlvbik7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXRyaWV2ZSBhIG9iamVjdCBtb2RlbCBmcm9tIHN0b3JhZ2UuIFJldHVybiB1bmRlZmluZWQgaWYgbm9uZSBpcyBmb3VuZC5cclxuICAgICAqIEBwYXJhbSBvYmplY3RNb2RlbElkXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBhc3luYyBnZXRPYmplY3RNb2RlbCggb2JqZWN0TW9kZWxJZDogc3RyaW5nICk6IFByb21pc2U8T2JqZWN0TW9kZWx8bnVsbD4ge1xyXG5cclxuICAgICAgICBsZXQgcG9vbCA9IHRoaXMuZ2V0UG9vbCgpO1xyXG4gICAgICAgIGxldCByZXN1bHRTZXQ6IGFueSA9IGF3YWl0IHBvb2wucXVlcnkoXHJcbiAgICAgICAgICAgIFwiU0VMRUNUIG9iamVjdG1vZGVsX2lkLCBzaGFwZV9pZCwgb2JqZWN0bW9kZWxfdHlwZSwgb2JqZWN0bW9kZWxfd29ybGRuYW1lLCBvYmplY3Rtb2RlbF9tb2RlbG5hbWUsIFwiICtcclxuICAgICAgICAgICAgXCIgICAgICAgb2JqZWN0bW9kZWxfZGlzcGxheW5hbWUsIG9iamVjdG1vZGVsX21vZGVsdGV4dHVyZSwgb3duZXJfdXNlcl9pZCBcIiArXHJcbiAgICAgICAgICAgIFwiICBGUk9NIGlvZG9iamVjdG1vZGVsIFdIRVJFIG9iamVjdG1vZGVsX2lkID0gJDFcIiwgW29iamVjdE1vZGVsSWRdKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGF3YWl0IHRoaXMuX2dldE9iamVjdE1vZGVsRnJvbVJlc3VsdChyZXN1bHRTZXQpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGFzeW5jIF9nZXRPYmplY3RNb2RlbEZyb21SZXN1bHQoIHJlc3VsdFNldDogYW55ICk6IFByb21pc2U8T2JqZWN0TW9kZWx8bnVsbD4ge1xyXG5cclxuICAgICAgICBsZXQgcmVzdWx0T2JqOiBPYmplY3RNb2RlbHxudWxsID0gbnVsbDtcclxuXHJcbiAgICAgICAgbGV0IHJlc3VsdFJvdzogYW55ID0gcmVzdWx0U2V0LnJvd3NbMF07XHJcbiAgICAgICAgaWYgKCByZXN1bHRTZXQucm93Q291bnQgPT09IDAgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHNoYXBlSWQ6IHN0cmluZyA9IHJlc3VsdFJvdy5zaGFwZV9pZDtcclxuICAgICAgICBsZXQgc2hhcGU6IFNoYXBlfG51bGwgPSBhd2FpdCB0aGlzLl9nZXRTaGFwZShzaGFwZUlkKTtcclxuICAgICAgICBpZiAoIHNoYXBlICE9IG51bGwgKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdE9iaiA9XHJcbiAgICAgICAgICAgICAgICBuZXcgT2JqZWN0TW9kZWwocmVzdWx0Um93Lm9iamVjdG1vZGVsX2lkLCBzaGFwZSBhcyBTaGFwZSwgcmVzdWx0Um93Lm9iamVjdG1vZGVsX21vZGVsbmFtZSk7XHJcbiAgICAgICAgICAgIHJlc3VsdE9iai5vYmplY3RUeXBlID0gcmVzdWx0Um93Lm9iamVjdG1vZGVsX3R5cGU7XHJcbiAgICAgICAgICAgIHJlc3VsdE9iai5tb2RlbFRleHR1cmUgPSByZXN1bHRSb3cub2JqZWN0bW9kZWxfbW9kZWx0ZXh0dXJlO1xyXG4gICAgICAgICAgICByZXN1bHRPYmoud29ybGROYW1lID0gcmVzdWx0Um93Lm9iamVjdG1vZGVsX3dvcmxkbmFtZTtcclxuICAgICAgICAgICAgcmVzdWx0T2JqLm1vZGVsTmFtZSA9IHJlc3VsdFJvdy5vYmplY3Rtb2RlbF9tb2RlbG5hbWU7XHJcbiAgICAgICAgICAgIHJlc3VsdE9iai5kaXNwbGF5TmFtZSA9IHJlc3VsdFJvdy5vYmplY3Rtb2RlbF9kaXNwbGF5bmFtZTtcclxuICAgICAgICAgICAgcmVzdWx0T2JqLm93bmVyVXNlcklkID0gcmVzdWx0Um93Lm93bmVyX3VzZXJfaWQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzdWx0T2JqO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGFzeW5jIF9pbnNlcnRPYmplY3RNb2RlbCggY2xpZW50OiBhbnksIG9iamVjdE1vZGVsOiBPYmplY3RNb2RlbCApOiBQcm9taXNlPHN0cmluZz4ge1xyXG4gICAgICAgIGxldCBvYmplY3RNb2RlbElkOiBzdHJpbmcgPSBvYmplY3RNb2RlbC5pZDtcclxuICAgICAgICBsZXQgc2hhcGVJZCA9IFwiXCI7XHJcbiAgICAgICAgaWYgKCBvYmplY3RNb2RlbC5zaGFwZS50eXBlID09PSBDb25zdGFudHMuQ0lSQ0xFICkge1xyXG4gICAgICAgICAgICBzaGFwZUlkID0gYXdhaXQgdGhpcy5faW5zZXJ0Q2lyY2xlU2hhcGUoIGNsaWVudCwgb2JqZWN0TW9kZWwuc2hhcGUgYXMgQ2lyY2xlICk7XHJcbiAgICAgICAgfSBlbHNlIGlmICggb2JqZWN0TW9kZWwuc2hhcGUudHlwZSA9PT0gQ29uc3RhbnRzLlJFQ1RBTkdMRSApIHtcclxuICAgICAgICAgICAgc2hhcGVJZCA9IGF3YWl0IHRoaXMuX2luc2VydFJlY3RhbmdsZVNoYXBlKCBjbGllbnQsIG9iamVjdE1vZGVsLnNoYXBlIGFzIFJlY3RhbmdsZSApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBhd2FpdCBjbGllbnQucXVlcnkoXHJcbiAgICAgICAgICAgIFwiSU5TRVJUIElOVE8gaW9kb2JqZWN0bW9kZWwgKG9iamVjdG1vZGVsX2lkLCBzaGFwZV9pZCwgb2JqZWN0bW9kZWxfdHlwZSwgXCIgK1xyXG4gICAgICAgICAgICBcIiBvYmplY3Rtb2RlbF93b3JsZG5hbWUsIG9iamVjdG1vZGVsX21vZGVsbmFtZSwgb2JqZWN0bW9kZWxfZGlzcGxheW5hbWUsIFwiICtcclxuICAgICAgICAgICAgXCIgb2JqZWN0bW9kZWxfbW9kZWx0ZXh0dXJlLCBvd25lcl91c2VyX2lkKSBcIiArXHJcbiAgICAgICAgICAgIFwiVkFMVUVTICgkMSwgJDIsICQzLCAkNCwgJDUsICQ2LCAkNywgJDgpIFwiLFxyXG4gICAgICAgICAgICBbb2JqZWN0TW9kZWwuaWQsXHJcbiAgICAgICAgICAgICBzaGFwZUlkLFxyXG4gICAgICAgICAgICAgb2JqZWN0TW9kZWwub2JqZWN0VHlwZSxcclxuICAgICAgICAgICAgIG9iamVjdE1vZGVsLndvcmxkTmFtZSxcclxuICAgICAgICAgICAgIG9iamVjdE1vZGVsLm1vZGVsTmFtZSxcclxuICAgICAgICAgICAgIG9iamVjdE1vZGVsLmRpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICAgb2JqZWN0TW9kZWwubW9kZWxUZXh0dXJlLFxyXG4gICAgICAgICAgICAgb2JqZWN0TW9kZWwub3duZXJVc2VySWRdXHJcbiAgICAgICAgKTtcclxuICAgICAgICByZXR1cm4gb2JqZWN0TW9kZWxJZDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGFzeW5jIF91cGRhdGVPYmplY3RNb2RlbCggY2xpZW50OiBhbnksIG9iamVjdE1vZGVsOiBPYmplY3RNb2RlbCApOiBQcm9taXNlPHN0cmluZz4ge1xyXG4gICAgICAgIGxldCByZXN1bHRTZXQ6IGFueSA9XHJcbiAgICAgICAgICAgIGF3YWl0IGNsaWVudC5xdWVyeShcIlNFTEVDVCBzaGFwZV9pZCBGUk9NIGlvZG9iamVjdG1vZGVsIFdIRVJFIG9iamVjdG1vZGVsX2lkID0gJDFcIiwgW29iamVjdE1vZGVsLmlkXSk7XHJcbiAgICAgICAgbGV0IHNoYXBlSWQgPSByZXN1bHRTZXQucm93c1swXS5zaGFwZV9pZDtcclxuICAgICAgICBpZiAoIG9iamVjdE1vZGVsLnNoYXBlLnR5cGUgPT09IENvbnN0YW50cy5DSVJDTEUgKSB7XHJcbiAgICAgICAgICAgIHNoYXBlSWQgPSBhd2FpdCB0aGlzLl91cGRhdGVDaXJjbGVTaGFwZSggY2xpZW50LCBzaGFwZUlkLCBvYmplY3RNb2RlbC5zaGFwZSBhcyBDaXJjbGUgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCBvYmplY3RNb2RlbC5zaGFwZS50eXBlID09PSBDb25zdGFudHMuUkVDVEFOR0xFICkge1xyXG4gICAgICAgICAgICBzaGFwZUlkID0gYXdhaXQgdGhpcy5fdXBkYXRlUmVjdGFuZ2xlU2hhcGUoIGNsaWVudCwgc2hhcGVJZCwgb2JqZWN0TW9kZWwuc2hhcGUgYXMgUmVjdGFuZ2xlICk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGF3YWl0IGNsaWVudC5xdWVyeShcclxuICAgICAgICAgICAgXCJVUERBVEUgaW9kb2JqZWN0bW9kZWwgXCIgK1xyXG4gICAgICAgICAgICBcIlNFVCBvYmplY3Rtb2RlbF90eXBlID0gJDEsIG9iamVjdG1vZGVsX3dvcmxkbmFtZSA9ICQyLCBvYmplY3Rtb2RlbF9tb2RlbG5hbWUgPSAkMywgXCIgK1xyXG4gICAgICAgICAgICBcIm9iamVjdG1vZGVsX2Rpc3BsYXluYW1lID0gJDQsIG9iamVjdG1vZGVsX21vZGVsdGV4dHVyZSA9ICQ1LCBvd25lcl91c2VyX2lkID0gJDYgXCIgK1xyXG4gICAgICAgICAgICBcIldIRVJFIG9iamVjdG1vZGVsX2lkID0gJDcgXCIsXHJcbiAgICAgICAgICAgIFtvYmplY3RNb2RlbC5vYmplY3RUeXBlLFxyXG4gICAgICAgICAgICAgb2JqZWN0TW9kZWwud29ybGROYW1lLFxyXG4gICAgICAgICAgICAgb2JqZWN0TW9kZWwubW9kZWxOYW1lLFxyXG4gICAgICAgICAgICAgb2JqZWN0TW9kZWwuZGlzcGxheU5hbWUsXHJcbiAgICAgICAgICAgICBvYmplY3RNb2RlbC5tb2RlbFRleHR1cmUsXHJcbiAgICAgICAgICAgICBvYmplY3RNb2RlbC5vd25lclVzZXJJZCxcclxuICAgICAgICAgICAgIG9iamVjdE1vZGVsLmlkXVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgcmV0dXJuIG9iamVjdE1vZGVsLmlkO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgUGdPYmplY3RNb2RlbFN0b3JhZ2U7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
