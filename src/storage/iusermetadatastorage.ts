import UserMetaData from "isleofdarkness-common/src/model/usermetadata";
import IBaseStorage from "./ibasestorage";

interface IUserMetaDataStorage extends IBaseStorage {

    /**
     * Lock UserMetaData.
     * @param userId
     */
    lockUserMetaData( userMetaData: UserMetaData ): Promise<void>;

    /**
     * Add or update a user meta data in storage.
     * @param userMetaData
     */
    addUpdateUserMetaData( userMetaData: UserMetaData ): Promise<boolean>;

    /**
     * Retrieve a user meta data from storage. Return undefined if none is found.
     * @param userId
     */
    getUserMetaData( userId: string ): Promise<UserMetaData>;

    /**
     * Remove a user meta data from storage.
     * @param userId
     */
    removeUserMetaData( userId: string ): Promise<boolean>;

}

export default IUserMetaDataStorage;
