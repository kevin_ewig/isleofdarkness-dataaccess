import Ticket from "isleofdarkness-common/src/model/ticket";
import IBaseStorage from "./ibasestorage";

interface ITicketDataStorage extends IBaseStorage {

    /**
     * Add a ticket in storage.
     * @param ticket The ticket object
     */
    addTicket( ticket: Ticket ): Promise<boolean>;

    /**
     * Retrieve a ticket from storage. Return undefined if none is found.
     * @param ticketId
     */
    getTicket( ticketId: string ): Promise<Ticket>;

}

export default ITicketDataStorage;
