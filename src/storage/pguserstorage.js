"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = require("isleofdarkness-common/src/logger");
var user_1 = require("isleofdarkness-common/src/model/user");
var ibasestorage_1 = require("./ibasestorage");
var PgUserStorage = /** @class */ (function () {
    function PgUserStorage(pool) {
        this._pool = pool;
    }
    /**
     * Return the type of this storage.
     */
    PgUserStorage.prototype.getStorageType = function () {
        return ibasestorage_1.StorageType.User;
    };
    /**
     * Add or update user into the database.
     * @param user
     */
    PgUserStorage.prototype.addUpdateUser = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var result, client, resultSet, exception_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = false;
                        return [4 /*yield*/, this._pool.connect()];
                    case 1:
                        client = _a.sent();
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 5, 6, 7]);
                        return [4 /*yield*/, client.query("SET TIME ZONE 'PST8PDT'")];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, client.query("INSERT INTO ioduser (user_id, name, email, created_date, last_logged_in_date, " +
                                "       language, disabled, picture_url, avatar_objectmodel_id) " +
                                "VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) " +
                                "    ON CONFLICT (user_id) DO UPDATE " +
                                "   SET name = $2, email = $3, created_date = $4, last_logged_in_date = $5, " +
                                "       language = $6, disabled = $7, picture_url = $8, avatar_objectmodel_id = $9", [user.userId,
                                user.name,
                                user.email,
                                user.createdDate,
                                user.lastLoggedInDate,
                                user.language,
                                user.disabled === true ? "t" : "f",
                                user.pictureUrl,
                                user.avatarObjectModelId])];
                    case 4:
                        resultSet = _a.sent();
                        result = true;
                        return [3 /*break*/, 7];
                    case 5:
                        exception_1 = _a.sent();
                        logger_1.default.error("PgUserStorage", "addUpdateUser: " + exception_1);
                        return [3 /*break*/, 7];
                    case 6:
                        client.release();
                        return [7 /*endfinally*/];
                    case 7: return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * Retrieve a user from storage. Return undefined if none is found.
     * @param userId
     */
    PgUserStorage.prototype.getUser = function (userId) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            try {
                _this._pool.query("SELECT user_id, name, email, created_date, last_logged_in_date, language, " +
                    "       disabled, picture_url, avatar_objectmodel_id " +
                    "  FROM ioduser WHERE user_id = $1", [userId], function (err, result) {
                    if (err) {
                        reject(err);
                    }
                    else if (result.rows.length === 0) {
                        resolve(undefined);
                    }
                    else {
                        var row = result.rows[0];
                        var resultUser = _this._populateUserFromRow(row);
                        resolve(resultUser);
                    }
                });
            }
            catch (exception) {
                logger_1.default.error("PgUserStorage", "getUser: " + exception);
            }
        });
        return promise;
    };
    /**
     * Retrieve a user from storage by email. Return undefined if none is found.
     * @param email
     */
    PgUserStorage.prototype.getUserByEmail = function (email) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            try {
                _this._pool.query("SELECT user_id, name, email, created_date, last_logged_in_date, language, " +
                    "       disabled, picture_url, avatar_objectmodel_id " +
                    "  FROM ioduser " +
                    "WHERE LOWER(email) = LOWER($1)", [email], function (err, result) {
                    if (err) {
                        reject(err);
                    }
                    else if (result.rows.length === 0) {
                        resolve(undefined);
                    }
                    else {
                        var row = result.rows[0];
                        var resultUser = _this._populateUserFromRow(row);
                        resolve(resultUser);
                    }
                });
            }
            catch (exception) {
                logger_1.default.error("PgUserStorage", "getUserByEmail: " + exception);
            }
        });
        return promise;
    };
    /**
     * Delete a user.
     * @param user
     */
    PgUserStorage.prototype.deleteUser = function (userId) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            try {
                _this._pool.query("DELETE FROM ioduser WHERE user_Id = $1", [userId], function (err, result) {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(true);
                    }
                });
            }
            catch (exception) {
                logger_1.default.error("PgUserStorage", "deleteUser: " + exception);
            }
        });
        return promise;
    };
    PgUserStorage.prototype._populateUserFromRow = function (row) {
        var tempUserId = row.user_id;
        var tempUsername = row.name;
        var resultUser = new user_1.default(tempUserId, tempUsername);
        resultUser.email = row.email;
        resultUser.createdDate = row.created_date;
        resultUser.lastLoggedInDate = row.last_logged_in_date;
        resultUser.language = row.language;
        resultUser.disabled = (row.disabled === true ? true : false);
        resultUser.pictureUrl = row.picture_url;
        resultUser.avatarObjectModelId = row.avatar_objectmodel_id;
        return resultUser;
    };
    return PgUserStorage;
}());
exports.PgUserStorage = PgUserStorage;
exports.default = PgUserStorage;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvcGd1c2Vyc3RvcmFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMkRBQXNEO0FBQ3RELDZEQUF3RDtBQUV4RCwrQ0FBNkM7QUFFN0M7SUFJSSx1QkFBYSxJQUFTO1FBQ2xCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBQ3RCLENBQUM7SUFFRDs7T0FFRztJQUNJLHNDQUFjLEdBQXJCO1FBQ0ksTUFBTSxDQUFDLDBCQUFXLENBQUMsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFFRDs7O09BR0c7SUFDVSxxQ0FBYSxHQUExQixVQUE0QixJQUFVOzs7Ozs7d0JBRTlCLE1BQU0sR0FBRyxLQUFLLENBQUM7d0JBQ04scUJBQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsRUFBQTs7d0JBQW5DLE1BQU0sR0FBRyxTQUEwQjs7Ozt3QkFJbkMscUJBQU0sTUFBTSxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxFQUFBOzt3QkFBN0MsU0FBNkMsQ0FBQzt3QkFDekIscUJBQU0sTUFBTSxDQUFDLEtBQUssQ0FDbkMsZ0ZBQWdGO2dDQUNoRixpRUFBaUU7Z0NBQ2pFLDhDQUE4QztnQ0FDOUMsc0NBQXNDO2dDQUN0Qyw2RUFBNkU7Z0NBQzdFLG1GQUFtRixFQUNuRixDQUFDLElBQUksQ0FBQyxNQUFNO2dDQUNYLElBQUksQ0FBQyxJQUFJO2dDQUNULElBQUksQ0FBQyxLQUFLO2dDQUNWLElBQUksQ0FBQyxXQUFXO2dDQUNoQixJQUFJLENBQUMsZ0JBQWdCO2dDQUNyQixJQUFJLENBQUMsUUFBUTtnQ0FDYixJQUFJLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHO2dDQUNsQyxJQUFJLENBQUMsVUFBVTtnQ0FDZixJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxFQUFBOzt3QkFmM0IsU0FBUyxHQUFRLFNBZVU7d0JBRS9CLE1BQU0sR0FBRyxJQUFJLENBQUM7Ozs7d0JBR2QsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFLGlCQUFpQixHQUFHLFdBQVMsQ0FBQyxDQUFDOzs7d0JBRTdELE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQzs7NEJBR3JCLHNCQUFPLE1BQU0sRUFBQzs7OztLQUVqQjtJQUVEOzs7T0FHRztJQUNJLCtCQUFPLEdBQWQsVUFBZ0IsTUFBYztRQUE5QixpQkE2QkM7UUEzQkcsSUFBSSxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQU8sVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUU1QyxJQUFJLENBQUM7Z0JBQ0QsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQ1osNEVBQTRFO29CQUM1RSxzREFBc0Q7b0JBQ3RELG1DQUFtQyxFQUFFLENBQUMsTUFBTSxDQUFDLEVBQzdDLFVBQUMsR0FBUSxFQUFFLE1BQVc7b0JBQ2xCLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7d0JBQ04sTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNoQixDQUFDO29CQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxDQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUNwQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3ZCLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBRUosSUFBSSxHQUFHLEdBQVEsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDOUIsSUFBSSxVQUFVLEdBQVMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUN0RCxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBRXhCLENBQUM7Z0JBQ0wsQ0FBQyxDQUNKLENBQUM7WUFDTixDQUFDO1lBQUMsS0FBSyxDQUFDLENBQUUsU0FBVSxDQUFDLENBQUMsQ0FBQztnQkFDbkIsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFLFdBQVcsR0FBRyxTQUFTLENBQUMsQ0FBQztZQUMzRCxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFFRDs7O09BR0c7SUFDSSxzQ0FBYyxHQUFyQixVQUF1QixLQUFhO1FBQXBDLGlCQThCQztRQTVCRyxJQUFJLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBTyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRTVDLElBQUksQ0FBQztnQkFDRCxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FDWiw0RUFBNEU7b0JBQzVFLHNEQUFzRDtvQkFDdEQsaUJBQWlCO29CQUNqQixnQ0FBZ0MsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUN6QyxVQUFDLEdBQVEsRUFBRSxNQUFXO29CQUNsQixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUNOLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDaEIsQ0FBQztvQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBRSxDQUFDLENBQUMsQ0FBQzt3QkFDcEMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN2QixDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUVKLElBQUksR0FBRyxHQUFRLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzlCLElBQUksVUFBVSxHQUFTLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDdEQsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUV4QixDQUFDO2dCQUNMLENBQUMsQ0FDSixDQUFDO1lBQ04sQ0FBQztZQUFDLEtBQUssQ0FBQyxDQUFFLFNBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQ25CLGdCQUFNLENBQUMsS0FBSyxDQUFDLGVBQWUsRUFBRSxrQkFBa0IsR0FBRyxTQUFTLENBQUMsQ0FBQztZQUNsRSxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFFRDs7O09BR0c7SUFDSSxrQ0FBVSxHQUFqQixVQUFtQixNQUFjO1FBQWpDLGlCQW9CQztRQW5CRyxJQUFJLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBVSxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQy9DLElBQUksQ0FBQztnQkFDRCxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FDWix3Q0FBd0MsRUFDeEMsQ0FBQyxNQUFNLENBQUMsRUFDUixVQUFDLEdBQVEsRUFBRSxNQUFXO29CQUNsQixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUNOLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDaEIsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDSixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ2xCLENBQUM7Z0JBQ0wsQ0FBQyxDQUNKLENBQUM7WUFDTixDQUFDO1lBQUMsS0FBSyxDQUFDLENBQUUsU0FBVSxDQUFDLENBQUMsQ0FBQztnQkFDbkIsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFLGNBQWMsR0FBRyxTQUFTLENBQUMsQ0FBQztZQUM5RCxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFFTyw0Q0FBb0IsR0FBNUIsVUFBOEIsR0FBUTtRQUVsQyxJQUFJLFVBQVUsR0FBVyxHQUFHLENBQUMsT0FBTyxDQUFDO1FBQ3JDLElBQUksWUFBWSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUM7UUFDcEMsSUFBSSxVQUFVLEdBQVMsSUFBSSxjQUFJLENBQUMsVUFBVSxFQUFFLFlBQVksQ0FBQyxDQUFDO1FBRTFELFVBQVUsQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztRQUM3QixVQUFVLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQyxZQUFZLENBQUM7UUFDMUMsVUFBVSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQztRQUN0RCxVQUFVLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUM7UUFDbkMsVUFBVSxDQUFDLFFBQVEsR0FBRyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdELFVBQVUsQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLFdBQVcsQ0FBQztRQUN4QyxVQUFVLENBQUMsbUJBQW1CLEdBQUcsR0FBRyxDQUFDLHFCQUFxQixDQUFDO1FBRTNELE1BQU0sQ0FBQyxVQUFVLENBQUM7SUFFdEIsQ0FBQztJQUVMLG9CQUFDO0FBQUQsQ0EzS0EsQUEyS0MsSUFBQTtBQTNLWSxzQ0FBYTtBQTZLMUIsa0JBQWUsYUFBYSxDQUFDIiwiZmlsZSI6InN0b3JhZ2UvcGd1c2Vyc3RvcmFnZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBMb2dnZXIgZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbG9nZ2VyXCI7XHJcbmltcG9ydCBVc2VyIGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21vZGVsL3VzZXJcIjtcclxuaW1wb3J0IElQZ1VzZXJTdG9yYWdlIGZyb20gXCIuL2lwZ3VzZXJzdG9yYWdlXCI7XHJcbmltcG9ydCB7IFN0b3JhZ2VUeXBlIH0gZnJvbSBcIi4vaWJhc2VzdG9yYWdlXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgUGdVc2VyU3RvcmFnZSBpbXBsZW1lbnRzIElQZ1VzZXJTdG9yYWdlIHtcclxuXHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IF9wb29sOiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoIHBvb2w6IGFueSApIHtcclxuICAgICAgICB0aGlzLl9wb29sID0gcG9vbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybiB0aGUgdHlwZSBvZiB0aGlzIHN0b3JhZ2UuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRTdG9yYWdlVHlwZSgpOiBTdG9yYWdlVHlwZSB7XHJcbiAgICAgICAgcmV0dXJuIFN0b3JhZ2VUeXBlLlVzZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBZGQgb3IgdXBkYXRlIHVzZXIgaW50byB0aGUgZGF0YWJhc2UuXHJcbiAgICAgKiBAcGFyYW0gdXNlclxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgYXN5bmMgYWRkVXBkYXRlVXNlciggdXNlcjogVXNlciApOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuXHJcbiAgICAgICAgbGV0IHJlc3VsdCA9IGZhbHNlO1xyXG4gICAgICAgIGxldCBjbGllbnQgPSBhd2FpdCB0aGlzLl9wb29sLmNvbm5lY3QoKTtcclxuXHJcbiAgICAgICAgdHJ5IHtcclxuXHJcbiAgICAgICAgICAgIGF3YWl0IGNsaWVudC5xdWVyeShcIlNFVCBUSU1FIFpPTkUgJ1BTVDhQRFQnXCIpO1xyXG4gICAgICAgICAgICBsZXQgcmVzdWx0U2V0OiBhbnkgPSBhd2FpdCBjbGllbnQucXVlcnkoXHJcbiAgICAgICAgICAgICAgICBcIklOU0VSVCBJTlRPIGlvZHVzZXIgKHVzZXJfaWQsIG5hbWUsIGVtYWlsLCBjcmVhdGVkX2RhdGUsIGxhc3RfbG9nZ2VkX2luX2RhdGUsIFwiICtcclxuICAgICAgICAgICAgICAgIFwiICAgICAgIGxhbmd1YWdlLCBkaXNhYmxlZCwgcGljdHVyZV91cmwsIGF2YXRhcl9vYmplY3Rtb2RlbF9pZCkgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJWQUxVRVMgKCQxLCAkMiwgJDMsICQ0LCAkNSwgJDYsICQ3LCAkOCwgJDkpIFwiICtcclxuICAgICAgICAgICAgICAgIFwiICAgIE9OIENPTkZMSUNUICh1c2VyX2lkKSBETyBVUERBVEUgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCIgICBTRVQgbmFtZSA9ICQyLCBlbWFpbCA9ICQzLCBjcmVhdGVkX2RhdGUgPSAkNCwgbGFzdF9sb2dnZWRfaW5fZGF0ZSA9ICQ1LCBcIiArXHJcbiAgICAgICAgICAgICAgICBcIiAgICAgICBsYW5ndWFnZSA9ICQ2LCBkaXNhYmxlZCA9ICQ3LCBwaWN0dXJlX3VybCA9ICQ4LCBhdmF0YXJfb2JqZWN0bW9kZWxfaWQgPSAkOVwiLFxyXG4gICAgICAgICAgICAgICAgW3VzZXIudXNlcklkLFxyXG4gICAgICAgICAgICAgICAgIHVzZXIubmFtZSxcclxuICAgICAgICAgICAgICAgICB1c2VyLmVtYWlsLFxyXG4gICAgICAgICAgICAgICAgIHVzZXIuY3JlYXRlZERhdGUsXHJcbiAgICAgICAgICAgICAgICAgdXNlci5sYXN0TG9nZ2VkSW5EYXRlLFxyXG4gICAgICAgICAgICAgICAgIHVzZXIubGFuZ3VhZ2UsXHJcbiAgICAgICAgICAgICAgICAgdXNlci5kaXNhYmxlZCA9PT0gdHJ1ZSA/IFwidFwiIDogXCJmXCIsXHJcbiAgICAgICAgICAgICAgICAgdXNlci5waWN0dXJlVXJsLFxyXG4gICAgICAgICAgICAgICAgIHVzZXIuYXZhdGFyT2JqZWN0TW9kZWxJZF0pO1xyXG5cclxuICAgICAgICAgICAgcmVzdWx0ID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgfSBjYXRjaCAoIGV4Y2VwdGlvbiApIHtcclxuICAgICAgICAgICAgTG9nZ2VyLmVycm9yKFwiUGdVc2VyU3RvcmFnZVwiLCBcImFkZFVwZGF0ZVVzZXI6IFwiICsgZXhjZXB0aW9uKTtcclxuICAgICAgICB9IGZpbmFsbHkge1xyXG4gICAgICAgICAgICBjbGllbnQucmVsZWFzZSgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXRyaWV2ZSBhIHVzZXIgZnJvbSBzdG9yYWdlLiBSZXR1cm4gdW5kZWZpbmVkIGlmIG5vbmUgaXMgZm91bmQuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRVc2VyKCB1c2VySWQ6IHN0cmluZyApOiBQcm9taXNlPFVzZXI+IHtcclxuXHJcbiAgICAgICAgbGV0IHByb21pc2UgPSBuZXcgUHJvbWlzZTxVc2VyPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fcG9vbC5xdWVyeShcclxuICAgICAgICAgICAgICAgICAgICBcIlNFTEVDVCB1c2VyX2lkLCBuYW1lLCBlbWFpbCwgY3JlYXRlZF9kYXRlLCBsYXN0X2xvZ2dlZF9pbl9kYXRlLCBsYW5ndWFnZSwgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiICAgICAgIGRpc2FibGVkLCBwaWN0dXJlX3VybCwgYXZhdGFyX29iamVjdG1vZGVsX2lkIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIiAgRlJPTSBpb2R1c2VyIFdIRVJFIHVzZXJfaWQgPSAkMVwiLCBbdXNlcklkXSxcclxuICAgICAgICAgICAgICAgICAgICAoZXJyOiBhbnksIHJlc3VsdDogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlcnIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKCByZXN1bHQucm93cy5sZW5ndGggPT09IDAgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHVuZGVmaW5lZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHJvdzogYW55ID0gcmVzdWx0LnJvd3NbMF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgcmVzdWx0VXNlcjogVXNlciA9IHRoaXMuX3BvcHVsYXRlVXNlckZyb21Sb3cocm93KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUocmVzdWx0VXNlcik7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSBjYXRjaCAoIGV4Y2VwdGlvbiApIHtcclxuICAgICAgICAgICAgICAgIExvZ2dlci5lcnJvcihcIlBnVXNlclN0b3JhZ2VcIiwgXCJnZXRVc2VyOiBcIiArIGV4Y2VwdGlvbik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHByb21pc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXRyaWV2ZSBhIHVzZXIgZnJvbSBzdG9yYWdlIGJ5IGVtYWlsLiBSZXR1cm4gdW5kZWZpbmVkIGlmIG5vbmUgaXMgZm91bmQuXHJcbiAgICAgKiBAcGFyYW0gZW1haWxcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFVzZXJCeUVtYWlsKCBlbWFpbDogc3RyaW5nICk6IFByb21pc2U8VXNlcj4ge1xyXG5cclxuICAgICAgICBsZXQgcHJvbWlzZSA9IG5ldyBQcm9taXNlPFVzZXI+KChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuXHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9wb29sLnF1ZXJ5KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiU0VMRUNUIHVzZXJfaWQsIG5hbWUsIGVtYWlsLCBjcmVhdGVkX2RhdGUsIGxhc3RfbG9nZ2VkX2luX2RhdGUsIGxhbmd1YWdlLCBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCIgICAgICAgZGlzYWJsZWQsIHBpY3R1cmVfdXJsLCBhdmF0YXJfb2JqZWN0bW9kZWxfaWQgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiICBGUk9NIGlvZHVzZXIgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiV0hFUkUgTE9XRVIoZW1haWwpID0gTE9XRVIoJDEpXCIsIFtlbWFpbF0sXHJcbiAgICAgICAgICAgICAgICAgICAgKGVycjogYW55LCByZXN1bHQ6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWplY3QoZXJyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICggcmVzdWx0LnJvd3MubGVuZ3RoID09PSAwICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh1bmRlZmluZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCByb3c6IGFueSA9IHJlc3VsdC5yb3dzWzBdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHJlc3VsdFVzZXI6IFVzZXIgPSB0aGlzLl9wb3B1bGF0ZVVzZXJGcm9tUm93KHJvdyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHJlc3VsdFVzZXIpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gY2F0Y2ggKCBleGNlcHRpb24gKSB7XHJcbiAgICAgICAgICAgICAgICBMb2dnZXIuZXJyb3IoXCJQZ1VzZXJTdG9yYWdlXCIsIFwiZ2V0VXNlckJ5RW1haWw6IFwiICsgZXhjZXB0aW9uKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gcHJvbWlzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERlbGV0ZSBhIHVzZXIuXHJcbiAgICAgKiBAcGFyYW0gdXNlclxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZGVsZXRlVXNlciggdXNlcklkOiBzdHJpbmcgKTogUHJvbWlzZTxib29sZWFuPiB7XHJcbiAgICAgICAgbGV0IHByb21pc2UgPSBuZXcgUHJvbWlzZTxib29sZWFuPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9wb29sLnF1ZXJ5KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiREVMRVRFIEZST00gaW9kdXNlciBXSEVSRSB1c2VyX0lkID0gJDFcIixcclxuICAgICAgICAgICAgICAgICAgICBbdXNlcklkXSxcclxuICAgICAgICAgICAgICAgICAgICAoZXJyOiBhbnksIHJlc3VsdDogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlcnIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gY2F0Y2ggKCBleGNlcHRpb24gKSB7XHJcbiAgICAgICAgICAgICAgICBMb2dnZXIuZXJyb3IoXCJQZ1VzZXJTdG9yYWdlXCIsIFwiZGVsZXRlVXNlcjogXCIgKyBleGNlcHRpb24pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX3BvcHVsYXRlVXNlckZyb21Sb3coIHJvdzogYW55ICk6IFVzZXIge1xyXG5cclxuICAgICAgICBsZXQgdGVtcFVzZXJJZDogc3RyaW5nID0gcm93LnVzZXJfaWQ7XHJcbiAgICAgICAgbGV0IHRlbXBVc2VybmFtZTogc3RyaW5nID0gcm93Lm5hbWU7XHJcbiAgICAgICAgbGV0IHJlc3VsdFVzZXI6IFVzZXIgPSBuZXcgVXNlcih0ZW1wVXNlcklkLCB0ZW1wVXNlcm5hbWUpO1xyXG5cclxuICAgICAgICByZXN1bHRVc2VyLmVtYWlsID0gcm93LmVtYWlsO1xyXG4gICAgICAgIHJlc3VsdFVzZXIuY3JlYXRlZERhdGUgPSByb3cuY3JlYXRlZF9kYXRlO1xyXG4gICAgICAgIHJlc3VsdFVzZXIubGFzdExvZ2dlZEluRGF0ZSA9IHJvdy5sYXN0X2xvZ2dlZF9pbl9kYXRlO1xyXG4gICAgICAgIHJlc3VsdFVzZXIubGFuZ3VhZ2UgPSByb3cubGFuZ3VhZ2U7XHJcbiAgICAgICAgcmVzdWx0VXNlci5kaXNhYmxlZCA9IChyb3cuZGlzYWJsZWQgPT09IHRydWUgPyB0cnVlIDogZmFsc2UpO1xyXG4gICAgICAgIHJlc3VsdFVzZXIucGljdHVyZVVybCA9IHJvdy5waWN0dXJlX3VybDtcclxuICAgICAgICByZXN1bHRVc2VyLmF2YXRhck9iamVjdE1vZGVsSWQgPSByb3cuYXZhdGFyX29iamVjdG1vZGVsX2lkO1xyXG5cclxuICAgICAgICByZXR1cm4gcmVzdWx0VXNlcjtcclxuXHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBQZ1VzZXJTdG9yYWdlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
