"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ibasestorage_1 = require("./ibasestorage");
var pguserstorage_1 = require("./pguserstorage");
var pgobjectmodelstorage_1 = require("./pgobjectmodelstorage");
var pgbaseanimatedcharacterstorage_1 = require("./pgbaseanimatedcharacterstorage");
var pganimationprofilestorage_1 = require("./pganimationprofilestorage");
var pganimatedcharacterstorage_1 = require("./pganimatedcharacterstorage");
var PgStorageFactory = /** @class */ (function () {
    function PgStorageFactory(pool) {
        if (pool) {
            this._pool = pool;
            this._userStorage = new pguserstorage_1.default(pool);
            this._objectModelStorage = new pgobjectmodelstorage_1.default(pool);
            this._animationProfileStorage = new pganimationprofilestorage_1.default(pool);
            this._baseAnimatedCharacterStorage = new pgbaseanimatedcharacterstorage_1.default(pool, this._animationProfileStorage);
            this._animatedCharacterStorage = new pganimatedcharacterstorage_1.default(pool, this._objectModelStorage, this._animationProfileStorage);
        }
        else {
            throw new Error("No Postgres Connection pool defined");
        }
    }
    /**
     * Check if connection to database is established.
     */
    PgStorageFactory.prototype.isConnected = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, client, resultSet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = false;
                        return [4 /*yield*/, this._pool.connect()];
                    case 1:
                        client = _a.sent();
                        return [4 /*yield*/, client.query("select now()")];
                    case 2:
                        resultSet = _a.sent();
                        result = resultSet.rowCount === 1;
                        client.release();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * Get storage type
     */
    PgStorageFactory.prototype.getStorage = function (storageType) {
        if (storageType === ibasestorage_1.StorageType.User) {
            return this._userStorage;
        }
        else if (storageType === ibasestorage_1.StorageType.ObjectModel) {
            return this._objectModelStorage;
        }
        else if (storageType === ibasestorage_1.StorageType.BaseAnimatedCharacter) {
            return this._baseAnimatedCharacterStorage;
        }
        else if (storageType === ibasestorage_1.StorageType.AnimationProfile) {
            return this._animationProfileStorage;
        }
        else if (storageType === ibasestorage_1.StorageType.AnimationCharacter) {
            return this._animatedCharacterStorage;
        }
        throw new Error("No enum type of StorageType." + ibasestorage_1.StorageType[storageType] + " found");
    };
    return PgStorageFactory;
}());
exports.PgStorageFactory = PgStorageFactory;
exports.default = PgStorageFactory;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvcGdzdG9yYWdlZmFjdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsK0NBQXlEO0FBQ3pELGlEQUE0QztBQUc1QywrREFBMEQ7QUFFMUQsbUZBQThFO0FBRTlFLHlFQUFvRTtBQUVwRSwyRUFBc0U7QUFFdEU7SUFTSSwwQkFBYSxJQUFTO1FBQ2xCLEVBQUUsQ0FBQyxDQUFFLElBQUssQ0FBQyxDQUFDLENBQUM7WUFDVCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztZQUNsQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksdUJBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSw4QkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxRCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxtQ0FBeUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwRSxJQUFJLENBQUMsNkJBQTZCLEdBQUcsSUFBSSx3Q0FBOEIsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDN0csSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksb0NBQTBCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsRUFDMUYsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDdkMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLEtBQUssQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDO1FBQzNELENBQUM7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDVSxzQ0FBVyxHQUF4Qjs7Ozs7O3dCQUNRLE1BQU0sR0FBRyxLQUFLLENBQUM7d0JBQ0QscUJBQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsRUFBQTs7d0JBQXhDLE1BQU0sR0FBUSxTQUEwQjt3QkFDdkIscUJBQU0sTUFBTSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsRUFBQTs7d0JBQW5ELFNBQVMsR0FBUSxTQUFrQzt3QkFDdkQsTUFBTSxHQUFHLFNBQVMsQ0FBQyxRQUFRLEtBQUssQ0FBQyxDQUFDO3dCQUNsQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBQ2pCLHNCQUFPLE1BQU0sRUFBQzs7OztLQUNqQjtJQUVEOztPQUVHO0lBQ0kscUNBQVUsR0FBakIsVUFBMkMsV0FBd0I7UUFDL0QsRUFBRSxDQUFDLENBQUUsV0FBVyxLQUFLLDBCQUFXLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNyQyxNQUFNLENBQVUsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUN0QyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLFdBQVcsS0FBSywwQkFBVyxDQUFDLFdBQVksQ0FBQyxDQUFDLENBQUM7WUFDbkQsTUFBTSxDQUFVLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztRQUM3QyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLFdBQVcsS0FBSywwQkFBVyxDQUFDLHFCQUFzQixDQUFDLENBQUMsQ0FBQztZQUM3RCxNQUFNLENBQVUsSUFBSSxDQUFDLDZCQUE2QixDQUFDO1FBQ3ZELENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsV0FBVyxLQUFLLDBCQUFXLENBQUMsZ0JBQWlCLENBQUMsQ0FBQyxDQUFDO1lBQ3hELE1BQU0sQ0FBVSxJQUFJLENBQUMsd0JBQXdCLENBQUM7UUFDbEQsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxXQUFXLEtBQUssMEJBQVcsQ0FBQyxrQkFBbUIsQ0FBQyxDQUFDLENBQUM7WUFDMUQsTUFBTSxDQUFVLElBQUksQ0FBQyx5QkFBeUIsQ0FBQztRQUNuRCxDQUFDO1FBQ0QsTUFBTSxJQUFJLEtBQUssQ0FBQyw4QkFBOEIsR0FBRywwQkFBVyxDQUFDLFdBQVcsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDO0lBQzFGLENBQUM7SUFFTCx1QkFBQztBQUFELENBckRBLEFBcURDLElBQUE7QUFyRFksNENBQWdCO0FBdUQ3QixrQkFBZSxnQkFBZ0IsQ0FBQyIsImZpbGUiOiJzdG9yYWdlL3Bnc3RvcmFnZWZhY3RvcnkuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgSVBnU3RvcmFnZUZhY3RvcnkgZnJvbSBcIi4vaXBnc3RvcmFnZWZhY3RvcnlcIjtcclxuaW1wb3J0IElCYXNlU3RvcmFnZSwge1N0b3JhZ2VUeXBlfSBmcm9tIFwiLi9pYmFzZXN0b3JhZ2VcIjtcclxuaW1wb3J0IFBnVXNlclN0b3JhZ2UgZnJvbSBcIi4vcGd1c2Vyc3RvcmFnZVwiO1xyXG5pbXBvcnQgSVBnT2JqZWN0TW9kZWxTdG9yYWdlIGZyb20gXCIuL2lwZ29iamVjdG1vZGVsc3RvcmFnZVwiO1xyXG5pbXBvcnQgSVBnVXNlclN0b3JhZ2UgZnJvbSBcIi4vaXBndXNlcnN0b3JhZ2VcIjtcclxuaW1wb3J0IFBnT2JqZWN0TW9kZWxTdG9yYWdlIGZyb20gXCIuL3Bnb2JqZWN0bW9kZWxzdG9yYWdlXCI7XHJcbmltcG9ydCBJUGdBbmltYXRpb25Qcm9maWxlU3RvcmFnZSBmcm9tIFwiLi9pcGdhbmltYXRpb25wcm9maWxlc3RvcmFnZVwiO1xyXG5pbXBvcnQgUGdCYXNlQW5pbWF0ZWRDaGFyYWN0ZXJTdG9yYWdlIGZyb20gXCIuL3BnYmFzZWFuaW1hdGVkY2hhcmFjdGVyc3RvcmFnZVwiO1xyXG5pbXBvcnQgSVBnQmFzZUFuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZSBmcm9tIFwiLi9pcGdiYXNlYW5pbWF0ZWRjaGFyYWN0ZXJzdG9yYWdlXCI7XHJcbmltcG9ydCBQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlIGZyb20gXCIuL3BnYW5pbWF0aW9ucHJvZmlsZXN0b3JhZ2VcIjtcclxuaW1wb3J0IElQZ0FuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZSBmcm9tIFwiLi9pcGdhbmltYXRlZGNoYXJhY3RlcnN0b3JhZ2VcIjtcclxuaW1wb3J0IFBnQW5pbWF0ZWRDaGFyYWN0ZXJTdG9yYWdlIGZyb20gXCIuL3BnYW5pbWF0ZWRjaGFyYWN0ZXJzdG9yYWdlXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgUGdTdG9yYWdlRmFjdG9yeSBpbXBsZW1lbnRzIElQZ1N0b3JhZ2VGYWN0b3J5IHtcclxuXHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IF91c2VyU3RvcmFnZTogSVBnVXNlclN0b3JhZ2U7XHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IF9vYmplY3RNb2RlbFN0b3JhZ2U6IElQZ09iamVjdE1vZGVsU3RvcmFnZTtcclxuICAgIHByaXZhdGUgcmVhZG9ubHkgX2FuaW1hdGlvblByb2ZpbGVTdG9yYWdlOiBJUGdBbmltYXRpb25Qcm9maWxlU3RvcmFnZTtcclxuICAgIHByaXZhdGUgcmVhZG9ubHkgX2Jhc2VBbmltYXRlZENoYXJhY3RlclN0b3JhZ2U6IElQZ0Jhc2VBbmltYXRlZENoYXJhY3RlclN0b3JhZ2U7XHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IF9hbmltYXRlZENoYXJhY3RlclN0b3JhZ2U6IElQZ0FuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZTtcclxuICAgIHByaXZhdGUgcmVhZG9ubHkgX3Bvb2w6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvciggcG9vbDogYW55ICkge1xyXG4gICAgICAgIGlmICggcG9vbCApIHtcclxuICAgICAgICAgICAgdGhpcy5fcG9vbCA9IHBvb2w7XHJcbiAgICAgICAgICAgIHRoaXMuX3VzZXJTdG9yYWdlID0gbmV3IFBnVXNlclN0b3JhZ2UocG9vbCk7XHJcbiAgICAgICAgICAgIHRoaXMuX29iamVjdE1vZGVsU3RvcmFnZSA9IG5ldyBQZ09iamVjdE1vZGVsU3RvcmFnZShwb29sKTtcclxuICAgICAgICAgICAgdGhpcy5fYW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2UgPSBuZXcgUGdBbmltYXRpb25Qcm9maWxlU3RvcmFnZShwb29sKTtcclxuICAgICAgICAgICAgdGhpcy5fYmFzZUFuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZSA9IG5ldyBQZ0Jhc2VBbmltYXRlZENoYXJhY3RlclN0b3JhZ2UocG9vbCwgdGhpcy5fYW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2UpO1xyXG4gICAgICAgICAgICB0aGlzLl9hbmltYXRlZENoYXJhY3RlclN0b3JhZ2UgPSBuZXcgUGdBbmltYXRlZENoYXJhY3RlclN0b3JhZ2UocG9vbCwgdGhpcy5fb2JqZWN0TW9kZWxTdG9yYWdlLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fYW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2UpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIk5vIFBvc3RncmVzIENvbm5lY3Rpb24gcG9vbCBkZWZpbmVkXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIGlmIGNvbm5lY3Rpb24gdG8gZGF0YWJhc2UgaXMgZXN0YWJsaXNoZWQuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBhc3luYyBpc0Nvbm5lY3RlZCgpOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuICAgICAgICBsZXQgcmVzdWx0ID0gZmFsc2U7XHJcbiAgICAgICAgbGV0IGNsaWVudDogYW55ID0gYXdhaXQgdGhpcy5fcG9vbC5jb25uZWN0KCk7XHJcbiAgICAgICAgbGV0IHJlc3VsdFNldDogYW55ID0gYXdhaXQgY2xpZW50LnF1ZXJ5KFwic2VsZWN0IG5vdygpXCIpO1xyXG4gICAgICAgIHJlc3VsdCA9IHJlc3VsdFNldC5yb3dDb3VudCA9PT0gMTtcclxuICAgICAgICBjbGllbnQucmVsZWFzZSgpO1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgc3RvcmFnZSB0eXBlXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRTdG9yYWdlPFQgZXh0ZW5kcyBJQmFzZVN0b3JhZ2U+KCBzdG9yYWdlVHlwZTogU3RvcmFnZVR5cGUgKTogVCB7XHJcbiAgICAgICAgaWYgKCBzdG9yYWdlVHlwZSA9PT0gU3RvcmFnZVR5cGUuVXNlciApIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxUPjxhbnk+IHRoaXMuX3VzZXJTdG9yYWdlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIHN0b3JhZ2VUeXBlID09PSBTdG9yYWdlVHlwZS5PYmplY3RNb2RlbCApIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxUPjxhbnk+IHRoaXMuX29iamVjdE1vZGVsU3RvcmFnZTtcclxuICAgICAgICB9IGVsc2UgaWYgKCBzdG9yYWdlVHlwZSA9PT0gU3RvcmFnZVR5cGUuQmFzZUFuaW1hdGVkQ2hhcmFjdGVyICkge1xyXG4gICAgICAgICAgICByZXR1cm4gPFQ+PGFueT4gdGhpcy5fYmFzZUFuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZTtcclxuICAgICAgICB9IGVsc2UgaWYgKCBzdG9yYWdlVHlwZSA9PT0gU3RvcmFnZVR5cGUuQW5pbWF0aW9uUHJvZmlsZSApIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxUPjxhbnk+IHRoaXMuX2FuaW1hdGlvblByb2ZpbGVTdG9yYWdlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIHN0b3JhZ2VUeXBlID09PSBTdG9yYWdlVHlwZS5BbmltYXRpb25DaGFyYWN0ZXIgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiA8VD48YW55PiB0aGlzLl9hbmltYXRlZENoYXJhY3RlclN0b3JhZ2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIk5vIGVudW0gdHlwZSBvZiBTdG9yYWdlVHlwZS5cIiArIFN0b3JhZ2VUeXBlW3N0b3JhZ2VUeXBlXSArIFwiIGZvdW5kXCIpO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgUGdTdG9yYWdlRmFjdG9yeTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
