import Logger from "isleofdarkness-common/src/logger";
import User from "isleofdarkness-common/src/model/user";
import IPgUserStorage from "./ipguserstorage";
import { StorageType } from "./ibasestorage";

export class PgUserStorage implements IPgUserStorage {

    private readonly _pool: any;

    constructor( pool: any ) {
        this._pool = pool;
    }

    /**
     * Return the type of this storage.
     */
    public getStorageType(): StorageType {
        return StorageType.User;
    }

    /**
     * Add or update user into the database.
     * @param user
     */
    public async addUpdateUser( user: User ): Promise<boolean> {

        let result = false;
        let client = await this._pool.connect();

        try {

            await client.query("SET TIME ZONE 'PST8PDT'");
            let resultSet: any = await client.query(
                "INSERT INTO ioduser (user_id, name, email, created_date, last_logged_in_date, " +
                "       language, disabled, picture_url, avatar_objectmodel_id) " +
                "VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) " +
                "    ON CONFLICT (user_id) DO UPDATE " +
                "   SET name = $2, email = $3, created_date = $4, last_logged_in_date = $5, " +
                "       language = $6, disabled = $7, picture_url = $8, avatar_objectmodel_id = $9",
                [user.userId,
                 user.name,
                 user.email,
                 user.createdDate,
                 user.lastLoggedInDate,
                 user.language,
                 user.disabled === true ? "t" : "f",
                 user.pictureUrl,
                 user.avatarObjectModelId]);

            result = true;

        } catch ( exception ) {
            Logger.error("PgUserStorage", "addUpdateUser: " + exception);
        } finally {
            client.release();
        }

        return result;

    }

    /**
     * Retrieve a user from storage. Return undefined if none is found.
     * @param userId
     */
    public getUser( userId: string ): Promise<User> {

        let promise = new Promise<User>((resolve, reject) => {

            try {
                this._pool.query(
                    "SELECT user_id, name, email, created_date, last_logged_in_date, language, " +
                    "       disabled, picture_url, avatar_objectmodel_id " +
                    "  FROM ioduser WHERE user_id = $1", [userId],
                    (err: any, result: any) => {
                        if (err) {
                            reject(err);
                        } else if ( result.rows.length === 0 ) {
                            resolve(undefined);
                        } else {

                            let row: any = result.rows[0];
                            let resultUser: User = this._populateUserFromRow(row);
                            resolve(resultUser);

                        }
                    }
                );
            } catch ( exception ) {
                Logger.error("PgUserStorage", "getUser: " + exception);
            }
        });

        return promise;
    }

    /**
     * Retrieve a user from storage by email. Return undefined if none is found.
     * @param email
     */
    public getUserByEmail( email: string ): Promise<User> {

        let promise = new Promise<User>((resolve, reject) => {

            try {
                this._pool.query(
                    "SELECT user_id, name, email, created_date, last_logged_in_date, language, " +
                    "       disabled, picture_url, avatar_objectmodel_id " +
                    "  FROM ioduser " +
                    "WHERE LOWER(email) = LOWER($1)", [email],
                    (err: any, result: any) => {
                        if (err) {
                            reject(err);
                        } else if ( result.rows.length === 0 ) {
                            resolve(undefined);
                        } else {

                            let row: any = result.rows[0];
                            let resultUser: User = this._populateUserFromRow(row);
                            resolve(resultUser);

                        }
                    }
                );
            } catch ( exception ) {
                Logger.error("PgUserStorage", "getUserByEmail: " + exception);
            }
        });

        return promise;
    }

    /**
     * Delete a user.
     * @param user
     */
    public deleteUser( userId: string ): Promise<boolean> {
        let promise = new Promise<boolean>((resolve, reject) => {
            try {
                this._pool.query(
                    "DELETE FROM ioduser WHERE user_Id = $1",
                    [userId],
                    (err: any, result: any) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(true);
                        }
                    }
                );
            } catch ( exception ) {
                Logger.error("PgUserStorage", "deleteUser: " + exception);
            }
        });

        return promise;
    }

    private _populateUserFromRow( row: any ): User {

        let tempUserId: string = row.user_id;
        let tempUsername: string = row.name;
        let resultUser: User = new User(tempUserId, tempUsername);

        resultUser.email = row.email;
        resultUser.createdDate = row.created_date;
        resultUser.lastLoggedInDate = row.last_logged_in_date;
        resultUser.language = row.language;
        resultUser.disabled = (row.disabled === true ? true : false);
        resultUser.pictureUrl = row.picture_url;
        resultUser.avatarObjectModelId = row.avatar_objectmodel_id;

        return resultUser;

    }

}

export default PgUserStorage;
