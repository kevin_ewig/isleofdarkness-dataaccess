"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var user_1 = require("isleofdarkness-common/src/model/user");
var basestorage_1 = require("./basestorage");
var ibasestorage_1 = require("./ibasestorage");
var redis = require("redis");
var UserStorage = /** @class */ (function (_super) {
    __extends(UserStorage, _super);
    function UserStorage(redisClient) {
        var _this = _super.call(this, redisClient) || this;
        _this.WORLD_USER = "user.";
        _this.WORLD_USER_ID = _this.WORLD_USER + "id.";
        _this.WORLD_USER_LIST = _this.WORLD_USER + "list";
        if (redisClient) {
            _this._redisClient = redisClient;
        }
        else {
            throw new Error("No Redis Client defined");
        }
        return _this;
    }
    /**
     * Return the type of this storage.
     */
    UserStorage.prototype.getStorageType = function () {
        return ibasestorage_1.StorageType.User;
    };
    /**
     * Store a user in storage.
     * @param user
     */
    UserStorage.prototype.addUpdateUser = function (user) {
        var self = this;
        var key = user.userId;
        var value = JSON.stringify(user.toJSON());
        var promise = new Promise(function (resolve, reject) {
            function afterExec(err) {
                if (err) {
                    reject();
                }
                else {
                    resolve(true);
                }
            }
            function afterWatchingList(err) {
                if (err) {
                    reject();
                }
                else {
                    self._redisClient.multi().hset(self.WORLD_USER_LIST, key, value).exec(afterExec);
                }
            }
            self._redisClient.watch(self.WORLD_USER_LIST, afterWatchingList);
        });
        return promise;
    };
    /**
     * Retrieve a user from storage. Return undefined if none is found.
     * @param userId
     */
    UserStorage.prototype.getUser = function (userId) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            function afterHGet(err, value) {
                if (err) {
                    reject();
                }
                else {
                    if (value === null) {
                        resolve(undefined);
                    }
                    else {
                        var json = JSON.parse(value);
                        var result = user_1.default.fromJSON(json);
                        resolve(result);
                    }
                }
            }
            _this._redisClient.hget(_this.WORLD_USER_LIST, userId, afterHGet);
        });
        return promise;
    };
    /**
     * Remove a user from storage.
     * @param userId
     */
    UserStorage.prototype.deleteUser = function (userId) {
        var self = this;
        var key = userId;
        var promise = new Promise(function (resolve, reject) {
            function afterExec(err) {
                if (err) {
                    reject();
                }
                else {
                    resolve(true);
                }
            }
            function afterWatchingList(err) {
                if (err) {
                    reject();
                }
                else {
                    self._redisClient.multi().hdel(self.WORLD_USER_LIST, key).exec(afterExec);
                }
            }
            self._redisClient.watch(self.WORLD_USER_LIST, afterWatchingList);
        });
        return promise;
    };
    return UserStorage;
}(basestorage_1.default));
exports.UserStorage = UserStorage;
exports.default = UserStorage;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvdXNlcnN0b3JhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsNkRBQXdEO0FBRXhELDZDQUF3QztBQUN4QywrQ0FBMkM7QUFFM0MsSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBRTdCO0lBQWlDLCtCQUFXO0lBUXhDLHFCQUFhLFdBQWdCO1FBQTdCLFlBQ0ksa0JBQU0sV0FBVyxDQUFDLFNBTXJCO1FBYmdCLGdCQUFVLEdBQVcsT0FBTyxDQUFDO1FBQzdCLG1CQUFhLEdBQVcsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDaEQscUJBQWUsR0FBVyxLQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztRQU1oRSxFQUFFLENBQUMsQ0FBRSxXQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLEtBQUksQ0FBQyxZQUFZLEdBQUcsV0FBVyxDQUFDO1FBQ3BDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUMvQyxDQUFDOztJQUNMLENBQUM7SUFFRDs7T0FFRztJQUNJLG9DQUFjLEdBQXJCO1FBQ0ksTUFBTSxDQUFDLDBCQUFXLENBQUMsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFFRDs7O09BR0c7SUFDSSxtQ0FBYSxHQUFwQixVQUFzQixJQUFVO1FBRTVCLElBQUksSUFBSSxHQUFnQixJQUFJLENBQUM7UUFDN0IsSUFBSSxHQUFHLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUM5QixJQUFJLEtBQUssR0FBVyxJQUFJLENBQUMsU0FBUyxDQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBRSxDQUFDO1FBRXBELElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxDQUFVLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFL0MsbUJBQW9CLEdBQVE7Z0JBQ3hCLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLENBQUM7WUFDTCxDQUFDO1lBRUQsMkJBQTRCLEdBQVE7Z0JBQ2hDLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3JGLENBQUM7WUFDTCxDQUFDO1lBRUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxpQkFBaUIsQ0FBRSxDQUFDO1FBRXZFLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUVuQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksNkJBQU8sR0FBZCxVQUFnQixNQUFjO1FBQTlCLGlCQXdCQztRQXRCRyxJQUFJLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBTyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRTVDLG1CQUFtQixHQUFRLEVBQUUsS0FBYTtnQkFDdEMsRUFBRSxDQUFDLENBQUUsR0FBSSxDQUFDLENBQUMsQ0FBQztvQkFDUixNQUFNLEVBQUUsQ0FBQztnQkFDYixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLEVBQUUsQ0FBQyxDQUFFLEtBQUssS0FBSyxJQUFLLENBQUMsQ0FBQyxDQUFDO3dCQUNuQixPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3ZCLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDN0IsSUFBSSxNQUFNLEdBQVMsY0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDdkMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNwQixDQUFDO2dCQUNMLENBQUM7WUFDTCxDQUFDO1lBRUQsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGVBQWUsRUFBRSxNQUFNLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFFcEUsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBRW5CLENBQUM7SUFFRDs7O09BR0c7SUFDSSxnQ0FBVSxHQUFqQixVQUFtQixNQUFjO1FBRTdCLElBQUksSUFBSSxHQUFnQixJQUFJLENBQUM7UUFDN0IsSUFBSSxHQUFHLEdBQVcsTUFBTSxDQUFDO1FBRXpCLElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxDQUFVLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFL0MsbUJBQW9CLEdBQVE7Z0JBQ3hCLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLENBQUM7WUFDTCxDQUFDO1lBRUQsMkJBQTRCLEdBQVE7Z0JBQ2hDLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDOUUsQ0FBQztZQUNMLENBQUM7WUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBRSxJQUFJLENBQUMsZUFBZSxFQUFFLGlCQUFpQixDQUFFLENBQUM7UUFFdkUsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBRW5CLENBQUM7SUFFTCxrQkFBQztBQUFELENBN0hBLEFBNkhDLENBN0hnQyxxQkFBVyxHQTZIM0M7QUE3SFksa0NBQVc7QUErSHhCLGtCQUFlLFdBQVcsQ0FBQyIsImZpbGUiOiJzdG9yYWdlL3VzZXJzdG9yYWdlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFVzZXIgZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbW9kZWwvdXNlclwiO1xyXG5pbXBvcnQgSVVzZXJTdG9yYWdlIGZyb20gXCIuL2l1c2Vyc3RvcmFnZVwiO1xyXG5pbXBvcnQgQmFzZVN0b3JhZ2UgZnJvbSBcIi4vYmFzZXN0b3JhZ2VcIjtcclxuaW1wb3J0IHtTdG9yYWdlVHlwZX0gZnJvbSBcIi4vaWJhc2VzdG9yYWdlXCI7XHJcblxyXG5sZXQgcmVkaXMgPSByZXF1aXJlKFwicmVkaXNcIik7XHJcblxyXG5leHBvcnQgY2xhc3MgVXNlclN0b3JhZ2UgZXh0ZW5kcyBCYXNlU3RvcmFnZSBpbXBsZW1lbnRzIElVc2VyU3RvcmFnZSB7XHJcblxyXG4gICAgcHJpdmF0ZSByZWFkb25seSBXT1JMRF9VU0VSOiBzdHJpbmcgPSBcInVzZXIuXCI7XHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IFdPUkxEX1VTRVJfSUQ6IHN0cmluZyA9IHRoaXMuV09STERfVVNFUiArIFwiaWQuXCI7XHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IFdPUkxEX1VTRVJfTElTVDogc3RyaW5nID0gdGhpcy5XT1JMRF9VU0VSICsgXCJsaXN0XCI7XHJcblxyXG4gICAgcHJpdmF0ZSBfcmVkaXNDbGllbnQ6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvciggcmVkaXNDbGllbnQ6IGFueSApIHtcclxuICAgICAgICBzdXBlcihyZWRpc0NsaWVudCk7XHJcbiAgICAgICAgaWYgKCByZWRpc0NsaWVudCApIHtcclxuICAgICAgICAgICAgdGhpcy5fcmVkaXNDbGllbnQgPSByZWRpc0NsaWVudDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJObyBSZWRpcyBDbGllbnQgZGVmaW5lZFwiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm4gdGhlIHR5cGUgb2YgdGhpcyBzdG9yYWdlLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0U3RvcmFnZVR5cGUoKTogU3RvcmFnZVR5cGUge1xyXG4gICAgICAgIHJldHVybiBTdG9yYWdlVHlwZS5Vc2VyO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU3RvcmUgYSB1c2VyIGluIHN0b3JhZ2UuXHJcbiAgICAgKiBAcGFyYW0gdXNlclxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgYWRkVXBkYXRlVXNlciggdXNlcjogVXNlciApOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuXHJcbiAgICAgICAgbGV0IHNlbGY6IFVzZXJTdG9yYWdlID0gdGhpcztcclxuICAgICAgICBsZXQga2V5OiBzdHJpbmcgPSB1c2VyLnVzZXJJZDtcclxuICAgICAgICBsZXQgdmFsdWU6IHN0cmluZyA9IEpTT04uc3RyaW5naWZ5KCB1c2VyLnRvSlNPTigpICk7XHJcblxyXG4gICAgICAgIGxldCBwcm9taXNlID0gbmV3IFByb21pc2U8Ym9vbGVhbj4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gYWZ0ZXJFeGVjKCBlcnI6IGFueSApIHtcclxuICAgICAgICAgICAgICAgIGlmICggZXJyICkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdCgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBhZnRlcldhdGNoaW5nTGlzdCggZXJyOiBhbnkgKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIGVyciApIHtcclxuICAgICAgICAgICAgICAgICAgICByZWplY3QoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5fcmVkaXNDbGllbnQubXVsdGkoKS5oc2V0KHNlbGYuV09STERfVVNFUl9MSVNULCBrZXksIHZhbHVlKS5leGVjKGFmdGVyRXhlYyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHNlbGYuX3JlZGlzQ2xpZW50LndhdGNoKCBzZWxmLldPUkxEX1VTRVJfTElTVCwgYWZ0ZXJXYXRjaGluZ0xpc3QgKTtcclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHJpZXZlIGEgdXNlciBmcm9tIHN0b3JhZ2UuIFJldHVybiB1bmRlZmluZWQgaWYgbm9uZSBpcyBmb3VuZC5cclxuICAgICAqIEBwYXJhbSB1c2VySWRcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFVzZXIoIHVzZXJJZDogc3RyaW5nICk6IFByb21pc2U8VXNlcj4ge1xyXG5cclxuICAgICAgICBsZXQgcHJvbWlzZSA9IG5ldyBQcm9taXNlPFVzZXI+KChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIGFmdGVySEdldChlcnI6IGFueSwgdmFsdWU6IHN0cmluZykge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBlcnIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICggdmFsdWUgPT09IG51bGwgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUodW5kZWZpbmVkKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQganNvbiA9IEpTT04ucGFyc2UodmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgcmVzdWx0OiBVc2VyID0gVXNlci5mcm9tSlNPTihqc29uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShyZXN1bHQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5fcmVkaXNDbGllbnQuaGdldCh0aGlzLldPUkxEX1VTRVJfTElTVCwgdXNlcklkLCBhZnRlckhHZXQpO1xyXG5cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHByb21pc2U7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVtb3ZlIGEgdXNlciBmcm9tIHN0b3JhZ2UuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBkZWxldGVVc2VyKCB1c2VySWQ6IHN0cmluZyApOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuXHJcbiAgICAgICAgbGV0IHNlbGY6IFVzZXJTdG9yYWdlID0gdGhpcztcclxuICAgICAgICBsZXQga2V5OiBzdHJpbmcgPSB1c2VySWQ7XHJcblxyXG4gICAgICAgIGxldCBwcm9taXNlID0gbmV3IFByb21pc2U8Ym9vbGVhbj4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gYWZ0ZXJFeGVjKCBlcnI6IGFueSApIHtcclxuICAgICAgICAgICAgICAgIGlmICggZXJyICkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdCgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBhZnRlcldhdGNoaW5nTGlzdCggZXJyOiBhbnkgKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIGVyciApIHtcclxuICAgICAgICAgICAgICAgICAgICByZWplY3QoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5fcmVkaXNDbGllbnQubXVsdGkoKS5oZGVsKHNlbGYuV09STERfVVNFUl9MSVNULCBrZXkpLmV4ZWMoYWZ0ZXJFeGVjKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgc2VsZi5fcmVkaXNDbGllbnQud2F0Y2goIHNlbGYuV09STERfVVNFUl9MSVNULCBhZnRlcldhdGNoaW5nTGlzdCApO1xyXG5cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHByb21pc2U7XHJcblxyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgVXNlclN0b3JhZ2U7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
