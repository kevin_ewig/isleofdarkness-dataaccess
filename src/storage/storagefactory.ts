import Uuid from "isleofdarkness-common/src/uuid";

import IStorageFactory from "./istoragefactory";
import BaseStorage from "./basestorage";
import IBaseStorage, {StorageType} from "./ibasestorage";
import IUserMetaDataStorage from "./iusermetadatastorage";
import IObjectModelStorage from "./iobjectmodelstorage";
import UserMetaDataStorage from "./usermetadatastorage";
import ObjectModelStorage from "./objectmodelstorage";
import TicketStorage from "./ticketstorage";
import UserStorage from "./userstorage";
import ITicketStorage from "./iticketstorage";
import IUserStorage from "./iuserstorage";

export class StorageFactory implements IStorageFactory {

    private readonly _userMetaDataStorage: IUserMetaDataStorage;
    private readonly _objectModelStorage: IObjectModelStorage;
    private readonly _ticketStorage: ITicketStorage;
    private readonly _userStorage: IUserStorage;
    private readonly _userMetaDataStorge: IUserMetaDataStorage;
    private readonly _redisClient: any;

    constructor( redisClient: any ) {
        if ( redisClient ) {
            this._redisClient = redisClient;
            this._userMetaDataStorage = new UserMetaDataStorage(redisClient);
            this._objectModelStorage = new ObjectModelStorage(redisClient);
            this._ticketStorage = new TicketStorage(redisClient);
            this._userStorage = new UserStorage(redisClient);
            this._userMetaDataStorage = new UserMetaDataStorage(redisClient);
        } else {
            throw new Error("No Redis Client defined");
        }
    }

    /**
     * Check if connection to database is established.
     */
    public async isConnected(): Promise<boolean> {

        let promise = new Promise<boolean>((resolve, reject) => {

            let uuid: string = Uuid.getUuid();

            function afterGet( err: any, reply: any ) {
                if ( err ) {
                    reject();
                } else {
                    resolve(true);
                }
            }

            // This will expire in 5 seconds. Check if client can set.
            this._redisClient.set(uuid, "OK", "EX", 5);

            // This will check if client can get.
            this._redisClient.get(uuid, afterGet);

        });

        return promise;

    }

    /**
     * Get storage type
     */
    public getStorage<T extends IBaseStorage>( storageType: StorageType ): T {
        if ( storageType === StorageType.ObjectModel ) {
            return <T><any>this._objectModelStorage;
        } else if ( storageType === StorageType.UserMetaData ) {
            return <T><any>this._userMetaDataStorage;
        } else if ( storageType === StorageType.Ticket ) {
            return <T><any>this._ticketStorage;
        } else if ( storageType === StorageType.User ) {
            return <T><any>this._userStorage;
        }
        throw new Error("No enum type of StorageType." + StorageType[storageType] + " found");
    }

}

export default StorageFactory;
