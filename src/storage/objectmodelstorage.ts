import ObjectModel from "isleofdarkness-common/src/model/objectmodel";
import Point from "isleofdarkness-common/src/math/Point";
import IObjectModelStorage from "./iobjectmodelstorage";
import BaseStorage from "./basestorage";
import {StorageType} from "./ibasestorage";

let redis = require("redis");

export class ObjectModelStorage extends BaseStorage implements IObjectModelStorage {

    public readonly WORLD_OBJECTMODEL: string = "objectmodel.";
    public readonly WORLD_OBJECTMODEL_ID: string = this.WORLD_OBJECTMODEL + "id.";
    public readonly WORLD_OBJECTMODEL_LIST: string = this.WORLD_OBJECTMODEL + "list";

    private _redisClient: any;

    constructor( redisClient: any ) {
        super(redisClient);
        if ( redisClient ) {
            this._redisClient = redisClient;
        } else {
            throw new Error("No Redis Client defined");
        }
    }

    /**
     * Return the type of this storage.
     */
    public getStorageType(): StorageType {
        return StorageType.ObjectModel;
    }

    /**
     * Lock ObjectModel.
     * @param objectModel
     */
    public lockObjectModel( objectModel: ObjectModel ): Promise<void> {

        let promise = new Promise<void>((resolve, reject) => {

            function afterWatch(err: any) {
                if (err) {
                    reject();
                } else {
                    resolve();
                }
            }

            this._redisClient.watch( this.WORLD_OBJECTMODEL_ID + objectModel.id, afterWatch );

        });

        return promise;

    }

    /**
     * Store a object model in a storage.
     * @param objectModel
     */
    public addUpdateObjectModel( objectModel: ObjectModel ): Promise<boolean> {

        let promise = new Promise<boolean>((resolve, reject) => {

            let self: ObjectModelStorage = this;
            let key: string = objectModel.id;
            let value: string = JSON.stringify( objectModel.toJSON() );
            let sectionKey: string = this._getKeyFromCoordinate( objectModel );

            function afterExec( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    resolve(true);
                }
            }

            function afterWatchingList( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    self._redisClient.
                    multi().
                    sadd(sectionKey, key).
                    hset(self.WORLD_OBJECTMODEL_LIST, key, value).exec(afterExec);
                }
            }

            self._redisClient.watch( self.WORLD_OBJECTMODEL_LIST, afterWatchingList );

        });

        return promise;

    }

    /**
     * Retrieve all object model ids from a section coodinate.
     * @param coord The section coordinate
     */
    public getObjectModelsIdsInSection( coord: Point ): Promise<Array<string>> {

        let self: ObjectModelStorage = this;
        let sectionKey: string = this._getKeyFromCoordinateFromPoint( coord );

        let promise = new Promise<Array<string>>((resolve, reject) => {

            function afterSMembers(err: any, listOfObjectModelIds: Array<string>) {
                if ( err ) {
                    reject();
                } else {
                    if ( listOfObjectModelIds !== null && listOfObjectModelIds.length !== undefined ) {
                        resolve(listOfObjectModelIds);
                    } else {
                        resolve([]);
                    }
                }
            }

            self._redisClient.smembers(sectionKey, afterSMembers);

        });

        return promise;

    }

    /**
     * Retrieve an object model from quick storage.
     * @param objectModelId
     */
    public getObjectModel( objectModelId: string ): Promise<ObjectModel> {

        let self: ObjectModelStorage = this;
        let key: string = objectModelId;

        let promise = new Promise<ObjectModel>((resolve, reject) => {

            function afterHGet(err: any, value: string) {
                if ( err ) {
                    reject();
                } else {
                    if ( value === null ) {
                        resolve(undefined);
                    } else {
                        let json = JSON.parse(value);
                        let result: ObjectModel = ObjectModel.fromJSON(json);
                        resolve(result);
                    }
                }
            }

            self._redisClient.hget(self.WORLD_OBJECTMODEL_LIST, key, afterHGet);

        });

        return promise;

    }

    /**
     * Remove an object model from storage.
     * @param objectModelId
     */
    public removeObjectModel( objectModelId: string ): Promise<boolean> {

        let self: ObjectModelStorage = this;
        let key: string = objectModelId;
        let sectionKey = "";

        let promise = new Promise<boolean>((resolve, reject) => {

            function afterExec( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    resolve(true);
                }
            }

            function afterWatchingList( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    self._redisClient.
                    multi().
                    srem(sectionKey, key).
                    hdel(self.WORLD_OBJECTMODEL_LIST, key).
                    exec(afterExec);
                }
            }

            function afterHGet(err: any, value: string) {
                if ( err ) {
                    reject();
                } else {
                    if ( value === null ) {
                        resolve(undefined);
                    } else {
                        let json = JSON.parse(value);
                        let objectModel: ObjectModel = ObjectModel.fromJSON(json);
                        sectionKey = self._getKeyFromCoordinate( objectModel );
                        self._redisClient.watch( self.WORLD_OBJECTMODEL_LIST, afterWatchingList );
                    }
                }
            }

            self._redisClient.hget(self.WORLD_OBJECTMODEL_LIST, key, afterHGet);

        });

        return promise;

    }

    /**
     * Get the key representing the section coordinate
     * @param objModel
     */
    private _getKeyFromCoordinate( objectModel: ObjectModel ) {
        let coord = objectModel.getSectionCoordinate();
        return coord.x + "_" + coord.y;
    }

    /**
     * Get the key representing the section coordinate
     * @param objModel
     */
    private _getKeyFromCoordinateFromPoint( coord: Point ) {
        return coord.x + "_" + coord.y;
    }

}

export default ObjectModelStorage;
