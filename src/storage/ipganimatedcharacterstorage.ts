import AnimatedCharacter from "isleofdarkness-common/src/model/animatedcharacter";
import IBaseStorage from "./ibasestorage";

interface IPgAnimatedCharacterStorage extends IBaseStorage {

    /**
     * Add or update the animated character into the database.
     * @param animatedCharacter
     */
    addUpdateAnimatedCharacter( animatedCharacter: AnimatedCharacter ): Promise<boolean>;

    /**
     * Retrieve a animated character from storage. Return undefined if none is found.
     * @param objectModelId
     */
    getAnimatedCharacter( animatedCharacterId: string ): Promise<AnimatedCharacter|null>;

    /**
     * Delete an animated character.
     * @param objectModelId
     */
    deleteAnimatedCharacter( objectModelId: string ): Promise<boolean>;

}

export default IPgAnimatedCharacterStorage;
