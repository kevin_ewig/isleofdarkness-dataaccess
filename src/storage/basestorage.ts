export class BaseStorage {

    private _client: any;

    constructor( redisClient: any ) {
        if ( redisClient ) {
            this._client = redisClient;
        } else {
            throw new Error("No Redis Client defined");
        }
    }

    /**
     * Get an array of strings as a list.
     * @param key
     */
    public getListAsync(key: string): Promise<Array<string>> {

        let redisClient = this._client;
        let promise: Promise<Array<string>> = new Promise<Array<string>>((resolve, reject) => {

            let numberOfItems = 0;

            function afterLRange(err: any, result: Array<string>) {
                if ( err ) {
                    reject(err);
                } else {
                    resolve(result);
                }
            }

            function afterLlen(err: any, lenOfList: number) {
                if ( err ) {
                    reject(err);
                } else {
                    numberOfItems = lenOfList;
                    redisClient.lrange(key, 0, numberOfItems, afterLRange);
                }
            }

            redisClient.llen(key, afterLlen);

        });

        return promise;

    }

    /**
     * Execute transaction.
     */
    public execute(): Promise<void> {

        let redisClient = this._client;

        let promise: Promise<void> = new Promise<void>((resolve, reject) => {

            function afterExec(err: any, result: any) {
                if ( err ) {
                    reject();
                } else {
                    resolve();
                }
            }

            redisClient.exec(afterExec);

        });

        return promise;

    }

    /**
     * Start multiple transaction.
     */
    public multi(): Promise<void> {

        let redisClient = this._client;

        let promise: Promise<void> = new Promise<void>((resolve, reject) => {

            function afterMulti(err: any, result: any) {
                if ( err ) {
                    reject();
                } else {
                    resolve();
                }
            }

            redisClient.multi(afterMulti);

        });

        return promise;

    }

    /**
     * Set and replace a list of strings in memory.
     * @param set
     */
    public setListAsync(key: string, list: Array<string>): Promise<void> {

        let redisClient = this._client;

        let promise: Promise<void> = new Promise<void>((resolve, reject) => {

            function afterLPush(err: any) {
                if ( err ) {
                    reject(err);
                } else {
                    resolve();
                }
            }

            function afterDel(err: any) {
                if ( err ) {
                    reject(err);
                } else {
                    if ( list ) {
                        if ( list.length > 0 ) {
                            redisClient.lpush(key, list, afterLPush);
                        } else {
                            resolve();
                        }
                    } else {
                        resolve();
                    }
                }
            }

            // Delete all elements first.
            redisClient.del(key, afterDel);

        });
        return promise;

    }

}

export default BaseStorage;
