import BaseAnimatedCharacter from "isleofdarkness-common/src/model/baseanimatedcharacter";
import IBaseStorage from "./ibasestorage";

interface IPgBaseAnimatedCharacterStorage extends IBaseStorage {

    /**
     * Get a base animated character object.
     */
    getBaseAnimatedCharacter( baseAnimatedCharacterId: string ): Promise<BaseAnimatedCharacter>;

}

export default IPgBaseAnimatedCharacterStorage;
