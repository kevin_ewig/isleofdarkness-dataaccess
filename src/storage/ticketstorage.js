"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ticket_1 = require("isleofdarkness-common/src/model/ticket");
var basestorage_1 = require("./basestorage");
var ibasestorage_1 = require("./ibasestorage");
var redis = require("redis");
var TicketStorage = /** @class */ (function (_super) {
    __extends(TicketStorage, _super);
    function TicketStorage(redisClient) {
        var _this = _super.call(this, redisClient) || this;
        _this.TICKET_PREFIX = "ticket.";
        if (redisClient) {
            _this._redisClient = redisClient;
        }
        else {
            throw new Error("No Redis Client defined");
        }
        return _this;
    }
    /**
     * Return the type of this storage.
     */
    TicketStorage.prototype.getStorageType = function () {
        return ibasestorage_1.StorageType.UserMetaData;
    };
    /**
     * Add a ticket in storage.
     * @param ticket The ticket object
     */
    TicketStorage.prototype.addTicket = function (ticket) {
        var self = this;
        var key = this.TICKET_PREFIX + ticket.ticketId;
        var value = JSON.stringify(ticket.toJSON());
        var promise = new Promise(function (resolve, reject) {
            function afterSetEx(err) {
                if (err) {
                    reject();
                }
                else {
                    resolve(true);
                }
            }
            var now = new Date();
            var dateToExpire = ticket.expirationDate;
            var secondsToExpire = Math.round((dateToExpire.getTime() - now.getTime()) / 1000);
            if (secondsToExpire > 0) {
                self._redisClient.setex(key, secondsToExpire, value, afterSetEx);
            }
            else {
                resolve(true);
            }
        });
        return promise;
    };
    /**
     * Retrieve a ticket from storage. Return undefined if none is found.
     * @param ticketId
     */
    TicketStorage.prototype.getTicket = function (ticketId) {
        var _this = this;
        var self = this;
        var key = this.TICKET_PREFIX + ticketId;
        var promise = new Promise(function (resolve, reject) {
            function afterHGet(err, value) {
                if (err) {
                    reject();
                }
                else {
                    if (value === null) {
                        resolve(undefined);
                    }
                    else {
                        var json = JSON.parse(value);
                        var result = ticket_1.default.fromJSON(json);
                        resolve(result);
                    }
                }
            }
            _this._redisClient.get(key, afterHGet);
        });
        return promise;
    };
    return TicketStorage;
}(basestorage_1.default));
exports.TicketStorage = TicketStorage;
exports.default = TicketStorage;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvdGlja2V0c3RvcmFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSxpRUFBNEQ7QUFFNUQsNkNBQXdDO0FBQ3hDLCtDQUEyQztBQUUzQyxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7QUFFN0I7SUFBbUMsaUNBQVc7SUFNMUMsdUJBQWEsV0FBZ0I7UUFBN0IsWUFDSSxrQkFBTSxXQUFXLENBQUMsU0FNckI7UUFYZ0IsbUJBQWEsR0FBVyxTQUFTLENBQUM7UUFNL0MsRUFBRSxDQUFDLENBQUUsV0FBWSxDQUFDLENBQUMsQ0FBQztZQUNoQixLQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQztRQUNwQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDL0MsQ0FBQzs7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxzQ0FBYyxHQUFyQjtRQUNJLE1BQU0sQ0FBQywwQkFBVyxDQUFDLFlBQVksQ0FBQztJQUNwQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksaUNBQVMsR0FBaEIsVUFBa0IsTUFBYztRQUU1QixJQUFJLElBQUksR0FBa0IsSUFBSSxDQUFDO1FBQy9CLElBQUksR0FBRyxHQUFXLElBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUN2RCxJQUFJLEtBQUssR0FBVyxJQUFJLENBQUMsU0FBUyxDQUFFLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBRSxDQUFDO1FBRXRELElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxDQUFVLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFL0Msb0JBQXFCLEdBQVE7Z0JBQ3pCLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLENBQUM7WUFDTCxDQUFDO1lBRUQsSUFBSSxHQUFHLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztZQUNyQixJQUFJLFlBQVksR0FBRyxNQUFNLENBQUMsY0FBYyxDQUFDO1lBQ3pDLElBQUksZUFBZSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUUsQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLEdBQUcsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFFLENBQUM7WUFFcEYsRUFBRSxDQUFDLENBQUUsZUFBZSxHQUFHLENBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFFLEdBQUcsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLFVBQVUsQ0FBRSxDQUFDO1lBQ3ZFLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsQ0FBQztRQUVMLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUVuQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksaUNBQVMsR0FBaEIsVUFBa0IsUUFBZ0I7UUFBbEMsaUJBMkJDO1FBekJHLElBQUksSUFBSSxHQUFrQixJQUFJLENBQUM7UUFDL0IsSUFBSSxHQUFHLEdBQVcsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUM7UUFFaEQsSUFBSSxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQVMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUU5QyxtQkFBbUIsR0FBUSxFQUFFLEtBQWE7Z0JBQ3RDLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxFQUFFLENBQUM7Z0JBQ2IsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixFQUFFLENBQUMsQ0FBRSxLQUFLLEtBQUssSUFBSyxDQUFDLENBQUMsQ0FBQzt3QkFDbkIsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN2QixDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNKLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQzdCLElBQUksTUFBTSxHQUFXLGdCQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUMzQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3BCLENBQUM7Z0JBQ0wsQ0FBQztZQUNMLENBQUM7WUFFRCxLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFFMUMsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBRW5CLENBQUM7SUFFTCxvQkFBQztBQUFELENBM0ZBLEFBMkZDLENBM0ZrQyxxQkFBVyxHQTJGN0M7QUEzRlksc0NBQWE7QUE2RjFCLGtCQUFlLGFBQWEsQ0FBQyIsImZpbGUiOiJzdG9yYWdlL3RpY2tldHN0b3JhZ2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgVGlja2V0IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21vZGVsL3RpY2tldFwiO1xyXG5pbXBvcnQgSVRpY2tldFN0b3JhZ2UgZnJvbSBcIi4vaXRpY2tldHN0b3JhZ2VcIjtcclxuaW1wb3J0IEJhc2VTdG9yYWdlIGZyb20gXCIuL2Jhc2VzdG9yYWdlXCI7XHJcbmltcG9ydCB7U3RvcmFnZVR5cGV9IGZyb20gXCIuL2liYXNlc3RvcmFnZVwiO1xyXG5cclxubGV0IHJlZGlzID0gcmVxdWlyZShcInJlZGlzXCIpO1xyXG5cclxuZXhwb3J0IGNsYXNzIFRpY2tldFN0b3JhZ2UgZXh0ZW5kcyBCYXNlU3RvcmFnZSBpbXBsZW1lbnRzIElUaWNrZXRTdG9yYWdlIHtcclxuXHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IFRJQ0tFVF9QUkVGSVg6IHN0cmluZyA9IFwidGlja2V0LlwiO1xyXG5cclxuICAgIHByaXZhdGUgX3JlZGlzQ2xpZW50OiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoIHJlZGlzQ2xpZW50OiBhbnkgKSB7XHJcbiAgICAgICAgc3VwZXIocmVkaXNDbGllbnQpO1xyXG4gICAgICAgIGlmICggcmVkaXNDbGllbnQgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlZGlzQ2xpZW50ID0gcmVkaXNDbGllbnQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiTm8gUmVkaXMgQ2xpZW50IGRlZmluZWRcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJuIHRoZSB0eXBlIG9mIHRoaXMgc3RvcmFnZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFN0b3JhZ2VUeXBlKCk6IFN0b3JhZ2VUeXBlIHtcclxuICAgICAgICByZXR1cm4gU3RvcmFnZVR5cGUuVXNlck1ldGFEYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkIGEgdGlja2V0IGluIHN0b3JhZ2UuXHJcbiAgICAgKiBAcGFyYW0gdGlja2V0IFRoZSB0aWNrZXQgb2JqZWN0XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBhZGRUaWNrZXQoIHRpY2tldDogVGlja2V0ICk6IFByb21pc2U8Ym9vbGVhbj4ge1xyXG5cclxuICAgICAgICBsZXQgc2VsZjogVGlja2V0U3RvcmFnZSA9IHRoaXM7XHJcbiAgICAgICAgbGV0IGtleTogc3RyaW5nID0gdGhpcy5USUNLRVRfUFJFRklYICsgdGlja2V0LnRpY2tldElkO1xyXG4gICAgICAgIGxldCB2YWx1ZTogc3RyaW5nID0gSlNPTi5zdHJpbmdpZnkoIHRpY2tldC50b0pTT04oKSApO1xyXG5cclxuICAgICAgICBsZXQgcHJvbWlzZSA9IG5ldyBQcm9taXNlPGJvb2xlYW4+KChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIGFmdGVyU2V0RXgoIGVycjogYW55ICkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBlcnIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxldCBub3cgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgICAgICBsZXQgZGF0ZVRvRXhwaXJlID0gdGlja2V0LmV4cGlyYXRpb25EYXRlO1xyXG4gICAgICAgICAgICBsZXQgc2Vjb25kc1RvRXhwaXJlID0gTWF0aC5yb3VuZCggKGRhdGVUb0V4cGlyZS5nZXRUaW1lKCkgLSBub3cuZ2V0VGltZSgpKSAvIDEwMDAgKTtcclxuXHJcbiAgICAgICAgICAgIGlmICggc2Vjb25kc1RvRXhwaXJlID4gMCApIHtcclxuICAgICAgICAgICAgICAgIHNlbGYuX3JlZGlzQ2xpZW50LnNldGV4KCBrZXksIHNlY29uZHNUb0V4cGlyZSwgdmFsdWUsIGFmdGVyU2V0RXggKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUodHJ1ZSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHJpZXZlIGEgdGlja2V0IGZyb20gc3RvcmFnZS4gUmV0dXJuIHVuZGVmaW5lZCBpZiBub25lIGlzIGZvdW5kLlxyXG4gICAgICogQHBhcmFtIHRpY2tldElkXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRUaWNrZXQoIHRpY2tldElkOiBzdHJpbmcgKTogUHJvbWlzZTxUaWNrZXQ+IHtcclxuXHJcbiAgICAgICAgbGV0IHNlbGY6IFRpY2tldFN0b3JhZ2UgPSB0aGlzO1xyXG4gICAgICAgIGxldCBrZXk6IHN0cmluZyA9IHRoaXMuVElDS0VUX1BSRUZJWCArIHRpY2tldElkO1xyXG5cclxuICAgICAgICBsZXQgcHJvbWlzZSA9IG5ldyBQcm9taXNlPFRpY2tldD4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gYWZ0ZXJIR2V0KGVycjogYW55LCB2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIGVyciApIHtcclxuICAgICAgICAgICAgICAgICAgICByZWplY3QoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCB2YWx1ZSA9PT0gbnVsbCApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh1bmRlZmluZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBqc29uID0gSlNPTi5wYXJzZSh2YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCByZXN1bHQ6IFRpY2tldCA9IFRpY2tldC5mcm9tSlNPTihqc29uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShyZXN1bHQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5fcmVkaXNDbGllbnQuZ2V0KGtleSwgYWZ0ZXJIR2V0KTtcclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG5cclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFRpY2tldFN0b3JhZ2U7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
