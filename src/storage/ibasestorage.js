"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This determines what the storage type is for the factory.
 */
var StorageType;
(function (StorageType) {
    StorageType[StorageType["ObjectModel"] = 1] = "ObjectModel";
    StorageType[StorageType["UserMetaData"] = 2] = "UserMetaData";
    StorageType[StorageType["Ticket"] = 3] = "Ticket";
    StorageType[StorageType["User"] = 4] = "User";
    StorageType[StorageType["AnimationProfile"] = 5] = "AnimationProfile";
    StorageType[StorageType["BaseAnimatedCharacter"] = 6] = "BaseAnimatedCharacter";
    StorageType[StorageType["AnimationCharacter"] = 7] = "AnimationCharacter";
})(StorageType = exports.StorageType || (exports.StorageType = {}));

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvaWJhc2VzdG9yYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBU0E7O0dBRUc7QUFDSCxJQUFZLFdBUVg7QUFSRCxXQUFZLFdBQVc7SUFDbkIsMkRBQWUsQ0FBQTtJQUNmLDZEQUFnQixDQUFBO0lBQ2hCLGlEQUFVLENBQUE7SUFDViw2Q0FBUSxDQUFBO0lBQ1IscUVBQW9CLENBQUE7SUFDcEIsK0VBQXlCLENBQUE7SUFDekIseUVBQXNCLENBQUE7QUFDMUIsQ0FBQyxFQVJXLFdBQVcsR0FBWCxtQkFBVyxLQUFYLG1CQUFXLFFBUXRCIiwiZmlsZSI6InN0b3JhZ2UvaWJhc2VzdG9yYWdlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW50ZXJmYWNlIElCYXNlU3RvcmFnZSB7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm4gdGhlIHR5cGUgb2YgdGhpcyBzdG9yYWdlLlxyXG4gICAgICovXHJcbiAgICBnZXRTdG9yYWdlVHlwZSgpOiBTdG9yYWdlVHlwZTtcclxuXHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUaGlzIGRldGVybWluZXMgd2hhdCB0aGUgc3RvcmFnZSB0eXBlIGlzIGZvciB0aGUgZmFjdG9yeS5cclxuICovXHJcbmV4cG9ydCBlbnVtIFN0b3JhZ2VUeXBlIHtcclxuICAgIE9iamVjdE1vZGVsID0gMSxcclxuICAgIFVzZXJNZXRhRGF0YSA9IDIsXHJcbiAgICBUaWNrZXQgPSAzLFxyXG4gICAgVXNlciA9IDQsXHJcbiAgICBBbmltYXRpb25Qcm9maWxlID0gNSxcclxuICAgIEJhc2VBbmltYXRlZENoYXJhY3RlciA9IDYsXHJcbiAgICBBbmltYXRpb25DaGFyYWN0ZXIgPSA3XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IElCYXNlU3RvcmFnZTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
