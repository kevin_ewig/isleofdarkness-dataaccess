import User from "isleofdarkness-common/src/model/user";
import IUserStorage from "./iuserstorage";
import BaseStorage from "./basestorage";
import {StorageType} from "./ibasestorage";

let redis = require("redis");

export class UserStorage extends BaseStorage implements IUserStorage {

    private readonly WORLD_USER: string = "user.";
    private readonly WORLD_USER_ID: string = this.WORLD_USER + "id.";
    private readonly WORLD_USER_LIST: string = this.WORLD_USER + "list";

    private _redisClient: any;

    constructor( redisClient: any ) {
        super(redisClient);
        if ( redisClient ) {
            this._redisClient = redisClient;
        } else {
            throw new Error("No Redis Client defined");
        }
    }

    /**
     * Return the type of this storage.
     */
    public getStorageType(): StorageType {
        return StorageType.User;
    }

    /**
     * Store a user in storage.
     * @param user
     */
    public addUpdateUser( user: User ): Promise<boolean> {

        let self: UserStorage = this;
        let key: string = user.userId;
        let value: string = JSON.stringify( user.toJSON() );

        let promise = new Promise<boolean>((resolve, reject) => {

            function afterExec( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    resolve(true);
                }
            }

            function afterWatchingList( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    self._redisClient.multi().hset(self.WORLD_USER_LIST, key, value).exec(afterExec);
                }
            }

            self._redisClient.watch( self.WORLD_USER_LIST, afterWatchingList );

        });

        return promise;

    }

    /**
     * Retrieve a user from storage. Return undefined if none is found.
     * @param userId
     */
    public getUser( userId: string ): Promise<User> {

        let promise = new Promise<User>((resolve, reject) => {

            function afterHGet(err: any, value: string) {
                if ( err ) {
                    reject();
                } else {
                    if ( value === null ) {
                        resolve(undefined);
                    } else {
                        let json = JSON.parse(value);
                        let result: User = User.fromJSON(json);
                        resolve(result);
                    }
                }
            }

            this._redisClient.hget(this.WORLD_USER_LIST, userId, afterHGet);

        });

        return promise;

    }

    /**
     * Remove a user from storage.
     * @param userId
     */
    public deleteUser( userId: string ): Promise<boolean> {

        let self: UserStorage = this;
        let key: string = userId;

        let promise = new Promise<boolean>((resolve, reject) => {

            function afterExec( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    resolve(true);
                }
            }

            function afterWatchingList( err: any ) {
                if ( err ) {
                    reject();
                } else {
                    self._redisClient.multi().hdel(self.WORLD_USER_LIST, key).exec(afterExec);
                }
            }

            self._redisClient.watch( self.WORLD_USER_LIST, afterWatchingList );

        });

        return promise;

    }

}

export default UserStorage;
