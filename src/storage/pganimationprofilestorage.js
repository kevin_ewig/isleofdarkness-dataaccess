"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = require("isleofdarkness-common/src/logger");
var missingconstructorargumenterror_1 = require("isleofdarkness-common/src/error/missingconstructorargumenterror");
var animationprofile_1 = require("isleofdarkness-common/src/model/animationprofile");
var ibasestorage_1 = require("./ibasestorage");
var PgAnimationProfileStorage = /** @class */ (function () {
    function PgAnimationProfileStorage(pool) {
        if (pool) {
            this._pool = pool;
        }
        else {
            throw new missingconstructorargumenterror_1.MissingConstructorArgumentError();
        }
    }
    /**
     * Return the type of this storage.
     */
    PgAnimationProfileStorage.prototype.getStorageType = function () {
        return ibasestorage_1.StorageType.AnimationProfile;
    };
    /**
     * Retrieve a animated character from storage. Return undefined if none is found.
     * @param objectModelId
     */
    PgAnimationProfileStorage.prototype.getAnimationProfile = function (animationProfileId) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            try {
                _this._pool.query("SELECT animationprofile_id, animationprofile_name, animationprofile_idle, animationprofile_run, " +
                    "       animationprofile_walk, animationprofile_die " +
                    "  FROM iodanimationprofile " +
                    " WHERE animationprofile_id = $1", [animationProfileId], function (err, result) {
                    if (err) {
                        reject(err);
                    }
                    else if (result.rows.length === 0) {
                        resolve(undefined);
                    }
                    else {
                        var row = result.rows[0];
                        var id = row.animationprofile_id;
                        var name_1 = row.animationprofile_name;
                        var die = row.animationprofile_die;
                        var idle = row.animationprofile_idle;
                        var run = row.animationprofile_run;
                        var walk = row.animationprofile_walk;
                        var profile = new animationprofile_1.AnimationProfile(id, name_1, idle, run, walk, die);
                        resolve(profile);
                    }
                });
            }
            catch (exception) {
                logger_1.default.error("PgAnimationProfileStorage", "getAnimatedCharacter: " + exception);
            }
        });
        return promise;
    };
    return PgAnimationProfileStorage;
}());
exports.PgAnimationProfileStorage = PgAnimationProfileStorage;
exports.default = PgAnimationProfileStorage;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvcGdhbmltYXRpb25wcm9maWxlc3RvcmFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDJEQUFzRDtBQUV0RCxtSEFBa0g7QUFDbEgscUZBQW9GO0FBQ3BGLCtDQUE2QztBQUU3QztJQUlJLG1DQUFZLElBQVM7UUFDakIsRUFBRSxDQUFDLENBQUUsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNULElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSxpRUFBK0IsRUFBRSxDQUFDO1FBQ2hELENBQUM7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxrREFBYyxHQUFyQjtRQUNJLE1BQU0sQ0FBQywwQkFBVyxDQUFDLGdCQUFnQixDQUFDO0lBQ3hDLENBQUM7SUFFRDs7O09BR0c7SUFDSSx1REFBbUIsR0FBMUIsVUFBNEIsa0JBQTBCO1FBQXRELGlCQW9DQztRQWxDRyxJQUFJLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBbUIsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUV4RCxJQUFJLENBQUM7Z0JBQ0QsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQ1osa0dBQWtHO29CQUNsRyxxREFBcUQ7b0JBQ3JELDZCQUE2QjtvQkFDN0IsaUNBQWlDLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQyxFQUN2RCxVQUFDLEdBQVEsRUFBRSxNQUFXO29CQUNsQixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUNOLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDaEIsQ0FBQztvQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBRSxDQUFDLENBQUMsQ0FBQzt3QkFDcEMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN2QixDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUVKLElBQUksR0FBRyxHQUFRLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzlCLElBQUksRUFBRSxHQUFHLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQzt3QkFDakMsSUFBSSxNQUFJLEdBQUcsR0FBRyxDQUFDLHFCQUFxQixDQUFDO3dCQUNyQyxJQUFJLEdBQUcsR0FBRyxHQUFHLENBQUMsb0JBQW9CLENBQUM7d0JBQ25DLElBQUksSUFBSSxHQUFHLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQzt3QkFDckMsSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDLG9CQUFvQixDQUFDO3dCQUNuQyxJQUFJLElBQUksR0FBRyxHQUFHLENBQUMscUJBQXFCLENBQUM7d0JBQ3JDLElBQUksT0FBTyxHQUFxQixJQUFJLG1DQUFnQixDQUFDLEVBQUUsRUFBRSxNQUFJLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7d0JBQ3JGLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFFckIsQ0FBQztnQkFDTCxDQUFDLENBQ0osQ0FBQztZQUNOLENBQUM7WUFBQyxLQUFLLENBQUMsQ0FBRSxTQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUNuQixnQkFBTSxDQUFDLEtBQUssQ0FBQywyQkFBMkIsRUFBRSx3QkFBd0IsR0FBRyxTQUFTLENBQUMsQ0FBQztZQUNwRixDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFFTCxnQ0FBQztBQUFELENBN0RBLEFBNkRDLElBQUE7QUE3RFksOERBQXlCO0FBK0R0QyxrQkFBZSx5QkFBeUIsQ0FBQyIsImZpbGUiOiJzdG9yYWdlL3BnYW5pbWF0aW9ucHJvZmlsZXN0b3JhZ2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgTG9nZ2VyIGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL2xvZ2dlclwiO1xyXG5pbXBvcnQgSVBnQW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2UgZnJvbSBcIi4vaXBnYW5pbWF0aW9ucHJvZmlsZXN0b3JhZ2VcIjtcclxuaW1wb3J0IHsgTWlzc2luZ0NvbnN0cnVjdG9yQXJndW1lbnRFcnJvciB9IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL2Vycm9yL21pc3Npbmdjb25zdHJ1Y3RvcmFyZ3VtZW50ZXJyb3JcIjtcclxuaW1wb3J0IHsgQW5pbWF0aW9uUHJvZmlsZSB9IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21vZGVsL2FuaW1hdGlvbnByb2ZpbGVcIjtcclxuaW1wb3J0IHsgU3RvcmFnZVR5cGUgfSBmcm9tIFwiLi9pYmFzZXN0b3JhZ2VcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlIGltcGxlbWVudHMgSVBnQW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2Uge1xyXG5cclxuICAgIHByaXZhdGUgcmVhZG9ubHkgX3Bvb2w6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwb29sOiBhbnkpIHtcclxuICAgICAgICBpZiAoIHBvb2wgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Bvb2wgPSBwb29sO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJuIHRoZSB0eXBlIG9mIHRoaXMgc3RvcmFnZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFN0b3JhZ2VUeXBlKCk6IFN0b3JhZ2VUeXBlIHtcclxuICAgICAgICByZXR1cm4gU3RvcmFnZVR5cGUuQW5pbWF0aW9uUHJvZmlsZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHJpZXZlIGEgYW5pbWF0ZWQgY2hhcmFjdGVyIGZyb20gc3RvcmFnZS4gUmV0dXJuIHVuZGVmaW5lZCBpZiBub25lIGlzIGZvdW5kLlxyXG4gICAgICogQHBhcmFtIG9iamVjdE1vZGVsSWRcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldEFuaW1hdGlvblByb2ZpbGUoIGFuaW1hdGlvblByb2ZpbGVJZDogc3RyaW5nICk6IFByb21pc2U8QW5pbWF0aW9uUHJvZmlsZT4ge1xyXG5cclxuICAgICAgICBsZXQgcHJvbWlzZSA9IG5ldyBQcm9taXNlPEFuaW1hdGlvblByb2ZpbGU+KChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuXHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9wb29sLnF1ZXJ5KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiU0VMRUNUIGFuaW1hdGlvbnByb2ZpbGVfaWQsIGFuaW1hdGlvbnByb2ZpbGVfbmFtZSwgYW5pbWF0aW9ucHJvZmlsZV9pZGxlLCBhbmltYXRpb25wcm9maWxlX3J1biwgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiICAgICAgIGFuaW1hdGlvbnByb2ZpbGVfd2FsaywgYW5pbWF0aW9ucHJvZmlsZV9kaWUgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiICBGUk9NIGlvZGFuaW1hdGlvbnByb2ZpbGUgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiIFdIRVJFIGFuaW1hdGlvbnByb2ZpbGVfaWQgPSAkMVwiLCBbYW5pbWF0aW9uUHJvZmlsZUlkXSxcclxuICAgICAgICAgICAgICAgICAgICAoZXJyOiBhbnksIHJlc3VsdDogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlcnIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKCByZXN1bHQucm93cy5sZW5ndGggPT09IDAgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHVuZGVmaW5lZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHJvdzogYW55ID0gcmVzdWx0LnJvd3NbMF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgaWQgPSByb3cuYW5pbWF0aW9ucHJvZmlsZV9pZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBuYW1lID0gcm93LmFuaW1hdGlvbnByb2ZpbGVfbmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBkaWUgPSByb3cuYW5pbWF0aW9ucHJvZmlsZV9kaWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgaWRsZSA9IHJvdy5hbmltYXRpb25wcm9maWxlX2lkbGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgcnVuID0gcm93LmFuaW1hdGlvbnByb2ZpbGVfcnVuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHdhbGsgPSByb3cuYW5pbWF0aW9ucHJvZmlsZV93YWxrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHByb2ZpbGU6IEFuaW1hdGlvblByb2ZpbGUgPSBuZXcgQW5pbWF0aW9uUHJvZmlsZShpZCwgbmFtZSwgaWRsZSwgcnVuLCB3YWxrLCBkaWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShwcm9maWxlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9IGNhdGNoICggZXhjZXB0aW9uICkge1xyXG4gICAgICAgICAgICAgICAgTG9nZ2VyLmVycm9yKFwiUGdBbmltYXRpb25Qcm9maWxlU3RvcmFnZVwiLCBcImdldEFuaW1hdGVkQ2hhcmFjdGVyOiBcIiArIGV4Y2VwdGlvbik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHByb21pc2U7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
