"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var baseanimatedcharacter_1 = require("isleofdarkness-common/src/model/baseanimatedcharacter");
var missingconstructorargumenterror_1 = require("isleofdarkness-common/src/error/missingconstructorargumenterror");
var ibasestorage_1 = require("./ibasestorage");
var basepgstorage_1 = require("./basepgstorage");
var PgBaseAnimatedCharacterStorage = /** @class */ (function (_super) {
    __extends(PgBaseAnimatedCharacterStorage, _super);
    function PgBaseAnimatedCharacterStorage(pool, pgAnimationProfileStorage) {
        var _this = _super.call(this, pool) || this;
        if (pgAnimationProfileStorage) {
            _this._pgAnimationStorage = pgAnimationProfileStorage;
        }
        else {
            throw new missingconstructorargumenterror_1.MissingConstructorArgumentError();
        }
        return _this;
    }
    /**
     * Return the type of this storage.
     */
    PgBaseAnimatedCharacterStorage.prototype.getStorageType = function () {
        return ibasestorage_1.StorageType.BaseAnimatedCharacter;
    };
    /**
     * Get a base animated character object.
     */
    PgBaseAnimatedCharacterStorage.prototype.getBaseAnimatedCharacter = function (baseAnimatedCharacterId) {
        return __awaiter(this, void 0, void 0, function () {
            var pool, result, row, id, shapeId, modelName, modelTexture, animationProfileId, speed, animationProfile, shape, animChar;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        pool = this.getPool();
                        return [4 /*yield*/, pool.query("SELECT baseanimatedcharacter_id, shape_id, baseanimatedcharacter_modelname, " +
                                "       baseanimatedcharacter_modeltexture, animationprofile_id, speed " +
                                "  FROM iodbaseanimatedcharacter AS a " +
                                " WHERE baseanimatedcharacter_id = $1", [baseAnimatedCharacterId])];
                    case 1:
                        result = _a.sent();
                        row = result.rows[0];
                        id = row.baseanimatedcharacter_id;
                        shapeId = row.shape_id;
                        modelName = row.baseanimatedcharacter_modelname;
                        modelTexture = row.baseanimatedcharacter_modeltexture;
                        animationProfileId = row.animationprofile_id;
                        speed = row.baseanimatedcharacter_speed;
                        return [4 /*yield*/, this._pgAnimationStorage.getAnimationProfile(animationProfileId)];
                    case 2:
                        animationProfile = _a.sent();
                        return [4 /*yield*/, this._getShape(shapeId)];
                    case 3:
                        shape = _a.sent();
                        animChar = new baseanimatedcharacter_1.default(id, shape, modelName, animationProfile);
                        animChar.modelTexture = modelTexture;
                        animChar.speed = speed;
                        return [2 /*return*/, animChar];
                }
            });
        });
    };
    return PgBaseAnimatedCharacterStorage;
}(basepgstorage_1.default));
exports.PgBaseAnimatedCharacterStorage = PgBaseAnimatedCharacterStorage;
exports.default = PgBaseAnimatedCharacterStorage;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvcGdiYXNlYW5pbWF0ZWRjaGFyYWN0ZXJzdG9yYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsK0ZBQTBGO0FBRTFGLG1IQUFrSDtBQUNsSCwrQ0FBNkM7QUFDN0MsaURBQTRDO0FBSTVDO0lBQW9ELGtEQUFhO0lBSTdELHdDQUFZLElBQVMsRUFBRSx5QkFBcUQ7UUFBNUUsWUFDSSxrQkFBTSxJQUFJLENBQUMsU0FNZDtRQUxHLEVBQUUsQ0FBQyxDQUFFLHlCQUEwQixDQUFDLENBQUMsQ0FBQztZQUM5QixLQUFJLENBQUMsbUJBQW1CLEdBQUcseUJBQXlCLENBQUM7UUFDekQsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLGlFQUErQixFQUFFLENBQUM7UUFDaEQsQ0FBQzs7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSSx1REFBYyxHQUFyQjtRQUNJLE1BQU0sQ0FBQywwQkFBVyxDQUFDLHFCQUFxQixDQUFDO0lBQzdDLENBQUM7SUFFRDs7T0FFRztJQUNVLGlFQUF3QixHQUFyQyxVQUF1Qyx1QkFBK0I7Ozs7Ozt3QkFFOUQsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDYixxQkFBTSxJQUFJLENBQUMsS0FBSyxDQUN6Qiw4RUFBOEU7Z0NBQzlFLHdFQUF3RTtnQ0FDeEUsdUNBQXVDO2dDQUN2QyxzQ0FBc0MsRUFBRSxDQUFDLHVCQUF1QixDQUFDLENBQUMsRUFBQTs7d0JBSmxFLE1BQU0sR0FBRyxTQUl5RDt3QkFFbEUsR0FBRyxHQUFRLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzFCLEVBQUUsR0FBVyxHQUFHLENBQUMsd0JBQXdCLENBQUM7d0JBQzFDLE9BQU8sR0FBVyxHQUFHLENBQUMsUUFBUSxDQUFDO3dCQUMvQixTQUFTLEdBQVcsR0FBRyxDQUFDLCtCQUErQixDQUFDO3dCQUN4RCxZQUFZLEdBQVcsR0FBRyxDQUFDLGtDQUFrQyxDQUFDO3dCQUM5RCxrQkFBa0IsR0FBVyxHQUFHLENBQUMsbUJBQW1CLENBQUM7d0JBQ3JELEtBQUssR0FBVyxHQUFHLENBQUMsMkJBQTJCLENBQUM7d0JBRTdCLHFCQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxtQkFBbUIsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFBOzt3QkFBekYsZ0JBQWdCLEdBQUcsU0FBc0U7d0JBQzlELHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUE7O3dCQUF4RCxLQUFLLEdBQXNCLFNBQTZCO3dCQUN4RCxRQUFRLEdBQTBCLElBQUksK0JBQXFCLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQzt3QkFDeEcsUUFBUSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7d0JBQ3JDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO3dCQUN2QixzQkFBTyxRQUFRLEVBQUM7Ozs7S0FFbkI7SUFFTCxxQ0FBQztBQUFELENBakRBLEFBaURDLENBakRtRCx1QkFBYSxHQWlEaEU7QUFqRFksd0VBQThCO0FBbUQzQyxrQkFBZSw4QkFBOEIsQ0FBQyIsImZpbGUiOiJzdG9yYWdlL3BnYmFzZWFuaW1hdGVkY2hhcmFjdGVyc3RvcmFnZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBCYXNlQW5pbWF0ZWRDaGFyYWN0ZXIgZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbW9kZWwvYmFzZWFuaW1hdGVkY2hhcmFjdGVyXCI7XHJcbmltcG9ydCBTaGFwZSBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tYXRoL3NoYXBlXCI7XHJcbmltcG9ydCB7IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IgfSBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcbmltcG9ydCB7IFN0b3JhZ2VUeXBlIH0gZnJvbSBcIi4vaWJhc2VzdG9yYWdlXCI7XHJcbmltcG9ydCBCYXNlUGdTdG9yYWdlIGZyb20gXCIuL2Jhc2VwZ3N0b3JhZ2VcIjtcclxuaW1wb3J0IElQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlIGZyb20gXCIuL2lwZ2FuaW1hdGlvbnByb2ZpbGVzdG9yYWdlXCI7XHJcbmltcG9ydCBJUGdCYXNlQW5pbWF0ZWRDaGFyYWN0ZXJTdG9yYWdlIGZyb20gXCIuL2lwZ2Jhc2VhbmltYXRlZGNoYXJhY3RlcnN0b3JhZ2VcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBQZ0Jhc2VBbmltYXRlZENoYXJhY3RlclN0b3JhZ2UgZXh0ZW5kcyBCYXNlUGdTdG9yYWdlIGltcGxlbWVudHMgSVBnQmFzZUFuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZSB7XHJcblxyXG4gICAgcHJpdmF0ZSBfcGdBbmltYXRpb25TdG9yYWdlOiBJUGdBbmltYXRpb25Qcm9maWxlU3RvcmFnZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwb29sOiBhbnksIHBnQW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2U6IElQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlKSB7XHJcbiAgICAgICAgc3VwZXIocG9vbCk7XHJcbiAgICAgICAgaWYgKCBwZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlICkge1xyXG4gICAgICAgICAgICB0aGlzLl9wZ0FuaW1hdGlvblN0b3JhZ2UgPSBwZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJuIHRoZSB0eXBlIG9mIHRoaXMgc3RvcmFnZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFN0b3JhZ2VUeXBlKCk6IFN0b3JhZ2VUeXBlIHtcclxuICAgICAgICByZXR1cm4gU3RvcmFnZVR5cGUuQmFzZUFuaW1hdGVkQ2hhcmFjdGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IGEgYmFzZSBhbmltYXRlZCBjaGFyYWN0ZXIgb2JqZWN0LlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgYXN5bmMgZ2V0QmFzZUFuaW1hdGVkQ2hhcmFjdGVyKCBiYXNlQW5pbWF0ZWRDaGFyYWN0ZXJJZDogc3RyaW5nICk6IFByb21pc2U8QmFzZUFuaW1hdGVkQ2hhcmFjdGVyPiB7XHJcblxyXG4gICAgICAgIGxldCBwb29sID0gdGhpcy5nZXRQb29sKCk7XHJcbiAgICAgICAgbGV0IHJlc3VsdCA9IGF3YWl0IHBvb2wucXVlcnkoXHJcbiAgICAgICAgICAgIFwiU0VMRUNUIGJhc2VhbmltYXRlZGNoYXJhY3Rlcl9pZCwgc2hhcGVfaWQsIGJhc2VhbmltYXRlZGNoYXJhY3Rlcl9tb2RlbG5hbWUsIFwiICtcclxuICAgICAgICAgICAgXCIgICAgICAgYmFzZWFuaW1hdGVkY2hhcmFjdGVyX21vZGVsdGV4dHVyZSwgYW5pbWF0aW9ucHJvZmlsZV9pZCwgc3BlZWQgXCIgK1xyXG4gICAgICAgICAgICBcIiAgRlJPTSBpb2RiYXNlYW5pbWF0ZWRjaGFyYWN0ZXIgQVMgYSBcIiArXHJcbiAgICAgICAgICAgIFwiIFdIRVJFIGJhc2VhbmltYXRlZGNoYXJhY3Rlcl9pZCA9ICQxXCIsIFtiYXNlQW5pbWF0ZWRDaGFyYWN0ZXJJZF0pO1xyXG5cclxuICAgICAgICBsZXQgcm93OiBhbnkgPSByZXN1bHQucm93c1swXTtcclxuICAgICAgICBsZXQgaWQ6IHN0cmluZyA9IHJvdy5iYXNlYW5pbWF0ZWRjaGFyYWN0ZXJfaWQ7XHJcbiAgICAgICAgbGV0IHNoYXBlSWQ6IHN0cmluZyA9IHJvdy5zaGFwZV9pZDtcclxuICAgICAgICBsZXQgbW9kZWxOYW1lOiBzdHJpbmcgPSByb3cuYmFzZWFuaW1hdGVkY2hhcmFjdGVyX21vZGVsbmFtZTtcclxuICAgICAgICBsZXQgbW9kZWxUZXh0dXJlOiBzdHJpbmcgPSByb3cuYmFzZWFuaW1hdGVkY2hhcmFjdGVyX21vZGVsdGV4dHVyZTtcclxuICAgICAgICBsZXQgYW5pbWF0aW9uUHJvZmlsZUlkOiBzdHJpbmcgPSByb3cuYW5pbWF0aW9ucHJvZmlsZV9pZDtcclxuICAgICAgICBsZXQgc3BlZWQ6IG51bWJlciA9IHJvdy5iYXNlYW5pbWF0ZWRjaGFyYWN0ZXJfc3BlZWQ7XHJcblxyXG4gICAgICAgIGxldCBhbmltYXRpb25Qcm9maWxlID0gYXdhaXQgdGhpcy5fcGdBbmltYXRpb25TdG9yYWdlLmdldEFuaW1hdGlvblByb2ZpbGUoYW5pbWF0aW9uUHJvZmlsZUlkKTtcclxuICAgICAgICBsZXQgc2hhcGU6IFNoYXBlID0gPFNoYXBlPjxhbnk+YXdhaXQgdGhpcy5fZ2V0U2hhcGUoc2hhcGVJZCk7XHJcbiAgICAgICAgbGV0IGFuaW1DaGFyOiBCYXNlQW5pbWF0ZWRDaGFyYWN0ZXIgPSBuZXcgQmFzZUFuaW1hdGVkQ2hhcmFjdGVyKGlkLCBzaGFwZSwgbW9kZWxOYW1lLCBhbmltYXRpb25Qcm9maWxlKTtcclxuICAgICAgICBhbmltQ2hhci5tb2RlbFRleHR1cmUgPSBtb2RlbFRleHR1cmU7XHJcbiAgICAgICAgYW5pbUNoYXIuc3BlZWQgPSBzcGVlZDtcclxuICAgICAgICByZXR1cm4gYW5pbUNoYXI7XHJcblxyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgUGdCYXNlQW5pbWF0ZWRDaGFyYWN0ZXJTdG9yYWdlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
