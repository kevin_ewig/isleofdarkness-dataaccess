interface IBaseStorage {

    /**
     * Return the type of this storage.
     */
    getStorageType(): StorageType;

}

/**
 * This determines what the storage type is for the factory.
 */
export enum StorageType {
    ObjectModel = 1,
    UserMetaData = 2,
    Ticket = 3,
    User = 4,
    AnimationProfile = 5,
    BaseAnimatedCharacter = 6,
    AnimationCharacter = 7
}

export default IBaseStorage;
