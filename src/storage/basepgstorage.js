"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var point_1 = require("isleofdarkness-common/src/math/point");
var circle_1 = require("isleofdarkness-common/src/math/circle");
var rectangle_1 = require("isleofdarkness-common/src/math/rectangle");
var constants_1 = require("isleofdarkness-common/src/math/constants");
var uuid_1 = require("isleofdarkness-common/src/uuid");
var BasePgStorage = /** @class */ (function () {
    function BasePgStorage(pool) {
        this._pool = pool;
    }
    /**
     * Get the application pool.
     */
    BasePgStorage.prototype.getPool = function () {
        return this._pool;
    };
    /**
     * Retrieve a shape from storage. Return undefined if none is found.
     * @param shapeId
     */
    BasePgStorage.prototype._getShape = function (shapeId) {
        return __awaiter(this, void 0, void 0, function () {
            var resultSet, resultRow, shapeType, shape, point, radius, point, rectWidth, rectHeight;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._pool.query("SELECT shape_id, shape_type, circle_point_id, circle_radius, rectangle_point_id, " +
                            "  rectangle_width, rectangle_height, poly_p0_point_id, poly_p1_point_id, poly_p2_point_id, " +
                            "  poly_p3_point_id, poly_p4_point_id, poly_p5_point_id, poly_p6_point_id, poly_p7_point_id, " +
                            "  poly_p8_point_id, poly_p9_point_id " +
                            "FROM IODShape WHERE shape_id = $1", [shapeId])];
                    case 1:
                        resultSet = _a.sent();
                        resultRow = resultSet.rows[0];
                        shapeType = resultRow.shape_type;
                        shape = null;
                        if (!(shapeType === constants_1.Constants.CIRCLE)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this._getPoint(resultRow.circle_point_id)];
                    case 2:
                        point = _a.sent();
                        if (point != null) {
                            radius = resultRow.circle_radius;
                            shape = new circle_1.Circle(point, radius);
                        }
                        return [3 /*break*/, 5];
                    case 3:
                        if (!(shapeType === constants_1.Constants.RECTANGLE)) return [3 /*break*/, 5];
                        return [4 /*yield*/, this._getPoint(resultRow.rectangle_point_id)];
                    case 4:
                        point = _a.sent();
                        if (point != null) {
                            rectWidth = resultRow.rectangle_width;
                            rectHeight = resultRow.rectangle_height;
                            shape = new rectangle_1.Rectangle(point, rectWidth, rectHeight);
                        }
                        _a.label = 5;
                    case 5: return [2 /*return*/, shape];
                }
            });
        });
    };
    BasePgStorage.prototype._getPoint = function (pointId) {
        return __awaiter(this, void 0, void 0, function () {
            var resultSet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._pool.query("SELECT point_id, point_x, point_y, point_z " +
                            "FROM IODPoint WHERE point_id = $1", [pointId])];
                    case 1:
                        resultSet = _a.sent();
                        if (resultSet.rowCount === 0) {
                            return [2 /*return*/, null];
                        }
                        return [2 /*return*/, new point_1.Point(resultSet.rows[0].point_x, resultSet.rows[0].point_y, resultSet.rows[0].point_z)];
                }
            });
        });
    };
    BasePgStorage.prototype._insertCircleShape = function (client, circle) {
        return __awaiter(this, void 0, void 0, function () {
            var shapeId, pointId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        shapeId = uuid_1.default.getUuid();
                        return [4 /*yield*/, this._insertPoint(client, circle.getCenter())];
                    case 1:
                        pointId = _a.sent();
                        return [4 /*yield*/, client.query("INSERT INTO IODShape (shape_id, shape_type, circle_point_id, circle_radius) " +
                                "VALUES ($1, $2, $3, $4) ", [shapeId, circle.type, pointId, circle.getRadius()])];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, shapeId];
                }
            });
        });
    };
    BasePgStorage.prototype._updateCircleShape = function (client, shapeId, circle) {
        return __awaiter(this, void 0, void 0, function () {
            var resultSet, pointId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, client.query("SELECT circle_point_id from IODShape WHERE shape_id = $1", [shapeId])];
                    case 1:
                        resultSet = _a.sent();
                        pointId = resultSet.rows[0].circle_point_id;
                        return [4 /*yield*/, this._updatePoint(client, pointId, circle.getCenter())];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, client.query("UPDATE IODShape SET circle_radius = $2 WHERE shape_id = $1", [shapeId, circle.getRadius()])];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, shapeId];
                }
            });
        });
    };
    BasePgStorage.prototype._insertRectangleShape = function (client, rect) {
        return __awaiter(this, void 0, void 0, function () {
            var shapeId, pointId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        shapeId = uuid_1.default.getUuid();
                        return [4 /*yield*/, this._insertPoint(client, rect.position())];
                    case 1:
                        pointId = _a.sent();
                        return [4 /*yield*/, client.query("INSERT INTO IODShape (shape_id, shape_type, rectangle_point_id, rectangle_width, rectangle_height) " +
                                "VALUES ($1, $2, $3, $4, $5) ", [shapeId, rect.type, pointId, rect.width(), rect.height()])];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, shapeId];
                }
            });
        });
    };
    BasePgStorage.prototype._updateRectangleShape = function (client, shapeId, rect) {
        return __awaiter(this, void 0, void 0, function () {
            var resultSet, pointId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, client.query("SELECT rectangle_point_id from IODShape WHERE shape_id = $1", [shapeId])];
                    case 1:
                        resultSet = _a.sent();
                        pointId = resultSet.rows[0].rectangle_point_id;
                        return [4 /*yield*/, this._updatePoint(client, pointId, rect.position())];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, client.query("UPDATE IODShape SET rectangle_width = $2, rectangle_height = $3 WHERE shape_id = $1", [shapeId, rect.width(), rect.height()])];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, shapeId];
                }
            });
        });
    };
    BasePgStorage.prototype._insertPoint = function (client, point) {
        return __awaiter(this, void 0, void 0, function () {
            var pointId, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        pointId = uuid_1.default.getUuid();
                        return [4 /*yield*/, client.query("INSERT INTO IODPoint (point_id, point_x, point_y, point_z) " +
                                "VALUES ($1, $2, $3, $4) ", [pointId, point.x, point.y, point.z])];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, pointId];
                }
            });
        });
    };
    BasePgStorage.prototype._updatePoint = function (client, pointId, point) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, client.query("UPDATE IODPoint SET point_x = $1, point_y = $2, point_z = $3 WHERE point_id = $4 ", [point.x, point.y, point.z, pointId])];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, pointId];
                }
            });
        });
    };
    return BasePgStorage;
}());
exports.BasePgStorage = BasePgStorage;
exports.default = BasePgStorage;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvYmFzZXBnc3RvcmFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsOERBQTZEO0FBRTdELGdFQUErRDtBQUMvRCxzRUFBcUU7QUFDckUsc0VBQXFFO0FBQ3JFLHVEQUFrRDtBQUVsRDtJQUlJLHVCQUFhLElBQVM7UUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7SUFDdEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksK0JBQU8sR0FBZDtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFFRDs7O09BR0c7SUFDVSxpQ0FBUyxHQUF0QixVQUF3QixPQUFlOzs7Ozs0QkFFZCxxQkFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FDdkMsbUZBQW1GOzRCQUNuRiw2RkFBNkY7NEJBQzdGLDhGQUE4Rjs0QkFDOUYsdUNBQXVDOzRCQUN2QyxtQ0FBbUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUNqRCxFQUFBOzt3QkFORyxTQUFTLEdBQVEsU0FNcEI7d0JBRUcsU0FBUyxHQUFRLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBRW5DLFNBQVMsR0FBVyxTQUFTLENBQUMsVUFBVSxDQUFDO3dCQUN6QyxLQUFLLEdBQWUsSUFBSSxDQUFDOzZCQUN4QixDQUFBLFNBQVMsS0FBSyxxQkFBUyxDQUFDLE1BQU0sQ0FBQSxFQUE5Qix3QkFBOEI7d0JBQ1AscUJBQU0sSUFBSSxDQUFDLFNBQVMsQ0FBRSxTQUFTLENBQUMsZUFBZSxDQUFDLEVBQUE7O3dCQUFwRSxLQUFLLEdBQWUsU0FBZ0Q7d0JBQ3hFLEVBQUUsQ0FBQyxDQUFFLEtBQUssSUFBSSxJQUFLLENBQUMsQ0FBQyxDQUFDOzRCQUNkLE1BQU0sR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDOzRCQUNyQyxLQUFLLEdBQUcsSUFBSSxlQUFNLENBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBRSxDQUFDO3dCQUN4QyxDQUFDOzs7NkJBQ08sQ0FBQSxTQUFTLEtBQUsscUJBQVMsQ0FBQyxTQUFTLENBQUEsRUFBakMsd0JBQWlDO3dCQUNqQixxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFFLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFBOzt3QkFBdkUsS0FBSyxHQUFlLFNBQW1EO3dCQUMzRSxFQUFFLENBQUMsQ0FBRSxLQUFLLElBQUksSUFBSyxDQUFDLENBQUMsQ0FBQzs0QkFDZCxTQUFTLEdBQUcsU0FBUyxDQUFDLGVBQWUsQ0FBQzs0QkFDdEMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQzs0QkFDNUMsS0FBSyxHQUFHLElBQUkscUJBQVMsQ0FBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLFVBQVUsQ0FBRSxDQUFDO3dCQUMxRCxDQUFDOzs0QkFHTCxzQkFBTyxLQUFLLEVBQUM7Ozs7S0FFaEI7SUFFWSxpQ0FBUyxHQUF0QixVQUF3QixPQUFlOzs7Ozs0QkFDZCxxQkFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FDdkMsNkNBQTZDOzRCQUM3QyxtQ0FBbUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUNqRCxFQUFBOzt3QkFIRyxTQUFTLEdBQVEsU0FHcEI7d0JBQ0QsRUFBRSxDQUFDLENBQUUsU0FBUyxDQUFDLFFBQVEsS0FBSyxDQUFFLENBQUMsQ0FBQyxDQUFDOzRCQUM3QixNQUFNLGdCQUFDLElBQUksRUFBQzt3QkFDaEIsQ0FBQzt3QkFDRCxzQkFBTyxJQUFJLGFBQUssQ0FBRSxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBRSxFQUFDOzs7O0tBQ3ZHO0lBRVksMENBQWtCLEdBQS9CLFVBQWlDLE1BQVcsRUFBRSxNQUFjOzs7Ozs7d0JBQ3BELE9BQU8sR0FBVyxjQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBQ2YscUJBQU0sSUFBSSxDQUFDLFlBQVksQ0FBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFFLEVBQUE7O3dCQUF2RSxPQUFPLEdBQVcsU0FBcUQ7d0JBQzNFLHFCQUFNLE1BQU0sQ0FBQyxLQUFLLENBQ2QsOEVBQThFO2dDQUM5RSwwQkFBMEIsRUFDMUIsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsRUFBQTs7d0JBSHhELFNBR3dELENBQUM7d0JBQ3pELHNCQUFPLE9BQU8sRUFBQzs7OztLQUNsQjtJQUVZLDBDQUFrQixHQUEvQixVQUFpQyxNQUFXLEVBQUUsT0FBZSxFQUFFLE1BQWM7Ozs7OzRCQUNwRCxxQkFBTSxNQUFNLENBQUMsS0FBSyxDQUFDLDBEQUEwRCxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBQTs7d0JBQTFHLFNBQVMsR0FBUSxTQUF5Rjt3QkFDMUcsT0FBTyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDO3dCQUNoRCxxQkFBTSxJQUFJLENBQUMsWUFBWSxDQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFFLEVBQUE7O3dCQUE5RCxTQUE4RCxDQUFDO3dCQUMvRCxxQkFBTSxNQUFNLENBQUMsS0FBSyxDQUNkLDREQUE0RCxFQUM1RCxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFBOzt3QkFGbEMsU0FFa0MsQ0FBQzt3QkFDbkMsc0JBQU8sT0FBTyxFQUFDOzs7O0tBQ2xCO0lBRVksNkNBQXFCLEdBQWxDLFVBQW9DLE1BQVcsRUFBRSxJQUFlOzs7Ozs7d0JBQ3hELE9BQU8sR0FBVyxjQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBQ2YscUJBQU0sSUFBSSxDQUFDLFlBQVksQ0FBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFFLEVBQUE7O3dCQUFwRSxPQUFPLEdBQVcsU0FBa0Q7d0JBQ3hFLHFCQUFNLE1BQU0sQ0FBQyxLQUFLLENBQ2QscUdBQXFHO2dDQUNyRyw4QkFBOEIsRUFDOUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLEVBQUE7O3dCQUgvRCxTQUcrRCxDQUFDO3dCQUNoRSxzQkFBTyxPQUFPLEVBQUM7Ozs7S0FDbEI7SUFFWSw2Q0FBcUIsR0FBbEMsVUFBb0MsTUFBVyxFQUFFLE9BQWUsRUFBRSxJQUFlOzs7Ozs0QkFDeEQscUJBQU0sTUFBTSxDQUFDLEtBQUssQ0FBQyw2REFBNkQsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUE7O3dCQUE3RyxTQUFTLEdBQVEsU0FBNEY7d0JBQzdHLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDO3dCQUNuRCxxQkFBTSxJQUFJLENBQUMsWUFBWSxDQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFFLEVBQUE7O3dCQUEzRCxTQUEyRCxDQUFDO3dCQUM1RCxxQkFBTSxNQUFNLENBQUMsS0FBSyxDQUNkLHFGQUFxRixFQUNyRixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsRUFBQTs7d0JBRjNDLFNBRTJDLENBQUM7d0JBQzVDLHNCQUFPLE9BQU8sRUFBQzs7OztLQUNsQjtJQUVZLG9DQUFZLEdBQXpCLFVBQTJCLE1BQVcsRUFBRSxLQUFZOzs7Ozs7d0JBQzVDLE9BQU8sR0FBVyxjQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBQ3hCLHFCQUFNLE1BQU0sQ0FBQyxLQUFLLENBQzNCLDZEQUE2RDtnQ0FDN0QsMEJBQTBCLEVBQzFCLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBQTs7d0JBSHJDLE1BQU0sR0FBRyxTQUc0Qjt3QkFDekMsc0JBQU8sT0FBTyxFQUFDOzs7O0tBQ2xCO0lBRVksb0NBQVksR0FBekIsVUFBMkIsTUFBVyxFQUFFLE9BQWUsRUFBRSxLQUFZOzs7OzRCQUNqRSxxQkFBTSxNQUFNLENBQUMsS0FBSyxDQUNkLG1GQUFtRixFQUNuRixDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDLEVBQUE7O3dCQUZ6QyxTQUV5QyxDQUFDO3dCQUMxQyxzQkFBTyxPQUFPLEVBQUM7Ozs7S0FDbEI7SUFHTCxvQkFBQztBQUFELENBeEhBLEFBd0hDLElBQUE7QUF4SFksc0NBQWE7QUEwSDFCLGtCQUFlLGFBQWEsQ0FBQyIsImZpbGUiOiJzdG9yYWdlL2Jhc2VwZ3N0b3JhZ2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQb2ludCB9IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21hdGgvcG9pbnRcIjtcclxuaW1wb3J0IHsgU2hhcGUgfSBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tYXRoL3NoYXBlXCI7XHJcbmltcG9ydCB7IENpcmNsZSB9IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21hdGgvY2lyY2xlXCI7XHJcbmltcG9ydCB7IFJlY3RhbmdsZSB9IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21hdGgvcmVjdGFuZ2xlXCI7XHJcbmltcG9ydCB7IENvbnN0YW50cyB9IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21hdGgvY29uc3RhbnRzXCI7XHJcbmltcG9ydCBVdWlkIGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL3V1aWRcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBCYXNlUGdTdG9yYWdlIHtcclxuXHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IF9wb29sOiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoIHBvb2w6IGFueSApIHtcclxuICAgICAgICB0aGlzLl9wb29sID0gcG9vbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgYXBwbGljYXRpb24gcG9vbC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFBvb2woKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3Bvb2w7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXRyaWV2ZSBhIHNoYXBlIGZyb20gc3RvcmFnZS4gUmV0dXJuIHVuZGVmaW5lZCBpZiBub25lIGlzIGZvdW5kLlxyXG4gICAgICogQHBhcmFtIHNoYXBlSWRcclxuICAgICAqL1xyXG4gICAgcHVibGljIGFzeW5jIF9nZXRTaGFwZSggc2hhcGVJZDogc3RyaW5nICk6IFByb21pc2U8U2hhcGV8bnVsbD4ge1xyXG5cclxuICAgICAgICBsZXQgcmVzdWx0U2V0OiBhbnkgPSBhd2FpdCB0aGlzLl9wb29sLnF1ZXJ5KFxyXG4gICAgICAgICAgICBcIlNFTEVDVCBzaGFwZV9pZCwgc2hhcGVfdHlwZSwgY2lyY2xlX3BvaW50X2lkLCBjaXJjbGVfcmFkaXVzLCByZWN0YW5nbGVfcG9pbnRfaWQsIFwiICtcclxuICAgICAgICAgICAgXCIgIHJlY3RhbmdsZV93aWR0aCwgcmVjdGFuZ2xlX2hlaWdodCwgcG9seV9wMF9wb2ludF9pZCwgcG9seV9wMV9wb2ludF9pZCwgcG9seV9wMl9wb2ludF9pZCwgXCIgK1xyXG4gICAgICAgICAgICBcIiAgcG9seV9wM19wb2ludF9pZCwgcG9seV9wNF9wb2ludF9pZCwgcG9seV9wNV9wb2ludF9pZCwgcG9seV9wNl9wb2ludF9pZCwgcG9seV9wN19wb2ludF9pZCwgXCIgK1xyXG4gICAgICAgICAgICBcIiAgcG9seV9wOF9wb2ludF9pZCwgcG9seV9wOV9wb2ludF9pZCBcIiArXHJcbiAgICAgICAgICAgIFwiRlJPTSBJT0RTaGFwZSBXSEVSRSBzaGFwZV9pZCA9ICQxXCIsIFtzaGFwZUlkXVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGxldCByZXN1bHRSb3c6IGFueSA9IHJlc3VsdFNldC5yb3dzWzBdO1xyXG5cclxuICAgICAgICBsZXQgc2hhcGVUeXBlOiBzdHJpbmcgPSByZXN1bHRSb3cuc2hhcGVfdHlwZTtcclxuICAgICAgICBsZXQgc2hhcGU6IFNoYXBlfG51bGwgPSBudWxsO1xyXG4gICAgICAgIGlmICggc2hhcGVUeXBlID09PSBDb25zdGFudHMuQ0lSQ0xFICkge1xyXG4gICAgICAgICAgICBsZXQgcG9pbnQ6IFBvaW50fG51bGwgPSBhd2FpdCB0aGlzLl9nZXRQb2ludCggcmVzdWx0Um93LmNpcmNsZV9wb2ludF9pZCk7XHJcbiAgICAgICAgICAgIGlmICggcG9pbnQgIT0gbnVsbCApIHtcclxuICAgICAgICAgICAgICAgIGxldCByYWRpdXMgPSByZXN1bHRSb3cuY2lyY2xlX3JhZGl1cztcclxuICAgICAgICAgICAgICAgIHNoYXBlID0gbmV3IENpcmNsZSggcG9pbnQsIHJhZGl1cyApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmICggc2hhcGVUeXBlID09PSBDb25zdGFudHMuUkVDVEFOR0xFICkge1xyXG4gICAgICAgICAgICBsZXQgcG9pbnQ6IFBvaW50fG51bGwgPSBhd2FpdCB0aGlzLl9nZXRQb2ludCggcmVzdWx0Um93LnJlY3RhbmdsZV9wb2ludF9pZCk7XHJcbiAgICAgICAgICAgIGlmICggcG9pbnQgIT0gbnVsbCApIHtcclxuICAgICAgICAgICAgICAgIGxldCByZWN0V2lkdGggPSByZXN1bHRSb3cucmVjdGFuZ2xlX3dpZHRoO1xyXG4gICAgICAgICAgICAgICAgbGV0IHJlY3RIZWlnaHQgPSByZXN1bHRSb3cucmVjdGFuZ2xlX2hlaWdodDtcclxuICAgICAgICAgICAgICAgIHNoYXBlID0gbmV3IFJlY3RhbmdsZSggcG9pbnQsIHJlY3RXaWR0aCwgcmVjdEhlaWdodCApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gc2hhcGU7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhc3luYyBfZ2V0UG9pbnQoIHBvaW50SWQ6IHN0cmluZyApOiBQcm9taXNlPFBvaW50fG51bGw+IHtcclxuICAgICAgICBsZXQgcmVzdWx0U2V0OiBhbnkgPSBhd2FpdCB0aGlzLl9wb29sLnF1ZXJ5KFxyXG4gICAgICAgICAgICBcIlNFTEVDVCBwb2ludF9pZCwgcG9pbnRfeCwgcG9pbnRfeSwgcG9pbnRfeiBcIiArXHJcbiAgICAgICAgICAgIFwiRlJPTSBJT0RQb2ludCBXSEVSRSBwb2ludF9pZCA9ICQxXCIsIFtwb2ludElkXVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgaWYgKCByZXN1bHRTZXQucm93Q291bnQgPT09IDAgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbmV3IFBvaW50KCByZXN1bHRTZXQucm93c1swXS5wb2ludF94LCByZXN1bHRTZXQucm93c1swXS5wb2ludF95LCByZXN1bHRTZXQucm93c1swXS5wb2ludF96ICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFzeW5jIF9pbnNlcnRDaXJjbGVTaGFwZSggY2xpZW50OiBhbnksIGNpcmNsZTogQ2lyY2xlICk6IFByb21pc2U8c3RyaW5nPiB7XHJcbiAgICAgICAgbGV0IHNoYXBlSWQ6IHN0cmluZyA9IFV1aWQuZ2V0VXVpZCgpO1xyXG4gICAgICAgIGxldCBwb2ludElkOiBzdHJpbmcgPSBhd2FpdCB0aGlzLl9pbnNlcnRQb2ludCggY2xpZW50LCBjaXJjbGUuZ2V0Q2VudGVyKCkgKTtcclxuICAgICAgICBhd2FpdCBjbGllbnQucXVlcnkoXHJcbiAgICAgICAgICAgIFwiSU5TRVJUIElOVE8gSU9EU2hhcGUgKHNoYXBlX2lkLCBzaGFwZV90eXBlLCBjaXJjbGVfcG9pbnRfaWQsIGNpcmNsZV9yYWRpdXMpIFwiICtcclxuICAgICAgICAgICAgXCJWQUxVRVMgKCQxLCAkMiwgJDMsICQ0KSBcIixcclxuICAgICAgICAgICAgW3NoYXBlSWQsIGNpcmNsZS50eXBlLCBwb2ludElkLCBjaXJjbGUuZ2V0UmFkaXVzKCldKTtcclxuICAgICAgICByZXR1cm4gc2hhcGVJZDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXN5bmMgX3VwZGF0ZUNpcmNsZVNoYXBlKCBjbGllbnQ6IGFueSwgc2hhcGVJZDogc3RyaW5nLCBjaXJjbGU6IENpcmNsZSApOiBQcm9taXNlPHN0cmluZz4ge1xyXG4gICAgICAgIGxldCByZXN1bHRTZXQ6IGFueSA9IGF3YWl0IGNsaWVudC5xdWVyeShcIlNFTEVDVCBjaXJjbGVfcG9pbnRfaWQgZnJvbSBJT0RTaGFwZSBXSEVSRSBzaGFwZV9pZCA9ICQxXCIsIFtzaGFwZUlkXSk7XHJcbiAgICAgICAgbGV0IHBvaW50SWQgPSByZXN1bHRTZXQucm93c1swXS5jaXJjbGVfcG9pbnRfaWQ7XHJcbiAgICAgICAgYXdhaXQgdGhpcy5fdXBkYXRlUG9pbnQoIGNsaWVudCwgcG9pbnRJZCwgY2lyY2xlLmdldENlbnRlcigpICk7XHJcbiAgICAgICAgYXdhaXQgY2xpZW50LnF1ZXJ5KFxyXG4gICAgICAgICAgICBcIlVQREFURSBJT0RTaGFwZSBTRVQgY2lyY2xlX3JhZGl1cyA9ICQyIFdIRVJFIHNoYXBlX2lkID0gJDFcIixcclxuICAgICAgICAgICAgW3NoYXBlSWQsIGNpcmNsZS5nZXRSYWRpdXMoKV0pO1xyXG4gICAgICAgIHJldHVybiBzaGFwZUlkO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhc3luYyBfaW5zZXJ0UmVjdGFuZ2xlU2hhcGUoIGNsaWVudDogYW55LCByZWN0OiBSZWN0YW5nbGUgKTogUHJvbWlzZTxzdHJpbmc+IHtcclxuICAgICAgICBsZXQgc2hhcGVJZDogc3RyaW5nID0gVXVpZC5nZXRVdWlkKCk7XHJcbiAgICAgICAgbGV0IHBvaW50SWQ6IHN0cmluZyA9IGF3YWl0IHRoaXMuX2luc2VydFBvaW50KCBjbGllbnQsIHJlY3QucG9zaXRpb24oKSApO1xyXG4gICAgICAgIGF3YWl0IGNsaWVudC5xdWVyeShcclxuICAgICAgICAgICAgXCJJTlNFUlQgSU5UTyBJT0RTaGFwZSAoc2hhcGVfaWQsIHNoYXBlX3R5cGUsIHJlY3RhbmdsZV9wb2ludF9pZCwgcmVjdGFuZ2xlX3dpZHRoLCByZWN0YW5nbGVfaGVpZ2h0KSBcIiArXHJcbiAgICAgICAgICAgIFwiVkFMVUVTICgkMSwgJDIsICQzLCAkNCwgJDUpIFwiLFxyXG4gICAgICAgICAgICBbc2hhcGVJZCwgcmVjdC50eXBlLCBwb2ludElkLCByZWN0LndpZHRoKCksIHJlY3QuaGVpZ2h0KCldKTtcclxuICAgICAgICByZXR1cm4gc2hhcGVJZDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXN5bmMgX3VwZGF0ZVJlY3RhbmdsZVNoYXBlKCBjbGllbnQ6IGFueSwgc2hhcGVJZDogc3RyaW5nLCByZWN0OiBSZWN0YW5nbGUgKTogUHJvbWlzZTxzdHJpbmc+IHtcclxuICAgICAgICBsZXQgcmVzdWx0U2V0OiBhbnkgPSBhd2FpdCBjbGllbnQucXVlcnkoXCJTRUxFQ1QgcmVjdGFuZ2xlX3BvaW50X2lkIGZyb20gSU9EU2hhcGUgV0hFUkUgc2hhcGVfaWQgPSAkMVwiLCBbc2hhcGVJZF0pO1xyXG4gICAgICAgIGxldCBwb2ludElkID0gcmVzdWx0U2V0LnJvd3NbMF0ucmVjdGFuZ2xlX3BvaW50X2lkO1xyXG4gICAgICAgIGF3YWl0IHRoaXMuX3VwZGF0ZVBvaW50KCBjbGllbnQsIHBvaW50SWQsIHJlY3QucG9zaXRpb24oKSApO1xyXG4gICAgICAgIGF3YWl0IGNsaWVudC5xdWVyeShcclxuICAgICAgICAgICAgXCJVUERBVEUgSU9EU2hhcGUgU0VUIHJlY3RhbmdsZV93aWR0aCA9ICQyLCByZWN0YW5nbGVfaGVpZ2h0ID0gJDMgV0hFUkUgc2hhcGVfaWQgPSAkMVwiLFxyXG4gICAgICAgICAgICBbc2hhcGVJZCwgcmVjdC53aWR0aCgpLCByZWN0LmhlaWdodCgpXSk7XHJcbiAgICAgICAgcmV0dXJuIHNoYXBlSWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFzeW5jIF9pbnNlcnRQb2ludCggY2xpZW50OiBhbnksIHBvaW50OiBQb2ludCApOiBQcm9taXNlPHN0cmluZz4ge1xyXG4gICAgICAgIGxldCBwb2ludElkOiBzdHJpbmcgPSBVdWlkLmdldFV1aWQoKTtcclxuICAgICAgICBsZXQgcmVzdWx0ID0gYXdhaXQgY2xpZW50LnF1ZXJ5KFxyXG4gICAgICAgICAgICBcIklOU0VSVCBJTlRPIElPRFBvaW50IChwb2ludF9pZCwgcG9pbnRfeCwgcG9pbnRfeSwgcG9pbnRfeikgXCIgK1xyXG4gICAgICAgICAgICBcIlZBTFVFUyAoJDEsICQyLCAkMywgJDQpIFwiLFxyXG4gICAgICAgICAgICBbcG9pbnRJZCwgcG9pbnQueCwgcG9pbnQueSwgcG9pbnQuel0pO1xyXG4gICAgICAgIHJldHVybiBwb2ludElkO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhc3luYyBfdXBkYXRlUG9pbnQoIGNsaWVudDogYW55LCBwb2ludElkOiBzdHJpbmcsIHBvaW50OiBQb2ludCApOiBQcm9taXNlPHN0cmluZz4ge1xyXG4gICAgICAgIGF3YWl0IGNsaWVudC5xdWVyeShcclxuICAgICAgICAgICAgXCJVUERBVEUgSU9EUG9pbnQgU0VUIHBvaW50X3ggPSAkMSwgcG9pbnRfeSA9ICQyLCBwb2ludF96ID0gJDMgV0hFUkUgcG9pbnRfaWQgPSAkNCBcIixcclxuICAgICAgICAgICAgW3BvaW50LngsIHBvaW50LnksIHBvaW50LnosIHBvaW50SWRdKTtcclxuICAgICAgICByZXR1cm4gcG9pbnRJZDtcclxuICAgIH1cclxuXHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBCYXNlUGdTdG9yYWdlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
