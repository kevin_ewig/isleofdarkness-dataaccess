"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BaseStorage = /** @class */ (function () {
    function BaseStorage(redisClient) {
        if (redisClient) {
            this._client = redisClient;
        }
        else {
            throw new Error("No Redis Client defined");
        }
    }
    /**
     * Get an array of strings as a list.
     * @param key
     */
    BaseStorage.prototype.getListAsync = function (key) {
        var redisClient = this._client;
        var promise = new Promise(function (resolve, reject) {
            var numberOfItems = 0;
            function afterLRange(err, result) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(result);
                }
            }
            function afterLlen(err, lenOfList) {
                if (err) {
                    reject(err);
                }
                else {
                    numberOfItems = lenOfList;
                    redisClient.lrange(key, 0, numberOfItems, afterLRange);
                }
            }
            redisClient.llen(key, afterLlen);
        });
        return promise;
    };
    /**
     * Execute transaction.
     */
    BaseStorage.prototype.execute = function () {
        var redisClient = this._client;
        var promise = new Promise(function (resolve, reject) {
            function afterExec(err, result) {
                if (err) {
                    reject();
                }
                else {
                    resolve();
                }
            }
            redisClient.exec(afterExec);
        });
        return promise;
    };
    /**
     * Start multiple transaction.
     */
    BaseStorage.prototype.multi = function () {
        var redisClient = this._client;
        var promise = new Promise(function (resolve, reject) {
            function afterMulti(err, result) {
                if (err) {
                    reject();
                }
                else {
                    resolve();
                }
            }
            redisClient.multi(afterMulti);
        });
        return promise;
    };
    /**
     * Set and replace a list of strings in memory.
     * @param set
     */
    BaseStorage.prototype.setListAsync = function (key, list) {
        var redisClient = this._client;
        var promise = new Promise(function (resolve, reject) {
            function afterLPush(err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve();
                }
            }
            function afterDel(err) {
                if (err) {
                    reject(err);
                }
                else {
                    if (list) {
                        if (list.length > 0) {
                            redisClient.lpush(key, list, afterLPush);
                        }
                        else {
                            resolve();
                        }
                    }
                    else {
                        resolve();
                    }
                }
            }
            // Delete all elements first.
            redisClient.del(key, afterDel);
        });
        return promise;
    };
    return BaseStorage;
}());
exports.BaseStorage = BaseStorage;
exports.default = BaseStorage;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvYmFzZXN0b3JhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQTtJQUlJLHFCQUFhLFdBQWdCO1FBQ3pCLEVBQUUsQ0FBQyxDQUFFLFdBQVksQ0FBQyxDQUFDLENBQUM7WUFDaEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUM7UUFDL0IsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQy9DLENBQUM7SUFDTCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksa0NBQVksR0FBbkIsVUFBb0IsR0FBVztRQUUzQixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQy9CLElBQUksT0FBTyxHQUEyQixJQUFJLE9BQU8sQ0FBZ0IsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUU3RSxJQUFJLGFBQWEsR0FBRyxDQUFDLENBQUM7WUFFdEIscUJBQXFCLEdBQVEsRUFBRSxNQUFxQjtnQkFDaEQsRUFBRSxDQUFDLENBQUUsR0FBSSxDQUFDLENBQUMsQ0FBQztvQkFDUixNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2hCLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNwQixDQUFDO1lBQ0wsQ0FBQztZQUVELG1CQUFtQixHQUFRLEVBQUUsU0FBaUI7Z0JBQzFDLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNoQixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLGFBQWEsR0FBRyxTQUFTLENBQUM7b0JBQzFCLFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxhQUFhLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQzNELENBQUM7WUFDTCxDQUFDO1lBRUQsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFFckMsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBRW5CLENBQUM7SUFFRDs7T0FFRztJQUNJLDZCQUFPLEdBQWQ7UUFFSSxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBRS9CLElBQUksT0FBTyxHQUFrQixJQUFJLE9BQU8sQ0FBTyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRTNELG1CQUFtQixHQUFRLEVBQUUsTUFBVztnQkFDcEMsRUFBRSxDQUFDLENBQUUsR0FBSSxDQUFDLENBQUMsQ0FBQztvQkFDUixNQUFNLEVBQUUsQ0FBQztnQkFDYixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLE9BQU8sRUFBRSxDQUFDO2dCQUNkLENBQUM7WUFDTCxDQUFDO1lBRUQsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUVoQyxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFFbkIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksMkJBQUssR0FBWjtRQUVJLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFFL0IsSUFBSSxPQUFPLEdBQWtCLElBQUksT0FBTyxDQUFPLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFM0Qsb0JBQW9CLEdBQVEsRUFBRSxNQUFXO2dCQUNyQyxFQUFFLENBQUMsQ0FBRSxHQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNSLE1BQU0sRUFBRSxDQUFDO2dCQUNiLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osT0FBTyxFQUFFLENBQUM7Z0JBQ2QsQ0FBQztZQUNMLENBQUM7WUFFRCxXQUFXLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRWxDLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUVuQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksa0NBQVksR0FBbkIsVUFBb0IsR0FBVyxFQUFFLElBQW1CO1FBRWhELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFFL0IsSUFBSSxPQUFPLEdBQWtCLElBQUksT0FBTyxDQUFPLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFM0Qsb0JBQW9CLEdBQVE7Z0JBQ3hCLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNoQixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLE9BQU8sRUFBRSxDQUFDO2dCQUNkLENBQUM7WUFDTCxDQUFDO1lBRUQsa0JBQWtCLEdBQVE7Z0JBQ3RCLEVBQUUsQ0FBQyxDQUFFLEdBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNoQixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLEVBQUUsQ0FBQyxDQUFFLElBQUssQ0FBQyxDQUFDLENBQUM7d0JBQ1QsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFFLENBQUMsQ0FBQyxDQUFDOzRCQUNwQixXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7d0JBQzdDLENBQUM7d0JBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ0osT0FBTyxFQUFFLENBQUM7d0JBQ2QsQ0FBQztvQkFDTCxDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNKLE9BQU8sRUFBRSxDQUFDO29CQUNkLENBQUM7Z0JBQ0wsQ0FBQztZQUNMLENBQUM7WUFFRCw2QkFBNkI7WUFDN0IsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFFbkMsQ0FBQyxDQUFDLENBQUM7UUFDSCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBRW5CLENBQUM7SUFFTCxrQkFBQztBQUFELENBNUlBLEFBNElDLElBQUE7QUE1SVksa0NBQVc7QUE4SXhCLGtCQUFlLFdBQVcsQ0FBQyIsImZpbGUiOiJzdG9yYWdlL2Jhc2VzdG9yYWdlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEJhc2VTdG9yYWdlIHtcclxuXHJcbiAgICBwcml2YXRlIF9jbGllbnQ6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvciggcmVkaXNDbGllbnQ6IGFueSApIHtcclxuICAgICAgICBpZiAoIHJlZGlzQ2xpZW50ICkge1xyXG4gICAgICAgICAgICB0aGlzLl9jbGllbnQgPSByZWRpc0NsaWVudDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJObyBSZWRpcyBDbGllbnQgZGVmaW5lZFwiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgYW4gYXJyYXkgb2Ygc3RyaW5ncyBhcyBhIGxpc3QuXHJcbiAgICAgKiBAcGFyYW0ga2V5XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRMaXN0QXN5bmMoa2V5OiBzdHJpbmcpOiBQcm9taXNlPEFycmF5PHN0cmluZz4+IHtcclxuXHJcbiAgICAgICAgbGV0IHJlZGlzQ2xpZW50ID0gdGhpcy5fY2xpZW50O1xyXG4gICAgICAgIGxldCBwcm9taXNlOiBQcm9taXNlPEFycmF5PHN0cmluZz4+ID0gbmV3IFByb21pc2U8QXJyYXk8c3RyaW5nPj4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cclxuICAgICAgICAgICAgbGV0IG51bWJlck9mSXRlbXMgPSAwO1xyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gYWZ0ZXJMUmFuZ2UoZXJyOiBhbnksIHJlc3VsdDogQXJyYXk8c3RyaW5nPikge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBlcnIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycik7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUocmVzdWx0KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gYWZ0ZXJMbGVuKGVycjogYW55LCBsZW5PZkxpc3Q6IG51bWJlcikge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBlcnIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycik7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG51bWJlck9mSXRlbXMgPSBsZW5PZkxpc3Q7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVkaXNDbGllbnQubHJhbmdlKGtleSwgMCwgbnVtYmVyT2ZJdGVtcywgYWZ0ZXJMUmFuZ2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZWRpc0NsaWVudC5sbGVuKGtleSwgYWZ0ZXJMbGVuKTtcclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEV4ZWN1dGUgdHJhbnNhY3Rpb24uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBleGVjdXRlKCk6IFByb21pc2U8dm9pZD4ge1xyXG5cclxuICAgICAgICBsZXQgcmVkaXNDbGllbnQgPSB0aGlzLl9jbGllbnQ7XHJcblxyXG4gICAgICAgIGxldCBwcm9taXNlOiBQcm9taXNlPHZvaWQ+ID0gbmV3IFByb21pc2U8dm9pZD4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gYWZ0ZXJFeGVjKGVycjogYW55LCByZXN1bHQ6IGFueSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBlcnIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmVkaXNDbGllbnQuZXhlYyhhZnRlckV4ZWMpO1xyXG5cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHByb21pc2U7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU3RhcnQgbXVsdGlwbGUgdHJhbnNhY3Rpb24uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBtdWx0aSgpOiBQcm9taXNlPHZvaWQ+IHtcclxuXHJcbiAgICAgICAgbGV0IHJlZGlzQ2xpZW50ID0gdGhpcy5fY2xpZW50O1xyXG5cclxuICAgICAgICBsZXQgcHJvbWlzZTogUHJvbWlzZTx2b2lkPiA9IG5ldyBQcm9taXNlPHZvaWQ+KChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIGFmdGVyTXVsdGkoZXJyOiBhbnksIHJlc3VsdDogYW55KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIGVyciApIHtcclxuICAgICAgICAgICAgICAgICAgICByZWplY3QoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZWRpc0NsaWVudC5tdWx0aShhZnRlck11bHRpKTtcclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldCBhbmQgcmVwbGFjZSBhIGxpc3Qgb2Ygc3RyaW5ncyBpbiBtZW1vcnkuXHJcbiAgICAgKiBAcGFyYW0gc2V0XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzZXRMaXN0QXN5bmMoa2V5OiBzdHJpbmcsIGxpc3Q6IEFycmF5PHN0cmluZz4pOiBQcm9taXNlPHZvaWQ+IHtcclxuXHJcbiAgICAgICAgbGV0IHJlZGlzQ2xpZW50ID0gdGhpcy5fY2xpZW50O1xyXG5cclxuICAgICAgICBsZXQgcHJvbWlzZTogUHJvbWlzZTx2b2lkPiA9IG5ldyBQcm9taXNlPHZvaWQ+KChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIGFmdGVyTFB1c2goZXJyOiBhbnkpIHtcclxuICAgICAgICAgICAgICAgIGlmICggZXJyICkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIGFmdGVyRGVsKGVycjogYW55KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIGVyciApIHtcclxuICAgICAgICAgICAgICAgICAgICByZWplY3QoZXJyKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCBsaXN0ICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIGxpc3QubGVuZ3RoID4gMCApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlZGlzQ2xpZW50LmxwdXNoKGtleSwgbGlzdCwgYWZ0ZXJMUHVzaCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBEZWxldGUgYWxsIGVsZW1lbnRzIGZpcnN0LlxyXG4gICAgICAgICAgICByZWRpc0NsaWVudC5kZWwoa2V5LCBhZnRlckRlbCk7XHJcblxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiBwcm9taXNlO1xyXG5cclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEJhc2VTdG9yYWdlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
