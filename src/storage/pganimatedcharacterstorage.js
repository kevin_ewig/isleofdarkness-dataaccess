"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var missingconstructorargumenterror_1 = require("isleofdarkness-common/src/error/missingconstructorargumenterror");
var ibasestorage_1 = require("./ibasestorage");
var basepgstorage_1 = require("./basepgstorage");
var animatedcharacter_1 = require("isleofdarkness-common/src/model/animatedcharacter");
var PgAnimatedCharacterStorage = /** @class */ (function (_super) {
    __extends(PgAnimatedCharacterStorage, _super);
    function PgAnimatedCharacterStorage(pool, pgObjectModelStorage, pgAnimationProfileStorage) {
        var _this = _super.call(this, pool) || this;
        if (pgAnimationProfileStorage && pgObjectModelStorage) {
            _this._pgObjectModelStorage = pgObjectModelStorage;
            _this._pgAnimationProfileStorage = pgAnimationProfileStorage;
        }
        else {
            throw new missingconstructorargumenterror_1.MissingConstructorArgumentError();
        }
        return _this;
    }
    /**
     * Return the type of this storage.
     */
    PgAnimatedCharacterStorage.prototype.getStorageType = function () {
        return ibasestorage_1.StorageType.BaseAnimatedCharacter;
    };
    /**
     * Add or update the animated character into the database.
     * @param animatedCharacter
     */
    PgAnimatedCharacterStorage.prototype.addUpdateAnimatedCharacter = function (animatedCharacter) {
        return __awaiter(this, void 0, void 0, function () {
            var result, pool, animationProfileId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._pgObjectModelStorage.addUpdateObjectModel(animatedCharacter)];
                    case 1:
                        result = _a.sent();
                        pool = this.getPool();
                        animationProfileId = null;
                        if (animatedCharacter.animationProfile) {
                            animationProfileId = animatedCharacter.animationProfile.id;
                        }
                        return [4 /*yield*/, pool.query("INSERT INTO iodanimatedcharacter (objectmodel_id, animationprofile_id, speed) " +
                                "VALUES ($1, $2, $3) " +
                                "ON CONFLICT (objectmodel_id) DO UPDATE SET animationprofile_id = $2, speed = $3", [animatedCharacter.id,
                                animationProfileId,
                                animatedCharacter.speed])];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * Get a base animated character object.
     */
    PgAnimatedCharacterStorage.prototype.getAnimatedCharacter = function (animatedCharacterId) {
        return __awaiter(this, void 0, void 0, function () {
            var pool, result, row, objectModelId, animationProfileId, speed, animatedCharacter, objectModel, animationProfile;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        pool = this.getPool();
                        return [4 /*yield*/, pool.query("SELECT a.objectmodel_id, a.animationprofile_id, a.speed " +
                                "  FROM iodanimatedcharacter AS a " +
                                " WHERE a.objectmodel_id = $1", [animatedCharacterId])];
                    case 1:
                        result = _a.sent();
                        row = result.rows[0];
                        objectModelId = row.objectmodel_id;
                        animationProfileId = row.animationprofile_id;
                        speed = row.speed;
                        animatedCharacter = null;
                        return [4 /*yield*/, this._pgObjectModelStorage.getObjectModel(objectModelId)];
                    case 2:
                        objectModel = _a.sent();
                        if (!objectModel) return [3 /*break*/, 4];
                        return [4 /*yield*/, this._pgAnimationProfileStorage.getAnimationProfile(animationProfileId)];
                    case 3:
                        animationProfile = _a.sent();
                        animatedCharacter =
                            new animatedcharacter_1.AnimatedCharacter(animatedCharacterId, objectModel.shape, objectModel.modelName);
                        animatedCharacter.worldName = objectModel.worldName;
                        animatedCharacter.displayName = objectModel.displayName;
                        animatedCharacter.ownerUserId = objectModel.ownerUserId;
                        animatedCharacter.objectType = objectModel.objectType;
                        animatedCharacter.modelTexture = objectModel.modelTexture;
                        animatedCharacter.animationProfile = animationProfile;
                        animatedCharacter.speed = speed;
                        _a.label = 4;
                    case 4: return [2 /*return*/, animatedCharacter];
                }
            });
        });
    };
    /**
     * Delete an animated character.
     */
    PgAnimatedCharacterStorage.prototype.deleteAnimatedCharacter = function (objectModelId) {
        return __awaiter(this, void 0, void 0, function () {
            var pool, result1, result2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        pool = this.getPool();
                        return [4 /*yield*/, pool.query("DELETE FROM iodanimatedcharacter WHERE objectmodel_id = $1", [objectModelId])];
                    case 1:
                        result1 = _a.sent();
                        return [4 /*yield*/, pool.query("DELETE FROM iodobjectmodel WHERE objectmodel_id = $1", [objectModelId])];
                    case 2:
                        result2 = _a.sent();
                        return [2 /*return*/, result1 && result2];
                }
            });
        });
    };
    return PgAnimatedCharacterStorage;
}(basepgstorage_1.default));
exports.PgAnimatedCharacterStorage = PgAnimatedCharacterStorage;
exports.default = PgAnimatedCharacterStorage;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvcGdhbmltYXRlZGNoYXJhY3RlcnN0b3JhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHQSxtSEFBa0g7QUFDbEgsK0NBQTZDO0FBQzdDLGlEQUE0QztBQUs1Qyx1RkFBc0Y7QUFFdEY7SUFBZ0QsOENBQWE7SUFLekQsb0NBQVksSUFBUyxFQUNULG9CQUEyQyxFQUMzQyx5QkFBcUQ7UUFGakUsWUFHSSxrQkFBTSxJQUFJLENBQUMsU0FPZDtRQU5HLEVBQUUsQ0FBQyxDQUFFLHlCQUF5QixJQUFJLG9CQUFxQixDQUFDLENBQUMsQ0FBQztZQUN0RCxLQUFJLENBQUMscUJBQXFCLEdBQUcsb0JBQW9CLENBQUM7WUFDbEQsS0FBSSxDQUFDLDBCQUEwQixHQUFHLHlCQUF5QixDQUFDO1FBQ2hFLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSxpRUFBK0IsRUFBRSxDQUFDO1FBQ2hELENBQUM7O0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0ksbURBQWMsR0FBckI7UUFDSSxNQUFNLENBQUMsMEJBQVcsQ0FBQyxxQkFBcUIsQ0FBQztJQUM3QyxDQUFDO0lBRUQ7OztPQUdHO0lBQ1UsK0RBQTBCLEdBQXZDLFVBQXlDLGlCQUFvQzs7Ozs7NEJBRTVELHFCQUFNLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxvQkFBb0IsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFBOzt3QkFBakYsTUFBTSxHQUFHLFNBQXdFO3dCQUNqRixJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUN0QixrQkFBa0IsR0FBUSxJQUFJLENBQUM7d0JBQ25DLEVBQUUsQ0FBQyxDQUFFLGlCQUFpQixDQUFDLGdCQUFpQixDQUFDLENBQUMsQ0FBQzs0QkFDdkMsa0JBQWtCLEdBQUcsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxDQUFDO3dCQUMvRCxDQUFDO3dCQUNELHFCQUFNLElBQUksQ0FBQyxLQUFLLENBQ1osZ0ZBQWdGO2dDQUNoRixzQkFBc0I7Z0NBQ3RCLGlGQUFpRixFQUNqRixDQUFDLGlCQUFpQixDQUFDLEVBQUU7Z0NBQ3BCLGtCQUFrQjtnQ0FDbEIsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQzVCLEVBQUE7O3dCQVBELFNBT0MsQ0FBQzt3QkFDRixzQkFBTyxNQUFNLEVBQUM7Ozs7S0FDakI7SUFFRDs7T0FFRztJQUNVLHlEQUFvQixHQUFqQyxVQUFtQyxtQkFBMkI7Ozs7Ozt3QkFFdEQsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDYixxQkFBTSxJQUFJLENBQUMsS0FBSyxDQUN6QiwwREFBMEQ7Z0NBQzFELG1DQUFtQztnQ0FDbkMsOEJBQThCLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLEVBQUE7O3dCQUh0RCxNQUFNLEdBQUcsU0FHNkM7d0JBRXRELEdBQUcsR0FBUSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUUxQixhQUFhLEdBQVcsR0FBRyxDQUFDLGNBQWMsQ0FBQzt3QkFDM0Msa0JBQWtCLEdBQVcsR0FBRyxDQUFDLG1CQUFtQixDQUFDO3dCQUNyRCxLQUFLLEdBQVcsR0FBRyxDQUFDLEtBQUssQ0FBQzt3QkFFMUIsaUJBQWlCLEdBQTJCLElBQUksQ0FBQzt3QkFDakIscUJBQU0sSUFBSSxDQUFDLHFCQUFxQixDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsRUFBQTs7d0JBQTlGLFdBQVcsR0FBcUIsU0FBOEQ7NkJBQzdGLFdBQVcsRUFBWCx3QkFBVzt3QkFFVyxxQkFBTSxJQUFJLENBQUMsMEJBQTBCLENBQUMsbUJBQW1CLENBQUMsa0JBQWtCLENBQUMsRUFBQTs7d0JBQWhHLGdCQUFnQixHQUFHLFNBQTZFO3dCQUNwRyxpQkFBaUI7NEJBQ2IsSUFBSSxxQ0FBaUIsQ0FBQyxtQkFBbUIsRUFBRSxXQUFXLENBQUMsS0FBSyxFQUFFLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQzt3QkFDekYsaUJBQWlCLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUM7d0JBQ3BELGlCQUFpQixDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDO3dCQUN4RCxpQkFBaUIsQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDLFdBQVcsQ0FBQzt3QkFDeEQsaUJBQWlCLENBQUMsVUFBVSxHQUFHLFdBQVcsQ0FBQyxVQUFVLENBQUM7d0JBQ3RELGlCQUFpQixDQUFDLFlBQVksR0FBRyxXQUFXLENBQUMsWUFBWSxDQUFDO3dCQUUxRCxpQkFBaUIsQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQzt3QkFDdEQsaUJBQWlCLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQzs7NEJBR3BDLHNCQUFPLGlCQUFpQixFQUFDOzs7O0tBRTVCO0lBRUQ7O09BRUc7SUFDVSw0REFBdUIsR0FBcEMsVUFBc0MsYUFBcUI7Ozs7Ozt3QkFDbkQsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDWixxQkFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLDREQUE0RCxFQUFFLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBQTs7d0JBQXpHLE9BQU8sR0FBRyxTQUErRjt3QkFDL0YscUJBQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxzREFBc0QsRUFBRSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUE7O3dCQUFuRyxPQUFPLEdBQUcsU0FBeUY7d0JBQ3ZHLHNCQUFPLE9BQU8sSUFBSSxPQUFPLEVBQUM7Ozs7S0FDN0I7SUFFTCxpQ0FBQztBQUFELENBL0ZBLEFBK0ZDLENBL0YrQyx1QkFBYSxHQStGNUQ7QUEvRlksZ0VBQTBCO0FBaUd2QyxrQkFBZSwwQkFBMEIsQ0FBQyIsImZpbGUiOiJzdG9yYWdlL3BnYW5pbWF0ZWRjaGFyYWN0ZXJzdG9yYWdlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEJhc2VBbmltYXRlZENoYXJhY3RlciBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tb2RlbC9iYXNlYW5pbWF0ZWRjaGFyYWN0ZXJcIjtcclxuaW1wb3J0IE9iamVjdE1vZGVsIGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21vZGVsL29iamVjdG1vZGVsXCI7XHJcbmltcG9ydCBTaGFwZSBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tYXRoL3NoYXBlXCI7XHJcbmltcG9ydCB7IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IgfSBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcbmltcG9ydCB7IFN0b3JhZ2VUeXBlIH0gZnJvbSBcIi4vaWJhc2VzdG9yYWdlXCI7XHJcbmltcG9ydCBCYXNlUGdTdG9yYWdlIGZyb20gXCIuL2Jhc2VwZ3N0b3JhZ2VcIjtcclxuaW1wb3J0IElQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlIGZyb20gXCIuL2lwZ2FuaW1hdGlvbnByb2ZpbGVzdG9yYWdlXCI7XHJcbmltcG9ydCBJUGdBbmltYXRlZENoYXJhY3RlclN0b3JhZ2UgZnJvbSBcIi4vaXBnYW5pbWF0ZWRjaGFyYWN0ZXJzdG9yYWdlXCI7XHJcbmltcG9ydCBJUGdCYXNlQW5pbWF0ZWRDaGFyYWN0ZXJTdG9yYWdlIGZyb20gXCIuL2lwZ2Jhc2VhbmltYXRlZGNoYXJhY3RlcnN0b3JhZ2VcIjtcclxuaW1wb3J0IHsgSVBnT2JqZWN0TW9kZWxTdG9yYWdlIH0gZnJvbSBcIi4vaXBnb2JqZWN0bW9kZWxzdG9yYWdlXCI7XHJcbmltcG9ydCB7IEFuaW1hdGVkQ2hhcmFjdGVyIH0gZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbW9kZWwvYW5pbWF0ZWRjaGFyYWN0ZXJcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBQZ0FuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZSBleHRlbmRzIEJhc2VQZ1N0b3JhZ2UgaW1wbGVtZW50cyBJUGdBbmltYXRlZENoYXJhY3RlclN0b3JhZ2Uge1xyXG5cclxuICAgIHByaXZhdGUgX3BnQW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2U6IElQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlO1xyXG4gICAgcHJpdmF0ZSBfcGdPYmplY3RNb2RlbFN0b3JhZ2U6IElQZ09iamVjdE1vZGVsU3RvcmFnZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwb29sOiBhbnksXHJcbiAgICAgICAgICAgICAgICBwZ09iamVjdE1vZGVsU3RvcmFnZTogSVBnT2JqZWN0TW9kZWxTdG9yYWdlLFxyXG4gICAgICAgICAgICAgICAgcGdBbmltYXRpb25Qcm9maWxlU3RvcmFnZTogSVBnQW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2UpIHtcclxuICAgICAgICBzdXBlcihwb29sKTtcclxuICAgICAgICBpZiAoIHBnQW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2UgJiYgcGdPYmplY3RNb2RlbFN0b3JhZ2UgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3BnT2JqZWN0TW9kZWxTdG9yYWdlID0gcGdPYmplY3RNb2RlbFN0b3JhZ2U7XHJcbiAgICAgICAgICAgIHRoaXMuX3BnQW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2UgPSBwZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJuIHRoZSB0eXBlIG9mIHRoaXMgc3RvcmFnZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFN0b3JhZ2VUeXBlKCk6IFN0b3JhZ2VUeXBlIHtcclxuICAgICAgICByZXR1cm4gU3RvcmFnZVR5cGUuQmFzZUFuaW1hdGVkQ2hhcmFjdGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkIG9yIHVwZGF0ZSB0aGUgYW5pbWF0ZWQgY2hhcmFjdGVyIGludG8gdGhlIGRhdGFiYXNlLlxyXG4gICAgICogQHBhcmFtIGFuaW1hdGVkQ2hhcmFjdGVyXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBhc3luYyBhZGRVcGRhdGVBbmltYXRlZENoYXJhY3RlciggYW5pbWF0ZWRDaGFyYWN0ZXI6IEFuaW1hdGVkQ2hhcmFjdGVyICk6IFByb21pc2U8Ym9vbGVhbj4ge1xyXG5cclxuICAgICAgICBsZXQgcmVzdWx0ID0gYXdhaXQgdGhpcy5fcGdPYmplY3RNb2RlbFN0b3JhZ2UuYWRkVXBkYXRlT2JqZWN0TW9kZWwoYW5pbWF0ZWRDaGFyYWN0ZXIpO1xyXG4gICAgICAgIGxldCBwb29sID0gdGhpcy5nZXRQb29sKCk7XHJcbiAgICAgICAgbGV0IGFuaW1hdGlvblByb2ZpbGVJZDogYW55ID0gbnVsbDtcclxuICAgICAgICBpZiAoIGFuaW1hdGVkQ2hhcmFjdGVyLmFuaW1hdGlvblByb2ZpbGUgKSB7XHJcbiAgICAgICAgICAgIGFuaW1hdGlvblByb2ZpbGVJZCA9IGFuaW1hdGVkQ2hhcmFjdGVyLmFuaW1hdGlvblByb2ZpbGUuaWQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGF3YWl0IHBvb2wucXVlcnkoXHJcbiAgICAgICAgICAgIFwiSU5TRVJUIElOVE8gaW9kYW5pbWF0ZWRjaGFyYWN0ZXIgKG9iamVjdG1vZGVsX2lkLCBhbmltYXRpb25wcm9maWxlX2lkLCBzcGVlZCkgXCIgK1xyXG4gICAgICAgICAgICBcIlZBTFVFUyAoJDEsICQyLCAkMykgXCIgK1xyXG4gICAgICAgICAgICBcIk9OIENPTkZMSUNUIChvYmplY3Rtb2RlbF9pZCkgRE8gVVBEQVRFIFNFVCBhbmltYXRpb25wcm9maWxlX2lkID0gJDIsIHNwZWVkID0gJDNcIixcclxuICAgICAgICAgICAgW2FuaW1hdGVkQ2hhcmFjdGVyLmlkLFxyXG4gICAgICAgICAgICAgYW5pbWF0aW9uUHJvZmlsZUlkLFxyXG4gICAgICAgICAgICAgYW5pbWF0ZWRDaGFyYWN0ZXIuc3BlZWRdXHJcbiAgICAgICAgKTtcclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IGEgYmFzZSBhbmltYXRlZCBjaGFyYWN0ZXIgb2JqZWN0LlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgYXN5bmMgZ2V0QW5pbWF0ZWRDaGFyYWN0ZXIoIGFuaW1hdGVkQ2hhcmFjdGVySWQ6IHN0cmluZyApOiBQcm9taXNlPEFuaW1hdGVkQ2hhcmFjdGVyfG51bGw+IHtcclxuXHJcbiAgICAgICAgbGV0IHBvb2wgPSB0aGlzLmdldFBvb2woKTtcclxuICAgICAgICBsZXQgcmVzdWx0ID0gYXdhaXQgcG9vbC5xdWVyeShcclxuICAgICAgICAgICAgXCJTRUxFQ1QgYS5vYmplY3Rtb2RlbF9pZCwgYS5hbmltYXRpb25wcm9maWxlX2lkLCBhLnNwZWVkIFwiICtcclxuICAgICAgICAgICAgXCIgIEZST00gaW9kYW5pbWF0ZWRjaGFyYWN0ZXIgQVMgYSBcIiArXHJcbiAgICAgICAgICAgIFwiIFdIRVJFIGEub2JqZWN0bW9kZWxfaWQgPSAkMVwiLCBbYW5pbWF0ZWRDaGFyYWN0ZXJJZF0pO1xyXG5cclxuICAgICAgICBsZXQgcm93OiBhbnkgPSByZXN1bHQucm93c1swXTtcclxuXHJcbiAgICAgICAgbGV0IG9iamVjdE1vZGVsSWQ6IHN0cmluZyA9IHJvdy5vYmplY3Rtb2RlbF9pZDtcclxuICAgICAgICBsZXQgYW5pbWF0aW9uUHJvZmlsZUlkOiBzdHJpbmcgPSByb3cuYW5pbWF0aW9ucHJvZmlsZV9pZDtcclxuICAgICAgICBsZXQgc3BlZWQ6IG51bWJlciA9IHJvdy5zcGVlZDtcclxuXHJcbiAgICAgICAgbGV0IGFuaW1hdGVkQ2hhcmFjdGVyOiBBbmltYXRlZENoYXJhY3RlcnxudWxsID0gbnVsbDtcclxuICAgICAgICBsZXQgb2JqZWN0TW9kZWw6IE9iamVjdE1vZGVsfG51bGwgPSBhd2FpdCB0aGlzLl9wZ09iamVjdE1vZGVsU3RvcmFnZS5nZXRPYmplY3RNb2RlbChvYmplY3RNb2RlbElkKTtcclxuICAgICAgICBpZiAoIG9iamVjdE1vZGVsICkge1xyXG5cclxuICAgICAgICAgICAgbGV0IGFuaW1hdGlvblByb2ZpbGUgPSBhd2FpdCB0aGlzLl9wZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlLmdldEFuaW1hdGlvblByb2ZpbGUoYW5pbWF0aW9uUHJvZmlsZUlkKTtcclxuICAgICAgICAgICAgYW5pbWF0ZWRDaGFyYWN0ZXIgPVxyXG4gICAgICAgICAgICAgICAgbmV3IEFuaW1hdGVkQ2hhcmFjdGVyKGFuaW1hdGVkQ2hhcmFjdGVySWQsIG9iamVjdE1vZGVsLnNoYXBlLCBvYmplY3RNb2RlbC5tb2RlbE5hbWUpO1xyXG4gICAgICAgICAgICBhbmltYXRlZENoYXJhY3Rlci53b3JsZE5hbWUgPSBvYmplY3RNb2RlbC53b3JsZE5hbWU7XHJcbiAgICAgICAgICAgIGFuaW1hdGVkQ2hhcmFjdGVyLmRpc3BsYXlOYW1lID0gb2JqZWN0TW9kZWwuZGlzcGxheU5hbWU7XHJcbiAgICAgICAgICAgIGFuaW1hdGVkQ2hhcmFjdGVyLm93bmVyVXNlcklkID0gb2JqZWN0TW9kZWwub3duZXJVc2VySWQ7XHJcbiAgICAgICAgICAgIGFuaW1hdGVkQ2hhcmFjdGVyLm9iamVjdFR5cGUgPSBvYmplY3RNb2RlbC5vYmplY3RUeXBlO1xyXG4gICAgICAgICAgICBhbmltYXRlZENoYXJhY3Rlci5tb2RlbFRleHR1cmUgPSBvYmplY3RNb2RlbC5tb2RlbFRleHR1cmU7XHJcblxyXG4gICAgICAgICAgICBhbmltYXRlZENoYXJhY3Rlci5hbmltYXRpb25Qcm9maWxlID0gYW5pbWF0aW9uUHJvZmlsZTtcclxuICAgICAgICAgICAgYW5pbWF0ZWRDaGFyYWN0ZXIuc3BlZWQgPSBzcGVlZDtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBhbmltYXRlZENoYXJhY3RlcjtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZWxldGUgYW4gYW5pbWF0ZWQgY2hhcmFjdGVyLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgYXN5bmMgZGVsZXRlQW5pbWF0ZWRDaGFyYWN0ZXIoIG9iamVjdE1vZGVsSWQ6IHN0cmluZyApOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuICAgICAgICBsZXQgcG9vbCA9IHRoaXMuZ2V0UG9vbCgpO1xyXG4gICAgICAgIGxldCByZXN1bHQxID0gYXdhaXQgcG9vbC5xdWVyeShcIkRFTEVURSBGUk9NIGlvZGFuaW1hdGVkY2hhcmFjdGVyIFdIRVJFIG9iamVjdG1vZGVsX2lkID0gJDFcIiwgW29iamVjdE1vZGVsSWRdKTtcclxuICAgICAgICBsZXQgcmVzdWx0MiA9IGF3YWl0IHBvb2wucXVlcnkoXCJERUxFVEUgRlJPTSBpb2RvYmplY3Rtb2RlbCBXSEVSRSBvYmplY3Rtb2RlbF9pZCA9ICQxXCIsIFtvYmplY3RNb2RlbElkXSk7XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDEgJiYgcmVzdWx0MjtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFBnQW5pbWF0ZWRDaGFyYWN0ZXJTdG9yYWdlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
