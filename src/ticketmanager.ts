import ITicketStorage from "./storage/ticketstorage";
import IStorageFactory from "./storage/istoragefactory";
import {StorageType} from "./storage/ibasestorage";
import Ticket from "isleofdarkness-common/src/model/ticket";
import { MissingConstructorArgumentError } from "isleofdarkness-common/src/error/missingconstructorargumenterror";

/**
 * Contains logic that validates tickets and creates (and saves) new tickets.
 */
export class TicketManager {

    private _ticketStorage: ITicketStorage;
    private _numOfSecondsUntilExpiration = 360;

    /**
     * Create ticket manager.
     * @param storageFactory
     */
    constructor(storageFactory: IStorageFactory, numberOfSecondsUntilExpiration: number) {
        this._numOfSecondsUntilExpiration = numberOfSecondsUntilExpiration;
        if ( storageFactory ) {
            this._ticketStorage = storageFactory.getStorage<ITicketStorage>( StorageType.Ticket );
            if ( !this._ticketStorage ) {
                throw new Error("No ticket storage in storage factory given");
            }
        } else {
            throw new MissingConstructorArgumentError();
        }
    }

    /**
     * Get a new ticket for the given user. The ticket will be saved in the ticket storage
     * so it is valid from here on out.
     */
    public async getTicket(userId: string): Promise<Ticket> {
        let ticket = Ticket.NewTicket(userId, this._numOfSecondsUntilExpiration);
        let ticketResult = await this._ticketStorage.addTicket(ticket);
        return ticket;
    }

    /**
     * Validate that the given ticket is valid.
     * @param ticket
     */
    public async validateTicket(ticket: Ticket|null): Promise<boolean> {

        if ( !ticket ) {
            return false;
        }
        if ( !ticket.expirationDate ) {
            return false;
        }

        let now = new Date();
        let result = false;

        if ( ticket.expirationDate.getTime() < now.getTime() ) {
            return false;
        }

        let ticketFromStorage = await this._ticketStorage.getTicket(ticket.ticketId);
        if ( ticketFromStorage ) {
            if ( ticketFromStorage.expirationDate ) {
                if ( ticketFromStorage.expirationDate.getTime() > now.getTime() &&
                     ticketFromStorage.expirationDate.getTime() === ticket.expirationDate.getTime() &&
                     ticketFromStorage.ticketId === ticket.ticketId &&
                     ticketFromStorage.userId === ticket.userId ) {
                    result = true;
                }
            }
        }
        return result;

    }

}

export default TicketManager;
