"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var usermetadata_1 = require("isleofdarkness-common/src/model/usermetadata");
var usermetadatastorage_1 = require("../../src/storage/usermetadatastorage");
var assert = require("assert");
var redis = require("redis");
describe("UserMetaDataStorage Test", function () {
    var HOST = "redis://localhost:6379";
    it("Store UserMetaData object into Temporary Storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var userMetaData1, redisClient, storage, returnValue, json1, json2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    userMetaData1 = new usermetadata_1.default("U123", "Player 1");
                    redisClient = redis.createClient(this.HOST);
                    redisClient.on("error", function (err) {
                        console.log("Error " + err);
                    });
                    storage = new usermetadatastorage_1.default(redisClient);
                    return [4 /*yield*/, storage.addUpdateUserMetaData(userMetaData1)];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, storage.getUserMetaData("U123")];
                case 2:
                    returnValue = _a.sent();
                    json1 = JSON.stringify(returnValue.toJSON());
                    json2 = JSON.stringify(userMetaData1.toJSON());
                    assert(json1 == json2);
                    return [2 /*return*/];
            }
        });
    }); });
    it("Delete UserMetaData object from Temporary Storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var userMetaData1, redisClient, storage, returnValue;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    userMetaData1 = new usermetadata_1.default("U987", "Player 1");
                    redisClient = redis.createClient(this.HOST);
                    redisClient.on("error", function (err) {
                        console.log("Error " + err);
                    });
                    storage = new usermetadatastorage_1.default(redisClient);
                    return [4 /*yield*/, storage.addUpdateUserMetaData(userMetaData1)];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, storage.removeUserMetaData(userMetaData1.userId)];
                case 2:
                    _a.sent();
                    return [4 /*yield*/, storage.getUserMetaData("U987")];
                case 3:
                    returnValue = _a.sent();
                    assert(returnValue === undefined);
                    return [2 /*return*/];
            }
        });
    }); });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvdXNlcm1ldGFkYXRhc3RvcmFnZXRlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsaUJBb0RBOztBQXBEQSxpQkFBZTtBQUVmLDZFQUF3RTtBQUV4RSw2RUFBd0U7QUFFeEUsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQy9CLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUU3QixRQUFRLENBQUMsMEJBQTBCLEVBQUU7SUFFakMsSUFBTSxJQUFJLEdBQVUsd0JBQXdCLENBQUM7SUFFN0MsRUFBRSxDQUFDLGtEQUFrRCxFQUFFOzs7OztvQkFFL0MsYUFBYSxHQUFHLElBQUksc0JBQVksQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUM7b0JBRXJELFdBQVcsR0FBTyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEQsV0FBVyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxHQUFPO3dCQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsQ0FBQztvQkFDaEMsQ0FBQyxDQUFDLENBQUM7b0JBRUMsT0FBTyxHQUF3QixJQUFJLDZCQUFtQixDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUV4RSxxQkFBTSxPQUFPLENBQUMscUJBQXFCLENBQUMsYUFBYSxDQUFDLEVBQUE7O29CQUFsRCxTQUFrRCxDQUFDO29CQUNwQixxQkFBTSxPQUFPLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxFQUFBOztvQkFBaEUsV0FBVyxHQUFnQixTQUFxQztvQkFDaEUsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7b0JBQzdDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO29CQUNuRCxNQUFNLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxDQUFDOzs7O1NBRTFCLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQyxtREFBbUQsRUFBRTs7Ozs7b0JBRWhELGFBQWEsR0FBRyxJQUFJLHNCQUFZLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxDQUFDO29CQUVyRCxXQUFXLEdBQU8sS0FBSyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BELFdBQVcsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsR0FBTzt3QkFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLENBQUM7b0JBQ2hDLENBQUMsQ0FBQyxDQUFDO29CQUVDLE9BQU8sR0FBd0IsSUFBSSw2QkFBbUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFFeEUscUJBQU0sT0FBTyxDQUFDLHFCQUFxQixDQUFDLGFBQWEsQ0FBQyxFQUFBOztvQkFBbEQsU0FBa0QsQ0FBQztvQkFDbkQscUJBQU0sT0FBTyxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsRUFBQTs7b0JBQXRELFNBQXNELENBQUM7b0JBRXhCLHFCQUFNLE9BQU8sQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLEVBQUE7O29CQUFoRSxXQUFXLEdBQWdCLFNBQXFDO29CQUNwRSxNQUFNLENBQUUsV0FBVyxLQUFLLFNBQVMsQ0FBRSxDQUFDOzs7O1NBRXZDLENBQUMsQ0FBQztBQUVQLENBQUMsQ0FBQyxDQUFDIiwiZmlsZSI6InN0b3JhZ2UvdXNlcm1ldGFkYXRhc3RvcmFnZXRlc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXCJtb2NoYVwiO1xyXG5cclxuaW1wb3J0IFVzZXJNZXRhRGF0YSBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tb2RlbC91c2VybWV0YWRhdGFcIjtcclxuaW1wb3J0IElVc2VyTWV0YURhdGFTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pdXNlcm1ldGFkYXRhc3RvcmFnZVwiO1xyXG5pbXBvcnQgVXNlck1ldGFEYXRhU3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvdXNlcm1ldGFkYXRhc3RvcmFnZVwiO1xyXG5cclxubGV0IGFzc2VydCA9IHJlcXVpcmUoXCJhc3NlcnRcIik7XHJcbmxldCByZWRpcyA9IHJlcXVpcmUoXCJyZWRpc1wiKTtcclxuXHJcbmRlc2NyaWJlKFwiVXNlck1ldGFEYXRhU3RvcmFnZSBUZXN0XCIsICgpID0+IHtcclxuXHJcbiAgICBjb25zdCBIT1NUOnN0cmluZyA9IFwicmVkaXM6Ly9sb2NhbGhvc3Q6NjM3OVwiOyBcclxuXHJcbiAgICBpdChcIlN0b3JlIFVzZXJNZXRhRGF0YSBvYmplY3QgaW50byBUZW1wb3JhcnkgU3RvcmFnZVwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCB1c2VyTWV0YURhdGExID0gbmV3IFVzZXJNZXRhRGF0YShcIlUxMjNcIiwgXCJQbGF5ZXIgMVwiKTtcclxuXHJcbiAgICAgICAgbGV0IHJlZGlzQ2xpZW50OmFueSA9IHJlZGlzLmNyZWF0ZUNsaWVudCh0aGlzLkhPU1QpO1xyXG4gICAgICAgIHJlZGlzQ2xpZW50Lm9uKFwiZXJyb3JcIiwgZnVuY3Rpb24gKGVycjphbnkpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnJvciBcIiArIGVycik7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGxldCBzdG9yYWdlOklVc2VyTWV0YURhdGFTdG9yYWdlID0gbmV3IFVzZXJNZXRhRGF0YVN0b3JhZ2UocmVkaXNDbGllbnQpO1xyXG5cclxuICAgICAgICBhd2FpdCBzdG9yYWdlLmFkZFVwZGF0ZVVzZXJNZXRhRGF0YSh1c2VyTWV0YURhdGExKTtcclxuICAgICAgICBsZXQgcmV0dXJuVmFsdWU6VXNlck1ldGFEYXRhID0gYXdhaXQgc3RvcmFnZS5nZXRVc2VyTWV0YURhdGEoXCJVMTIzXCIpO1xyXG4gICAgICAgIGxldCBqc29uMSA9IEpTT04uc3RyaW5naWZ5KHJldHVyblZhbHVlLnRvSlNPTigpKTtcclxuICAgICAgICBsZXQganNvbjIgPSBKU09OLnN0cmluZ2lmeSh1c2VyTWV0YURhdGExLnRvSlNPTigpKTtcclxuICAgICAgICBhc3NlcnQoanNvbjEgPT0ganNvbjIpO1xyXG5cclxuICAgIH0pO1xyXG5cclxuICAgIGl0KFwiRGVsZXRlIFVzZXJNZXRhRGF0YSBvYmplY3QgZnJvbSBUZW1wb3JhcnkgU3RvcmFnZVwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCB1c2VyTWV0YURhdGExID0gbmV3IFVzZXJNZXRhRGF0YShcIlU5ODdcIiwgXCJQbGF5ZXIgMVwiKTtcclxuXHJcbiAgICAgICAgbGV0IHJlZGlzQ2xpZW50OmFueSA9IHJlZGlzLmNyZWF0ZUNsaWVudCh0aGlzLkhPU1QpO1xyXG4gICAgICAgIHJlZGlzQ2xpZW50Lm9uKFwiZXJyb3JcIiwgZnVuY3Rpb24gKGVycjphbnkpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnJvciBcIiArIGVycik7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGxldCBzdG9yYWdlOklVc2VyTWV0YURhdGFTdG9yYWdlID0gbmV3IFVzZXJNZXRhRGF0YVN0b3JhZ2UocmVkaXNDbGllbnQpO1xyXG5cclxuICAgICAgICBhd2FpdCBzdG9yYWdlLmFkZFVwZGF0ZVVzZXJNZXRhRGF0YSh1c2VyTWV0YURhdGExKTtcclxuICAgICAgICBhd2FpdCBzdG9yYWdlLnJlbW92ZVVzZXJNZXRhRGF0YSh1c2VyTWV0YURhdGExLnVzZXJJZCk7XHJcblxyXG4gICAgICAgIGxldCByZXR1cm5WYWx1ZTpVc2VyTWV0YURhdGEgPSBhd2FpdCBzdG9yYWdlLmdldFVzZXJNZXRhRGF0YShcIlU5ODdcIik7XHJcbiAgICAgICAgYXNzZXJ0KCByZXR1cm5WYWx1ZSA9PT0gdW5kZWZpbmVkICk7XHJcblxyXG4gICAgfSk7XHJcblxyXG59KTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
