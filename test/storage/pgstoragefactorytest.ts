import "mocha";
import IPgStorageFactory from "../../src/storage/ipgstoragefactory";
import PgStorageFactory from "../../src/storage/pgstoragefactory";
import IBaseStorage, {StorageType} from "../../src/storage/ibasestorage";
import IPgObjectModelStorage from "../../src/storage/ipgobjectmodelstorage";
import IPgUserStorage from "../../src/storage/ipguserstorage";

let parse = require('pg-connection-string').parse;
let assert = require("assert");
let { Client, Pool } = require('pg');

describe("PgStorageFactory Test", () => {

    const HOST: string = "postgresql://postgres:abc123@localhost:5432/isleofdarkness"; 
    const config: any = parse(HOST);
    const pool: any = new Pool(config);

    it("should be able to get the correct storage object from factory", (done:Function) => {

        let storageFactory:IPgStorageFactory = new PgStorageFactory(pool);

        let objModelStorage:IBaseStorage = storageFactory.getStorage( StorageType.ObjectModel );
        assert.equal( StorageType.ObjectModel, objModelStorage.getStorageType() );
        assert.ok( objModelStorage as IPgObjectModelStorage !== null );

        let userMetaDataStorage:IBaseStorage = storageFactory.getStorage( StorageType.User );
        assert.equal( StorageType.User, userMetaDataStorage.getStorageType() );
        assert.ok( userMetaDataStorage as IPgUserStorage !== null );
        
        done();
        
    });  

    it("should be able to check if connection is still present", async () => {

        let storageFactory:IPgStorageFactory = new PgStorageFactory(pool);
        let connected: boolean = await storageFactory.isConnected();
        assert.ok( connected === true );

    });

    it("should be able to typecast the correct storage object using generics", (done: Function) => {

        let storageFactory:IPgStorageFactory = new PgStorageFactory(pool);

        let objModelStorage:IPgObjectModelStorage = storageFactory.getStorage<IPgObjectModelStorage>( StorageType.ObjectModel );
        assert.equal( StorageType.ObjectModel, objModelStorage.getStorageType() );
        assert.ok( objModelStorage as IPgObjectModelStorage !== null );

        let userMetaDataStorage:IPgUserStorage = storageFactory.getStorage<IPgUserStorage>( StorageType.User );
        assert.equal( StorageType.User, userMetaDataStorage.getStorageType() );
        assert.ok( userMetaDataStorage as IPgUserStorage !== null );
        
        done();

    });

});
