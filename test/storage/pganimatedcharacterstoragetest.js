"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var pganimatedcharacterstorage_1 = require("../../src/storage/pganimatedcharacterstorage");
var pganimationprofilestorage_1 = require("../../src/storage/pganimationprofilestorage");
var pgobjectmodelstorage_1 = require("../../src/storage/pgobjectmodelstorage");
var animatedcharacter_1 = require("isleofdarkness-common/src/model/animatedcharacter");
var circle_1 = require("isleofdarkness-common/src/math/circle");
var point_1 = require("isleofdarkness-common/src/math/point");
var animationprofile_1 = require("isleofdarkness-common/src/model/animationprofile");
var parse = require('pg-connection-string').parse;
var assert = require("assert");
var _a = require('pg'), Client = _a.Client, Pool = _a.Pool;
describe("PgAnimatedCharacterStorage Test", function () {
    var HOST = "postgresql://postgres:abc123@localhost:5432/isleofdarkness";
    var config = parse(HOST);
    var pool = new Pool(config);
    var animationProfileStorage = new pganimationprofilestorage_1.default(pool);
    var objectModelStorage = new pgobjectmodelstorage_1.default(pool);
    it("should be able to insert an animated character", function () { return __awaiter(_this, void 0, void 0, function () {
        var animatedCharacterStorage, id, shape, animatedCharacter, animationProfile, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    animatedCharacterStorage = new pganimatedcharacterstorage_1.default(pool, objectModelStorage, animationProfileStorage);
                    id = "ASDASDID";
                    shape = new circle_1.Circle(new point_1.Point(0, 0, 0), 5);
                    animatedCharacter = new animatedcharacter_1.default(id, shape, "Test Model");
                    animationProfile = new animationprofile_1.AnimationProfile("aland1", "Aland", "", "", "", "");
                    animatedCharacter.animationProfile = animationProfile;
                    return [4 /*yield*/, animatedCharacterStorage.addUpdateAnimatedCharacter(animatedCharacter)];
                case 1:
                    result = _a.sent();
                    assert.ok(result);
                    return [2 /*return*/];
            }
        });
    }); });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvcGdhbmltYXRlZGNoYXJhY3RlcnN0b3JhZ2V0ZXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGlCQStDQTs7QUEvQ0EsaUJBQWU7QUFLZiwyRkFBc0Y7QUFFdEYseUZBQW9GO0FBRXBGLCtFQUEwRTtBQUcxRSx1RkFBa0Y7QUFFbEYsZ0VBQStEO0FBQy9ELDhEQUE2RDtBQUM3RCxxRkFBb0Y7QUFFcEYsSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUMsS0FBSyxDQUFDO0FBQ2xELElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMzQixJQUFBLGtCQUFnQyxFQUE5QixrQkFBTSxFQUFFLGNBQUksQ0FBbUI7QUFFckMsUUFBUSxDQUFDLGlDQUFpQyxFQUFFO0lBRXhDLElBQU0sSUFBSSxHQUFXLDREQUE0RCxDQUFDO0lBQ2xGLElBQU0sTUFBTSxHQUFRLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoQyxJQUFNLElBQUksR0FBUSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuQyxJQUFNLHVCQUF1QixHQUE4QixJQUFJLG1DQUF5QixDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9GLElBQU0sa0JBQWtCLEdBQXlCLElBQUksOEJBQW9CLENBQUMsSUFBSSxDQUFDLENBQUM7SUFFaEYsRUFBRSxDQUFDLGdEQUFnRCxFQUFFOzs7OztvQkFFN0Msd0JBQXdCLEdBQWdDLElBQUksb0NBQTBCLENBQUMsSUFBSSxFQUMzRixrQkFBa0IsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO29CQUU3QyxFQUFFLEdBQUcsVUFBVSxDQUFDO29CQUNoQixLQUFLLEdBQUcsSUFBSSxlQUFNLENBQUUsSUFBSSxhQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUUsQ0FBQztvQkFDNUMsaUJBQWlCLEdBQXNCLElBQUksMkJBQWlCLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxZQUFZLENBQUMsQ0FBQztvQkFDdEYsZ0JBQWdCLEdBQXFCLElBQUksbUNBQWdCLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztvQkFDakcsaUJBQWlCLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7b0JBQ3pDLHFCQUFNLHdCQUF3QixDQUFDLDBCQUEwQixDQUFDLGlCQUFpQixDQUFDLEVBQUE7O29CQUFyRixNQUFNLEdBQUcsU0FBNEU7b0JBRXpGLE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7Ozs7U0FFckIsQ0FBQyxDQUFDO0FBRVAsQ0FBQyxDQUFDLENBQUMiLCJmaWxlIjoic3RvcmFnZS9wZ2FuaW1hdGVkY2hhcmFjdGVyc3RvcmFnZXRlc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXCJtb2NoYVwiO1xyXG5pbXBvcnQgSUJhc2VTdG9yYWdlLCB7U3RvcmFnZVR5cGV9IGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pYmFzZXN0b3JhZ2VcIjtcclxuaW1wb3J0IElQZ09iamVjdE1vZGVsU3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvaXBnb2JqZWN0bW9kZWxzdG9yYWdlXCI7XHJcbmltcG9ydCBJUGdVc2VyU3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvaXBndXNlcnN0b3JhZ2VcIjtcclxuaW1wb3J0IElQZ0FuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvaXBnYW5pbWF0ZWRjaGFyYWN0ZXJzdG9yYWdlXCI7XHJcbmltcG9ydCBQZ0FuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvcGdhbmltYXRlZGNoYXJhY3RlcnN0b3JhZ2VcIjtcclxuaW1wb3J0IElQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pcGdhbmltYXRpb25wcm9maWxlc3RvcmFnZVwiO1xyXG5pbXBvcnQgUGdBbmltYXRpb25Qcm9maWxlU3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvcGdhbmltYXRpb25wcm9maWxlc3RvcmFnZVwiO1xyXG5pbXBvcnQgSU9iamVjdE1vZGVsU3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvaW9iamVjdG1vZGVsc3RvcmFnZVwiO1xyXG5pbXBvcnQgUGdPYmplY3RNb2RlbFN0b3JhZ2UgZnJvbSBcIi4uLy4uL3NyYy9zdG9yYWdlL3Bnb2JqZWN0bW9kZWxzdG9yYWdlXCI7XHJcbmltcG9ydCBJUGdCYXNlQW5pbWF0ZWRDaGFyYWN0ZXJTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pcGdiYXNlYW5pbWF0ZWRjaGFyYWN0ZXJzdG9yYWdlXCI7XHJcbmltcG9ydCBQZ0Jhc2VBbmltYXRlZENoYXJhY3RlclN0b3JhZ2UgZnJvbSBcIi4uLy4uL3NyYy9zdG9yYWdlL3BnYmFzZWFuaW1hdGVkY2hhcmFjdGVyc3RvcmFnZVwiO1xyXG5pbXBvcnQgQW5pbWF0ZWRDaGFyYWN0ZXIgZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbW9kZWwvYW5pbWF0ZWRjaGFyYWN0ZXJcIjtcclxuaW1wb3J0IHsgU2hhcGUgfSBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tYXRoL3NoYXBlXCI7XHJcbmltcG9ydCB7IENpcmNsZSB9IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21hdGgvY2lyY2xlXCI7XHJcbmltcG9ydCB7IFBvaW50IH0gZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbWF0aC9wb2ludFwiO1xyXG5pbXBvcnQgeyBBbmltYXRpb25Qcm9maWxlIH0gZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbW9kZWwvYW5pbWF0aW9ucHJvZmlsZVwiO1xyXG5cclxubGV0IHBhcnNlID0gcmVxdWlyZSgncGctY29ubmVjdGlvbi1zdHJpbmcnKS5wYXJzZTtcclxubGV0IGFzc2VydCA9IHJlcXVpcmUoXCJhc3NlcnRcIik7XHJcbmxldCB7IENsaWVudCwgUG9vbCB9ID0gcmVxdWlyZSgncGcnKTtcclxuXHJcbmRlc2NyaWJlKFwiUGdBbmltYXRlZENoYXJhY3RlclN0b3JhZ2UgVGVzdFwiLCAoKSA9PiB7XHJcblxyXG4gICAgY29uc3QgSE9TVDogc3RyaW5nID0gXCJwb3N0Z3Jlc3FsOi8vcG9zdGdyZXM6YWJjMTIzQGxvY2FsaG9zdDo1NDMyL2lzbGVvZmRhcmtuZXNzXCI7IFxyXG4gICAgY29uc3QgY29uZmlnOiBhbnkgPSBwYXJzZShIT1NUKTtcclxuICAgIGNvbnN0IHBvb2w6IGFueSA9IG5ldyBQb29sKGNvbmZpZyk7XHJcbiAgICBjb25zdCBhbmltYXRpb25Qcm9maWxlU3RvcmFnZTpJUGdBbmltYXRpb25Qcm9maWxlU3RvcmFnZSA9IG5ldyBQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlKHBvb2wpO1xyXG4gICAgY29uc3Qgb2JqZWN0TW9kZWxTdG9yYWdlOklQZ09iamVjdE1vZGVsU3RvcmFnZSA9IG5ldyBQZ09iamVjdE1vZGVsU3RvcmFnZShwb29sKTtcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIGluc2VydCBhbiBhbmltYXRlZCBjaGFyYWN0ZXJcIiwgYXN5bmMoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCBhbmltYXRlZENoYXJhY3RlclN0b3JhZ2U6IElQZ0FuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZSA9IG5ldyBQZ0FuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZShwb29sLCBcclxuICAgICAgICAgICAgb2JqZWN0TW9kZWxTdG9yYWdlLCBhbmltYXRpb25Qcm9maWxlU3RvcmFnZSk7ICAgICAgICBcclxuXHJcbiAgICAgICAgbGV0IGlkID0gXCJBU0RBU0RJRFwiO1xyXG4gICAgICAgIGxldCBzaGFwZSA9IG5ldyBDaXJjbGUoIG5ldyBQb2ludCgwLCAwLCAwKSwgNSApO1xyXG4gICAgICAgIHZhciBhbmltYXRlZENoYXJhY3RlcjogQW5pbWF0ZWRDaGFyYWN0ZXIgPSBuZXcgQW5pbWF0ZWRDaGFyYWN0ZXIoaWQsIHNoYXBlLCBcIlRlc3QgTW9kZWxcIik7XHJcbiAgICAgICAgbGV0IGFuaW1hdGlvblByb2ZpbGU6IEFuaW1hdGlvblByb2ZpbGUgPSBuZXcgQW5pbWF0aW9uUHJvZmlsZShcImFsYW5kMVwiLCBcIkFsYW5kXCIsIFwiXCIsIFwiXCIsIFwiXCIsIFwiXCIpO1xyXG4gICAgICAgIGFuaW1hdGVkQ2hhcmFjdGVyLmFuaW1hdGlvblByb2ZpbGUgPSBhbmltYXRpb25Qcm9maWxlO1xyXG4gICAgICAgIGxldCByZXN1bHQgPSBhd2FpdCBhbmltYXRlZENoYXJhY3RlclN0b3JhZ2UuYWRkVXBkYXRlQW5pbWF0ZWRDaGFyYWN0ZXIoYW5pbWF0ZWRDaGFyYWN0ZXIpO1xyXG5cclxuICAgICAgICBhc3NlcnQub2socmVzdWx0KTtcclxuXHJcbiAgICB9KTsgXHJcblxyXG59KTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
