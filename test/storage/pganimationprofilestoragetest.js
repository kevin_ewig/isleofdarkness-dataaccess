"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var pganimationprofilestorage_1 = require("../../src/storage/pganimationprofilestorage");
var parse = require('pg-connection-string').parse;
var assert = require("assert");
var _a = require('pg'), Client = _a.Client, Pool = _a.Pool;
describe("PgAnimationProfileStorage Test", function () {
    var HOST = "postgresql://postgres:abc123@localhost:5432/isleofdarkness";
    var config = parse(HOST);
    var pool = new Pool(config);
    it("should be able to get data from an animation profile storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var animationProfileStorage, animationProfile;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    animationProfileStorage = new pganimationprofilestorage_1.default(pool);
                    return [4 /*yield*/, animationProfileStorage.getAnimationProfile("aland1")];
                case 1:
                    animationProfile = _a.sent();
                    assert.ok(!!animationProfile == true);
                    assert.equal("Aland", animationProfile.name);
                    return [2 /*return*/];
            }
        });
    }); });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvcGdhbmltYXRpb25wcm9maWxlc3RvcmFnZXRlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsaUJBMEJBOztBQTFCQSxpQkFBZTtBQUtmLHlGQUFvRjtBQUVwRixJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxLQUFLLENBQUM7QUFDbEQsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQzNCLElBQUEsa0JBQWdDLEVBQTlCLGtCQUFNLEVBQUUsY0FBSSxDQUFtQjtBQUVyQyxRQUFRLENBQUMsZ0NBQWdDLEVBQUU7SUFFdkMsSUFBTSxJQUFJLEdBQVcsNERBQTRELENBQUM7SUFDbEYsSUFBTSxNQUFNLEdBQVEsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2hDLElBQU0sSUFBSSxHQUFRLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBRW5DLEVBQUUsQ0FBQyw4REFBOEQsRUFBRTs7Ozs7b0JBRTNELHVCQUF1QixHQUE4QixJQUFJLG1DQUF5QixDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0RSxxQkFBTSx1QkFBdUIsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsRUFBQTs7b0JBQTlFLGdCQUFnQixHQUFHLFNBQTJEO29CQUNsRixNQUFNLENBQUMsRUFBRSxDQUFFLENBQUMsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUUsQ0FBQztvQkFDeEMsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7Ozs7U0FDaEQsQ0FBQyxDQUFDO0FBRVAsQ0FBQyxDQUFDLENBQUMiLCJmaWxlIjoic3RvcmFnZS9wZ2FuaW1hdGlvbnByb2ZpbGVzdG9yYWdldGVzdC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBcIm1vY2hhXCI7XHJcbmltcG9ydCBJQmFzZVN0b3JhZ2UsIHtTdG9yYWdlVHlwZX0gZnJvbSBcIi4uLy4uL3NyYy9zdG9yYWdlL2liYXNlc3RvcmFnZVwiO1xyXG5pbXBvcnQgSVBnT2JqZWN0TW9kZWxTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pcGdvYmplY3Rtb2RlbHN0b3JhZ2VcIjtcclxuaW1wb3J0IElQZ1VzZXJTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pcGd1c2Vyc3RvcmFnZVwiO1xyXG5pbXBvcnQgSVBnQW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2UgZnJvbSBcIi4uLy4uL3NyYy9zdG9yYWdlL2lwZ2FuaW1hdGlvbnByb2ZpbGVzdG9yYWdlXCI7XHJcbmltcG9ydCBQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9wZ2FuaW1hdGlvbnByb2ZpbGVzdG9yYWdlXCI7XHJcblxyXG5sZXQgcGFyc2UgPSByZXF1aXJlKCdwZy1jb25uZWN0aW9uLXN0cmluZycpLnBhcnNlO1xyXG5sZXQgYXNzZXJ0ID0gcmVxdWlyZShcImFzc2VydFwiKTtcclxubGV0IHsgQ2xpZW50LCBQb29sIH0gPSByZXF1aXJlKCdwZycpO1xyXG5cclxuZGVzY3JpYmUoXCJQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlIFRlc3RcIiwgKCkgPT4ge1xyXG5cclxuICAgIGNvbnN0IEhPU1Q6IHN0cmluZyA9IFwicG9zdGdyZXNxbDovL3Bvc3RncmVzOmFiYzEyM0Bsb2NhbGhvc3Q6NTQzMi9pc2xlb2ZkYXJrbmVzc1wiOyBcclxuICAgIGNvbnN0IGNvbmZpZzogYW55ID0gcGFyc2UoSE9TVCk7XHJcbiAgICBjb25zdCBwb29sOiBhbnkgPSBuZXcgUG9vbChjb25maWcpO1xyXG5cclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gZ2V0IGRhdGEgZnJvbSBhbiBhbmltYXRpb24gcHJvZmlsZSBzdG9yYWdlXCIsIGFzeW5jKCkgPT4ge1xyXG5cclxuICAgICAgICBsZXQgYW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2U6SVBnQW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2UgPSBuZXcgUGdBbmltYXRpb25Qcm9maWxlU3RvcmFnZShwb29sKTtcclxuICAgICAgICBsZXQgYW5pbWF0aW9uUHJvZmlsZSA9IGF3YWl0IGFuaW1hdGlvblByb2ZpbGVTdG9yYWdlLmdldEFuaW1hdGlvblByb2ZpbGUoXCJhbGFuZDFcIik7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKCAhIWFuaW1hdGlvblByb2ZpbGUgPT0gdHJ1ZSApO1xyXG4gICAgICAgIGFzc2VydC5lcXVhbChcIkFsYW5kXCIsIGFuaW1hdGlvblByb2ZpbGUubmFtZSk7XHJcbiAgICB9KTsgXHJcblxyXG59KTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
