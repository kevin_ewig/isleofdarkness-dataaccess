import "mocha";

import Ticket from "isleofdarkness-common/src/model/ticket";
import ITicketStorage from "../../src/storage/iticketstorage";
import TicketStorage from "../../src/storage/ticketstorage";

let assert = require("assert");
let redis = require("redis");

describe("TicketStorage Test", () => {

    const HOST:string = "redis://localhost:6379"; 

    it("should store Ticket object into Temporary Storage", async () => {

        let ticket = Ticket.NewTicket("U12345", 10000);

        let redisClient:any = redis.createClient(HOST);
        redisClient.on("error", function (err:any) {
            console.log("Error " + err);
        });

        let storage:ITicketStorage = new TicketStorage(redisClient);

        let result:boolean = await storage.addTicket(ticket);

        setTimeout( function() {
            let promise:Promise<Ticket> = storage.getTicket(ticket.ticketId);
            promise.then(
                (retrievedTicket) => {
                    assert.ok( retrievedTicket.ticketId == ticket.ticketId );
                }
            ).catch(
                (err) => {
                    console.log(err);
                    assert.ok(false);
                }
            );
        }, 500 );

    }).timeout(4000);

    it("should expire ticket at the correct time", async () => {

        let ticket = Ticket.NewTicket("U12345", 1);

        let redisClient:any = redis.createClient(HOST);
        redisClient.on("error", function (err:any) {
            console.log("Error " + err);
        });

        let storage:ITicketStorage = new TicketStorage(redisClient);

        let result:boolean = await storage.addTicket(ticket);

        setTimeout( function() {
            let promise:Promise<Ticket> = storage.getTicket(ticket.ticketId);
            promise.then(
                (retrievedTicket) => {
                    assert.ok( retrievedTicket === undefined );
                }
            ).catch(
                (err) => {
                    console.log(err);
                    assert.ok(false);
                }
            );
        }, 3000 );

    }).timeout(4000);

});