import "mocha";
import IBaseStorage, {StorageType} from "../../src/storage/ibasestorage";
import IPgObjectModelStorage from "../../src/storage/ipgobjectmodelstorage";
import IPgUserStorage from "../../src/storage/ipguserstorage";
import IPgAnimatedCharacterStorage from "../../src/storage/ipganimatedcharacterstorage";
import PgAnimatedCharacterStorage from "../../src/storage/pganimatedcharacterstorage";
import IPgAnimationProfileStorage from "../../src/storage/ipganimationprofilestorage";
import PgAnimationProfileStorage from "../../src/storage/pganimationprofilestorage";
import IObjectModelStorage from "../../src/storage/iobjectmodelstorage";
import PgObjectModelStorage from "../../src/storage/pgobjectmodelstorage";
import IPgBaseAnimatedCharacterStorage from "../../src/storage/ipgbaseanimatedcharacterstorage";
import PgBaseAnimatedCharacterStorage from "../../src/storage/pgbaseanimatedcharacterstorage";
import AnimatedCharacter from "isleofdarkness-common/src/model/animatedcharacter";
import { Shape } from "isleofdarkness-common/src/math/shape";
import { Circle } from "isleofdarkness-common/src/math/circle";
import { Point } from "isleofdarkness-common/src/math/point";
import { AnimationProfile } from "isleofdarkness-common/src/model/animationprofile";

let parse = require('pg-connection-string').parse;
let assert = require("assert");
let { Client, Pool } = require('pg');

describe("PgAnimatedCharacterStorage Test", () => {

    const HOST: string = "postgresql://postgres:abc123@localhost:5432/isleofdarkness"; 
    const config: any = parse(HOST);
    const pool: any = new Pool(config);
    const animationProfileStorage:IPgAnimationProfileStorage = new PgAnimationProfileStorage(pool);
    const objectModelStorage:IPgObjectModelStorage = new PgObjectModelStorage(pool);

    it("should be able to insert an animated character", async() => {

        let animatedCharacterStorage: IPgAnimatedCharacterStorage = new PgAnimatedCharacterStorage(pool, 
            objectModelStorage, animationProfileStorage);        

        let id = "ASDASDID";
        let shape = new Circle( new Point(0, 0, 0), 5 );
        var animatedCharacter: AnimatedCharacter = new AnimatedCharacter(id, shape, "Test Model");
        let animationProfile: AnimationProfile = new AnimationProfile("aland1", "Aland", "", "", "", "");
        animatedCharacter.animationProfile = animationProfile;
        let result = await animatedCharacterStorage.addUpdateAnimatedCharacter(animatedCharacter);

        assert.ok(result);

    }); 

});
