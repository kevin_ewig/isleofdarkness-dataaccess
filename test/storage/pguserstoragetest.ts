import "mocha";
import PgUserStorage from "../../src/storage/pguserstorage";
import IPgUserStorage from "../../src/storage/ipguserstorage";
import User from "isleofdarkness-common/src/model/user";

let parse = require('pg-connection-string').parse;
let assert = require("assert");
let { Client, Pool } = require('pg');

describe("PgUserStorage Test", () => {

    const HOST: string = "postgresql://postgres:abc123@localhost:5432/isleofdarkness"; 
    const config: any = parse(HOST);
    const pool: any = new Pool(config);
    const storage: IPgUserStorage = new PgUserStorage(pool);
    const uuidUserId: string = "6890d470";
    const pictureUrl: string = "http://www.picture.com/pic.jpg";

    it("should be able to add a user into storage", async () => {

        let user = new User(uuidUserId, "john");
        user.createdDate = new Date();
        user.disabled = false;
        user.email = "email@john.com";
        user.language = "en";
        user.lastLoggedInDate = new Date();
        user.pictureUrl = pictureUrl;
        let result: boolean = await storage.addUpdateUser( user );
        assert.ok(result);
       
    });

    it("should be able to update a user in storage using add/update", async () => {

        let user = new User(uuidUserId, "john");
        user.createdDate = new Date();
        user.disabled = true;
        user.email = "john@email.com";
        user.language = "gb";
        user.lastLoggedInDate = new Date();
        user.pictureUrl = pictureUrl;

        let result: boolean = await storage.addUpdateUser( user );
        assert.ok(result);

    });     

    it("should be able to get a user that does not exist from storage", async () => {

        let user: User = await storage.getUser("00000000");
        assert.ok(user == undefined);

    });

    it("should be able to get a user from storage", async () => {

        let user: User = await storage.getUser(uuidUserId);
        assert.ok(user !== undefined);
        assert.ok(user.userId == uuidUserId);
        assert.ok(user.name == "john");
        assert.ok(user.email == "john@email.com");
        assert.ok(user.language == "gb");
        assert.ok(user.disabled == true);
        assert.ok(user.createdDate !== undefined);
        assert.deepEqual(user.pictureUrl, pictureUrl);
        assert.ok(user.pictureUrl == pictureUrl);

    });

    it("should be able to get a user from storage by email (case insensitive)", async () => {

        let user: User = await storage.getUserByEmail("john@EMAIL.com");
        assert.ok(user !== undefined);
        assert.ok(user.userId == uuidUserId);
        assert.ok(user.name == "john");
        assert.ok(user.email == "john@email.com");
        assert.ok(user.language == "gb");
        assert.ok(user.disabled == true);
        assert.ok(user.createdDate !== undefined );        

    });
    
    it("should be able to store a user creation date at the correct timezone.", async () => {

        let user: User = await storage.getUser(uuidUserId);
        assert.ok(user !== undefined);
        assert.ok(user.userId == uuidUserId);
        assert.ok(user.name == "john");
        assert.ok(user.email == "john@email.com");

        let now: Date = new Date();    
        assert.ok(( user.createdDate.getTime() - now.getTime() ) < 2000 );
        
    });        

    it("should be able to delete a user", async () => {

        await storage.deleteUser(uuidUserId);
        let user: User = await storage.getUser(uuidUserId);
        assert.ok(user == undefined);

    });

});
