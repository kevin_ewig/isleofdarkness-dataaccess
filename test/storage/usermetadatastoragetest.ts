import "mocha";

import UserMetaData from "isleofdarkness-common/src/model/usermetadata";
import IUserMetaDataStorage from "../../src/storage/iusermetadatastorage";
import UserMetaDataStorage from "../../src/storage/usermetadatastorage";

let assert = require("assert");
let redis = require("redis");

describe("UserMetaDataStorage Test", () => {

    const HOST:string = "redis://localhost:6379"; 

    it("Store UserMetaData object into Temporary Storage", async () => {

        let userMetaData1 = new UserMetaData("U123", "Player 1");

        let redisClient:any = redis.createClient(this.HOST);
        redisClient.on("error", function (err:any) {
            console.log("Error " + err);
        });

        let storage:IUserMetaDataStorage = new UserMetaDataStorage(redisClient);

        await storage.addUpdateUserMetaData(userMetaData1);
        let returnValue:UserMetaData = await storage.getUserMetaData("U123");
        let json1 = JSON.stringify(returnValue.toJSON());
        let json2 = JSON.stringify(userMetaData1.toJSON());
        assert(json1 == json2);

    });

    it("Delete UserMetaData object from Temporary Storage", async () => {

        let userMetaData1 = new UserMetaData("U987", "Player 1");

        let redisClient:any = redis.createClient(this.HOST);
        redisClient.on("error", function (err:any) {
            console.log("Error " + err);
        });

        let storage:IUserMetaDataStorage = new UserMetaDataStorage(redisClient);

        await storage.addUpdateUserMetaData(userMetaData1);
        await storage.removeUserMetaData(userMetaData1.userId);

        let returnValue:UserMetaData = await storage.getUserMetaData("U987");
        assert( returnValue === undefined );

    });

});
