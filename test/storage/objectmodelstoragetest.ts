import "mocha";
import ObjectModel, { ObjectModelType } from "isleofdarkness-common/src/model/objectmodel";
import Circle from "isleofdarkness-common/src/math/circle";
import Point from "isleofdarkness-common/src/math/point";
import IObjectModelStorage from "../../src/storage/iobjectmodelstorage";
import ObjectModelStorage from "../../src/storage/objectmodelstorage"

let assert = require("assert");
let redis = require("redis");

describe("ObjectModelStorage Test", () => {

    const HOST:string = "redis://localhost:6379"; 
    var redisClient:any = null;

    beforeEach(() => {

        redisClient = redis.createClient(HOST);
        redisClient.on("error", function (err:any) {
            console.log("Error:" + err);
        });

        redisClient.flushdb( function(err:any) {
            if( err ) {
                console.log("Error: FlushDB: " + err);
            }
        } );

    });


    it("should store/retrieve ObjectModel object into temporary storage", async () => {

        let circle:Circle = new Circle(new Point(32, 12), 3);
        let objectModelData = new ObjectModel("OBJ123", circle, "ModelName");

        let storage:IObjectModelStorage = new ObjectModelStorage(redisClient);

        await storage.addUpdateObjectModel(objectModelData);
        let returnValue:ObjectModel = await storage.getObjectModel("OBJ123");
        let json1 = JSON.stringify(returnValue.toJSON());
        let json2 = JSON.stringify(objectModelData.toJSON());
        assert(json1 == json2);

    });

    it("should delete objectModel object from temporary storage", async () => {

        let circle:Circle = new Circle(new Point(32, 12), 3);
        let objectModelData = new ObjectModel("OBJ123", circle, "ModelName");

        let storage:IObjectModelStorage = new ObjectModelStorage(redisClient);

        await storage.addUpdateObjectModel(objectModelData);
        await storage.removeObjectModel(objectModelData.id);

        let returnValue:ObjectModel = await storage.getObjectModel("OBJ123");
        assert( returnValue === undefined );        

    });
    
    // it("should update objectModel object from temporary storage", async () => {

    //     let circle:Circle = new Circle(new Point(32, 12), 3);
    //     let objectModelData = new ObjectModel("OBJ123", circle, "ModelName");

    //     let storage:IObjectModelStorage = new ObjectModelStorage(redisClient);

    //     // Add object model to storage.
    //     await storage.addUpdateObjectModel(objectModelData);

    //     // Add a path to the object model.
    //     let points:Array<Point> = [];
    //     points.push( new Point(3,2) );
    //     points.push( new Point(6,4) );
    //     points.push( new Point(1,9) );
    //     objectModelData.setPath(points);
    //     await storage.addUpdateObjectModel(objectModelData);

    //     // Test object model can be retrieved successfully. 
    //     let retrievedObj:ObjectModel = await storage.getObjectModel("OBJ123");
    //     assert( JSON.stringify(retrievedObj.toJSON()) === JSON.stringify(objectModelData.toJSON()) );

    //     // Delete object model.
    //     await storage.removeObjectModel(objectModelData.id);
    //     let returnValue:ObjectModel = await storage.getObjectModel("OBJ123");
    //     assert( returnValue === undefined );

    // });
    
    it("should store/retrieve objectModel object by its section coordinate", async () => {

        let circle1:Circle = new Circle(new Point(532, 312), 3);
        let objectModelData1 = new ObjectModel("OBJ890890", circle1, "ModelName1");

        let circle2:Circle = new Circle(new Point(502, 302), 3);
        let objectModelData2 = new ObjectModel("OBJ999111", circle2, "ModelName2");        

        let storage:IObjectModelStorage = new ObjectModelStorage(redisClient);

        await storage.addUpdateObjectModel(objectModelData1);
        await storage.addUpdateObjectModel(objectModelData2);

        let listOfObjsId:string[] = await storage.getObjectModelsIdsInSection( new Point(5,3) );
        assert.equal(listOfObjsId.length, 2);

        listOfObjsId = await storage.getObjectModelsIdsInSection( new Point(1,1) );
        assert.equal(listOfObjsId.length, 0);

    });

});
