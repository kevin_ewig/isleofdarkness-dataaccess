"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var pgobjectmodelstorage_1 = require("../../src/storage/pgobjectmodelstorage");
var pguserstorage_1 = require("../../src/storage/pguserstorage");
var objectmodel_1 = require("isleofdarkness-common/src/model/objectmodel");
var circle_1 = require("isleofdarkness-common/src/math/circle");
var user_1 = require("isleofdarkness-common/src/model/user");
var rectangle_1 = require("isleofdarkness-common/src/math/rectangle");
var point_1 = require("isleofdarkness-common/src/math/point");
var parse = require('pg-connection-string').parse;
var assert = require("assert");
var _a = require('pg'), Client = _a.Client, Pool = _a.Pool;
describe("PgObjectModelStorage Test", function () {
    var HOST = "postgresql://postgres:abc123@localhost:5432/isleofdarkness";
    var config = parse(HOST);
    var pool = new Pool(config);
    var storage = new pgobjectmodelstorage_1.default(pool);
    var objectModelUuid1 = "2b52c870";
    var objectModelUuid2 = "4182c64b";
    var owner_userId = "0f338c06";
    it("should be able to insert an ObjectModel object (circle) into a Postgres database", function () { return __awaiter(_this, void 0, void 0, function () {
        var circle, objectModelData, storage, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    circle = new circle_1.default(new point_1.Point(32, 12), 3);
                    objectModelData = new objectmodel_1.ObjectModel(objectModelUuid1, circle, "ModelName");
                    storage = new pgobjectmodelstorage_1.default(pool);
                    return [4 /*yield*/, storage.addUpdateObjectModel(objectModelData)];
                case 1:
                    result = _a.sent();
                    assert.ok(result === true);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to update an ObjectModel object (circle) in a Postgres database", function () { return __awaiter(_this, void 0, void 0, function () {
        var circle, objectModelData, storage, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    circle = new circle_1.default(new point_1.Point(23, 21), 6);
                    objectModelData = new objectmodel_1.ObjectModel(objectModelUuid1, circle, "ModelName2");
                    objectModelData.modelTexture = "ModelTexture1";
                    objectModelData.objectType = objectmodel_1.ObjectModelType.Impassable;
                    storage = new pgobjectmodelstorage_1.default(pool);
                    return [4 /*yield*/, storage.addUpdateObjectModel(objectModelData)];
                case 1:
                    result = _a.sent();
                    assert.ok(result === true);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to get an ObjectModel object (circle) from a Postgres database", function () { return __awaiter(_this, void 0, void 0, function () {
        var storage, result, objModel;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    storage = new pgobjectmodelstorage_1.default(pool);
                    return [4 /*yield*/, storage.getObjectModel(objectModelUuid1)];
                case 1:
                    result = _a.sent();
                    assert.ok(result !== null);
                    objModel = result;
                    assert.deepEqual("ModelName2", objModel.modelName);
                    assert.deepEqual("ModelTexture1", objModel.modelTexture);
                    assert.deepEqual(objectmodel_1.ObjectModelType.Impassable, objModel.objectType);
                    assert.deepEqual(23, objModel.shape.x);
                    assert.deepEqual(21, objModel.shape.y);
                    assert.deepEqual(0, objModel.shape.z);
                    assert.deepEqual(6, objModel.shape.getRadius());
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to insert an ObjectModel object (rectangle) into a Postgres database", function () { return __awaiter(_this, void 0, void 0, function () {
        var rect, objectModelData, storage, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    rect = new rectangle_1.default(new point_1.Point(45, 98), 30, 20);
                    objectModelData = new objectmodel_1.ObjectModel(objectModelUuid2, rect, "TestModelName");
                    storage = new pgobjectmodelstorage_1.default(pool);
                    return [4 /*yield*/, storage.addUpdateObjectModel(objectModelData)];
                case 1:
                    result = _a.sent();
                    assert.ok(result === true);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to update an ObjectModel object (rectangle) in a Postgres database", function () { return __awaiter(_this, void 0, void 0, function () {
        var user, userStorage, userAddedToDb, rect, objectModelData, storage, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    user = new user_1.default(owner_userId, "test123");
                    user.email = "asdit.asd.com";
                    user.createdDate = new Date();
                    user.lastLoggedInDate = new Date();
                    userStorage = new pguserstorage_1.default(pool);
                    return [4 /*yield*/, userStorage.addUpdateUser(user)];
                case 1:
                    userAddedToDb = _a.sent();
                    rect = new rectangle_1.default(new point_1.Point(54, 89, 2), 20, 30);
                    objectModelData = new objectmodel_1.ObjectModel(objectModelUuid2, rect, "TestModelName2");
                    objectModelData.modelTexture = "ModelTexture2";
                    objectModelData.ownerUserId = owner_userId;
                    objectModelData.objectType = objectmodel_1.ObjectModelType.Impassable;
                    storage = new pgobjectmodelstorage_1.default(pool);
                    return [4 /*yield*/, storage.addUpdateObjectModel(objectModelData)];
                case 2:
                    result = _a.sent();
                    assert.ok(result === true);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to get an ObjectModel object (rectangle) from a Postgres database", function () { return __awaiter(_this, void 0, void 0, function () {
        var storage, result, objModel, rect;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    storage = new pgobjectmodelstorage_1.default(pool);
                    return [4 /*yield*/, storage.getObjectModel(objectModelUuid2)];
                case 1:
                    result = _a.sent();
                    assert.ok(result !== null);
                    objModel = result;
                    assert.deepEqual("TestModelName2", objModel.modelName);
                    assert.deepEqual("ModelTexture2", objModel.modelTexture);
                    assert.deepEqual(objectmodel_1.ObjectModelType.Impassable, objModel.objectType);
                    rect = objModel.shape;
                    assert.deepEqual(54, rect.position().x);
                    assert.deepEqual(89, rect.position().y);
                    assert.deepEqual(2, rect.position().z);
                    assert.deepEqual(20, rect.width());
                    assert.deepEqual(30, rect.height());
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to tell if no object model exists in the Postgres database", function () { return __awaiter(_this, void 0, void 0, function () {
        var storage, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    storage = new pgobjectmodelstorage_1.default(pool);
                    return [4 /*yield*/, storage.getObjectModel("00000000")];
                case 1:
                    result = _a.sent();
                    assert.ok(result === null);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to delete an ObjectModel from Postgres", function () { return __awaiter(_this, void 0, void 0, function () {
        var storage, result1, result2, objModel1, objModel2, userStorage;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    storage = new pgobjectmodelstorage_1.default(pool);
                    return [4 /*yield*/, storage.deleteObjectModel(objectModelUuid1)];
                case 1:
                    result1 = _a.sent();
                    assert.ok(result1 === true);
                    return [4 /*yield*/, storage.deleteObjectModel(objectModelUuid2)];
                case 2:
                    result2 = _a.sent();
                    assert.ok(result2 === true);
                    return [4 /*yield*/, storage.getObjectModel(objectModelUuid1)];
                case 3:
                    objModel1 = _a.sent();
                    assert.ok(objModel1 === null);
                    return [4 /*yield*/, storage.getObjectModel(objectModelUuid2)];
                case 4:
                    objModel2 = _a.sent();
                    assert.ok(objModel2 === null);
                    userStorage = new pguserstorage_1.default(pool);
                    userStorage.deleteUser(owner_userId);
                    return [2 /*return*/];
            }
        });
    }); });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvcGdvYmplY3Rtb2RlbHN0b3JhZ2V0ZXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGlCQXFKQTs7QUFySkEsaUJBQWU7QUFDZiwrRUFBMEU7QUFHMUUsaUVBQTREO0FBQzVELDJFQUEyRjtBQUMzRixnRUFBMkQ7QUFDM0QsNkRBQXdEO0FBQ3hELHNFQUFpRTtBQUNqRSw4REFBNkQ7QUFFN0QsSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUMsS0FBSyxDQUFDO0FBQ2xELElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMzQixJQUFBLGtCQUFnQyxFQUE5QixrQkFBTSxFQUFFLGNBQUksQ0FBbUI7QUFFckMsUUFBUSxDQUFDLDJCQUEyQixFQUFFO0lBRWxDLElBQU0sSUFBSSxHQUFXLDREQUE0RCxDQUFDO0lBQ2xGLElBQU0sTUFBTSxHQUFRLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoQyxJQUFNLElBQUksR0FBUSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuQyxJQUFNLE9BQU8sR0FBMEIsSUFBSSw4QkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0RSxJQUFNLGdCQUFnQixHQUFXLFVBQVUsQ0FBQztJQUM1QyxJQUFNLGdCQUFnQixHQUFXLFVBQVUsQ0FBQztJQUM1QyxJQUFNLFlBQVksR0FBVyxVQUFVLENBQUM7SUFFeEMsRUFBRSxDQUFDLGtGQUFrRixFQUFFOzs7OztvQkFFL0UsTUFBTSxHQUFVLElBQUksZ0JBQU0sQ0FBQyxJQUFJLGFBQUssQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ2pELGVBQWUsR0FBRyxJQUFJLHlCQUFXLENBQUMsZ0JBQWdCLEVBQUUsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUV6RSxPQUFPLEdBQXlCLElBQUksOEJBQW9CLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RELHFCQUFNLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsRUFBQTs7b0JBQTVELE1BQU0sR0FBRyxTQUFtRDtvQkFDaEUsTUFBTSxDQUFDLEVBQUUsQ0FBRSxNQUFNLEtBQUssSUFBSSxDQUFFLENBQUM7Ozs7U0FFaEMsQ0FBQyxDQUFDO0lBRUgsRUFBRSxDQUFDLGdGQUFnRixFQUFFOzs7OztvQkFFN0UsTUFBTSxHQUFVLElBQUksZ0JBQU0sQ0FBQyxJQUFJLGFBQUssQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ2pELGVBQWUsR0FBRyxJQUFJLHlCQUFXLENBQUMsZ0JBQWdCLEVBQUUsTUFBTSxFQUFFLFlBQVksQ0FBQyxDQUFDO29CQUM5RSxlQUFlLENBQUMsWUFBWSxHQUFHLGVBQWUsQ0FBQztvQkFDL0MsZUFBZSxDQUFDLFVBQVUsR0FBRyw2QkFBZSxDQUFDLFVBQVUsQ0FBQztvQkFFcEQsT0FBTyxHQUF5QixJQUFJLDhCQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0RCxxQkFBTSxPQUFPLENBQUMsb0JBQW9CLENBQUMsZUFBZSxDQUFDLEVBQUE7O29CQUE1RCxNQUFNLEdBQUcsU0FBbUQ7b0JBQ2hFLE1BQU0sQ0FBQyxFQUFFLENBQUUsTUFBTSxLQUFLLElBQUksQ0FBRSxDQUFDOzs7O1NBRWhDLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQywrRUFBK0UsRUFBRTs7Ozs7b0JBRTVFLE9BQU8sR0FBeUIsSUFBSSw4QkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEMscUJBQU0sT0FBTyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFBOztvQkFBekUsTUFBTSxHQUFxQixTQUE4QztvQkFDN0UsTUFBTSxDQUFDLEVBQUUsQ0FBRSxNQUFNLEtBQUssSUFBSSxDQUFFLENBQUM7b0JBQ3pCLFFBQVEsR0FBZ0IsTUFBcUIsQ0FBQztvQkFFbEQsTUFBTSxDQUFDLFNBQVMsQ0FBRSxZQUFZLEVBQUUsUUFBUSxDQUFDLFNBQVMsQ0FBRSxDQUFDO29CQUNyRCxNQUFNLENBQUMsU0FBUyxDQUFFLGVBQWUsRUFBRSxRQUFRLENBQUMsWUFBWSxDQUFFLENBQUM7b0JBQzNELE1BQU0sQ0FBQyxTQUFTLENBQUUsNkJBQWUsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVUsQ0FBRSxDQUFDO29CQUNwRSxNQUFNLENBQUMsU0FBUyxDQUFFLEVBQUUsRUFBRSxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBRSxDQUFDO29CQUN6QyxNQUFNLENBQUMsU0FBUyxDQUFFLEVBQUUsRUFBRSxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBRSxDQUFDO29CQUN6QyxNQUFNLENBQUMsU0FBUyxDQUFFLENBQUMsRUFBRSxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBRSxDQUFDO29CQUN4QyxNQUFNLENBQUMsU0FBUyxDQUFFLENBQUMsRUFBRyxRQUFRLENBQUMsS0FBZ0IsQ0FBQyxTQUFTLEVBQUUsQ0FBRSxDQUFDOzs7O1NBRWpFLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQyxxRkFBcUYsRUFBRTs7Ozs7b0JBRWxGLElBQUksR0FBYSxJQUFJLG1CQUFTLENBQUMsSUFBSSxhQUFLLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztvQkFDMUQsZUFBZSxHQUFHLElBQUkseUJBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLENBQUM7b0JBRTNFLE9BQU8sR0FBeUIsSUFBSSw4QkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDdEQscUJBQU0sT0FBTyxDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQyxFQUFBOztvQkFBNUQsTUFBTSxHQUFHLFNBQW1EO29CQUNoRSxNQUFNLENBQUMsRUFBRSxDQUFFLE1BQU0sS0FBSyxJQUFJLENBQUUsQ0FBQzs7OztTQUVoQyxDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsbUZBQW1GLEVBQUU7Ozs7O29CQUVoRixJQUFJLEdBQVMsSUFBSSxjQUFJLENBQUMsWUFBWSxFQUFFLFNBQVMsQ0FBQyxDQUFDO29CQUNuRCxJQUFJLENBQUMsS0FBSyxHQUFHLGVBQWUsQ0FBQTtvQkFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO29CQUM5QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztvQkFDL0IsV0FBVyxHQUFtQixJQUFJLHVCQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RDLHFCQUFNLFdBQVcsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUE7O29CQUFyRCxhQUFhLEdBQUcsU0FBcUM7b0JBRXJELElBQUksR0FBYSxJQUFJLG1CQUFTLENBQUMsSUFBSSxhQUFLLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQzdELGVBQWUsR0FBRyxJQUFJLHlCQUFXLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixDQUFDLENBQUM7b0JBQ2hGLGVBQWUsQ0FBQyxZQUFZLEdBQUcsZUFBZSxDQUFDO29CQUMvQyxlQUFlLENBQUMsV0FBVyxHQUFHLFlBQVksQ0FBQztvQkFDM0MsZUFBZSxDQUFDLFVBQVUsR0FBRyw2QkFBZSxDQUFDLFVBQVUsQ0FBQztvQkFFcEQsT0FBTyxHQUF5QixJQUFJLDhCQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0RCxxQkFBTSxPQUFPLENBQUMsb0JBQW9CLENBQUMsZUFBZSxDQUFDLEVBQUE7O29CQUE1RCxNQUFNLEdBQUcsU0FBbUQ7b0JBQ2hFLE1BQU0sQ0FBQyxFQUFFLENBQUUsTUFBTSxLQUFLLElBQUksQ0FBRSxDQUFDOzs7O1NBRWhDLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQyxrRkFBa0YsRUFBRTs7Ozs7b0JBRS9FLE9BQU8sR0FBeUIsSUFBSSw4QkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEMscUJBQU0sT0FBTyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFBOztvQkFBekUsTUFBTSxHQUFxQixTQUE4QztvQkFDN0UsTUFBTSxDQUFDLEVBQUUsQ0FBRSxNQUFNLEtBQUssSUFBSSxDQUFFLENBQUM7b0JBQ3pCLFFBQVEsR0FBZ0IsTUFBcUIsQ0FBQztvQkFFbEQsTUFBTSxDQUFDLFNBQVMsQ0FBRSxnQkFBZ0IsRUFBRSxRQUFRLENBQUMsU0FBUyxDQUFFLENBQUM7b0JBQ3pELE1BQU0sQ0FBQyxTQUFTLENBQUUsZUFBZSxFQUFFLFFBQVEsQ0FBQyxZQUFZLENBQUUsQ0FBQztvQkFDM0QsTUFBTSxDQUFDLFNBQVMsQ0FBRSw2QkFBZSxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVSxDQUFFLENBQUM7b0JBRWhFLElBQUksR0FBZSxRQUFRLENBQUMsS0FBbUIsQ0FBQztvQkFDcEQsTUFBTSxDQUFDLFNBQVMsQ0FBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBRSxDQUFDO29CQUMxQyxNQUFNLENBQUMsU0FBUyxDQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFFLENBQUM7b0JBQzFDLE1BQU0sQ0FBQyxTQUFTLENBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUUsQ0FBQztvQkFDekMsTUFBTSxDQUFDLFNBQVMsQ0FBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFFLENBQUM7b0JBQ3JDLE1BQU0sQ0FBQyxTQUFTLENBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBRSxDQUFDOzs7O1NBRXpDLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQywyRUFBMkUsRUFBRTs7Ozs7b0JBRXhFLE9BQU8sR0FBeUIsSUFBSSw4QkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEMscUJBQU0sT0FBTyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsRUFBQTs7b0JBQW5FLE1BQU0sR0FBcUIsU0FBd0M7b0JBQ3ZFLE1BQU0sQ0FBQyxFQUFFLENBQUUsTUFBTSxLQUFLLElBQUksQ0FBRSxDQUFDOzs7O1NBRWhDLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQyx1REFBdUQsRUFBRTs7Ozs7b0JBRXBELE9BQU8sR0FBeUIsSUFBSSw4QkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFFckQscUJBQU0sT0FBTyxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLEVBQUE7O29CQUEzRCxPQUFPLEdBQUcsU0FBaUQ7b0JBQy9ELE1BQU0sQ0FBQyxFQUFFLENBQUUsT0FBTyxLQUFLLElBQUksQ0FBRSxDQUFDO29CQUVoQixxQkFBTSxPQUFPLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsRUFBQTs7b0JBQTNELE9BQU8sR0FBRyxTQUFpRDtvQkFDL0QsTUFBTSxDQUFDLEVBQUUsQ0FBRSxPQUFPLEtBQUssSUFBSSxDQUFFLENBQUM7b0JBRUkscUJBQU0sT0FBTyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFBOztvQkFBNUUsU0FBUyxHQUFxQixTQUE4QztvQkFDaEYsTUFBTSxDQUFDLEVBQUUsQ0FBRSxTQUFTLEtBQUssSUFBSSxDQUFFLENBQUM7b0JBRUUscUJBQU0sT0FBTyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFBOztvQkFBNUUsU0FBUyxHQUFxQixTQUE4QztvQkFDaEYsTUFBTSxDQUFDLEVBQUUsQ0FBRSxTQUFTLEtBQUssSUFBSSxDQUFFLENBQUM7b0JBRzVCLFdBQVcsR0FBbUIsSUFBSSx1QkFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMxRCxXQUFXLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDOzs7O1NBRXhDLENBQUMsQ0FBQztBQUVQLENBQUMsQ0FBQyxDQUFDIiwiZmlsZSI6InN0b3JhZ2UvcGdvYmplY3Rtb2RlbHN0b3JhZ2V0ZXN0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFwibW9jaGFcIjtcclxuaW1wb3J0IFBnT2JqZWN0TW9kZWxTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9wZ29iamVjdG1vZGVsc3RvcmFnZVwiO1xyXG5pbXBvcnQgSVBnT2JqZWN0TW9kZWxTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pcGdvYmplY3Rtb2RlbHN0b3JhZ2VcIjtcclxuaW1wb3J0IElQZ1VzZXJTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pcGd1c2Vyc3RvcmFnZVwiO1xyXG5pbXBvcnQgUGdVc2VyU3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvcGd1c2Vyc3RvcmFnZVwiO1xyXG5pbXBvcnQgeyBPYmplY3RNb2RlbCwgT2JqZWN0TW9kZWxUeXBlIH0gZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbW9kZWwvb2JqZWN0bW9kZWxcIjtcclxuaW1wb3J0IENpcmNsZSBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tYXRoL2NpcmNsZVwiO1xyXG5pbXBvcnQgVXNlciBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tb2RlbC91c2VyXCI7XHJcbmltcG9ydCBSZWN0YW5nbGUgZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbWF0aC9yZWN0YW5nbGVcIjtcclxuaW1wb3J0IHsgUG9pbnQgfSBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tYXRoL3BvaW50XCI7XHJcblxyXG5sZXQgcGFyc2UgPSByZXF1aXJlKCdwZy1jb25uZWN0aW9uLXN0cmluZycpLnBhcnNlO1xyXG5sZXQgYXNzZXJ0ID0gcmVxdWlyZShcImFzc2VydFwiKTtcclxubGV0IHsgQ2xpZW50LCBQb29sIH0gPSByZXF1aXJlKCdwZycpO1xyXG5cclxuZGVzY3JpYmUoXCJQZ09iamVjdE1vZGVsU3RvcmFnZSBUZXN0XCIsICgpID0+IHtcclxuXHJcbiAgICBjb25zdCBIT1NUOiBzdHJpbmcgPSBcInBvc3RncmVzcWw6Ly9wb3N0Z3JlczphYmMxMjNAbG9jYWxob3N0OjU0MzIvaXNsZW9mZGFya25lc3NcIjsgXHJcbiAgICBjb25zdCBjb25maWc6IGFueSA9IHBhcnNlKEhPU1QpO1xyXG4gICAgY29uc3QgcG9vbDogYW55ID0gbmV3IFBvb2woY29uZmlnKTtcclxuICAgIGNvbnN0IHN0b3JhZ2U6IElQZ09iamVjdE1vZGVsU3RvcmFnZSA9IG5ldyBQZ09iamVjdE1vZGVsU3RvcmFnZShwb29sKTtcclxuICAgIGNvbnN0IG9iamVjdE1vZGVsVXVpZDE6IHN0cmluZyA9IFwiMmI1MmM4NzBcIjtcclxuICAgIGNvbnN0IG9iamVjdE1vZGVsVXVpZDI6IHN0cmluZyA9IFwiNDE4MmM2NGJcIjtcclxuICAgIGNvbnN0IG93bmVyX3VzZXJJZDogc3RyaW5nID0gXCIwZjMzOGMwNlwiO1xyXG5cclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gaW5zZXJ0IGFuIE9iamVjdE1vZGVsIG9iamVjdCAoY2lyY2xlKSBpbnRvIGEgUG9zdGdyZXMgZGF0YWJhc2VcIiwgYXN5bmMgKCkgPT4ge1xyXG5cclxuICAgICAgICBsZXQgY2lyY2xlOkNpcmNsZSA9IG5ldyBDaXJjbGUobmV3IFBvaW50KDMyLCAxMiksIDMpO1xyXG4gICAgICAgIGxldCBvYmplY3RNb2RlbERhdGEgPSBuZXcgT2JqZWN0TW9kZWwob2JqZWN0TW9kZWxVdWlkMSwgY2lyY2xlLCBcIk1vZGVsTmFtZVwiKTtcclxuXHJcbiAgICAgICAgbGV0IHN0b3JhZ2U6SVBnT2JqZWN0TW9kZWxTdG9yYWdlID0gbmV3IFBnT2JqZWN0TW9kZWxTdG9yYWdlKHBvb2wpO1xyXG4gICAgICAgIGxldCByZXN1bHQgPSBhd2FpdCBzdG9yYWdlLmFkZFVwZGF0ZU9iamVjdE1vZGVsKG9iamVjdE1vZGVsRGF0YSk7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKCByZXN1bHQgPT09IHRydWUgKTtcclxuXHJcbiAgICB9KTtcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIHVwZGF0ZSBhbiBPYmplY3RNb2RlbCBvYmplY3QgKGNpcmNsZSkgaW4gYSBQb3N0Z3JlcyBkYXRhYmFzZVwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCBjaXJjbGU6Q2lyY2xlID0gbmV3IENpcmNsZShuZXcgUG9pbnQoMjMsIDIxKSwgNik7XHJcbiAgICAgICAgbGV0IG9iamVjdE1vZGVsRGF0YSA9IG5ldyBPYmplY3RNb2RlbChvYmplY3RNb2RlbFV1aWQxLCBjaXJjbGUsIFwiTW9kZWxOYW1lMlwiKTtcclxuICAgICAgICBvYmplY3RNb2RlbERhdGEubW9kZWxUZXh0dXJlID0gXCJNb2RlbFRleHR1cmUxXCI7XHJcbiAgICAgICAgb2JqZWN0TW9kZWxEYXRhLm9iamVjdFR5cGUgPSBPYmplY3RNb2RlbFR5cGUuSW1wYXNzYWJsZTtcclxuXHJcbiAgICAgICAgbGV0IHN0b3JhZ2U6SVBnT2JqZWN0TW9kZWxTdG9yYWdlID0gbmV3IFBnT2JqZWN0TW9kZWxTdG9yYWdlKHBvb2wpO1xyXG4gICAgICAgIGxldCByZXN1bHQgPSBhd2FpdCBzdG9yYWdlLmFkZFVwZGF0ZU9iamVjdE1vZGVsKG9iamVjdE1vZGVsRGF0YSk7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKCByZXN1bHQgPT09IHRydWUgKTtcclxuICAgICAgIFxyXG4gICAgfSk7XHJcblxyXG4gICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBnZXQgYW4gT2JqZWN0TW9kZWwgb2JqZWN0IChjaXJjbGUpIGZyb20gYSBQb3N0Z3JlcyBkYXRhYmFzZVwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCBzdG9yYWdlOklQZ09iamVjdE1vZGVsU3RvcmFnZSA9IG5ldyBQZ09iamVjdE1vZGVsU3RvcmFnZShwb29sKTtcclxuICAgICAgICBsZXQgcmVzdWx0OiBPYmplY3RNb2RlbHxudWxsID0gYXdhaXQgc3RvcmFnZS5nZXRPYmplY3RNb2RlbChvYmplY3RNb2RlbFV1aWQxKTtcclxuICAgICAgICBhc3NlcnQub2soIHJlc3VsdCAhPT0gbnVsbCApO1xyXG4gICAgICAgIGxldCBvYmpNb2RlbDogT2JqZWN0TW9kZWwgPSByZXN1bHQgYXMgT2JqZWN0TW9kZWw7XHJcblxyXG4gICAgICAgIGFzc2VydC5kZWVwRXF1YWwoIFwiTW9kZWxOYW1lMlwiLCBvYmpNb2RlbC5tb2RlbE5hbWUgKTtcclxuICAgICAgICBhc3NlcnQuZGVlcEVxdWFsKCBcIk1vZGVsVGV4dHVyZTFcIiwgb2JqTW9kZWwubW9kZWxUZXh0dXJlICk7XHJcbiAgICAgICAgYXNzZXJ0LmRlZXBFcXVhbCggT2JqZWN0TW9kZWxUeXBlLkltcGFzc2FibGUsIG9iak1vZGVsLm9iamVjdFR5cGUgKTtcclxuICAgICAgICBhc3NlcnQuZGVlcEVxdWFsKCAyMywgb2JqTW9kZWwuc2hhcGUueCApO1xyXG4gICAgICAgIGFzc2VydC5kZWVwRXF1YWwoIDIxLCBvYmpNb2RlbC5zaGFwZS55ICk7XHJcbiAgICAgICAgYXNzZXJ0LmRlZXBFcXVhbCggMCwgb2JqTW9kZWwuc2hhcGUueiApO1xyXG4gICAgICAgIGFzc2VydC5kZWVwRXF1YWwoIDYsIChvYmpNb2RlbC5zaGFwZSBhcyBDaXJjbGUpLmdldFJhZGl1cygpICk7XHJcbiAgICAgICBcclxuICAgIH0pO1xyXG5cclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gaW5zZXJ0IGFuIE9iamVjdE1vZGVsIG9iamVjdCAocmVjdGFuZ2xlKSBpbnRvIGEgUG9zdGdyZXMgZGF0YWJhc2VcIiwgYXN5bmMgKCkgPT4ge1xyXG5cclxuICAgICAgICBsZXQgcmVjdDpSZWN0YW5nbGUgPSBuZXcgUmVjdGFuZ2xlKG5ldyBQb2ludCg0NSwgOTgpLCAzMCwgMjApO1xyXG4gICAgICAgIGxldCBvYmplY3RNb2RlbERhdGEgPSBuZXcgT2JqZWN0TW9kZWwob2JqZWN0TW9kZWxVdWlkMiwgcmVjdCwgXCJUZXN0TW9kZWxOYW1lXCIpO1xyXG5cclxuICAgICAgICBsZXQgc3RvcmFnZTpJUGdPYmplY3RNb2RlbFN0b3JhZ2UgPSBuZXcgUGdPYmplY3RNb2RlbFN0b3JhZ2UocG9vbCk7XHJcbiAgICAgICAgbGV0IHJlc3VsdCA9IGF3YWl0IHN0b3JhZ2UuYWRkVXBkYXRlT2JqZWN0TW9kZWwob2JqZWN0TW9kZWxEYXRhKTtcclxuICAgICAgICBhc3NlcnQub2soIHJlc3VsdCA9PT0gdHJ1ZSApO1xyXG4gICAgICAgXHJcbiAgICB9KTtcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIHVwZGF0ZSBhbiBPYmplY3RNb2RlbCBvYmplY3QgKHJlY3RhbmdsZSkgaW4gYSBQb3N0Z3JlcyBkYXRhYmFzZVwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCB1c2VyOiBVc2VyID0gbmV3IFVzZXIob3duZXJfdXNlcklkLCBcInRlc3QxMjNcIik7XHJcbiAgICAgICAgdXNlci5lbWFpbCA9IFwiYXNkaXQuYXNkLmNvbVwiXHJcbiAgICAgICAgdXNlci5jcmVhdGVkRGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgdXNlci5sYXN0TG9nZ2VkSW5EYXRlID0gbmV3IERhdGUoKTtcclxuICAgICAgICBsZXQgdXNlclN0b3JhZ2U6IElQZ1VzZXJTdG9yYWdlID0gbmV3IFBnVXNlclN0b3JhZ2UocG9vbCk7XHJcbiAgICAgICAgbGV0IHVzZXJBZGRlZFRvRGIgPSBhd2FpdCB1c2VyU3RvcmFnZS5hZGRVcGRhdGVVc2VyKHVzZXIpO1xyXG5cclxuICAgICAgICBsZXQgcmVjdDpSZWN0YW5nbGUgPSBuZXcgUmVjdGFuZ2xlKG5ldyBQb2ludCg1NCwgODksIDIpLCAyMCwgMzApO1xyXG4gICAgICAgIGxldCBvYmplY3RNb2RlbERhdGEgPSBuZXcgT2JqZWN0TW9kZWwob2JqZWN0TW9kZWxVdWlkMiwgcmVjdCwgXCJUZXN0TW9kZWxOYW1lMlwiKTtcclxuICAgICAgICBvYmplY3RNb2RlbERhdGEubW9kZWxUZXh0dXJlID0gXCJNb2RlbFRleHR1cmUyXCI7XHJcbiAgICAgICAgb2JqZWN0TW9kZWxEYXRhLm93bmVyVXNlcklkID0gb3duZXJfdXNlcklkO1xyXG4gICAgICAgIG9iamVjdE1vZGVsRGF0YS5vYmplY3RUeXBlID0gT2JqZWN0TW9kZWxUeXBlLkltcGFzc2FibGU7XHJcblxyXG4gICAgICAgIGxldCBzdG9yYWdlOklQZ09iamVjdE1vZGVsU3RvcmFnZSA9IG5ldyBQZ09iamVjdE1vZGVsU3RvcmFnZShwb29sKTtcclxuICAgICAgICBsZXQgcmVzdWx0ID0gYXdhaXQgc3RvcmFnZS5hZGRVcGRhdGVPYmplY3RNb2RlbChvYmplY3RNb2RlbERhdGEpO1xyXG4gICAgICAgIGFzc2VydC5vayggcmVzdWx0ID09PSB0cnVlICk7XHJcbiAgICAgICBcclxuICAgIH0pO1xyXG5cclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gZ2V0IGFuIE9iamVjdE1vZGVsIG9iamVjdCAocmVjdGFuZ2xlKSBmcm9tIGEgUG9zdGdyZXMgZGF0YWJhc2VcIiwgYXN5bmMgKCkgPT4ge1xyXG5cclxuICAgICAgICBsZXQgc3RvcmFnZTpJUGdPYmplY3RNb2RlbFN0b3JhZ2UgPSBuZXcgUGdPYmplY3RNb2RlbFN0b3JhZ2UocG9vbCk7XHJcbiAgICAgICAgbGV0IHJlc3VsdDogT2JqZWN0TW9kZWx8bnVsbCA9IGF3YWl0IHN0b3JhZ2UuZ2V0T2JqZWN0TW9kZWwob2JqZWN0TW9kZWxVdWlkMik7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKCByZXN1bHQgIT09IG51bGwgKTtcclxuICAgICAgICBsZXQgb2JqTW9kZWw6IE9iamVjdE1vZGVsID0gcmVzdWx0IGFzIE9iamVjdE1vZGVsO1xyXG5cclxuICAgICAgICBhc3NlcnQuZGVlcEVxdWFsKCBcIlRlc3RNb2RlbE5hbWUyXCIsIG9iak1vZGVsLm1vZGVsTmFtZSApO1xyXG4gICAgICAgIGFzc2VydC5kZWVwRXF1YWwoIFwiTW9kZWxUZXh0dXJlMlwiLCBvYmpNb2RlbC5tb2RlbFRleHR1cmUgKTtcclxuICAgICAgICBhc3NlcnQuZGVlcEVxdWFsKCBPYmplY3RNb2RlbFR5cGUuSW1wYXNzYWJsZSwgb2JqTW9kZWwub2JqZWN0VHlwZSApO1xyXG5cclxuICAgICAgICBsZXQgcmVjdDogUmVjdGFuZ2xlID0gKG9iak1vZGVsLnNoYXBlIGFzIFJlY3RhbmdsZSk7XHJcbiAgICAgICAgYXNzZXJ0LmRlZXBFcXVhbCggNTQsIHJlY3QucG9zaXRpb24oKS54ICk7XHJcbiAgICAgICAgYXNzZXJ0LmRlZXBFcXVhbCggODksIHJlY3QucG9zaXRpb24oKS55ICk7XHJcbiAgICAgICAgYXNzZXJ0LmRlZXBFcXVhbCggMiwgcmVjdC5wb3NpdGlvbigpLnogKTtcclxuICAgICAgICBhc3NlcnQuZGVlcEVxdWFsKCAyMCwgcmVjdC53aWR0aCgpICk7XHJcbiAgICAgICAgYXNzZXJ0LmRlZXBFcXVhbCggMzAsIHJlY3QuaGVpZ2h0KCkgKTtcclxuXHJcbiAgICB9KTtcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIHRlbGwgaWYgbm8gb2JqZWN0IG1vZGVsIGV4aXN0cyBpbiB0aGUgUG9zdGdyZXMgZGF0YWJhc2VcIiwgYXN5bmMgKCkgPT4ge1xyXG5cclxuICAgICAgICBsZXQgc3RvcmFnZTpJUGdPYmplY3RNb2RlbFN0b3JhZ2UgPSBuZXcgUGdPYmplY3RNb2RlbFN0b3JhZ2UocG9vbCk7XHJcbiAgICAgICAgbGV0IHJlc3VsdDogT2JqZWN0TW9kZWx8bnVsbCA9IGF3YWl0IHN0b3JhZ2UuZ2V0T2JqZWN0TW9kZWwoXCIwMDAwMDAwMFwiKTtcclxuICAgICAgICBhc3NlcnQub2soIHJlc3VsdCA9PT0gbnVsbCApOyAgICAgICAgXHJcblxyXG4gICAgfSk7ICAgICAgICAgICBcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIGRlbGV0ZSBhbiBPYmplY3RNb2RlbCBmcm9tIFBvc3RncmVzXCIsIGFzeW5jICgpID0+IHtcclxuXHJcbiAgICAgICAgbGV0IHN0b3JhZ2U6SVBnT2JqZWN0TW9kZWxTdG9yYWdlID0gbmV3IFBnT2JqZWN0TW9kZWxTdG9yYWdlKHBvb2wpO1xyXG5cclxuICAgICAgICBsZXQgcmVzdWx0MSA9IGF3YWl0IHN0b3JhZ2UuZGVsZXRlT2JqZWN0TW9kZWwob2JqZWN0TW9kZWxVdWlkMSk7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKCByZXN1bHQxID09PSB0cnVlICk7XHJcblxyXG4gICAgICAgIGxldCByZXN1bHQyID0gYXdhaXQgc3RvcmFnZS5kZWxldGVPYmplY3RNb2RlbChvYmplY3RNb2RlbFV1aWQyKTtcclxuICAgICAgICBhc3NlcnQub2soIHJlc3VsdDIgPT09IHRydWUgKTtcclxuICAgICAgICBcclxuICAgICAgICBsZXQgb2JqTW9kZWwxOiBPYmplY3RNb2RlbHxudWxsID0gYXdhaXQgc3RvcmFnZS5nZXRPYmplY3RNb2RlbChvYmplY3RNb2RlbFV1aWQxKTtcclxuICAgICAgICBhc3NlcnQub2soIG9iak1vZGVsMSA9PT0gbnVsbCApO1xyXG5cclxuICAgICAgICBsZXQgb2JqTW9kZWwyOiBPYmplY3RNb2RlbHxudWxsID0gYXdhaXQgc3RvcmFnZS5nZXRPYmplY3RNb2RlbChvYmplY3RNb2RlbFV1aWQyKTtcclxuICAgICAgICBhc3NlcnQub2soIG9iak1vZGVsMiA9PT0gbnVsbCApOyAgICAgICAgXHJcblxyXG4gICAgICAgIC8vIENsZWFuIHVwXHJcbiAgICAgICAgbGV0IHVzZXJTdG9yYWdlOiBJUGdVc2VyU3RvcmFnZSA9IG5ldyBQZ1VzZXJTdG9yYWdlKHBvb2wpO1xyXG4gICAgICAgIHVzZXJTdG9yYWdlLmRlbGV0ZVVzZXIob3duZXJfdXNlcklkKTtcclxuXHJcbiAgICB9KTtcclxuXHJcbn0pO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
