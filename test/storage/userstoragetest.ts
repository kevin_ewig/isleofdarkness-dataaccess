import "mocha";

import User from "isleofdarkness-common/src/model/user";
import IUserStorage from "../../src/storage/iuserstorage";
import UserStorage from "../../src/storage/userstorage";

let assert = require("assert");
let redis = require("redis");

describe("UserStorage Test", () => {

    const HOST:string = "redis://localhost:6379"; 

    it("should store User object into Temporary Storage", async () => {

        let user1 = new User("U123000", "Player 1");

        let redisClient:any = redis.createClient(HOST);
        redisClient.on("error", function (err:any) {
            console.log("Error " + err);
        });

        let storage:IUserStorage = new UserStorage(redisClient);

        await storage.addUpdateUser(user1);

        let returnValue:User = await storage.getUser("U123000");

        let json1 = JSON.stringify(returnValue.toJSON());
        let json2 = JSON.stringify(user1.toJSON());
        assert(json1 == json2);

    });

    it("should delete User object from Temporary Storage", async () => {

        let user1 = new User("U987", "Player 1");

        let redisClient:any = redis.createClient(this.HOST);
        redisClient.on("error", function (err:any) {
            console.log("Error " + err);
        });

        let storage:IUserStorage = new UserStorage(redisClient);

        await storage.addUpdateUser(user1);
        await storage.deleteUser(user1.userId);

        let returnValue:User = await storage.getUser("U987");
        assert( returnValue === undefined );

    });

});