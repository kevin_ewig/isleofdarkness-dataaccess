"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var pgstoragefactory_1 = require("../../src/storage/pgstoragefactory");
var ibasestorage_1 = require("../../src/storage/ibasestorage");
var parse = require('pg-connection-string').parse;
var assert = require("assert");
var _a = require('pg'), Client = _a.Client, Pool = _a.Pool;
describe("PgStorageFactory Test", function () {
    var HOST = "postgresql://postgres:abc123@localhost:5432/isleofdarkness";
    var config = parse(HOST);
    var pool = new Pool(config);
    it("should be able to get the correct storage object from factory", function (done) {
        var storageFactory = new pgstoragefactory_1.default(pool);
        var objModelStorage = storageFactory.getStorage(ibasestorage_1.StorageType.ObjectModel);
        assert.equal(ibasestorage_1.StorageType.ObjectModel, objModelStorage.getStorageType());
        assert.ok(objModelStorage !== null);
        var userMetaDataStorage = storageFactory.getStorage(ibasestorage_1.StorageType.User);
        assert.equal(ibasestorage_1.StorageType.User, userMetaDataStorage.getStorageType());
        assert.ok(userMetaDataStorage !== null);
        done();
    });
    it("should be able to check if connection is still present", function () { return __awaiter(_this, void 0, void 0, function () {
        var storageFactory, connected;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    storageFactory = new pgstoragefactory_1.default(pool);
                    return [4 /*yield*/, storageFactory.isConnected()];
                case 1:
                    connected = _a.sent();
                    assert.ok(connected === true);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to typecast the correct storage object using generics", function (done) {
        var storageFactory = new pgstoragefactory_1.default(pool);
        var objModelStorage = storageFactory.getStorage(ibasestorage_1.StorageType.ObjectModel);
        assert.equal(ibasestorage_1.StorageType.ObjectModel, objModelStorage.getStorageType());
        assert.ok(objModelStorage !== null);
        var userMetaDataStorage = storageFactory.getStorage(ibasestorage_1.StorageType.User);
        assert.equal(ibasestorage_1.StorageType.User, userMetaDataStorage.getStorageType());
        assert.ok(userMetaDataStorage !== null);
        done();
    });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvcGdzdG9yYWdlZmFjdG9yeXRlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsaUJBMERBOztBQTFEQSxpQkFBZTtBQUVmLHVFQUFrRTtBQUNsRSwrREFBeUU7QUFJekUsSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUMsS0FBSyxDQUFDO0FBQ2xELElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMzQixJQUFBLGtCQUFnQyxFQUE5QixrQkFBTSxFQUFFLGNBQUksQ0FBbUI7QUFFckMsUUFBUSxDQUFDLHVCQUF1QixFQUFFO0lBRTlCLElBQU0sSUFBSSxHQUFXLDREQUE0RCxDQUFDO0lBQ2xGLElBQU0sTUFBTSxHQUFRLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoQyxJQUFNLElBQUksR0FBUSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUVuQyxFQUFFLENBQUMsK0RBQStELEVBQUUsVUFBQyxJQUFhO1FBRTlFLElBQUksY0FBYyxHQUFxQixJQUFJLDBCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBRWxFLElBQUksZUFBZSxHQUFnQixjQUFjLENBQUMsVUFBVSxDQUFFLDBCQUFXLENBQUMsV0FBVyxDQUFFLENBQUM7UUFDeEYsTUFBTSxDQUFDLEtBQUssQ0FBRSwwQkFBVyxDQUFDLFdBQVcsRUFBRSxlQUFlLENBQUMsY0FBYyxFQUFFLENBQUUsQ0FBQztRQUMxRSxNQUFNLENBQUMsRUFBRSxDQUFFLGVBQXdDLEtBQUssSUFBSSxDQUFFLENBQUM7UUFFL0QsSUFBSSxtQkFBbUIsR0FBZ0IsY0FBYyxDQUFDLFVBQVUsQ0FBRSwwQkFBVyxDQUFDLElBQUksQ0FBRSxDQUFDO1FBQ3JGLE1BQU0sQ0FBQyxLQUFLLENBQUUsMEJBQVcsQ0FBQyxJQUFJLEVBQUUsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUUsQ0FBQztRQUN2RSxNQUFNLENBQUMsRUFBRSxDQUFFLG1CQUFxQyxLQUFLLElBQUksQ0FBRSxDQUFDO1FBRTVELElBQUksRUFBRSxDQUFDO0lBRVgsQ0FBQyxDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsd0RBQXdELEVBQUU7Ozs7O29CQUVyRCxjQUFjLEdBQXFCLElBQUksMEJBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3pDLHFCQUFNLGNBQWMsQ0FBQyxXQUFXLEVBQUUsRUFBQTs7b0JBQXZELFNBQVMsR0FBWSxTQUFrQztvQkFDM0QsTUFBTSxDQUFDLEVBQUUsQ0FBRSxTQUFTLEtBQUssSUFBSSxDQUFFLENBQUM7Ozs7U0FFbkMsQ0FBQyxDQUFDO0lBRUgsRUFBRSxDQUFDLHNFQUFzRSxFQUFFLFVBQUMsSUFBYztRQUV0RixJQUFJLGNBQWMsR0FBcUIsSUFBSSwwQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVsRSxJQUFJLGVBQWUsR0FBeUIsY0FBYyxDQUFDLFVBQVUsQ0FBeUIsMEJBQVcsQ0FBQyxXQUFXLENBQUUsQ0FBQztRQUN4SCxNQUFNLENBQUMsS0FBSyxDQUFFLDBCQUFXLENBQUMsV0FBVyxFQUFFLGVBQWUsQ0FBQyxjQUFjLEVBQUUsQ0FBRSxDQUFDO1FBQzFFLE1BQU0sQ0FBQyxFQUFFLENBQUUsZUFBd0MsS0FBSyxJQUFJLENBQUUsQ0FBQztRQUUvRCxJQUFJLG1CQUFtQixHQUFrQixjQUFjLENBQUMsVUFBVSxDQUFrQiwwQkFBVyxDQUFDLElBQUksQ0FBRSxDQUFDO1FBQ3ZHLE1BQU0sQ0FBQyxLQUFLLENBQUUsMEJBQVcsQ0FBQyxJQUFJLEVBQUUsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUUsQ0FBQztRQUN2RSxNQUFNLENBQUMsRUFBRSxDQUFFLG1CQUFxQyxLQUFLLElBQUksQ0FBRSxDQUFDO1FBRTVELElBQUksRUFBRSxDQUFDO0lBRVgsQ0FBQyxDQUFDLENBQUM7QUFFUCxDQUFDLENBQUMsQ0FBQyIsImZpbGUiOiJzdG9yYWdlL3Bnc3RvcmFnZWZhY3Rvcnl0ZXN0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFwibW9jaGFcIjtcclxuaW1wb3J0IElQZ1N0b3JhZ2VGYWN0b3J5IGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pcGdzdG9yYWdlZmFjdG9yeVwiO1xyXG5pbXBvcnQgUGdTdG9yYWdlRmFjdG9yeSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvcGdzdG9yYWdlZmFjdG9yeVwiO1xyXG5pbXBvcnQgSUJhc2VTdG9yYWdlLCB7U3RvcmFnZVR5cGV9IGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pYmFzZXN0b3JhZ2VcIjtcclxuaW1wb3J0IElQZ09iamVjdE1vZGVsU3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvaXBnb2JqZWN0bW9kZWxzdG9yYWdlXCI7XHJcbmltcG9ydCBJUGdVc2VyU3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvaXBndXNlcnN0b3JhZ2VcIjtcclxuXHJcbmxldCBwYXJzZSA9IHJlcXVpcmUoJ3BnLWNvbm5lY3Rpb24tc3RyaW5nJykucGFyc2U7XHJcbmxldCBhc3NlcnQgPSByZXF1aXJlKFwiYXNzZXJ0XCIpO1xyXG5sZXQgeyBDbGllbnQsIFBvb2wgfSA9IHJlcXVpcmUoJ3BnJyk7XHJcblxyXG5kZXNjcmliZShcIlBnU3RvcmFnZUZhY3RvcnkgVGVzdFwiLCAoKSA9PiB7XHJcblxyXG4gICAgY29uc3QgSE9TVDogc3RyaW5nID0gXCJwb3N0Z3Jlc3FsOi8vcG9zdGdyZXM6YWJjMTIzQGxvY2FsaG9zdDo1NDMyL2lzbGVvZmRhcmtuZXNzXCI7IFxyXG4gICAgY29uc3QgY29uZmlnOiBhbnkgPSBwYXJzZShIT1NUKTtcclxuICAgIGNvbnN0IHBvb2w6IGFueSA9IG5ldyBQb29sKGNvbmZpZyk7XHJcblxyXG4gICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBnZXQgdGhlIGNvcnJlY3Qgc3RvcmFnZSBvYmplY3QgZnJvbSBmYWN0b3J5XCIsIChkb25lOkZ1bmN0aW9uKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCBzdG9yYWdlRmFjdG9yeTpJUGdTdG9yYWdlRmFjdG9yeSA9IG5ldyBQZ1N0b3JhZ2VGYWN0b3J5KHBvb2wpO1xyXG5cclxuICAgICAgICBsZXQgb2JqTW9kZWxTdG9yYWdlOklCYXNlU3RvcmFnZSA9IHN0b3JhZ2VGYWN0b3J5LmdldFN0b3JhZ2UoIFN0b3JhZ2VUeXBlLk9iamVjdE1vZGVsICk7XHJcbiAgICAgICAgYXNzZXJ0LmVxdWFsKCBTdG9yYWdlVHlwZS5PYmplY3RNb2RlbCwgb2JqTW9kZWxTdG9yYWdlLmdldFN0b3JhZ2VUeXBlKCkgKTtcclxuICAgICAgICBhc3NlcnQub2soIG9iak1vZGVsU3RvcmFnZSBhcyBJUGdPYmplY3RNb2RlbFN0b3JhZ2UgIT09IG51bGwgKTtcclxuXHJcbiAgICAgICAgbGV0IHVzZXJNZXRhRGF0YVN0b3JhZ2U6SUJhc2VTdG9yYWdlID0gc3RvcmFnZUZhY3RvcnkuZ2V0U3RvcmFnZSggU3RvcmFnZVR5cGUuVXNlciApO1xyXG4gICAgICAgIGFzc2VydC5lcXVhbCggU3RvcmFnZVR5cGUuVXNlciwgdXNlck1ldGFEYXRhU3RvcmFnZS5nZXRTdG9yYWdlVHlwZSgpICk7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKCB1c2VyTWV0YURhdGFTdG9yYWdlIGFzIElQZ1VzZXJTdG9yYWdlICE9PSBudWxsICk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgZG9uZSgpO1xyXG4gICAgICAgIFxyXG4gICAgfSk7ICBcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIGNoZWNrIGlmIGNvbm5lY3Rpb24gaXMgc3RpbGwgcHJlc2VudFwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCBzdG9yYWdlRmFjdG9yeTpJUGdTdG9yYWdlRmFjdG9yeSA9IG5ldyBQZ1N0b3JhZ2VGYWN0b3J5KHBvb2wpO1xyXG4gICAgICAgIGxldCBjb25uZWN0ZWQ6IGJvb2xlYW4gPSBhd2FpdCBzdG9yYWdlRmFjdG9yeS5pc0Nvbm5lY3RlZCgpO1xyXG4gICAgICAgIGFzc2VydC5vayggY29ubmVjdGVkID09PSB0cnVlICk7XHJcblxyXG4gICAgfSk7XHJcblxyXG4gICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byB0eXBlY2FzdCB0aGUgY29ycmVjdCBzdG9yYWdlIG9iamVjdCB1c2luZyBnZW5lcmljc1wiLCAoZG9uZTogRnVuY3Rpb24pID0+IHtcclxuXHJcbiAgICAgICAgbGV0IHN0b3JhZ2VGYWN0b3J5OklQZ1N0b3JhZ2VGYWN0b3J5ID0gbmV3IFBnU3RvcmFnZUZhY3RvcnkocG9vbCk7XHJcblxyXG4gICAgICAgIGxldCBvYmpNb2RlbFN0b3JhZ2U6SVBnT2JqZWN0TW9kZWxTdG9yYWdlID0gc3RvcmFnZUZhY3RvcnkuZ2V0U3RvcmFnZTxJUGdPYmplY3RNb2RlbFN0b3JhZ2U+KCBTdG9yYWdlVHlwZS5PYmplY3RNb2RlbCApO1xyXG4gICAgICAgIGFzc2VydC5lcXVhbCggU3RvcmFnZVR5cGUuT2JqZWN0TW9kZWwsIG9iak1vZGVsU3RvcmFnZS5nZXRTdG9yYWdlVHlwZSgpICk7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKCBvYmpNb2RlbFN0b3JhZ2UgYXMgSVBnT2JqZWN0TW9kZWxTdG9yYWdlICE9PSBudWxsICk7XHJcblxyXG4gICAgICAgIGxldCB1c2VyTWV0YURhdGFTdG9yYWdlOklQZ1VzZXJTdG9yYWdlID0gc3RvcmFnZUZhY3RvcnkuZ2V0U3RvcmFnZTxJUGdVc2VyU3RvcmFnZT4oIFN0b3JhZ2VUeXBlLlVzZXIgKTtcclxuICAgICAgICBhc3NlcnQuZXF1YWwoIFN0b3JhZ2VUeXBlLlVzZXIsIHVzZXJNZXRhRGF0YVN0b3JhZ2UuZ2V0U3RvcmFnZVR5cGUoKSApO1xyXG4gICAgICAgIGFzc2VydC5vayggdXNlck1ldGFEYXRhU3RvcmFnZSBhcyBJUGdVc2VyU3RvcmFnZSAhPT0gbnVsbCApO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGRvbmUoKTtcclxuXHJcbiAgICB9KTtcclxuXHJcbn0pO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
