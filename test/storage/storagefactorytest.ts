import "mocha";
import IStorageFactory from "../../src/storage/istoragefactory";
import StorageFactory from "../../src/storage/storagefactory";
import IBaseStorage, {StorageType} from "../../src/storage/ibasestorage";
import IObjectModelStorage from "../../src/storage/iobjectmodelstorage";
import IUserMetaDataStorage from "../../src/storage/iusermetadatastorage";

let assert = require("assert");
let redis = require("redis");

describe("StorageFactory Test", () => {

    const HOST:string = "redis://localhost:6379"; 
    var redisClient:any = null;

    before(function() {
        redisClient = redis.createClient(HOST);
        redisClient.on("error", function (err:any) {
            console.log("Error:" + err);
        });
    });

    it("should be able to get the correct storage object from factory", () => {

        let storageFactory:IStorageFactory = new StorageFactory(redisClient);

        let objModelStorage:IBaseStorage = storageFactory.getStorage( StorageType.ObjectModel );
        assert.equal( StorageType.ObjectModel, objModelStorage.getStorageType() );
        assert.ok( objModelStorage as IObjectModelStorage !== null );

        let userMetaDataStorage:IBaseStorage = storageFactory.getStorage( StorageType.UserMetaData );
        assert.equal( StorageType.UserMetaData, userMetaDataStorage.getStorageType() );
        assert.ok( userMetaDataStorage as IUserMetaDataStorage !== null );
       
    });  

    it("should be able to check if connection is still present", async () => {

        let storageFactory:IStorageFactory = new StorageFactory(redisClient);
        let connected: boolean = await storageFactory.isConnected();
        assert.ok( connected === true );
        
    });

});
