import "mocha";
import IPgBaseAnimatedCharacterStorage from "../../src/storage/ipgbaseanimatedcharacterstorage";
import PgBaseAnimatedCharacterStorage from "../../src/storage/pgbaseanimatedcharacterstorage";
import PgAnimationProfileStorage from "../../src/storage/pganimationprofilestorage";
import IPgAnimationProfileStorage from "../../src/storage/ipganimationprofilestorage";

let parse = require('pg-connection-string').parse;
let assert = require("assert");
let { Client, Pool } = require('pg');

describe("PgBaseAnimatedCharacterStorage Test", () => {

    const HOST: string = "postgresql://postgres:abc123@localhost:5432/isleofdarkness"; 
    const config: any = parse(HOST);
    const pool: any = new Pool(config);

    it("should be able to get base animated character from storage", async() => {

        let animationProfileStorage:IPgAnimationProfileStorage = new PgAnimationProfileStorage(pool);
        let baseAnimatedCharacterStorage:IPgBaseAnimatedCharacterStorage = new PgBaseAnimatedCharacterStorage(pool, animationProfileStorage);
        let baseAnimatedCharacter = await baseAnimatedCharacterStorage.getBaseAnimatedCharacter("b1");
        assert.ok( !!baseAnimatedCharacter == true );
        assert.equal("Aland", baseAnimatedCharacter.modelName);
        assert.ok( !!baseAnimatedCharacter.animationProfile );
        assert.equal("Aland", baseAnimatedCharacter.animationProfile.name);

    }); 

});
