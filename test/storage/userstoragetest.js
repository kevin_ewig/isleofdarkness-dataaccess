"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var user_1 = require("isleofdarkness-common/src/model/user");
var userstorage_1 = require("../../src/storage/userstorage");
var assert = require("assert");
var redis = require("redis");
describe("UserStorage Test", function () {
    var HOST = "redis://localhost:6379";
    it("should store User object into Temporary Storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var user1, redisClient, storage, returnValue, json1, json2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    user1 = new user_1.default("U123000", "Player 1");
                    redisClient = redis.createClient(HOST);
                    redisClient.on("error", function (err) {
                        console.log("Error " + err);
                    });
                    storage = new userstorage_1.default(redisClient);
                    return [4 /*yield*/, storage.addUpdateUser(user1)];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, storage.getUser("U123000")];
                case 2:
                    returnValue = _a.sent();
                    json1 = JSON.stringify(returnValue.toJSON());
                    json2 = JSON.stringify(user1.toJSON());
                    assert(json1 == json2);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should delete User object from Temporary Storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var user1, redisClient, storage, returnValue;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    user1 = new user_1.default("U987", "Player 1");
                    redisClient = redis.createClient(this.HOST);
                    redisClient.on("error", function (err) {
                        console.log("Error " + err);
                    });
                    storage = new userstorage_1.default(redisClient);
                    return [4 /*yield*/, storage.addUpdateUser(user1)];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, storage.deleteUser(user1.userId)];
                case 2:
                    _a.sent();
                    return [4 /*yield*/, storage.getUser("U987")];
                case 3:
                    returnValue = _a.sent();
                    assert(returnValue === undefined);
                    return [2 /*return*/];
            }
        });
    }); });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvdXNlcnN0b3JhZ2V0ZXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGlCQXFERzs7QUFyREgsaUJBQWU7QUFFZiw2REFBd0Q7QUFFeEQsNkRBQXdEO0FBRXhELElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMvQixJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7QUFFN0IsUUFBUSxDQUFDLGtCQUFrQixFQUFFO0lBRXpCLElBQU0sSUFBSSxHQUFVLHdCQUF3QixDQUFDO0lBRTdDLEVBQUUsQ0FBQyxpREFBaUQsRUFBRTs7Ozs7b0JBRTlDLEtBQUssR0FBRyxJQUFJLGNBQUksQ0FBQyxTQUFTLEVBQUUsVUFBVSxDQUFDLENBQUM7b0JBRXhDLFdBQVcsR0FBTyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMvQyxXQUFXLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFVLEdBQU87d0JBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxDQUFDO29CQUNoQyxDQUFDLENBQUMsQ0FBQztvQkFFQyxPQUFPLEdBQWdCLElBQUkscUJBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFFeEQscUJBQU0sT0FBTyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsRUFBQTs7b0JBQWxDLFNBQWtDLENBQUM7b0JBRVoscUJBQU0sT0FBTyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBQTs7b0JBQW5ELFdBQVcsR0FBUSxTQUFnQztvQkFFbkQsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7b0JBQzdDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO29CQUMzQyxNQUFNLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxDQUFDOzs7O1NBRTFCLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQyxrREFBa0QsRUFBRTs7Ozs7b0JBRS9DLEtBQUssR0FBRyxJQUFJLGNBQUksQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUM7b0JBRXJDLFdBQVcsR0FBTyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEQsV0FBVyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxHQUFPO3dCQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsQ0FBQztvQkFDaEMsQ0FBQyxDQUFDLENBQUM7b0JBRUMsT0FBTyxHQUFnQixJQUFJLHFCQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBRXhELHFCQUFNLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEVBQUE7O29CQUFsQyxTQUFrQyxDQUFDO29CQUNuQyxxQkFBTSxPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBQTs7b0JBQXRDLFNBQXNDLENBQUM7b0JBRWhCLHFCQUFNLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUE7O29CQUFoRCxXQUFXLEdBQVEsU0FBNkI7b0JBQ3BELE1BQU0sQ0FBRSxXQUFXLEtBQUssU0FBUyxDQUFFLENBQUM7Ozs7U0FFdkMsQ0FBQyxDQUFDO0FBRVAsQ0FBQyxDQUFDLENBQUMiLCJmaWxlIjoic3RvcmFnZS91c2Vyc3RvcmFnZXRlc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXCJtb2NoYVwiO1xyXG5cclxuaW1wb3J0IFVzZXIgZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbW9kZWwvdXNlclwiO1xyXG5pbXBvcnQgSVVzZXJTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pdXNlcnN0b3JhZ2VcIjtcclxuaW1wb3J0IFVzZXJTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS91c2Vyc3RvcmFnZVwiO1xyXG5cclxubGV0IGFzc2VydCA9IHJlcXVpcmUoXCJhc3NlcnRcIik7XHJcbmxldCByZWRpcyA9IHJlcXVpcmUoXCJyZWRpc1wiKTtcclxuXHJcbmRlc2NyaWJlKFwiVXNlclN0b3JhZ2UgVGVzdFwiLCAoKSA9PiB7XHJcblxyXG4gICAgY29uc3QgSE9TVDpzdHJpbmcgPSBcInJlZGlzOi8vbG9jYWxob3N0OjYzNzlcIjsgXHJcblxyXG4gICAgaXQoXCJzaG91bGQgc3RvcmUgVXNlciBvYmplY3QgaW50byBUZW1wb3JhcnkgU3RvcmFnZVwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCB1c2VyMSA9IG5ldyBVc2VyKFwiVTEyMzAwMFwiLCBcIlBsYXllciAxXCIpO1xyXG5cclxuICAgICAgICBsZXQgcmVkaXNDbGllbnQ6YW55ID0gcmVkaXMuY3JlYXRlQ2xpZW50KEhPU1QpO1xyXG4gICAgICAgIHJlZGlzQ2xpZW50Lm9uKFwiZXJyb3JcIiwgZnVuY3Rpb24gKGVycjphbnkpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnJvciBcIiArIGVycik7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGxldCBzdG9yYWdlOklVc2VyU3RvcmFnZSA9IG5ldyBVc2VyU3RvcmFnZShyZWRpc0NsaWVudCk7XHJcblxyXG4gICAgICAgIGF3YWl0IHN0b3JhZ2UuYWRkVXBkYXRlVXNlcih1c2VyMSk7XHJcblxyXG4gICAgICAgIGxldCByZXR1cm5WYWx1ZTpVc2VyID0gYXdhaXQgc3RvcmFnZS5nZXRVc2VyKFwiVTEyMzAwMFwiKTtcclxuXHJcbiAgICAgICAgbGV0IGpzb24xID0gSlNPTi5zdHJpbmdpZnkocmV0dXJuVmFsdWUudG9KU09OKCkpO1xyXG4gICAgICAgIGxldCBqc29uMiA9IEpTT04uc3RyaW5naWZ5KHVzZXIxLnRvSlNPTigpKTtcclxuICAgICAgICBhc3NlcnQoanNvbjEgPT0ganNvbjIpO1xyXG5cclxuICAgIH0pO1xyXG5cclxuICAgIGl0KFwic2hvdWxkIGRlbGV0ZSBVc2VyIG9iamVjdCBmcm9tIFRlbXBvcmFyeSBTdG9yYWdlXCIsIGFzeW5jICgpID0+IHtcclxuXHJcbiAgICAgICAgbGV0IHVzZXIxID0gbmV3IFVzZXIoXCJVOTg3XCIsIFwiUGxheWVyIDFcIik7XHJcblxyXG4gICAgICAgIGxldCByZWRpc0NsaWVudDphbnkgPSByZWRpcy5jcmVhdGVDbGllbnQodGhpcy5IT1NUKTtcclxuICAgICAgICByZWRpc0NsaWVudC5vbihcImVycm9yXCIsIGZ1bmN0aW9uIChlcnI6YW55KSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3IgXCIgKyBlcnIpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBsZXQgc3RvcmFnZTpJVXNlclN0b3JhZ2UgPSBuZXcgVXNlclN0b3JhZ2UocmVkaXNDbGllbnQpO1xyXG5cclxuICAgICAgICBhd2FpdCBzdG9yYWdlLmFkZFVwZGF0ZVVzZXIodXNlcjEpO1xyXG4gICAgICAgIGF3YWl0IHN0b3JhZ2UuZGVsZXRlVXNlcih1c2VyMS51c2VySWQpO1xyXG5cclxuICAgICAgICBsZXQgcmV0dXJuVmFsdWU6VXNlciA9IGF3YWl0IHN0b3JhZ2UuZ2V0VXNlcihcIlU5ODdcIik7XHJcbiAgICAgICAgYXNzZXJ0KCByZXR1cm5WYWx1ZSA9PT0gdW5kZWZpbmVkICk7XHJcblxyXG4gICAgfSk7XHJcblxyXG59KTsiXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
