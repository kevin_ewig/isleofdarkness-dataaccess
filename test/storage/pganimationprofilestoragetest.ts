import "mocha";
import IBaseStorage, {StorageType} from "../../src/storage/ibasestorage";
import IPgObjectModelStorage from "../../src/storage/ipgobjectmodelstorage";
import IPgUserStorage from "../../src/storage/ipguserstorage";
import IPgAnimationProfileStorage from "../../src/storage/ipganimationprofilestorage";
import PgAnimationProfileStorage from "../../src/storage/pganimationprofilestorage";

let parse = require('pg-connection-string').parse;
let assert = require("assert");
let { Client, Pool } = require('pg');

describe("PgAnimationProfileStorage Test", () => {

    const HOST: string = "postgresql://postgres:abc123@localhost:5432/isleofdarkness"; 
    const config: any = parse(HOST);
    const pool: any = new Pool(config);

    it("should be able to get data from an animation profile storage", async() => {

        let animationProfileStorage:IPgAnimationProfileStorage = new PgAnimationProfileStorage(pool);
        let animationProfile = await animationProfileStorage.getAnimationProfile("aland1");
        assert.ok( !!animationProfile == true );
        assert.equal("Aland", animationProfile.name);
    }); 

});
