"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var ticket_1 = require("isleofdarkness-common/src/model/ticket");
var ticketstorage_1 = require("../../src/storage/ticketstorage");
var assert = require("assert");
var redis = require("redis");
describe("TicketStorage Test", function () {
    var HOST = "redis://localhost:6379";
    it("should store Ticket object into Temporary Storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var ticket, redisClient, storage, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    ticket = ticket_1.default.NewTicket("U12345", 10000);
                    redisClient = redis.createClient(HOST);
                    redisClient.on("error", function (err) {
                        console.log("Error " + err);
                    });
                    storage = new ticketstorage_1.default(redisClient);
                    return [4 /*yield*/, storage.addTicket(ticket)];
                case 1:
                    result = _a.sent();
                    setTimeout(function () {
                        var promise = storage.getTicket(ticket.ticketId);
                        promise.then(function (retrievedTicket) {
                            assert.ok(retrievedTicket.ticketId == ticket.ticketId);
                        }).catch(function (err) {
                            console.log(err);
                            assert.ok(false);
                        });
                    }, 500);
                    return [2 /*return*/];
            }
        });
    }); }).timeout(4000);
    it("should expire ticket at the correct time", function () { return __awaiter(_this, void 0, void 0, function () {
        var ticket, redisClient, storage, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    ticket = ticket_1.default.NewTicket("U12345", 1);
                    redisClient = redis.createClient(HOST);
                    redisClient.on("error", function (err) {
                        console.log("Error " + err);
                    });
                    storage = new ticketstorage_1.default(redisClient);
                    return [4 /*yield*/, storage.addTicket(ticket)];
                case 1:
                    result = _a.sent();
                    setTimeout(function () {
                        var promise = storage.getTicket(ticket.ticketId);
                        promise.then(function (retrievedTicket) {
                            assert.ok(retrievedTicket === undefined);
                        }).catch(function (err) {
                            console.log(err);
                            assert.ok(false);
                        });
                    }, 3000);
                    return [2 /*return*/];
            }
        });
    }); }).timeout(4000);
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvdGlja2V0c3RvcmFnZXRlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsaUJBdUVHOztBQXZFSCxpQkFBZTtBQUVmLGlFQUE0RDtBQUU1RCxpRUFBNEQ7QUFFNUQsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQy9CLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUU3QixRQUFRLENBQUMsb0JBQW9CLEVBQUU7SUFFM0IsSUFBTSxJQUFJLEdBQVUsd0JBQXdCLENBQUM7SUFFN0MsRUFBRSxDQUFDLG1EQUFtRCxFQUFFOzs7OztvQkFFaEQsTUFBTSxHQUFHLGdCQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFFM0MsV0FBVyxHQUFPLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQy9DLFdBQVcsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsR0FBTzt3QkFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLENBQUM7b0JBQ2hDLENBQUMsQ0FBQyxDQUFDO29CQUVDLE9BQU8sR0FBa0IsSUFBSSx1QkFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUV2QyxxQkFBTSxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFBOztvQkFBaEQsTUFBTSxHQUFXLFNBQStCO29CQUVwRCxVQUFVLENBQUU7d0JBQ1IsSUFBSSxPQUFPLEdBQW1CLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUNqRSxPQUFPLENBQUMsSUFBSSxDQUNSLFVBQUMsZUFBZTs0QkFDWixNQUFNLENBQUMsRUFBRSxDQUFFLGVBQWUsQ0FBQyxRQUFRLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBRSxDQUFDO3dCQUM3RCxDQUFDLENBQ0osQ0FBQyxLQUFLLENBQ0gsVUFBQyxHQUFHOzRCQUNBLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7NEJBQ2pCLE1BQU0sQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ3JCLENBQUMsQ0FDSixDQUFDO29CQUNOLENBQUMsRUFBRSxHQUFHLENBQUUsQ0FBQzs7OztTQUVaLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7SUFFakIsRUFBRSxDQUFDLDBDQUEwQyxFQUFFOzs7OztvQkFFdkMsTUFBTSxHQUFHLGdCQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFFdkMsV0FBVyxHQUFPLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQy9DLFdBQVcsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsR0FBTzt3QkFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLENBQUM7b0JBQ2hDLENBQUMsQ0FBQyxDQUFDO29CQUVDLE9BQU8sR0FBa0IsSUFBSSx1QkFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUV2QyxxQkFBTSxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFBOztvQkFBaEQsTUFBTSxHQUFXLFNBQStCO29CQUVwRCxVQUFVLENBQUU7d0JBQ1IsSUFBSSxPQUFPLEdBQW1CLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUNqRSxPQUFPLENBQUMsSUFBSSxDQUNSLFVBQUMsZUFBZTs0QkFDWixNQUFNLENBQUMsRUFBRSxDQUFFLGVBQWUsS0FBSyxTQUFTLENBQUUsQ0FBQzt3QkFDL0MsQ0FBQyxDQUNKLENBQUMsS0FBSyxDQUNILFVBQUMsR0FBRzs0QkFDQSxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDOzRCQUNqQixNQUFNLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNyQixDQUFDLENBQ0osQ0FBQztvQkFDTixDQUFDLEVBQUUsSUFBSSxDQUFFLENBQUM7Ozs7U0FFYixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0FBRXJCLENBQUMsQ0FBQyxDQUFDIiwiZmlsZSI6InN0b3JhZ2UvdGlja2V0c3RvcmFnZXRlc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXCJtb2NoYVwiO1xyXG5cclxuaW1wb3J0IFRpY2tldCBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tb2RlbC90aWNrZXRcIjtcclxuaW1wb3J0IElUaWNrZXRTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pdGlja2V0c3RvcmFnZVwiO1xyXG5pbXBvcnQgVGlja2V0U3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvdGlja2V0c3RvcmFnZVwiO1xyXG5cclxubGV0IGFzc2VydCA9IHJlcXVpcmUoXCJhc3NlcnRcIik7XHJcbmxldCByZWRpcyA9IHJlcXVpcmUoXCJyZWRpc1wiKTtcclxuXHJcbmRlc2NyaWJlKFwiVGlja2V0U3RvcmFnZSBUZXN0XCIsICgpID0+IHtcclxuXHJcbiAgICBjb25zdCBIT1NUOnN0cmluZyA9IFwicmVkaXM6Ly9sb2NhbGhvc3Q6NjM3OVwiOyBcclxuXHJcbiAgICBpdChcInNob3VsZCBzdG9yZSBUaWNrZXQgb2JqZWN0IGludG8gVGVtcG9yYXJ5IFN0b3JhZ2VcIiwgYXN5bmMgKCkgPT4ge1xyXG5cclxuICAgICAgICBsZXQgdGlja2V0ID0gVGlja2V0Lk5ld1RpY2tldChcIlUxMjM0NVwiLCAxMDAwMCk7XHJcblxyXG4gICAgICAgIGxldCByZWRpc0NsaWVudDphbnkgPSByZWRpcy5jcmVhdGVDbGllbnQoSE9TVCk7XHJcbiAgICAgICAgcmVkaXNDbGllbnQub24oXCJlcnJvclwiLCBmdW5jdGlvbiAoZXJyOmFueSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yIFwiICsgZXJyKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IHN0b3JhZ2U6SVRpY2tldFN0b3JhZ2UgPSBuZXcgVGlja2V0U3RvcmFnZShyZWRpc0NsaWVudCk7XHJcblxyXG4gICAgICAgIGxldCByZXN1bHQ6Ym9vbGVhbiA9IGF3YWl0IHN0b3JhZ2UuYWRkVGlja2V0KHRpY2tldCk7XHJcblxyXG4gICAgICAgIHNldFRpbWVvdXQoIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICBsZXQgcHJvbWlzZTpQcm9taXNlPFRpY2tldD4gPSBzdG9yYWdlLmdldFRpY2tldCh0aWNrZXQudGlja2V0SWQpO1xyXG4gICAgICAgICAgICBwcm9taXNlLnRoZW4oXHJcbiAgICAgICAgICAgICAgICAocmV0cmlldmVkVGlja2V0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXNzZXJ0Lm9rKCByZXRyaWV2ZWRUaWNrZXQudGlja2V0SWQgPT0gdGlja2V0LnRpY2tldElkICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICkuY2F0Y2goXHJcbiAgICAgICAgICAgICAgICAoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyKTtcclxuICAgICAgICAgICAgICAgICAgICBhc3NlcnQub2soZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0sIDUwMCApO1xyXG5cclxuICAgIH0pLnRpbWVvdXQoNDAwMCk7XHJcblxyXG4gICAgaXQoXCJzaG91bGQgZXhwaXJlIHRpY2tldCBhdCB0aGUgY29ycmVjdCB0aW1lXCIsIGFzeW5jICgpID0+IHtcclxuXHJcbiAgICAgICAgbGV0IHRpY2tldCA9IFRpY2tldC5OZXdUaWNrZXQoXCJVMTIzNDVcIiwgMSk7XHJcblxyXG4gICAgICAgIGxldCByZWRpc0NsaWVudDphbnkgPSByZWRpcy5jcmVhdGVDbGllbnQoSE9TVCk7XHJcbiAgICAgICAgcmVkaXNDbGllbnQub24oXCJlcnJvclwiLCBmdW5jdGlvbiAoZXJyOmFueSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yIFwiICsgZXJyKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IHN0b3JhZ2U6SVRpY2tldFN0b3JhZ2UgPSBuZXcgVGlja2V0U3RvcmFnZShyZWRpc0NsaWVudCk7XHJcblxyXG4gICAgICAgIGxldCByZXN1bHQ6Ym9vbGVhbiA9IGF3YWl0IHN0b3JhZ2UuYWRkVGlja2V0KHRpY2tldCk7XHJcblxyXG4gICAgICAgIHNldFRpbWVvdXQoIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICBsZXQgcHJvbWlzZTpQcm9taXNlPFRpY2tldD4gPSBzdG9yYWdlLmdldFRpY2tldCh0aWNrZXQudGlja2V0SWQpO1xyXG4gICAgICAgICAgICBwcm9taXNlLnRoZW4oXHJcbiAgICAgICAgICAgICAgICAocmV0cmlldmVkVGlja2V0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXNzZXJ0Lm9rKCByZXRyaWV2ZWRUaWNrZXQgPT09IHVuZGVmaW5lZCApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApLmNhdGNoKFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XHJcbiAgICAgICAgICAgICAgICAgICAgYXNzZXJ0Lm9rKGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9LCAzMDAwICk7XHJcblxyXG4gICAgfSkudGltZW91dCg0MDAwKTtcclxuXHJcbn0pOyJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
