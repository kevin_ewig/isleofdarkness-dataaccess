"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var pguserstorage_1 = require("../../src/storage/pguserstorage");
var user_1 = require("isleofdarkness-common/src/model/user");
var parse = require('pg-connection-string').parse;
var assert = require("assert");
var _a = require('pg'), Client = _a.Client, Pool = _a.Pool;
describe("PgUserStorage Test", function () {
    var HOST = "postgresql://postgres:abc123@localhost:5432/isleofdarkness";
    var config = parse(HOST);
    var pool = new Pool(config);
    var storage = new pguserstorage_1.default(pool);
    var uuidUserId = "6890d470";
    var pictureUrl = "http://www.picture.com/pic.jpg";
    it("should be able to add a user into storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var user, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    user = new user_1.default(uuidUserId, "john");
                    user.createdDate = new Date();
                    user.disabled = false;
                    user.email = "email@john.com";
                    user.language = "en";
                    user.lastLoggedInDate = new Date();
                    user.pictureUrl = pictureUrl;
                    return [4 /*yield*/, storage.addUpdateUser(user)];
                case 1:
                    result = _a.sent();
                    assert.ok(result);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to update a user in storage using add/update", function () { return __awaiter(_this, void 0, void 0, function () {
        var user, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    user = new user_1.default(uuidUserId, "john");
                    user.createdDate = new Date();
                    user.disabled = true;
                    user.email = "john@email.com";
                    user.language = "gb";
                    user.lastLoggedInDate = new Date();
                    user.pictureUrl = pictureUrl;
                    return [4 /*yield*/, storage.addUpdateUser(user)];
                case 1:
                    result = _a.sent();
                    assert.ok(result);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to get a user that does not exist from storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, storage.getUser("00000000")];
                case 1:
                    user = _a.sent();
                    assert.ok(user == undefined);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to get a user from storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, storage.getUser(uuidUserId)];
                case 1:
                    user = _a.sent();
                    assert.ok(user !== undefined);
                    assert.ok(user.userId == uuidUserId);
                    assert.ok(user.name == "john");
                    assert.ok(user.email == "john@email.com");
                    assert.ok(user.language == "gb");
                    assert.ok(user.disabled == true);
                    assert.ok(user.createdDate !== undefined);
                    assert.deepEqual(user.pictureUrl, pictureUrl);
                    assert.ok(user.pictureUrl == pictureUrl);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to get a user from storage by email (case insensitive)", function () { return __awaiter(_this, void 0, void 0, function () {
        var user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, storage.getUserByEmail("john@EMAIL.com")];
                case 1:
                    user = _a.sent();
                    assert.ok(user !== undefined);
                    assert.ok(user.userId == uuidUserId);
                    assert.ok(user.name == "john");
                    assert.ok(user.email == "john@email.com");
                    assert.ok(user.language == "gb");
                    assert.ok(user.disabled == true);
                    assert.ok(user.createdDate !== undefined);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to store a user creation date at the correct timezone.", function () { return __awaiter(_this, void 0, void 0, function () {
        var user, now;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, storage.getUser(uuidUserId)];
                case 1:
                    user = _a.sent();
                    assert.ok(user !== undefined);
                    assert.ok(user.userId == uuidUserId);
                    assert.ok(user.name == "john");
                    assert.ok(user.email == "john@email.com");
                    now = new Date();
                    assert.ok((user.createdDate.getTime() - now.getTime()) < 2000);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to delete a user", function () { return __awaiter(_this, void 0, void 0, function () {
        var user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, storage.deleteUser(uuidUserId)];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, storage.getUser(uuidUserId)];
                case 2:
                    user = _a.sent();
                    assert.ok(user == undefined);
                    return [2 /*return*/];
            }
        });
    }); });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvcGd1c2Vyc3RvcmFnZXRlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsaUJBd0dBOztBQXhHQSxpQkFBZTtBQUNmLGlFQUE0RDtBQUU1RCw2REFBd0Q7QUFFeEQsSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUMsS0FBSyxDQUFDO0FBQ2xELElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMzQixJQUFBLGtCQUFnQyxFQUE5QixrQkFBTSxFQUFFLGNBQUksQ0FBbUI7QUFFckMsUUFBUSxDQUFDLG9CQUFvQixFQUFFO0lBRTNCLElBQU0sSUFBSSxHQUFXLDREQUE0RCxDQUFDO0lBQ2xGLElBQU0sTUFBTSxHQUFRLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoQyxJQUFNLElBQUksR0FBUSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuQyxJQUFNLE9BQU8sR0FBbUIsSUFBSSx1QkFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3hELElBQU0sVUFBVSxHQUFXLFVBQVUsQ0FBQztJQUN0QyxJQUFNLFVBQVUsR0FBVyxnQ0FBZ0MsQ0FBQztJQUU1RCxFQUFFLENBQUMsMkNBQTJDLEVBQUU7Ozs7O29CQUV4QyxJQUFJLEdBQUcsSUFBSSxjQUFJLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO29CQUN4QyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7b0JBQzlCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO29CQUN0QixJQUFJLENBQUMsS0FBSyxHQUFHLGdCQUFnQixDQUFDO29CQUM5QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDckIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7b0JBQ25DLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO29CQUNQLHFCQUFNLE9BQU8sQ0FBQyxhQUFhLENBQUUsSUFBSSxDQUFFLEVBQUE7O29CQUFyRCxNQUFNLEdBQVksU0FBbUM7b0JBQ3pELE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7Ozs7U0FFckIsQ0FBQyxDQUFDO0lBRUgsRUFBRSxDQUFDLDZEQUE2RCxFQUFFOzs7OztvQkFFMUQsSUFBSSxHQUFHLElBQUksY0FBSSxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztvQkFDeEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO29CQUM5QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDckIsSUFBSSxDQUFDLEtBQUssR0FBRyxnQkFBZ0IsQ0FBQztvQkFDOUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7b0JBQ3JCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO29CQUNuQyxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztvQkFFUCxxQkFBTSxPQUFPLENBQUMsYUFBYSxDQUFFLElBQUksQ0FBRSxFQUFBOztvQkFBckQsTUFBTSxHQUFZLFNBQW1DO29CQUN6RCxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDOzs7O1NBRXJCLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQywrREFBK0QsRUFBRTs7Ozt3QkFFL0MscUJBQU0sT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBQTs7b0JBQTlDLElBQUksR0FBUyxTQUFpQztvQkFDbEQsTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLENBQUM7Ozs7U0FFaEMsQ0FBQyxDQUFDO0lBRUgsRUFBRSxDQUFDLDJDQUEyQyxFQUFFOzs7O3dCQUUzQixxQkFBTSxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFBOztvQkFBOUMsSUFBSSxHQUFTLFNBQWlDO29CQUNsRCxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsQ0FBQztvQkFDOUIsTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLFVBQVUsQ0FBQyxDQUFDO29CQUNyQyxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLENBQUM7b0JBQy9CLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxnQkFBZ0IsQ0FBQyxDQUFDO29CQUMxQyxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLENBQUM7b0JBQ2pDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsQ0FBQztvQkFDakMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxLQUFLLFNBQVMsQ0FBQyxDQUFDO29CQUMxQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7b0JBQzlDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxVQUFVLENBQUMsQ0FBQzs7OztTQUU1QyxDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsdUVBQXVFLEVBQUU7Ozs7d0JBRXZELHFCQUFNLE9BQU8sQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsRUFBQTs7b0JBQTNELElBQUksR0FBUyxTQUE4QztvQkFDL0QsTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDLENBQUM7b0JBQzlCLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxVQUFVLENBQUMsQ0FBQztvQkFDckMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxDQUFDO29CQUMvQixNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksZ0JBQWdCLENBQUMsQ0FBQztvQkFDMUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxDQUFDO29CQUNqQyxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLENBQUM7b0JBQ2pDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsS0FBSyxTQUFTLENBQUUsQ0FBQzs7OztTQUU5QyxDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsdUVBQXVFLEVBQUU7Ozs7d0JBRXZELHFCQUFNLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEVBQUE7O29CQUE5QyxJQUFJLEdBQVMsU0FBaUM7b0JBQ2xELE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxLQUFLLFNBQVMsQ0FBQyxDQUFDO29CQUM5QixNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksVUFBVSxDQUFDLENBQUM7b0JBQ3JDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxNQUFNLENBQUMsQ0FBQztvQkFDL0IsTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLGdCQUFnQixDQUFDLENBQUM7b0JBRXRDLEdBQUcsR0FBUyxJQUFJLElBQUksRUFBRSxDQUFDO29CQUMzQixNQUFNLENBQUMsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsR0FBRyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUUsR0FBRyxJQUFJLENBQUUsQ0FBQzs7OztTQUVyRSxDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsaUNBQWlDLEVBQUU7Ozs7d0JBRWxDLHFCQUFNLE9BQU8sQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLEVBQUE7O29CQUFwQyxTQUFvQyxDQUFDO29CQUNwQixxQkFBTSxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFBOztvQkFBOUMsSUFBSSxHQUFTLFNBQWlDO29CQUNsRCxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsQ0FBQzs7OztTQUVoQyxDQUFDLENBQUM7QUFFUCxDQUFDLENBQUMsQ0FBQyIsImZpbGUiOiJzdG9yYWdlL3BndXNlcnN0b3JhZ2V0ZXN0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFwibW9jaGFcIjtcclxuaW1wb3J0IFBnVXNlclN0b3JhZ2UgZnJvbSBcIi4uLy4uL3NyYy9zdG9yYWdlL3BndXNlcnN0b3JhZ2VcIjtcclxuaW1wb3J0IElQZ1VzZXJTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pcGd1c2Vyc3RvcmFnZVwiO1xyXG5pbXBvcnQgVXNlciBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tb2RlbC91c2VyXCI7XHJcblxyXG5sZXQgcGFyc2UgPSByZXF1aXJlKCdwZy1jb25uZWN0aW9uLXN0cmluZycpLnBhcnNlO1xyXG5sZXQgYXNzZXJ0ID0gcmVxdWlyZShcImFzc2VydFwiKTtcclxubGV0IHsgQ2xpZW50LCBQb29sIH0gPSByZXF1aXJlKCdwZycpO1xyXG5cclxuZGVzY3JpYmUoXCJQZ1VzZXJTdG9yYWdlIFRlc3RcIiwgKCkgPT4ge1xyXG5cclxuICAgIGNvbnN0IEhPU1Q6IHN0cmluZyA9IFwicG9zdGdyZXNxbDovL3Bvc3RncmVzOmFiYzEyM0Bsb2NhbGhvc3Q6NTQzMi9pc2xlb2ZkYXJrbmVzc1wiOyBcclxuICAgIGNvbnN0IGNvbmZpZzogYW55ID0gcGFyc2UoSE9TVCk7XHJcbiAgICBjb25zdCBwb29sOiBhbnkgPSBuZXcgUG9vbChjb25maWcpO1xyXG4gICAgY29uc3Qgc3RvcmFnZTogSVBnVXNlclN0b3JhZ2UgPSBuZXcgUGdVc2VyU3RvcmFnZShwb29sKTtcclxuICAgIGNvbnN0IHV1aWRVc2VySWQ6IHN0cmluZyA9IFwiNjg5MGQ0NzBcIjtcclxuICAgIGNvbnN0IHBpY3R1cmVVcmw6IHN0cmluZyA9IFwiaHR0cDovL3d3dy5waWN0dXJlLmNvbS9waWMuanBnXCI7XHJcblxyXG4gICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBhZGQgYSB1c2VyIGludG8gc3RvcmFnZVwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCB1c2VyID0gbmV3IFVzZXIodXVpZFVzZXJJZCwgXCJqb2huXCIpO1xyXG4gICAgICAgIHVzZXIuY3JlYXRlZERhdGUgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgIHVzZXIuZGlzYWJsZWQgPSBmYWxzZTtcclxuICAgICAgICB1c2VyLmVtYWlsID0gXCJlbWFpbEBqb2huLmNvbVwiO1xyXG4gICAgICAgIHVzZXIubGFuZ3VhZ2UgPSBcImVuXCI7XHJcbiAgICAgICAgdXNlci5sYXN0TG9nZ2VkSW5EYXRlID0gbmV3IERhdGUoKTtcclxuICAgICAgICB1c2VyLnBpY3R1cmVVcmwgPSBwaWN0dXJlVXJsO1xyXG4gICAgICAgIGxldCByZXN1bHQ6IGJvb2xlYW4gPSBhd2FpdCBzdG9yYWdlLmFkZFVwZGF0ZVVzZXIoIHVzZXIgKTtcclxuICAgICAgICBhc3NlcnQub2socmVzdWx0KTtcclxuICAgICAgIFxyXG4gICAgfSk7XHJcblxyXG4gICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byB1cGRhdGUgYSB1c2VyIGluIHN0b3JhZ2UgdXNpbmcgYWRkL3VwZGF0ZVwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCB1c2VyID0gbmV3IFVzZXIodXVpZFVzZXJJZCwgXCJqb2huXCIpO1xyXG4gICAgICAgIHVzZXIuY3JlYXRlZERhdGUgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgIHVzZXIuZGlzYWJsZWQgPSB0cnVlO1xyXG4gICAgICAgIHVzZXIuZW1haWwgPSBcImpvaG5AZW1haWwuY29tXCI7XHJcbiAgICAgICAgdXNlci5sYW5ndWFnZSA9IFwiZ2JcIjtcclxuICAgICAgICB1c2VyLmxhc3RMb2dnZWRJbkRhdGUgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgIHVzZXIucGljdHVyZVVybCA9IHBpY3R1cmVVcmw7XHJcblxyXG4gICAgICAgIGxldCByZXN1bHQ6IGJvb2xlYW4gPSBhd2FpdCBzdG9yYWdlLmFkZFVwZGF0ZVVzZXIoIHVzZXIgKTtcclxuICAgICAgICBhc3NlcnQub2socmVzdWx0KTtcclxuXHJcbiAgICB9KTsgICAgIFxyXG5cclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gZ2V0IGEgdXNlciB0aGF0IGRvZXMgbm90IGV4aXN0IGZyb20gc3RvcmFnZVwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCB1c2VyOiBVc2VyID0gYXdhaXQgc3RvcmFnZS5nZXRVc2VyKFwiMDAwMDAwMDBcIik7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKHVzZXIgPT0gdW5kZWZpbmVkKTtcclxuXHJcbiAgICB9KTtcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIGdldCBhIHVzZXIgZnJvbSBzdG9yYWdlXCIsIGFzeW5jICgpID0+IHtcclxuXHJcbiAgICAgICAgbGV0IHVzZXI6IFVzZXIgPSBhd2FpdCBzdG9yYWdlLmdldFVzZXIodXVpZFVzZXJJZCk7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKHVzZXIgIT09IHVuZGVmaW5lZCk7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKHVzZXIudXNlcklkID09IHV1aWRVc2VySWQpO1xyXG4gICAgICAgIGFzc2VydC5vayh1c2VyLm5hbWUgPT0gXCJqb2huXCIpO1xyXG4gICAgICAgIGFzc2VydC5vayh1c2VyLmVtYWlsID09IFwiam9obkBlbWFpbC5jb21cIik7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKHVzZXIubGFuZ3VhZ2UgPT0gXCJnYlwiKTtcclxuICAgICAgICBhc3NlcnQub2sodXNlci5kaXNhYmxlZCA9PSB0cnVlKTtcclxuICAgICAgICBhc3NlcnQub2sodXNlci5jcmVhdGVkRGF0ZSAhPT0gdW5kZWZpbmVkKTtcclxuICAgICAgICBhc3NlcnQuZGVlcEVxdWFsKHVzZXIucGljdHVyZVVybCwgcGljdHVyZVVybCk7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKHVzZXIucGljdHVyZVVybCA9PSBwaWN0dXJlVXJsKTtcclxuXHJcbiAgICB9KTtcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIGdldCBhIHVzZXIgZnJvbSBzdG9yYWdlIGJ5IGVtYWlsIChjYXNlIGluc2Vuc2l0aXZlKVwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCB1c2VyOiBVc2VyID0gYXdhaXQgc3RvcmFnZS5nZXRVc2VyQnlFbWFpbChcImpvaG5ARU1BSUwuY29tXCIpO1xyXG4gICAgICAgIGFzc2VydC5vayh1c2VyICE9PSB1bmRlZmluZWQpO1xyXG4gICAgICAgIGFzc2VydC5vayh1c2VyLnVzZXJJZCA9PSB1dWlkVXNlcklkKTtcclxuICAgICAgICBhc3NlcnQub2sodXNlci5uYW1lID09IFwiam9oblwiKTtcclxuICAgICAgICBhc3NlcnQub2sodXNlci5lbWFpbCA9PSBcImpvaG5AZW1haWwuY29tXCIpO1xyXG4gICAgICAgIGFzc2VydC5vayh1c2VyLmxhbmd1YWdlID09IFwiZ2JcIik7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKHVzZXIuZGlzYWJsZWQgPT0gdHJ1ZSk7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKHVzZXIuY3JlYXRlZERhdGUgIT09IHVuZGVmaW5lZCApOyAgICAgICAgXHJcblxyXG4gICAgfSk7XHJcbiAgICBcclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gc3RvcmUgYSB1c2VyIGNyZWF0aW9uIGRhdGUgYXQgdGhlIGNvcnJlY3QgdGltZXpvbmUuXCIsIGFzeW5jICgpID0+IHtcclxuXHJcbiAgICAgICAgbGV0IHVzZXI6IFVzZXIgPSBhd2FpdCBzdG9yYWdlLmdldFVzZXIodXVpZFVzZXJJZCk7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKHVzZXIgIT09IHVuZGVmaW5lZCk7XHJcbiAgICAgICAgYXNzZXJ0Lm9rKHVzZXIudXNlcklkID09IHV1aWRVc2VySWQpO1xyXG4gICAgICAgIGFzc2VydC5vayh1c2VyLm5hbWUgPT0gXCJqb2huXCIpO1xyXG4gICAgICAgIGFzc2VydC5vayh1c2VyLmVtYWlsID09IFwiam9obkBlbWFpbC5jb21cIik7XHJcblxyXG4gICAgICAgIGxldCBub3c6IERhdGUgPSBuZXcgRGF0ZSgpOyAgICBcclxuICAgICAgICBhc3NlcnQub2soKCB1c2VyLmNyZWF0ZWREYXRlLmdldFRpbWUoKSAtIG5vdy5nZXRUaW1lKCkgKSA8IDIwMDAgKTtcclxuICAgICAgICBcclxuICAgIH0pOyAgICAgICAgXHJcblxyXG4gICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBkZWxldGUgYSB1c2VyXCIsIGFzeW5jICgpID0+IHtcclxuXHJcbiAgICAgICAgYXdhaXQgc3RvcmFnZS5kZWxldGVVc2VyKHV1aWRVc2VySWQpO1xyXG4gICAgICAgIGxldCB1c2VyOiBVc2VyID0gYXdhaXQgc3RvcmFnZS5nZXRVc2VyKHV1aWRVc2VySWQpO1xyXG4gICAgICAgIGFzc2VydC5vayh1c2VyID09IHVuZGVmaW5lZCk7XHJcblxyXG4gICAgfSk7XHJcblxyXG59KTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
