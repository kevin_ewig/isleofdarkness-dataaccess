"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var objectmodel_1 = require("isleofdarkness-common/src/model/objectmodel");
var circle_1 = require("isleofdarkness-common/src/math/circle");
var point_1 = require("isleofdarkness-common/src/math/point");
var objectmodelstorage_1 = require("../../src/storage/objectmodelstorage");
var assert = require("assert");
var redis = require("redis");
describe("ObjectModelStorage Test", function () {
    var HOST = "redis://localhost:6379";
    var redisClient = null;
    beforeEach(function () {
        redisClient = redis.createClient(HOST);
        redisClient.on("error", function (err) {
            console.log("Error:" + err);
        });
        redisClient.flushdb(function (err) {
            if (err) {
                console.log("Error: FlushDB: " + err);
            }
        });
    });
    it("should store/retrieve ObjectModel object into temporary storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var circle, objectModelData, storage, returnValue, json1, json2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    circle = new circle_1.default(new point_1.default(32, 12), 3);
                    objectModelData = new objectmodel_1.default("OBJ123", circle, "ModelName");
                    storage = new objectmodelstorage_1.default(redisClient);
                    return [4 /*yield*/, storage.addUpdateObjectModel(objectModelData)];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, storage.getObjectModel("OBJ123")];
                case 2:
                    returnValue = _a.sent();
                    json1 = JSON.stringify(returnValue.toJSON());
                    json2 = JSON.stringify(objectModelData.toJSON());
                    assert(json1 == json2);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should delete objectModel object from temporary storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var circle, objectModelData, storage, returnValue;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    circle = new circle_1.default(new point_1.default(32, 12), 3);
                    objectModelData = new objectmodel_1.default("OBJ123", circle, "ModelName");
                    storage = new objectmodelstorage_1.default(redisClient);
                    return [4 /*yield*/, storage.addUpdateObjectModel(objectModelData)];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, storage.removeObjectModel(objectModelData.id)];
                case 2:
                    _a.sent();
                    return [4 /*yield*/, storage.getObjectModel("OBJ123")];
                case 3:
                    returnValue = _a.sent();
                    assert(returnValue === undefined);
                    return [2 /*return*/];
            }
        });
    }); });
    // it("should update objectModel object from temporary storage", async () => {
    //     let circle:Circle = new Circle(new Point(32, 12), 3);
    //     let objectModelData = new ObjectModel("OBJ123", circle, "ModelName");
    //     let storage:IObjectModelStorage = new ObjectModelStorage(redisClient);
    //     // Add object model to storage.
    //     await storage.addUpdateObjectModel(objectModelData);
    //     // Add a path to the object model.
    //     let points:Array<Point> = [];
    //     points.push( new Point(3,2) );
    //     points.push( new Point(6,4) );
    //     points.push( new Point(1,9) );
    //     objectModelData.setPath(points);
    //     await storage.addUpdateObjectModel(objectModelData);
    //     // Test object model can be retrieved successfully. 
    //     let retrievedObj:ObjectModel = await storage.getObjectModel("OBJ123");
    //     assert( JSON.stringify(retrievedObj.toJSON()) === JSON.stringify(objectModelData.toJSON()) );
    //     // Delete object model.
    //     await storage.removeObjectModel(objectModelData.id);
    //     let returnValue:ObjectModel = await storage.getObjectModel("OBJ123");
    //     assert( returnValue === undefined );
    // });
    it("should store/retrieve objectModel object by its section coordinate", function () { return __awaiter(_this, void 0, void 0, function () {
        var circle1, objectModelData1, circle2, objectModelData2, storage, listOfObjsId;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    circle1 = new circle_1.default(new point_1.default(532, 312), 3);
                    objectModelData1 = new objectmodel_1.default("OBJ890890", circle1, "ModelName1");
                    circle2 = new circle_1.default(new point_1.default(502, 302), 3);
                    objectModelData2 = new objectmodel_1.default("OBJ999111", circle2, "ModelName2");
                    storage = new objectmodelstorage_1.default(redisClient);
                    return [4 /*yield*/, storage.addUpdateObjectModel(objectModelData1)];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, storage.addUpdateObjectModel(objectModelData2)];
                case 2:
                    _a.sent();
                    return [4 /*yield*/, storage.getObjectModelsIdsInSection(new point_1.default(5, 3))];
                case 3:
                    listOfObjsId = _a.sent();
                    assert.equal(listOfObjsId.length, 2);
                    return [4 /*yield*/, storage.getObjectModelsIdsInSection(new point_1.default(1, 1))];
                case 4:
                    listOfObjsId = _a.sent();
                    assert.equal(listOfObjsId.length, 0);
                    return [2 /*return*/];
            }
        });
    }); });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2Uvb2JqZWN0bW9kZWxzdG9yYWdldGVzdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxpQkFnSEE7O0FBaEhBLGlCQUFlO0FBQ2YsMkVBQTJGO0FBQzNGLGdFQUEyRDtBQUMzRCw4REFBeUQ7QUFFekQsMkVBQXFFO0FBRXJFLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMvQixJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7QUFFN0IsUUFBUSxDQUFDLHlCQUF5QixFQUFFO0lBRWhDLElBQU0sSUFBSSxHQUFVLHdCQUF3QixDQUFDO0lBQzdDLElBQUksV0FBVyxHQUFPLElBQUksQ0FBQztJQUUzQixVQUFVLENBQUM7UUFFUCxXQUFXLEdBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QyxXQUFXLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFVLEdBQU87WUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxXQUFXLENBQUMsT0FBTyxDQUFFLFVBQVMsR0FBTztZQUNqQyxFQUFFLENBQUEsQ0FBRSxHQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDMUMsQ0FBQztRQUNMLENBQUMsQ0FBRSxDQUFDO0lBRVIsQ0FBQyxDQUFDLENBQUM7SUFHSCxFQUFFLENBQUMsaUVBQWlFLEVBQUU7Ozs7O29CQUU5RCxNQUFNLEdBQVUsSUFBSSxnQkFBTSxDQUFDLElBQUksZUFBSyxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDakQsZUFBZSxHQUFHLElBQUkscUJBQVcsQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUVqRSxPQUFPLEdBQXVCLElBQUksNEJBQWtCLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBRXRFLHFCQUFNLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsRUFBQTs7b0JBQW5ELFNBQW1ELENBQUM7b0JBQ3RCLHFCQUFNLE9BQU8sQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEVBQUE7O29CQUFoRSxXQUFXLEdBQWUsU0FBc0M7b0JBQ2hFLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO29CQUM3QyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztvQkFDckQsTUFBTSxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsQ0FBQzs7OztTQUUxQixDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMseURBQXlELEVBQUU7Ozs7O29CQUV0RCxNQUFNLEdBQVUsSUFBSSxnQkFBTSxDQUFDLElBQUksZUFBSyxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDakQsZUFBZSxHQUFHLElBQUkscUJBQVcsQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUVqRSxPQUFPLEdBQXVCLElBQUksNEJBQWtCLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBRXRFLHFCQUFNLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsRUFBQTs7b0JBQW5ELFNBQW1ELENBQUM7b0JBQ3BELHFCQUFNLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDLEVBQUE7O29CQUFuRCxTQUFtRCxDQUFDO29CQUV0QixxQkFBTSxPQUFPLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxFQUFBOztvQkFBaEUsV0FBVyxHQUFlLFNBQXNDO29CQUNwRSxNQUFNLENBQUUsV0FBVyxLQUFLLFNBQVMsQ0FBRSxDQUFDOzs7O1NBRXZDLENBQUMsQ0FBQztJQUVILDhFQUE4RTtJQUU5RSw0REFBNEQ7SUFDNUQsNEVBQTRFO0lBRTVFLDZFQUE2RTtJQUU3RSxzQ0FBc0M7SUFDdEMsMkRBQTJEO0lBRTNELHlDQUF5QztJQUN6QyxvQ0FBb0M7SUFDcEMscUNBQXFDO0lBQ3JDLHFDQUFxQztJQUNyQyxxQ0FBcUM7SUFDckMsdUNBQXVDO0lBQ3ZDLDJEQUEyRDtJQUUzRCwyREFBMkQ7SUFDM0QsNkVBQTZFO0lBQzdFLG9HQUFvRztJQUVwRyw4QkFBOEI7SUFDOUIsMkRBQTJEO0lBQzNELDRFQUE0RTtJQUM1RSwyQ0FBMkM7SUFFM0MsTUFBTTtJQUVOLEVBQUUsQ0FBQyxvRUFBb0UsRUFBRTs7Ozs7b0JBRWpFLE9BQU8sR0FBVSxJQUFJLGdCQUFNLENBQUMsSUFBSSxlQUFLLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNwRCxnQkFBZ0IsR0FBRyxJQUFJLHFCQUFXLENBQUMsV0FBVyxFQUFFLE9BQU8sRUFBRSxZQUFZLENBQUMsQ0FBQztvQkFFdkUsT0FBTyxHQUFVLElBQUksZ0JBQU0sQ0FBQyxJQUFJLGVBQUssQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3BELGdCQUFnQixHQUFHLElBQUkscUJBQVcsQ0FBQyxXQUFXLEVBQUUsT0FBTyxFQUFFLFlBQVksQ0FBQyxDQUFDO29CQUV2RSxPQUFPLEdBQXVCLElBQUksNEJBQWtCLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBRXRFLHFCQUFNLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFBOztvQkFBcEQsU0FBb0QsQ0FBQztvQkFDckQscUJBQU0sT0FBTyxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixDQUFDLEVBQUE7O29CQUFwRCxTQUFvRCxDQUFDO29CQUV6QixxQkFBTSxPQUFPLENBQUMsMkJBQTJCLENBQUUsSUFBSSxlQUFLLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFFLEVBQUE7O29CQUFuRixZQUFZLEdBQVksU0FBMkQ7b0JBQ3ZGLE1BQU0sQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFFdEIscUJBQU0sT0FBTyxDQUFDLDJCQUEyQixDQUFFLElBQUksZUFBSyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBRSxFQUFBOztvQkFBMUUsWUFBWSxHQUFHLFNBQTJELENBQUM7b0JBQzNFLE1BQU0sQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQzs7OztTQUV4QyxDQUFDLENBQUM7QUFFUCxDQUFDLENBQUMsQ0FBQyIsImZpbGUiOiJzdG9yYWdlL29iamVjdG1vZGVsc3RvcmFnZXRlc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXCJtb2NoYVwiO1xyXG5pbXBvcnQgT2JqZWN0TW9kZWwsIHsgT2JqZWN0TW9kZWxUeXBlIH0gZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbW9kZWwvb2JqZWN0bW9kZWxcIjtcclxuaW1wb3J0IENpcmNsZSBmcm9tIFwiaXNsZW9mZGFya25lc3MtY29tbW9uL3NyYy9tYXRoL2NpcmNsZVwiO1xyXG5pbXBvcnQgUG9pbnQgZnJvbSBcImlzbGVvZmRhcmtuZXNzLWNvbW1vbi9zcmMvbWF0aC9wb2ludFwiO1xyXG5pbXBvcnQgSU9iamVjdE1vZGVsU3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvaW9iamVjdG1vZGVsc3RvcmFnZVwiO1xyXG5pbXBvcnQgT2JqZWN0TW9kZWxTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9vYmplY3Rtb2RlbHN0b3JhZ2VcIlxyXG5cclxubGV0IGFzc2VydCA9IHJlcXVpcmUoXCJhc3NlcnRcIik7XHJcbmxldCByZWRpcyA9IHJlcXVpcmUoXCJyZWRpc1wiKTtcclxuXHJcbmRlc2NyaWJlKFwiT2JqZWN0TW9kZWxTdG9yYWdlIFRlc3RcIiwgKCkgPT4ge1xyXG5cclxuICAgIGNvbnN0IEhPU1Q6c3RyaW5nID0gXCJyZWRpczovL2xvY2FsaG9zdDo2Mzc5XCI7IFxyXG4gICAgdmFyIHJlZGlzQ2xpZW50OmFueSA9IG51bGw7XHJcblxyXG4gICAgYmVmb3JlRWFjaCgoKSA9PiB7XHJcblxyXG4gICAgICAgIHJlZGlzQ2xpZW50ID0gcmVkaXMuY3JlYXRlQ2xpZW50KEhPU1QpO1xyXG4gICAgICAgIHJlZGlzQ2xpZW50Lm9uKFwiZXJyb3JcIiwgZnVuY3Rpb24gKGVycjphbnkpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnJvcjpcIiArIGVycik7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJlZGlzQ2xpZW50LmZsdXNoZGIoIGZ1bmN0aW9uKGVycjphbnkpIHtcclxuICAgICAgICAgICAgaWYoIGVyciApIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3I6IEZsdXNoREI6IFwiICsgZXJyKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gKTtcclxuXHJcbiAgICB9KTtcclxuXHJcblxyXG4gICAgaXQoXCJzaG91bGQgc3RvcmUvcmV0cmlldmUgT2JqZWN0TW9kZWwgb2JqZWN0IGludG8gdGVtcG9yYXJ5IHN0b3JhZ2VcIiwgYXN5bmMgKCkgPT4ge1xyXG5cclxuICAgICAgICBsZXQgY2lyY2xlOkNpcmNsZSA9IG5ldyBDaXJjbGUobmV3IFBvaW50KDMyLCAxMiksIDMpO1xyXG4gICAgICAgIGxldCBvYmplY3RNb2RlbERhdGEgPSBuZXcgT2JqZWN0TW9kZWwoXCJPQkoxMjNcIiwgY2lyY2xlLCBcIk1vZGVsTmFtZVwiKTtcclxuXHJcbiAgICAgICAgbGV0IHN0b3JhZ2U6SU9iamVjdE1vZGVsU3RvcmFnZSA9IG5ldyBPYmplY3RNb2RlbFN0b3JhZ2UocmVkaXNDbGllbnQpO1xyXG5cclxuICAgICAgICBhd2FpdCBzdG9yYWdlLmFkZFVwZGF0ZU9iamVjdE1vZGVsKG9iamVjdE1vZGVsRGF0YSk7XHJcbiAgICAgICAgbGV0IHJldHVyblZhbHVlOk9iamVjdE1vZGVsID0gYXdhaXQgc3RvcmFnZS5nZXRPYmplY3RNb2RlbChcIk9CSjEyM1wiKTtcclxuICAgICAgICBsZXQganNvbjEgPSBKU09OLnN0cmluZ2lmeShyZXR1cm5WYWx1ZS50b0pTT04oKSk7XHJcbiAgICAgICAgbGV0IGpzb24yID0gSlNPTi5zdHJpbmdpZnkob2JqZWN0TW9kZWxEYXRhLnRvSlNPTigpKTtcclxuICAgICAgICBhc3NlcnQoanNvbjEgPT0ganNvbjIpO1xyXG5cclxuICAgIH0pO1xyXG5cclxuICAgIGl0KFwic2hvdWxkIGRlbGV0ZSBvYmplY3RNb2RlbCBvYmplY3QgZnJvbSB0ZW1wb3Jhcnkgc3RvcmFnZVwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCBjaXJjbGU6Q2lyY2xlID0gbmV3IENpcmNsZShuZXcgUG9pbnQoMzIsIDEyKSwgMyk7XHJcbiAgICAgICAgbGV0IG9iamVjdE1vZGVsRGF0YSA9IG5ldyBPYmplY3RNb2RlbChcIk9CSjEyM1wiLCBjaXJjbGUsIFwiTW9kZWxOYW1lXCIpO1xyXG5cclxuICAgICAgICBsZXQgc3RvcmFnZTpJT2JqZWN0TW9kZWxTdG9yYWdlID0gbmV3IE9iamVjdE1vZGVsU3RvcmFnZShyZWRpc0NsaWVudCk7XHJcblxyXG4gICAgICAgIGF3YWl0IHN0b3JhZ2UuYWRkVXBkYXRlT2JqZWN0TW9kZWwob2JqZWN0TW9kZWxEYXRhKTtcclxuICAgICAgICBhd2FpdCBzdG9yYWdlLnJlbW92ZU9iamVjdE1vZGVsKG9iamVjdE1vZGVsRGF0YS5pZCk7XHJcblxyXG4gICAgICAgIGxldCByZXR1cm5WYWx1ZTpPYmplY3RNb2RlbCA9IGF3YWl0IHN0b3JhZ2UuZ2V0T2JqZWN0TW9kZWwoXCJPQkoxMjNcIik7XHJcbiAgICAgICAgYXNzZXJ0KCByZXR1cm5WYWx1ZSA9PT0gdW5kZWZpbmVkICk7ICAgICAgICBcclxuXHJcbiAgICB9KTtcclxuICAgIFxyXG4gICAgLy8gaXQoXCJzaG91bGQgdXBkYXRlIG9iamVjdE1vZGVsIG9iamVjdCBmcm9tIHRlbXBvcmFyeSBzdG9yYWdlXCIsIGFzeW5jICgpID0+IHtcclxuXHJcbiAgICAvLyAgICAgbGV0IGNpcmNsZTpDaXJjbGUgPSBuZXcgQ2lyY2xlKG5ldyBQb2ludCgzMiwgMTIpLCAzKTtcclxuICAgIC8vICAgICBsZXQgb2JqZWN0TW9kZWxEYXRhID0gbmV3IE9iamVjdE1vZGVsKFwiT0JKMTIzXCIsIGNpcmNsZSwgXCJNb2RlbE5hbWVcIik7XHJcblxyXG4gICAgLy8gICAgIGxldCBzdG9yYWdlOklPYmplY3RNb2RlbFN0b3JhZ2UgPSBuZXcgT2JqZWN0TW9kZWxTdG9yYWdlKHJlZGlzQ2xpZW50KTtcclxuXHJcbiAgICAvLyAgICAgLy8gQWRkIG9iamVjdCBtb2RlbCB0byBzdG9yYWdlLlxyXG4gICAgLy8gICAgIGF3YWl0IHN0b3JhZ2UuYWRkVXBkYXRlT2JqZWN0TW9kZWwob2JqZWN0TW9kZWxEYXRhKTtcclxuXHJcbiAgICAvLyAgICAgLy8gQWRkIGEgcGF0aCB0byB0aGUgb2JqZWN0IG1vZGVsLlxyXG4gICAgLy8gICAgIGxldCBwb2ludHM6QXJyYXk8UG9pbnQ+ID0gW107XHJcbiAgICAvLyAgICAgcG9pbnRzLnB1c2goIG5ldyBQb2ludCgzLDIpICk7XHJcbiAgICAvLyAgICAgcG9pbnRzLnB1c2goIG5ldyBQb2ludCg2LDQpICk7XHJcbiAgICAvLyAgICAgcG9pbnRzLnB1c2goIG5ldyBQb2ludCgxLDkpICk7XHJcbiAgICAvLyAgICAgb2JqZWN0TW9kZWxEYXRhLnNldFBhdGgocG9pbnRzKTtcclxuICAgIC8vICAgICBhd2FpdCBzdG9yYWdlLmFkZFVwZGF0ZU9iamVjdE1vZGVsKG9iamVjdE1vZGVsRGF0YSk7XHJcblxyXG4gICAgLy8gICAgIC8vIFRlc3Qgb2JqZWN0IG1vZGVsIGNhbiBiZSByZXRyaWV2ZWQgc3VjY2Vzc2Z1bGx5LiBcclxuICAgIC8vICAgICBsZXQgcmV0cmlldmVkT2JqOk9iamVjdE1vZGVsID0gYXdhaXQgc3RvcmFnZS5nZXRPYmplY3RNb2RlbChcIk9CSjEyM1wiKTtcclxuICAgIC8vICAgICBhc3NlcnQoIEpTT04uc3RyaW5naWZ5KHJldHJpZXZlZE9iai50b0pTT04oKSkgPT09IEpTT04uc3RyaW5naWZ5KG9iamVjdE1vZGVsRGF0YS50b0pTT04oKSkgKTtcclxuXHJcbiAgICAvLyAgICAgLy8gRGVsZXRlIG9iamVjdCBtb2RlbC5cclxuICAgIC8vICAgICBhd2FpdCBzdG9yYWdlLnJlbW92ZU9iamVjdE1vZGVsKG9iamVjdE1vZGVsRGF0YS5pZCk7XHJcbiAgICAvLyAgICAgbGV0IHJldHVyblZhbHVlOk9iamVjdE1vZGVsID0gYXdhaXQgc3RvcmFnZS5nZXRPYmplY3RNb2RlbChcIk9CSjEyM1wiKTtcclxuICAgIC8vICAgICBhc3NlcnQoIHJldHVyblZhbHVlID09PSB1bmRlZmluZWQgKTtcclxuXHJcbiAgICAvLyB9KTtcclxuICAgIFxyXG4gICAgaXQoXCJzaG91bGQgc3RvcmUvcmV0cmlldmUgb2JqZWN0TW9kZWwgb2JqZWN0IGJ5IGl0cyBzZWN0aW9uIGNvb3JkaW5hdGVcIiwgYXN5bmMgKCkgPT4ge1xyXG5cclxuICAgICAgICBsZXQgY2lyY2xlMTpDaXJjbGUgPSBuZXcgQ2lyY2xlKG5ldyBQb2ludCg1MzIsIDMxMiksIDMpO1xyXG4gICAgICAgIGxldCBvYmplY3RNb2RlbERhdGExID0gbmV3IE9iamVjdE1vZGVsKFwiT0JKODkwODkwXCIsIGNpcmNsZTEsIFwiTW9kZWxOYW1lMVwiKTtcclxuXHJcbiAgICAgICAgbGV0IGNpcmNsZTI6Q2lyY2xlID0gbmV3IENpcmNsZShuZXcgUG9pbnQoNTAyLCAzMDIpLCAzKTtcclxuICAgICAgICBsZXQgb2JqZWN0TW9kZWxEYXRhMiA9IG5ldyBPYmplY3RNb2RlbChcIk9CSjk5OTExMVwiLCBjaXJjbGUyLCBcIk1vZGVsTmFtZTJcIik7ICAgICAgICBcclxuXHJcbiAgICAgICAgbGV0IHN0b3JhZ2U6SU9iamVjdE1vZGVsU3RvcmFnZSA9IG5ldyBPYmplY3RNb2RlbFN0b3JhZ2UocmVkaXNDbGllbnQpO1xyXG5cclxuICAgICAgICBhd2FpdCBzdG9yYWdlLmFkZFVwZGF0ZU9iamVjdE1vZGVsKG9iamVjdE1vZGVsRGF0YTEpO1xyXG4gICAgICAgIGF3YWl0IHN0b3JhZ2UuYWRkVXBkYXRlT2JqZWN0TW9kZWwob2JqZWN0TW9kZWxEYXRhMik7XHJcblxyXG4gICAgICAgIGxldCBsaXN0T2ZPYmpzSWQ6c3RyaW5nW10gPSBhd2FpdCBzdG9yYWdlLmdldE9iamVjdE1vZGVsc0lkc0luU2VjdGlvbiggbmV3IFBvaW50KDUsMykgKTtcclxuICAgICAgICBhc3NlcnQuZXF1YWwobGlzdE9mT2Jqc0lkLmxlbmd0aCwgMik7XHJcblxyXG4gICAgICAgIGxpc3RPZk9ianNJZCA9IGF3YWl0IHN0b3JhZ2UuZ2V0T2JqZWN0TW9kZWxzSWRzSW5TZWN0aW9uKCBuZXcgUG9pbnQoMSwxKSApO1xyXG4gICAgICAgIGFzc2VydC5lcXVhbChsaXN0T2ZPYmpzSWQubGVuZ3RoLCAwKTtcclxuXHJcbiAgICB9KTtcclxuXHJcbn0pO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
