import "mocha";
import PgObjectModelStorage from "../../src/storage/pgobjectmodelstorage";
import IPgObjectModelStorage from "../../src/storage/ipgobjectmodelstorage";
import IPgUserStorage from "../../src/storage/ipguserstorage";
import PgUserStorage from "../../src/storage/pguserstorage";
import { ObjectModel, ObjectModelType } from "isleofdarkness-common/src/model/objectmodel";
import Circle from "isleofdarkness-common/src/math/circle";
import User from "isleofdarkness-common/src/model/user";
import Rectangle from "isleofdarkness-common/src/math/rectangle";
import { Point } from "isleofdarkness-common/src/math/point";

let parse = require('pg-connection-string').parse;
let assert = require("assert");
let { Client, Pool } = require('pg');

describe("PgObjectModelStorage Test", () => {

    const HOST: string = "postgresql://postgres:abc123@localhost:5432/isleofdarkness"; 
    const config: any = parse(HOST);
    const pool: any = new Pool(config);
    const storage: IPgObjectModelStorage = new PgObjectModelStorage(pool);
    const objectModelUuid1: string = "2b52c870";
    const objectModelUuid2: string = "4182c64b";
    const owner_userId: string = "0f338c06";

    it("should be able to insert an ObjectModel object (circle) into a Postgres database", async () => {

        let circle:Circle = new Circle(new Point(32, 12), 3);
        let objectModelData = new ObjectModel(objectModelUuid1, circle, "ModelName");

        let storage:IPgObjectModelStorage = new PgObjectModelStorage(pool);
        let result = await storage.addUpdateObjectModel(objectModelData);
        assert.ok( result === true );

    });

    it("should be able to update an ObjectModel object (circle) in a Postgres database", async () => {

        let circle:Circle = new Circle(new Point(23, 21), 6);
        let objectModelData = new ObjectModel(objectModelUuid1, circle, "ModelName2");
        objectModelData.modelTexture = "ModelTexture1";
        objectModelData.objectType = ObjectModelType.Impassable;

        let storage:IPgObjectModelStorage = new PgObjectModelStorage(pool);
        let result = await storage.addUpdateObjectModel(objectModelData);
        assert.ok( result === true );
       
    });

    it("should be able to get an ObjectModel object (circle) from a Postgres database", async () => {

        let storage:IPgObjectModelStorage = new PgObjectModelStorage(pool);
        let result: ObjectModel|null = await storage.getObjectModel(objectModelUuid1);
        assert.ok( result !== null );
        let objModel: ObjectModel = result as ObjectModel;

        assert.deepEqual( "ModelName2", objModel.modelName );
        assert.deepEqual( "ModelTexture1", objModel.modelTexture );
        assert.deepEqual( ObjectModelType.Impassable, objModel.objectType );
        assert.deepEqual( 23, objModel.shape.x );
        assert.deepEqual( 21, objModel.shape.y );
        assert.deepEqual( 0, objModel.shape.z );
        assert.deepEqual( 6, (objModel.shape as Circle).getRadius() );
       
    });

    it("should be able to insert an ObjectModel object (rectangle) into a Postgres database", async () => {

        let rect:Rectangle = new Rectangle(new Point(45, 98), 30, 20);
        let objectModelData = new ObjectModel(objectModelUuid2, rect, "TestModelName");

        let storage:IPgObjectModelStorage = new PgObjectModelStorage(pool);
        let result = await storage.addUpdateObjectModel(objectModelData);
        assert.ok( result === true );
       
    });

    it("should be able to update an ObjectModel object (rectangle) in a Postgres database", async () => {

        let user: User = new User(owner_userId, "test123");
        user.email = "asdit.asd.com"
        user.createdDate = new Date();
        user.lastLoggedInDate = new Date();
        let userStorage: IPgUserStorage = new PgUserStorage(pool);
        let userAddedToDb = await userStorage.addUpdateUser(user);

        let rect:Rectangle = new Rectangle(new Point(54, 89, 2), 20, 30);
        let objectModelData = new ObjectModel(objectModelUuid2, rect, "TestModelName2");
        objectModelData.modelTexture = "ModelTexture2";
        objectModelData.ownerUserId = owner_userId;
        objectModelData.objectType = ObjectModelType.Impassable;

        let storage:IPgObjectModelStorage = new PgObjectModelStorage(pool);
        let result = await storage.addUpdateObjectModel(objectModelData);
        assert.ok( result === true );
       
    });

    it("should be able to get an ObjectModel object (rectangle) from a Postgres database", async () => {

        let storage:IPgObjectModelStorage = new PgObjectModelStorage(pool);
        let result: ObjectModel|null = await storage.getObjectModel(objectModelUuid2);
        assert.ok( result !== null );
        let objModel: ObjectModel = result as ObjectModel;

        assert.deepEqual( "TestModelName2", objModel.modelName );
        assert.deepEqual( "ModelTexture2", objModel.modelTexture );
        assert.deepEqual( ObjectModelType.Impassable, objModel.objectType );

        let rect: Rectangle = (objModel.shape as Rectangle);
        assert.deepEqual( 54, rect.position().x );
        assert.deepEqual( 89, rect.position().y );
        assert.deepEqual( 2, rect.position().z );
        assert.deepEqual( 20, rect.width() );
        assert.deepEqual( 30, rect.height() );

    });

    it("should be able to tell if no object model exists in the Postgres database", async () => {

        let storage:IPgObjectModelStorage = new PgObjectModelStorage(pool);
        let result: ObjectModel|null = await storage.getObjectModel("00000000");
        assert.ok( result === null );        

    });           

    it("should be able to delete an ObjectModel from Postgres", async () => {

        let storage:IPgObjectModelStorage = new PgObjectModelStorage(pool);

        let result1 = await storage.deleteObjectModel(objectModelUuid1);
        assert.ok( result1 === true );

        let result2 = await storage.deleteObjectModel(objectModelUuid2);
        assert.ok( result2 === true );
        
        let objModel1: ObjectModel|null = await storage.getObjectModel(objectModelUuid1);
        assert.ok( objModel1 === null );

        let objModel2: ObjectModel|null = await storage.getObjectModel(objectModelUuid2);
        assert.ok( objModel2 === null );        

        // Clean up
        let userStorage: IPgUserStorage = new PgUserStorage(pool);
        userStorage.deleteUser(owner_userId);

    });

});
