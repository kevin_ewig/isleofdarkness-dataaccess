"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var storagefactory_1 = require("../../src/storage/storagefactory");
var ibasestorage_1 = require("../../src/storage/ibasestorage");
var assert = require("assert");
var redis = require("redis");
describe("StorageFactory Test", function () {
    var HOST = "redis://localhost:6379";
    var redisClient = null;
    before(function () {
        redisClient = redis.createClient(HOST);
        redisClient.on("error", function (err) {
            console.log("Error:" + err);
        });
    });
    it("should be able to get the correct storage object from factory", function () {
        var storageFactory = new storagefactory_1.default(redisClient);
        var objModelStorage = storageFactory.getStorage(ibasestorage_1.StorageType.ObjectModel);
        assert.equal(ibasestorage_1.StorageType.ObjectModel, objModelStorage.getStorageType());
        assert.ok(objModelStorage !== null);
        var userMetaDataStorage = storageFactory.getStorage(ibasestorage_1.StorageType.UserMetaData);
        assert.equal(ibasestorage_1.StorageType.UserMetaData, userMetaDataStorage.getStorageType());
        assert.ok(userMetaDataStorage !== null);
    });
    it("should be able to check if connection is still present", function () { return __awaiter(_this, void 0, void 0, function () {
        var storageFactory, connected;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    storageFactory = new storagefactory_1.default(redisClient);
                    return [4 /*yield*/, storageFactory.isConnected()];
                case 1:
                    connected = _a.sent();
                    assert.ok(connected === true);
                    return [2 /*return*/];
            }
        });
    }); });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2Uvc3RvcmFnZWZhY3Rvcnl0ZXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGlCQTZDQTs7QUE3Q0EsaUJBQWU7QUFFZixtRUFBOEQ7QUFDOUQsK0RBQXlFO0FBSXpFLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMvQixJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7QUFFN0IsUUFBUSxDQUFDLHFCQUFxQixFQUFFO0lBRTVCLElBQU0sSUFBSSxHQUFVLHdCQUF3QixDQUFDO0lBQzdDLElBQUksV0FBVyxHQUFPLElBQUksQ0FBQztJQUUzQixNQUFNLENBQUM7UUFDSCxXQUFXLEdBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QyxXQUFXLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFVLEdBQU87WUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQywrREFBK0QsRUFBRTtRQUVoRSxJQUFJLGNBQWMsR0FBbUIsSUFBSSx3QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRXJFLElBQUksZUFBZSxHQUFnQixjQUFjLENBQUMsVUFBVSxDQUFFLDBCQUFXLENBQUMsV0FBVyxDQUFFLENBQUM7UUFDeEYsTUFBTSxDQUFDLEtBQUssQ0FBRSwwQkFBVyxDQUFDLFdBQVcsRUFBRSxlQUFlLENBQUMsY0FBYyxFQUFFLENBQUUsQ0FBQztRQUMxRSxNQUFNLENBQUMsRUFBRSxDQUFFLGVBQXNDLEtBQUssSUFBSSxDQUFFLENBQUM7UUFFN0QsSUFBSSxtQkFBbUIsR0FBZ0IsY0FBYyxDQUFDLFVBQVUsQ0FBRSwwQkFBVyxDQUFDLFlBQVksQ0FBRSxDQUFDO1FBQzdGLE1BQU0sQ0FBQyxLQUFLLENBQUUsMEJBQVcsQ0FBQyxZQUFZLEVBQUUsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUUsQ0FBQztRQUMvRSxNQUFNLENBQUMsRUFBRSxDQUFFLG1CQUEyQyxLQUFLLElBQUksQ0FBRSxDQUFDO0lBRXRFLENBQUMsQ0FBQyxDQUFDO0lBRUgsRUFBRSxDQUFDLHdEQUF3RCxFQUFFOzs7OztvQkFFckQsY0FBYyxHQUFtQixJQUFJLHdCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzVDLHFCQUFNLGNBQWMsQ0FBQyxXQUFXLEVBQUUsRUFBQTs7b0JBQXZELFNBQVMsR0FBWSxTQUFrQztvQkFDM0QsTUFBTSxDQUFDLEVBQUUsQ0FBRSxTQUFTLEtBQUssSUFBSSxDQUFFLENBQUM7Ozs7U0FFbkMsQ0FBQyxDQUFDO0FBRVAsQ0FBQyxDQUFDLENBQUMiLCJmaWxlIjoic3RvcmFnZS9zdG9yYWdlZmFjdG9yeXRlc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXCJtb2NoYVwiO1xyXG5pbXBvcnQgSVN0b3JhZ2VGYWN0b3J5IGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pc3RvcmFnZWZhY3RvcnlcIjtcclxuaW1wb3J0IFN0b3JhZ2VGYWN0b3J5IGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9zdG9yYWdlZmFjdG9yeVwiO1xyXG5pbXBvcnQgSUJhc2VTdG9yYWdlLCB7U3RvcmFnZVR5cGV9IGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pYmFzZXN0b3JhZ2VcIjtcclxuaW1wb3J0IElPYmplY3RNb2RlbFN0b3JhZ2UgZnJvbSBcIi4uLy4uL3NyYy9zdG9yYWdlL2lvYmplY3Rtb2RlbHN0b3JhZ2VcIjtcclxuaW1wb3J0IElVc2VyTWV0YURhdGFTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pdXNlcm1ldGFkYXRhc3RvcmFnZVwiO1xyXG5cclxubGV0IGFzc2VydCA9IHJlcXVpcmUoXCJhc3NlcnRcIik7XHJcbmxldCByZWRpcyA9IHJlcXVpcmUoXCJyZWRpc1wiKTtcclxuXHJcbmRlc2NyaWJlKFwiU3RvcmFnZUZhY3RvcnkgVGVzdFwiLCAoKSA9PiB7XHJcblxyXG4gICAgY29uc3QgSE9TVDpzdHJpbmcgPSBcInJlZGlzOi8vbG9jYWxob3N0OjYzNzlcIjsgXHJcbiAgICB2YXIgcmVkaXNDbGllbnQ6YW55ID0gbnVsbDtcclxuXHJcbiAgICBiZWZvcmUoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmVkaXNDbGllbnQgPSByZWRpcy5jcmVhdGVDbGllbnQoSE9TVCk7XHJcbiAgICAgICAgcmVkaXNDbGllbnQub24oXCJlcnJvclwiLCBmdW5jdGlvbiAoZXJyOmFueSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yOlwiICsgZXJyKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG5cclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gZ2V0IHRoZSBjb3JyZWN0IHN0b3JhZ2Ugb2JqZWN0IGZyb20gZmFjdG9yeVwiLCAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCBzdG9yYWdlRmFjdG9yeTpJU3RvcmFnZUZhY3RvcnkgPSBuZXcgU3RvcmFnZUZhY3RvcnkocmVkaXNDbGllbnQpO1xyXG5cclxuICAgICAgICBsZXQgb2JqTW9kZWxTdG9yYWdlOklCYXNlU3RvcmFnZSA9IHN0b3JhZ2VGYWN0b3J5LmdldFN0b3JhZ2UoIFN0b3JhZ2VUeXBlLk9iamVjdE1vZGVsICk7XHJcbiAgICAgICAgYXNzZXJ0LmVxdWFsKCBTdG9yYWdlVHlwZS5PYmplY3RNb2RlbCwgb2JqTW9kZWxTdG9yYWdlLmdldFN0b3JhZ2VUeXBlKCkgKTtcclxuICAgICAgICBhc3NlcnQub2soIG9iak1vZGVsU3RvcmFnZSBhcyBJT2JqZWN0TW9kZWxTdG9yYWdlICE9PSBudWxsICk7XHJcblxyXG4gICAgICAgIGxldCB1c2VyTWV0YURhdGFTdG9yYWdlOklCYXNlU3RvcmFnZSA9IHN0b3JhZ2VGYWN0b3J5LmdldFN0b3JhZ2UoIFN0b3JhZ2VUeXBlLlVzZXJNZXRhRGF0YSApO1xyXG4gICAgICAgIGFzc2VydC5lcXVhbCggU3RvcmFnZVR5cGUuVXNlck1ldGFEYXRhLCB1c2VyTWV0YURhdGFTdG9yYWdlLmdldFN0b3JhZ2VUeXBlKCkgKTtcclxuICAgICAgICBhc3NlcnQub2soIHVzZXJNZXRhRGF0YVN0b3JhZ2UgYXMgSVVzZXJNZXRhRGF0YVN0b3JhZ2UgIT09IG51bGwgKTtcclxuICAgICAgIFxyXG4gICAgfSk7ICBcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIGNoZWNrIGlmIGNvbm5lY3Rpb24gaXMgc3RpbGwgcHJlc2VudFwiLCBhc3luYyAoKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCBzdG9yYWdlRmFjdG9yeTpJU3RvcmFnZUZhY3RvcnkgPSBuZXcgU3RvcmFnZUZhY3RvcnkocmVkaXNDbGllbnQpO1xyXG4gICAgICAgIGxldCBjb25uZWN0ZWQ6IGJvb2xlYW4gPSBhd2FpdCBzdG9yYWdlRmFjdG9yeS5pc0Nvbm5lY3RlZCgpO1xyXG4gICAgICAgIGFzc2VydC5vayggY29ubmVjdGVkID09PSB0cnVlICk7XHJcbiAgICAgICAgXHJcbiAgICB9KTtcclxuXHJcbn0pO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
