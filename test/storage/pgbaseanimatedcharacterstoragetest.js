"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var pgbaseanimatedcharacterstorage_1 = require("../../src/storage/pgbaseanimatedcharacterstorage");
var pganimationprofilestorage_1 = require("../../src/storage/pganimationprofilestorage");
var parse = require('pg-connection-string').parse;
var assert = require("assert");
var _a = require('pg'), Client = _a.Client, Pool = _a.Pool;
describe("PgBaseAnimatedCharacterStorage Test", function () {
    var HOST = "postgresql://postgres:abc123@localhost:5432/isleofdarkness";
    var config = parse(HOST);
    var pool = new Pool(config);
    it("should be able to get base animated character from storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var animationProfileStorage, baseAnimatedCharacterStorage, baseAnimatedCharacter;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    animationProfileStorage = new pganimationprofilestorage_1.default(pool);
                    baseAnimatedCharacterStorage = new pgbaseanimatedcharacterstorage_1.default(pool, animationProfileStorage);
                    return [4 /*yield*/, baseAnimatedCharacterStorage.getBaseAnimatedCharacter("b1")];
                case 1:
                    baseAnimatedCharacter = _a.sent();
                    assert.ok(!!baseAnimatedCharacter == true);
                    assert.equal("Aland", baseAnimatedCharacter.modelName);
                    assert.ok(!!baseAnimatedCharacter.animationProfile);
                    assert.equal("Aland", baseAnimatedCharacter.animationProfile.name);
                    return [2 /*return*/];
            }
        });
    }); });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UvcGdiYXNlYW5pbWF0ZWRjaGFyYWN0ZXJzdG9yYWdldGVzdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxpQkE2QkE7O0FBN0JBLGlCQUFlO0FBRWYsbUdBQThGO0FBQzlGLHlGQUFvRjtBQUdwRixJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxLQUFLLENBQUM7QUFDbEQsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQzNCLElBQUEsa0JBQWdDLEVBQTlCLGtCQUFNLEVBQUUsY0FBSSxDQUFtQjtBQUVyQyxRQUFRLENBQUMscUNBQXFDLEVBQUU7SUFFNUMsSUFBTSxJQUFJLEdBQVcsNERBQTRELENBQUM7SUFDbEYsSUFBTSxNQUFNLEdBQVEsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2hDLElBQU0sSUFBSSxHQUFRLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBRW5DLEVBQUUsQ0FBQyw0REFBNEQsRUFBRTs7Ozs7b0JBRXpELHVCQUF1QixHQUE4QixJQUFJLG1DQUF5QixDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN6Riw0QkFBNEIsR0FBbUMsSUFBSSx3Q0FBOEIsQ0FBQyxJQUFJLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztvQkFDekcscUJBQU0sNEJBQTRCLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLEVBQUE7O29CQUF6RixxQkFBcUIsR0FBRyxTQUFpRTtvQkFDN0YsTUFBTSxDQUFDLEVBQUUsQ0FBRSxDQUFDLENBQUMscUJBQXFCLElBQUksSUFBSSxDQUFFLENBQUM7b0JBQzdDLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN2RCxNQUFNLENBQUMsRUFBRSxDQUFFLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxnQkFBZ0IsQ0FBRSxDQUFDO29CQUN0RCxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxxQkFBcUIsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQzs7OztTQUV0RSxDQUFDLENBQUM7QUFFUCxDQUFDLENBQUMsQ0FBQyIsImZpbGUiOiJzdG9yYWdlL3BnYmFzZWFuaW1hdGVkY2hhcmFjdGVyc3RvcmFnZXRlc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXCJtb2NoYVwiO1xyXG5pbXBvcnQgSVBnQmFzZUFuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZSBmcm9tIFwiLi4vLi4vc3JjL3N0b3JhZ2UvaXBnYmFzZWFuaW1hdGVkY2hhcmFjdGVyc3RvcmFnZVwiO1xyXG5pbXBvcnQgUGdCYXNlQW5pbWF0ZWRDaGFyYWN0ZXJTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9wZ2Jhc2VhbmltYXRlZGNoYXJhY3RlcnN0b3JhZ2VcIjtcclxuaW1wb3J0IFBnQW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2UgZnJvbSBcIi4uLy4uL3NyYy9zdG9yYWdlL3BnYW5pbWF0aW9ucHJvZmlsZXN0b3JhZ2VcIjtcclxuaW1wb3J0IElQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlIGZyb20gXCIuLi8uLi9zcmMvc3RvcmFnZS9pcGdhbmltYXRpb25wcm9maWxlc3RvcmFnZVwiO1xyXG5cclxubGV0IHBhcnNlID0gcmVxdWlyZSgncGctY29ubmVjdGlvbi1zdHJpbmcnKS5wYXJzZTtcclxubGV0IGFzc2VydCA9IHJlcXVpcmUoXCJhc3NlcnRcIik7XHJcbmxldCB7IENsaWVudCwgUG9vbCB9ID0gcmVxdWlyZSgncGcnKTtcclxuXHJcbmRlc2NyaWJlKFwiUGdCYXNlQW5pbWF0ZWRDaGFyYWN0ZXJTdG9yYWdlIFRlc3RcIiwgKCkgPT4ge1xyXG5cclxuICAgIGNvbnN0IEhPU1Q6IHN0cmluZyA9IFwicG9zdGdyZXNxbDovL3Bvc3RncmVzOmFiYzEyM0Bsb2NhbGhvc3Q6NTQzMi9pc2xlb2ZkYXJrbmVzc1wiOyBcclxuICAgIGNvbnN0IGNvbmZpZzogYW55ID0gcGFyc2UoSE9TVCk7XHJcbiAgICBjb25zdCBwb29sOiBhbnkgPSBuZXcgUG9vbChjb25maWcpO1xyXG5cclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gZ2V0IGJhc2UgYW5pbWF0ZWQgY2hhcmFjdGVyIGZyb20gc3RvcmFnZVwiLCBhc3luYygpID0+IHtcclxuXHJcbiAgICAgICAgbGV0IGFuaW1hdGlvblByb2ZpbGVTdG9yYWdlOklQZ0FuaW1hdGlvblByb2ZpbGVTdG9yYWdlID0gbmV3IFBnQW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2UocG9vbCk7XHJcbiAgICAgICAgbGV0IGJhc2VBbmltYXRlZENoYXJhY3RlclN0b3JhZ2U6SVBnQmFzZUFuaW1hdGVkQ2hhcmFjdGVyU3RvcmFnZSA9IG5ldyBQZ0Jhc2VBbmltYXRlZENoYXJhY3RlclN0b3JhZ2UocG9vbCwgYW5pbWF0aW9uUHJvZmlsZVN0b3JhZ2UpO1xyXG4gICAgICAgIGxldCBiYXNlQW5pbWF0ZWRDaGFyYWN0ZXIgPSBhd2FpdCBiYXNlQW5pbWF0ZWRDaGFyYWN0ZXJTdG9yYWdlLmdldEJhc2VBbmltYXRlZENoYXJhY3RlcihcImIxXCIpO1xyXG4gICAgICAgIGFzc2VydC5vayggISFiYXNlQW5pbWF0ZWRDaGFyYWN0ZXIgPT0gdHJ1ZSApO1xyXG4gICAgICAgIGFzc2VydC5lcXVhbChcIkFsYW5kXCIsIGJhc2VBbmltYXRlZENoYXJhY3Rlci5tb2RlbE5hbWUpO1xyXG4gICAgICAgIGFzc2VydC5vayggISFiYXNlQW5pbWF0ZWRDaGFyYWN0ZXIuYW5pbWF0aW9uUHJvZmlsZSApO1xyXG4gICAgICAgIGFzc2VydC5lcXVhbChcIkFsYW5kXCIsIGJhc2VBbmltYXRlZENoYXJhY3Rlci5hbmltYXRpb25Qcm9maWxlLm5hbWUpO1xyXG5cclxuICAgIH0pOyBcclxuXHJcbn0pO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
