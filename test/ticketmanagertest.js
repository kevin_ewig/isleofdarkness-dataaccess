"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var storagefactory_1 = require("../src/storage/storagefactory");
var ticket_1 = require("isleofdarkness-common/src/model/ticket");
var ticketmanager_1 = require("../src/ticketmanager");
var assert = require("assert");
var redis = require("redis");
describe("TicketManager Test", function () {
    var HOST = "redis://localhost:6379";
    var redisClient = null;
    before(function () {
        redisClient = redis.createClient(HOST);
        redisClient.on("error", function (err) {
            console.log("Error:" + err);
        });
    });
    it("should be able to create a new ticket", function () { return __awaiter(_this, void 0, void 0, function () {
        var userId, storageFactory, ticketManager, ticket;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    userId = "testuser";
                    storageFactory = new storagefactory_1.default(redisClient);
                    ticketManager = new ticketmanager_1.TicketManager(storageFactory, 100);
                    return [4 /*yield*/, ticketManager.getTicket(userId)];
                case 1:
                    ticket = _a.sent();
                    assert(ticket !== undefined);
                    assert(ticket.userId === userId);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to create a new and validate it", function () { return __awaiter(_this, void 0, void 0, function () {
        var userId, storageFactory, ticketManager, ticket, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    userId = "testuser1";
                    storageFactory = new storagefactory_1.default(redisClient);
                    ticketManager = new ticketmanager_1.TicketManager(storageFactory, 100);
                    return [4 /*yield*/, ticketManager.getTicket(userId)];
                case 1:
                    ticket = _a.sent();
                    assert(ticket !== undefined);
                    assert(ticket.userId === userId);
                    return [4 /*yield*/, ticketManager.validateTicket(ticket)];
                case 2:
                    result = _a.sent();
                    assert(result === true);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to validate a bad ticket not in the storage", function () { return __awaiter(_this, void 0, void 0, function () {
        var userId, ticket, storageFactory, ticketManager, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    userId = "testuser1";
                    ticket = ticket_1.default.NewTicket(userId, 100);
                    storageFactory = new storagefactory_1.default(redisClient);
                    ticketManager = new ticketmanager_1.TicketManager(storageFactory, 100);
                    return [4 /*yield*/, ticketManager.validateTicket(ticket)];
                case 1:
                    result = _a.sent();
                    assert(result === false);
                    return [2 /*return*/];
            }
        });
    }); });
    it("should be able to validate an expired ticket", function () { return __awaiter(_this, void 0, void 0, function () {
        var userId, now, expiredDate, storageFactory, ticketManager, ticket, ticketDuplicate, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    userId = "testuser1";
                    now = new Date();
                    expiredDate = new Date();
                    expiredDate.setTime(now.getTime() - 1);
                    storageFactory = new storagefactory_1.default(redisClient);
                    ticketManager = new ticketmanager_1.TicketManager(storageFactory, 100);
                    return [4 /*yield*/, ticketManager.getTicket(userId)];
                case 1:
                    ticket = _a.sent();
                    ticketDuplicate = new ticket_1.default(userId, ticket.ticketId, expiredDate);
                    return [4 /*yield*/, ticketManager.validateTicket(ticketDuplicate)];
                case 2:
                    result = _a.sent();
                    assert(result === false);
                    return [2 /*return*/];
            }
        });
    }); });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRpY2tldG1hbmFnZXJ0ZXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGlCQThFQTs7QUE5RUEsaUJBQWU7QUFFZixnRUFBMkQ7QUFDM0QsaUVBQTREO0FBQzVELHNEQUFxRDtBQUVyRCxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDL0IsSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBRTdCLFFBQVEsQ0FBQyxvQkFBb0IsRUFBRTtJQUUzQixJQUFNLElBQUksR0FBVSx3QkFBd0IsQ0FBQztJQUM3QyxJQUFJLFdBQVcsR0FBTyxJQUFJLENBQUM7SUFFM0IsTUFBTSxDQUFDO1FBQ0gsV0FBVyxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdkMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxHQUFPO1lBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsdUNBQXVDLEVBQUU7Ozs7O29CQUVwQyxNQUFNLEdBQUcsVUFBVSxDQUFDO29CQUNwQixjQUFjLEdBQW1CLElBQUksd0JBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDakUsYUFBYSxHQUFpQixJQUFJLDZCQUFhLENBQUMsY0FBYyxFQUFFLEdBQUcsQ0FBQyxDQUFDO29CQUM1RCxxQkFBTSxhQUFhLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFBOztvQkFBOUMsTUFBTSxHQUFHLFNBQXFDO29CQUNsRCxNQUFNLENBQUUsTUFBTSxLQUFLLFNBQVMsQ0FBRSxDQUFDO29CQUMvQixNQUFNLENBQUUsTUFBTSxDQUFDLE1BQU0sS0FBSyxNQUFNLENBQUUsQ0FBQzs7OztTQUV0QyxDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsZ0RBQWdELEVBQUU7Ozs7O29CQUU3QyxNQUFNLEdBQUcsV0FBVyxDQUFDO29CQUNyQixjQUFjLEdBQW1CLElBQUksd0JBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDakUsYUFBYSxHQUFpQixJQUFJLDZCQUFhLENBQUUsY0FBYyxFQUFFLEdBQUcsQ0FBRSxDQUFDO29CQUM5RCxxQkFBTSxhQUFhLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFBOztvQkFBOUMsTUFBTSxHQUFHLFNBQXFDO29CQUVsRCxNQUFNLENBQUUsTUFBTSxLQUFLLFNBQVMsQ0FBRSxDQUFDO29CQUMvQixNQUFNLENBQUUsTUFBTSxDQUFDLE1BQU0sS0FBSyxNQUFNLENBQUUsQ0FBQztvQkFFdEIscUJBQU0sYUFBYSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsRUFBQTs7b0JBQW5ELE1BQU0sR0FBRyxTQUEwQztvQkFDdkQsTUFBTSxDQUFFLE1BQU0sS0FBSyxJQUFJLENBQUUsQ0FBQzs7OztTQUU3QixDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsNERBQTRELEVBQUU7Ozs7O29CQUV6RCxNQUFNLEdBQUcsV0FBVyxDQUFDO29CQUNyQixNQUFNLEdBQVUsZ0JBQU0sQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO29CQUU5QyxjQUFjLEdBQW1CLElBQUksd0JBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDakUsYUFBYSxHQUFpQixJQUFJLDZCQUFhLENBQUMsY0FBYyxFQUFFLEdBQUcsQ0FBQyxDQUFDO29CQUM1RCxxQkFBTSxhQUFhLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxFQUFBOztvQkFBbkQsTUFBTSxHQUFHLFNBQTBDO29CQUN2RCxNQUFNLENBQUUsTUFBTSxLQUFLLEtBQUssQ0FBRSxDQUFDOzs7O1NBRTlCLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQyw4Q0FBOEMsRUFBRTs7Ozs7b0JBRTNDLE1BQU0sR0FBRyxXQUFXLENBQUM7b0JBQ3JCLEdBQUcsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO29CQUNqQixXQUFXLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztvQkFDN0IsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBRW5DLGNBQWMsR0FBbUIsSUFBSSx3QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUNqRSxhQUFhLEdBQWlCLElBQUksNkJBQWEsQ0FBRSxjQUFjLEVBQUUsR0FBRyxDQUFFLENBQUM7b0JBQzlELHFCQUFNLGFBQWEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUE7O29CQUE5QyxNQUFNLEdBQUcsU0FBcUM7b0JBRTlDLGVBQWUsR0FBVSxJQUFJLGdCQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUM7b0JBRWpFLHFCQUFNLGFBQWEsQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLEVBQUE7O29CQUE1RCxNQUFNLEdBQUcsU0FBbUQ7b0JBQ2hFLE1BQU0sQ0FBRSxNQUFNLEtBQUssS0FBSyxDQUFFLENBQUM7Ozs7U0FFOUIsQ0FBQyxDQUFDO0FBRVAsQ0FBQyxDQUFDLENBQUMiLCJmaWxlIjoidGlja2V0bWFuYWdlcnRlc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXCJtb2NoYVwiO1xyXG5pbXBvcnQgSVN0b3JhZ2VGYWN0b3J5IGZyb20gXCIuLi9zcmMvc3RvcmFnZS9pc3RvcmFnZWZhY3RvcnlcIjtcclxuaW1wb3J0IFN0b3JhZ2VGYWN0b3J5IGZyb20gXCIuLi9zcmMvc3RvcmFnZS9zdG9yYWdlZmFjdG9yeVwiO1xyXG5pbXBvcnQgVGlja2V0IGZyb20gXCJpc2xlb2ZkYXJrbmVzcy1jb21tb24vc3JjL21vZGVsL3RpY2tldFwiO1xyXG5pbXBvcnQgeyBUaWNrZXRNYW5hZ2VyIH0gZnJvbSBcIi4uL3NyYy90aWNrZXRtYW5hZ2VyXCI7XHJcblxyXG5sZXQgYXNzZXJ0ID0gcmVxdWlyZShcImFzc2VydFwiKTtcclxubGV0IHJlZGlzID0gcmVxdWlyZShcInJlZGlzXCIpO1xyXG5cclxuZGVzY3JpYmUoXCJUaWNrZXRNYW5hZ2VyIFRlc3RcIiwgKCkgPT4ge1xyXG5cclxuICAgIGNvbnN0IEhPU1Q6c3RyaW5nID0gXCJyZWRpczovL2xvY2FsaG9zdDo2Mzc5XCI7IFxyXG4gICAgdmFyIHJlZGlzQ2xpZW50OmFueSA9IG51bGw7XHJcblxyXG4gICAgYmVmb3JlKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJlZGlzQ2xpZW50ID0gcmVkaXMuY3JlYXRlQ2xpZW50KEhPU1QpO1xyXG4gICAgICAgIHJlZGlzQ2xpZW50Lm9uKFwiZXJyb3JcIiwgZnVuY3Rpb24gKGVycjphbnkpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnJvcjpcIiArIGVycik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIGNyZWF0ZSBhIG5ldyB0aWNrZXRcIiwgYXN5bmMgKCkgPT4ge1xyXG5cclxuICAgICAgICBsZXQgdXNlcklkID0gXCJ0ZXN0dXNlclwiO1xyXG4gICAgICAgIGxldCBzdG9yYWdlRmFjdG9yeTpJU3RvcmFnZUZhY3RvcnkgPSBuZXcgU3RvcmFnZUZhY3RvcnkocmVkaXNDbGllbnQpO1xyXG4gICAgICAgIGxldCB0aWNrZXRNYW5hZ2VyOlRpY2tldE1hbmFnZXIgPSBuZXcgVGlja2V0TWFuYWdlcihzdG9yYWdlRmFjdG9yeSwgMTAwKTtcclxuICAgICAgICBsZXQgdGlja2V0ID0gYXdhaXQgdGlja2V0TWFuYWdlci5nZXRUaWNrZXQodXNlcklkKTtcclxuICAgICAgICBhc3NlcnQoIHRpY2tldCAhPT0gdW5kZWZpbmVkICk7XHJcbiAgICAgICAgYXNzZXJ0KCB0aWNrZXQudXNlcklkID09PSB1c2VySWQgKTtcclxuICAgICAgIFxyXG4gICAgfSk7ICBcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIGNyZWF0ZSBhIG5ldyBhbmQgdmFsaWRhdGUgaXRcIiwgYXN5bmMgKCkgPT4ge1xyXG5cclxuICAgICAgICBsZXQgdXNlcklkID0gXCJ0ZXN0dXNlcjFcIjtcclxuICAgICAgICBsZXQgc3RvcmFnZUZhY3Rvcnk6SVN0b3JhZ2VGYWN0b3J5ID0gbmV3IFN0b3JhZ2VGYWN0b3J5KHJlZGlzQ2xpZW50KTtcclxuICAgICAgICBsZXQgdGlja2V0TWFuYWdlcjpUaWNrZXRNYW5hZ2VyID0gbmV3IFRpY2tldE1hbmFnZXIoIHN0b3JhZ2VGYWN0b3J5LCAxMDAgKTtcclxuICAgICAgICBsZXQgdGlja2V0ID0gYXdhaXQgdGlja2V0TWFuYWdlci5nZXRUaWNrZXQodXNlcklkKTtcclxuICAgICAgICBcclxuICAgICAgICBhc3NlcnQoIHRpY2tldCAhPT0gdW5kZWZpbmVkICk7XHJcbiAgICAgICAgYXNzZXJ0KCB0aWNrZXQudXNlcklkID09PSB1c2VySWQgKTtcclxuXHJcbiAgICAgICAgbGV0IHJlc3VsdCA9IGF3YWl0IHRpY2tldE1hbmFnZXIudmFsaWRhdGVUaWNrZXQodGlja2V0KTtcclxuICAgICAgICBhc3NlcnQoIHJlc3VsdCA9PT0gdHJ1ZSApO1xyXG4gICAgICAgXHJcbiAgICB9KTsgICAgICBcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIHZhbGlkYXRlIGEgYmFkIHRpY2tldCBub3QgaW4gdGhlIHN0b3JhZ2VcIiwgYXN5bmMgKCkgPT4ge1xyXG5cclxuICAgICAgICBsZXQgdXNlcklkID0gXCJ0ZXN0dXNlcjFcIjtcclxuICAgICAgICBsZXQgdGlja2V0OlRpY2tldCA9IFRpY2tldC5OZXdUaWNrZXQodXNlcklkLCAxMDApO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGxldCBzdG9yYWdlRmFjdG9yeTpJU3RvcmFnZUZhY3RvcnkgPSBuZXcgU3RvcmFnZUZhY3RvcnkocmVkaXNDbGllbnQpO1xyXG4gICAgICAgIGxldCB0aWNrZXRNYW5hZ2VyOlRpY2tldE1hbmFnZXIgPSBuZXcgVGlja2V0TWFuYWdlcihzdG9yYWdlRmFjdG9yeSwgMTAwKTsgICAgICAgIFxyXG4gICAgICAgIGxldCByZXN1bHQgPSBhd2FpdCB0aWNrZXRNYW5hZ2VyLnZhbGlkYXRlVGlja2V0KHRpY2tldCk7XHJcbiAgICAgICAgYXNzZXJ0KCByZXN1bHQgPT09IGZhbHNlICk7XHJcbiAgICAgICBcclxuICAgIH0pOyAgICAgIFxyXG5cclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gdmFsaWRhdGUgYW4gZXhwaXJlZCB0aWNrZXRcIiwgYXN5bmMgKCkgPT4ge1xyXG5cclxuICAgICAgICBsZXQgdXNlcklkID0gXCJ0ZXN0dXNlcjFcIjsgICAgICAgIFxyXG4gICAgICAgIGxldCBub3cgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgIGxldCBleHBpcmVkRGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgZXhwaXJlZERhdGUuc2V0VGltZShub3cuZ2V0VGltZSgpIC0gMSk7XHJcblxyXG4gICAgICAgIGxldCBzdG9yYWdlRmFjdG9yeTpJU3RvcmFnZUZhY3RvcnkgPSBuZXcgU3RvcmFnZUZhY3RvcnkocmVkaXNDbGllbnQpO1xyXG4gICAgICAgIGxldCB0aWNrZXRNYW5hZ2VyOlRpY2tldE1hbmFnZXIgPSBuZXcgVGlja2V0TWFuYWdlciggc3RvcmFnZUZhY3RvcnksIDEwMCApO1xyXG4gICAgICAgIGxldCB0aWNrZXQgPSBhd2FpdCB0aWNrZXRNYW5hZ2VyLmdldFRpY2tldCh1c2VySWQpO1xyXG5cclxuICAgICAgICBsZXQgdGlja2V0RHVwbGljYXRlOlRpY2tldCA9IG5ldyBUaWNrZXQodXNlcklkLCB0aWNrZXQudGlja2V0SWQsIGV4cGlyZWREYXRlKTtcclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgbGV0IHJlc3VsdCA9IGF3YWl0IHRpY2tldE1hbmFnZXIudmFsaWRhdGVUaWNrZXQodGlja2V0RHVwbGljYXRlKTtcclxuICAgICAgICBhc3NlcnQoIHJlc3VsdCA9PT0gZmFsc2UgKTtcclxuICAgICAgIFxyXG4gICAgfSk7ICAgICAgICAgXHJcblxyXG59KTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
