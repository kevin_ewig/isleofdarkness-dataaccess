import "mocha";
import IStorageFactory from "../src/storage/istoragefactory";
import StorageFactory from "../src/storage/storagefactory";
import Ticket from "isleofdarkness-common/src/model/ticket";
import { TicketManager } from "../src/ticketmanager";

let assert = require("assert");
let redis = require("redis");

describe("TicketManager Test", () => {

    const HOST:string = "redis://localhost:6379"; 
    var redisClient:any = null;

    before(function() {
        redisClient = redis.createClient(HOST);
        redisClient.on("error", function (err:any) {
            console.log("Error:" + err);
        });
    });

    it("should be able to create a new ticket", async () => {

        let userId = "testuser";
        let storageFactory:IStorageFactory = new StorageFactory(redisClient);
        let ticketManager:TicketManager = new TicketManager(storageFactory, 100);
        let ticket = await ticketManager.getTicket(userId);
        assert( ticket !== undefined );
        assert( ticket.userId === userId );
       
    });  

    it("should be able to create a new and validate it", async () => {

        let userId = "testuser1";
        let storageFactory:IStorageFactory = new StorageFactory(redisClient);
        let ticketManager:TicketManager = new TicketManager( storageFactory, 100 );
        let ticket = await ticketManager.getTicket(userId);
        
        assert( ticket !== undefined );
        assert( ticket.userId === userId );

        let result = await ticketManager.validateTicket(ticket);
        assert( result === true );
       
    });      

    it("should be able to validate a bad ticket not in the storage", async () => {

        let userId = "testuser1";
        let ticket:Ticket = Ticket.NewTicket(userId, 100);
        
        let storageFactory:IStorageFactory = new StorageFactory(redisClient);
        let ticketManager:TicketManager = new TicketManager(storageFactory, 100);        
        let result = await ticketManager.validateTicket(ticket);
        assert( result === false );
       
    });      

    it("should be able to validate an expired ticket", async () => {

        let userId = "testuser1";        
        let now = new Date();
        let expiredDate = new Date();
        expiredDate.setTime(now.getTime() - 1);

        let storageFactory:IStorageFactory = new StorageFactory(redisClient);
        let ticketManager:TicketManager = new TicketManager( storageFactory, 100 );
        let ticket = await ticketManager.getTicket(userId);

        let ticketDuplicate:Ticket = new Ticket(userId, ticket.ticketId, expiredDate);
               
        let result = await ticketManager.validateTicket(ticketDuplicate);
        assert( result === false );
       
    });         

});
