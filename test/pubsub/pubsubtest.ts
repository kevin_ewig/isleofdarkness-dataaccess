import "mocha";
import {UserMetaData} from "isleofdarkness-common/src/model/usermetadata";
import {Ticket} from "isleofdarkness-common/src/model/ticket";
import {BroadcastMessageResponse} from "isleofdarkness-common/src/protocol/response/broadcastmessageresponse";
import Request from "isleofdarkness-common/src/protocol/request/request";
import IPublisher from "../../src/pubsub/ipublisher";
import Publisher from "../../src/pubsub/publisher";
import ISubscriber from "../../src/pubsub/isubscriber";
import Subscriber from "../../src/pubsub/subscriber";

let assert = require("assert");
let redis = require("redis");

describe("Publish and Subscribe Test", () => {

    const HOST:string = "redis://localhost:6379"; 

    it("should publish and subscribe", (done:Function) => {

        let publisher:IPublisher = new Publisher( createRedisClient() ); 
        let subscriber:ISubscriber = new Subscriber( createRedisClient() ); 
       
        let ticket = Ticket.NewTicket("U222333", 1);
        let userMetaData = new UserMetaData("U222333", "John Snow");
        let response = new BroadcastMessageResponse(userMetaData, ticket, "This is a message");

        subscriber.subscribe("all");
        subscriber.onMessage( (channel:string, message:Request) => {
            assert.equal("all", channel);
            assert.ok( message != null );
            done();
        } );

        // This makes sure that publish is called after subscribe.
        setTimeout( () => {
            publisher.publish("all", response);
        }, 500 );

    });

    function createRedisClient() {
        let redisClient1 = redis.createClient(HOST);
        redisClient1.on("error", function (err:any) {
            console.log("Error:" + err);
        });
        return redisClient1;
    }

});
