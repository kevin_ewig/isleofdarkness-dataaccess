alter table ioduser drop constraint fk_avatar_objectmodel_id;
alter table iodanimatedcharacter drop constraint fk_baseanimatedcharacter_id;
alter table iodanimatedcharacter drop constraint fk_objectmodel_id; 
drop table if exists iodanimatedcharacter;

alter table iodbaseanimatedcharacter drop constraint fk_shape_id;
alter table iodbaseanimatedcharacter drop constraint fk_animationprofile_id;
drop table if exists iodbaseanimatedcharacter;

drop table if exists iodanimationprofile;
drop table if exists iodobjectmodel;
drop table if exists iodshape;
drop table if exists iodpoint;
drop table if exists ioduser;

--
-- A user
--
create table ioduser(
    user_id varchar(10) primary key,
    name varchar(50) not null,
    email varchar(100) not null,
    created_date timestamptz not null,
    last_logged_in_date timestamptz not null,
    language char(2) null,
    picture_url varchar(500) null,
    disabled boolean not null default false,
    avatar_objectmodel_id varchar(10) null
);

--
-- A geometric point. Used to define the center of a circle
-- or points in a rectangle.
--
create table iodpoint(
    point_id varchar(10) primary key,
    point_x float,
    point_y float,
    point_z float
);

--
-- A shape (circle, rectangle, etc)
--
create table iodshape(
    shape_id varchar(10) primary key,
    shape_type varchar(10),
    circle_point_id varchar(10) null,
    circle_radius float,
    rectangle_point_id varchar(10) null,
    rectangle_width float,
    rectangle_height float,
    poly_p0_point_id varchar(10) null,
    poly_p1_point_id varchar(10) null,
    poly_p2_point_id varchar(10) null,
    poly_p3_point_id varchar(10) null,
    poly_p4_point_id varchar(10) null,
    poly_p5_point_id varchar(10) null,
    poly_p6_point_id varchar(10) null,
    poly_p7_point_id varchar(10) null,
    poly_p8_point_id varchar(10) null,
    poly_p9_point_id varchar(10) null
);

alter table iodshape add constraint fk_circle_point_id foreign key (circle_point_id) references iodpoint (point_id);
alter table iodshape add constraint fk_rectangle_point_id foreign key (rectangle_point_id) references iodpoint (point_id);
alter table iodshape add constraint fk_poly_p0_point_id foreign key (poly_p0_point_id) references iodpoint (point_id);
alter table iodshape add constraint fk_poly_p1_point_id foreign key (poly_p1_point_id) references iodpoint (point_id);
alter table iodshape add constraint fk_poly_p2_point_id foreign key (poly_p2_point_id) references iodpoint (point_id);
alter table iodshape add constraint fk_poly_p3_point_id foreign key (poly_p3_point_id) references iodpoint (point_id);
alter table iodshape add constraint fk_poly_p4_point_id foreign key (poly_p4_point_id) references iodpoint (point_id);
alter table iodshape add constraint fk_poly_p5_point_id foreign key (poly_p5_point_id) references iodpoint (point_id);
alter table iodshape add constraint fk_poly_p6_point_id foreign key (poly_p6_point_id) references iodpoint (point_id);
alter table iodshape add constraint fk_poly_p7_point_id foreign key (poly_p7_point_id) references iodpoint (point_id);
alter table iodshape add constraint fk_poly_p8_point_id foreign key (poly_p8_point_id) references iodpoint (point_id);
alter table iodshape add constraint fk_poly_p9_point_id foreign key (poly_p9_point_id) references iodpoint (point_id);

--
-- A base object model such as a wall, stairs or person.
--
create table iodobjectmodel(
    objectmodel_id varchar(10) primary key,
    objectmodel_type varchar(20) not null,
    shape_id varchar(10) not null,
    objectmodel_worldname varchar(50),
    objectmodel_modelname varchar(50),
    objectmodel_displayname varchar(50),    
    objectmodel_modeltexture varchar(50) null,
    owner_user_id varchar(10) null
);

alter table iodobjectmodel add constraint fk_shape_id foreign key (shape_id) references iodshape (shape_id);
alter table iodobjectmodel add constraint fk_owner_user_id foreign key (owner_user_id) references ioduser (user_id);

--
-- Describes the animation key for walking, running, idle, etc.
--
create table iodanimationprofile(
    animationprofile_id varchar(10) primary key,
    animationprofile_name varchar(10),
    animationprofile_idle varchar(10),
    animationprofile_run varchar(10),
    animationprofile_walk varchar(10),
    animationprofile_die varchar(10)
);

--
-- An base animated character with all the default values.
--
create table iodbaseanimatedcharacter(
    baseanimatedcharacter_id varchar(10) primary key,
    shape_id varchar(10) not null,
    baseanimatedcharacter_modelname varchar(50),
    baseanimatedcharacter_modeltexture varchar(50),
    animationprofile_id varchar(10),
    speed int not null
);

alter table iodbaseanimatedcharacter add constraint fk_shape_id foreign key (shape_id) references iodshape (shape_id);

--
-- An animated character (elf, dwarf, person).
--
create table iodanimatedcharacter(
    objectmodel_id varchar(10) primary key,
    animationprofile_id varchar(10),
    speed int not null
);

alter table iodanimatedcharacter add constraint fk_objectmodel_id foreign key(objectmodel_id) references iodobjectmodel;

alter table iodanimatedcharacter add constraint fk_animationprofile_id foreign key(animationprofile_id) references iodanimationprofile;

alter table ioduser add constraint fk_avatar_objectmodel_id foreign key (avatar_objectmodel_id) references iodanimatedcharacter (objectmodel_id);

