DELETE FROM iodanimationprofile;
DELETE FROM iodbaseanimatedcharacter;
DELETE FROM iodshape;
DELETE FROM iodpoint;

INSERT INTO iodanimationprofile VALUES('aland1', 'Aland', 'Action #3', 'Action #4', 'Action #10', 'Action #10');
INSERT INTO iodanimationprofile VALUES('alice1', 'Alice', 'Action #2', 'Action #4', 'Action #10', 'Action #10');
INSERT INTO iodanimationprofile VALUES('beldes1', 'Beldes', 'Action #3', 'Action #4', 'Action #10', 'Action #10');
INSERT INTO iodanimationprofile VALUES('dwarf1', 'Dwarf', 'Action #2', 'Action #4', 'Action #10', 'Action #10');

INSERT INTO iodpoint(point_id, point_x, point_y, point_z) VALUES ('p1', 0, 0, 0);

INSERT INTO iodshape(shape_id, shape_type, circle_point_id, circle_radius) VALUES ('s1', 'circle', 'p1', 5);

INSERT INTO iodbaseanimatedcharacter (baseanimatedcharacter_id, shape_id, 
    baseanimatedcharacter_modelname, baseanimatedcharacter_modeltexture, animationprofile_id, speed)
VALUES('b1', 's1', 'Aland', 'Aland.png', 'aland1', 300);
INSERT INTO iodbaseanimatedcharacter (baseanimatedcharacter_id, shape_id, 
    baseanimatedcharacter_modelname, baseanimatedcharacter_modeltexture, animationprofile_id, speed)
VALUES('b2', 's1', 'Alice', 'Alice.png', 'alice1', 300);
INSERT INTO iodbaseanimatedcharacter (baseanimatedcharacter_id, shape_id, 
    baseanimatedcharacter_modelname, baseanimatedcharacter_modeltexture, animationprofile_id, speed)
VALUES('b3', 's1', 'Beldes', 'Beldes.png', 'beldes1', 300);
INSERT INTO iodbaseanimatedcharacter (baseanimatedcharacter_id, shape_id, 
    baseanimatedcharacter_modelname, baseanimatedcharacter_modeltexture, animationprofile_id, speed)
VALUES('b4', 's1', 'Dwarf', 'Dwarf.png', 'dwarf1', 300);



