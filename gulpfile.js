"use strict";

//******************************************************************************
//* DEPENDENCIES
//******************************************************************************

var gulp        = require("gulp"),
    tslint      = require("gulp-tslint"),
    tsc         = require("gulp-typescript"),
    sourcemaps  = require("gulp-sourcemaps"),
    runSequence = require("run-sequence");

//******************************************************************************
//* LINT
//******************************************************************************
gulp.task("lint", function() {
    var config =  { formatter: "verbose", fix: true };
    return gulp.src([
        "src/**/**.ts"
    ])
    .pipe(tslint(config))
    .pipe(tslint.report());
});

//******************************************************************************
//* BUILD
//******************************************************************************
var tstProject = tsc.createProject("tsconfig.json", { typescript: require("typescript") });

gulp.task("buildall", function (cb) {
    runSequence("buildsrc", "buildtest", cb);
});

gulp.task("buildsrc", function() {
    return gulp.src([
        "src/**/*.ts"
    ])
    .pipe(sourcemaps.init())
    .pipe(tstProject())
    .on("error", function (err) {
        process.exit(1);
    })
    .js
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("src/"));
});

gulp.task("buildtest", function() {
    return gulp.src([
        "test/**/*.ts"
    ])
    .pipe(sourcemaps.init())
    .pipe(tstProject())
    .on("error", function (err) {
        process.exit(1);
    })
    .js
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("test/"));
});

//******************************************************************************
//* DEFAULT
//******************************************************************************
gulp.task("default", function (cb) {
    runSequence("lint", "buildall", cb);
});
